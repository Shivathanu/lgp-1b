use lgp_health_diary;
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("A", "USF LEÇA", "LEÇA DA PALMEIRA", "LEÇA DA PALMEIRA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("B", "USF MARESIA", "LEÇA DA PALMEIRA", "LEÇA DA PALMEIRA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("C", "USF DUNAS", "LAVRA", "LEÇA DA PALMEIRA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("D", "USF PROGRESSO", "PERAFITA", "LEÇA DA PALMEIRA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("E", "UCSP STª CRUZ", "STª CRUZ DO BISPO", "LEÇA DA PALMEIRA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("F", "UCSP MATOSINHOS", "MATOSINHOS", "MATOSINHOS", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("G", "USF HORIZONTE", "MATOSINHOS", "MATOSINHOS", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("H", "USF OCEANOS", "MATOSINHOS", "MATOSINHOS", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("I", "UCSP S. MAMEDE", "S.MAMEDE DE INFESTA", "S. MAMEDE DE INFESTA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("J", "USF INFESTA", "S.MAMEDE DE INFESTA", "S. MAMEDE DE INFESTA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("L", "USF PORTA DO SOL", "LEÇA DO BALIO", "S. MAMEDE DE INFESTA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("M", "USF LAGOA", "SENHORA DA HORA", "SENHORA DA HORA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("N", "USF CARAVELA", "SENHORA DA HORA", "SENHORA DA HORA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("O", "USF CUSTÓIAS", "CUSTÓIAS ", "SENHORA DA HORA", false);
INSERT INTO health_center (code, name, location, centro_saude, hospital) VALUES ("P", "Hospital Pedro Hispano", "Matosinhos", "", true);