package lgp.myhealthdiary.myhealthdiary;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import junit.framework.Assert;
import lgp.myhealthdiary.myhealthdiary.dto.PatientViewToProfessionalDto;
import lgp.myhealthdiary.myhealthdiary.dto.report.ReportUtils;
import lgp.myhealthdiary.myhealthdiary.model.Entry;
import lgp.myhealthdiary.myhealthdiary.model.Patient;

/**
 * Unit tests to the construction of Patient summary info for the Professionals
 *
 */
public class PatientReportTest {

	Patient patient;
	PatientViewToProfessionalDto victim;

	@BeforeEach
	public void dataGeneration() {
		patient = new Patient();

// Entry constructor fields: patient, date, migraine, headache_intensity, painkillers, tirptans, urgencies, work_incapacity, observations, menstruation
		// dec 2019
		patient.addEntry(new Entry(patient, LocalDate.of(2019, 12, 24), false, 0, false, true, true, true, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2019, 12, 31), true, 6, true, true, true, false, "", false));

		// january 2020
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 1, 1), true, 8, false, true, true, true, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 1, 2), false, 6, true, false, false, false, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 1, 3), true, 9, true, false, true, true, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 1, 4), false, 2, false, false, true, false, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 1, 5), false, 3, true, false, false, false, "", false));

		// feb
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 2, 1), true, 8, false, true, true, true, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 2, 2), false, 0, false, false, false, false, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 2, 3), true, 7, true, false, true, false, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 2, 4), false, 0, false, false, false, false, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 2, 5), false, 3, false, false, false, false, "", false));

		// march
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 3, 1), true, 10, true, true, true, true, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 3, 2), false, 6, true, true, false, false, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 3, 3), true, 9, true, false, false, true, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 3, 4), false, 1, true, false, false, false, "", false));
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 3, 5), true, 2, false, true, false, false, "", false));
	}

	@Test
	public void calendarViewSizeTest() {
		victim = new PatientViewToProfessionalDto(patient);

		Assert.assertEquals(victim.getCalendarView().size(), 4);
		Assert.assertEquals(victim.getCalendarView().get("2019-12").size(), 2);
		Assert.assertEquals(victim.getCalendarView().get("2020-01").size(), 5);
		Assert.assertEquals(victim.getCalendarView().get("2020-02").size(), 5);
		Assert.assertEquals(victim.getCalendarView().get("2020-03").size(), 5);
	}

	@Test
	public void reportHeadacheMigraineTest() {
		victim = new PatientViewToProfessionalDto(patient);

// dec
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getMonth(), "2019-12");
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getHeadacheDays(), 1);
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getMigraineDays(), 1);
// jan
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getMonth(), "2020-01");
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getHeadacheDays(), 5);
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getMigraineDays(), 2);
		// feb
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getMonth(), "2020-02");
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getHeadacheDays(), 3);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getMigraineDays(), 2);
		// mar
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getMonth(), "2020-03");
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getHeadacheDays(), 5);
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getMigraineDays(), 3);
	}


	@Test
	public void reportEvolutionIntensityTest() {
		victim = new PatientViewToProfessionalDto(patient);

// dec
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getPercentEvolutionHeadacheDay(), 0.0);
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getMaximumHeadacheIntensity(), 6);
// jan
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getPercentEvolutionHeadacheDay(), 4.0);
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getMaximumHeadacheIntensity(), 9);
		// feb
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getPercentEvolutionHeadacheDay(), -0.4);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getMaximumHeadacheIntensity(), 8);
		// mar
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getPercentEvolutionHeadacheDay(), 0.67);
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getMaximumHeadacheIntensity(), 10);
	}

	@Test
	public void atipicalEvolutionsTest() {
		Patient atipicalPatient = new Patient();
		atipicalPatient.addEntry(
				new Entry(atipicalPatient, LocalDate.of(2019, 12, 24), false, 0, false, true, true, true, "", false));
		atipicalPatient.addEntry(
				new Entry(atipicalPatient, LocalDate.of(2020, 1, 20), true, 6, true, true, true, false, "", false));
		atipicalPatient.addEntry(
				new Entry(atipicalPatient, LocalDate.of(2020, 1, 21), true, 6, true, true, true, false, "", false));
		victim = new PatientViewToProfessionalDto(atipicalPatient);

		Assert.assertEquals(victim.getReportView().getMonths().get(1).getPercentEvolutionHeadacheDay(), 2.0);
	}

	@Test
	public void reportPainkillerTriptanTest() {
		victim = new PatientViewToProfessionalDto(patient);

// dec
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getPainkillerDays(), 1);
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getTriptanDays(), 2);
// jan
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getPainkillerDays(), 3);
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getTriptanDays(), 1);
		// feb
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getPainkillerDays(), 1);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getTriptanDays(), 1);
		// mar
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getPainkillerDays(), 4);
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getTriptanDays(), 3);
	}

	@Test
	public void reportUrgencyWorkAbsenceTest() {
		victim = new PatientViewToProfessionalDto(patient);

// dec
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getUrgencyServiceVisits(), 2);
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getWorkAbsentDays(), 1);
// jan
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getUrgencyServiceVisits(), 3);
		Assert.assertEquals(victim.getReportView().getMonths().get(1).getWorkAbsentDays(), 2);
		// feb
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getUrgencyServiceVisits(), 2);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getWorkAbsentDays(), 1);
		// mar
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getUrgencyServiceVisits(), 1);
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getWorkAbsentDays(), 2);
	}

	@Test
	public void insertionOfDelayedEntryTest() {
		// add a delayed feb Entry and confirmchanges
		patient.addEntry(new Entry(patient, LocalDate.of(2020, 2, 6), true, 10, true, true, true, true, "", false));
		victim = new PatientViewToProfessionalDto(patient);

		Assert.assertEquals(victim.getCalendarView().get("2020-02").size(), 6);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getMonth(), "2020-02");
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getHeadacheDays(), 4);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getMigraineDays(), 3);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getPercentEvolutionHeadacheDay(), -0.2);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getMaximumHeadacheIntensity(), 10);
		Assert.assertEquals(victim.getReportView().getMonths().get(3).getPercentEvolutionHeadacheDay(), 0.25);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getPainkillerDays(), 2);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getTriptanDays(), 2);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getUrgencyServiceVisits(), 3);
		Assert.assertEquals(victim.getReportView().getMonths().get(2).getWorkAbsentDays(), 2);
	}

	@Test
	public void intensityOverHeadacheMigraineDaysTest() {
		Patient carelessPatient = new Patient();
		carelessPatient.addEntry(
				new Entry(carelessPatient, LocalDate.of(2020, 5, 9), true, 0, false, true, true, true, "", false));
		carelessPatient.addEntry(
				new Entry(carelessPatient, LocalDate.of(2020, 5, 11), false, 0, false, true, true, true, "", false));
		carelessPatient.addEntry(
				new Entry(carelessPatient, LocalDate.of(2020, 5, 12), true, 0, false, true, true, true, "", false));

		victim = new PatientViewToProfessionalDto(carelessPatient);

		Assert.assertEquals(victim.getReportView().getMonths().get(0).getHeadacheDays(), 0);
		Assert.assertEquals(victim.getReportView().getMonths().get(0).getMigraineDays(), 0);
	}

	@Test
	public void emptyMonthsTest() {
		Patient absentPatient = new Patient();
		absentPatient.addEntry(
				new Entry(absentPatient, LocalDate.of(2019, 12, 24), false, 0, false, true, true, true, "", false));
		absentPatient.addEntry(
				new Entry(absentPatient, LocalDate.of(2021, 2, 20), true, 6, true, true, true, false, "", false));
		absentPatient.addEntry(
				new Entry(absentPatient, LocalDate.of(2021, 4, 20), true, 6, true, true, true, false, "", false));
		victim = new PatientViewToProfessionalDto(absentPatient);

		Assert.assertEquals(victim.getCalendarView().size(), 17);
		Assert.assertEquals(victim.getReportView().getMonths().size(), 17);
		Assert.assertEquals(victim.getReportView().getMonths().get(12).getMonth(), "2020-12");
		Assert.assertEquals(victim.getReportView().getMonths().get(15).getMonth(), "2021-03");
		Assert.assertEquals(victim.getCalendarView().get("2020-11").size(), 0);
	}

	@Test
	public void exportTableAsStringSymetryTest() {
		victim = new PatientViewToProfessionalDto(patient);

		String[] splitToTest = victim.getReportView().getExportToString().split("\n");
		Assert.assertEquals(splitToTest.length, 9);
		final int lineSize = splitToTest[0].length();
		final String ignoreStartAndEndBars = splitToTest[0].substring(1, splitToTest[0].length() - 2);
		final int firstBarIndex = ignoreStartAndEndBars.indexOf("|");
		final int lastBarIndex = ignoreStartAndEndBars.lastIndexOf("|");
		Assert.assertTrue(firstBarIndex != lastBarIndex);
for (int i = 1; i < splitToTest.length; ++i) {
			Assert.assertEquals(splitToTest[i].length(), lineSize);
			final String posIgnoreStartAndEndBars = splitToTest[0].substring(1, splitToTest[0].length() - 2);
			final int posFirstBarIndex = posIgnoreStartAndEndBars.indexOf("|");
			final int posLastBarIndex = posIgnoreStartAndEndBars.lastIndexOf("|");
			Assert.assertEquals(firstBarIndex, posFirstBarIndex);
			Assert.assertEquals(lastBarIndex, posLastBarIndex);
}
	}

	@Test
	public void exportTableAsStringOneMonthTest() {
		Patient carelessPatient = new Patient();
		carelessPatient.addEntry(
				new Entry(carelessPatient, LocalDate.of(2020, 5, 9), false, 0, false, true, true, true, "", false));

		victim = new PatientViewToProfessionalDto(carelessPatient);

		String[] splitToTest = victim.getReportView().getExportToString().split("\n");
		Assert.assertEquals(splitToTest.length, 9);
		final int lineSize = splitToTest[0].length();
		final String ignoreStartAndEndBars = splitToTest[0].substring(1, splitToTest[0].length() - 2);
		final int firstBarIndex = ignoreStartAndEndBars.indexOf("|");
		final int lastBarIndex = ignoreStartAndEndBars.lastIndexOf("|");
		// as it only has header | month
		Assert.assertEquals(firstBarIndex, lastBarIndex);
		for (int i = 1; i < splitToTest.length; ++i) {
			Assert.assertEquals(splitToTest[i].length(), lineSize);
			final String posIgnoreStartAndEndBars = splitToTest[0].substring(1, splitToTest[0].length() - 2);
			final int posFirstBarIndex = posIgnoreStartAndEndBars.indexOf("|");
			final int posLastBarIndex = posIgnoreStartAndEndBars.lastIndexOf("|");
			Assert.assertEquals(firstBarIndex, posFirstBarIndex);
			Assert.assertEquals(lastBarIndex, posLastBarIndex);
		}
	}

	@Test
	public void exportTableAsStringNoDataTest() {
		victim = new PatientViewToProfessionalDto(new Patient());

		String[] splitToTest = victim.getReportView().getExportToString().split("\n");
		Assert.assertEquals(splitToTest.length, 9);
		final int lineSize = splitToTest[0].length();
		final int indexOfInexistentDivisorBar = splitToTest[0].substring(1, splitToTest[0].length() - 2).indexOf("|");
		Assert.assertEquals(indexOfInexistentDivisorBar, -1);
		for (int i = 1; i < splitToTest.length; ++i) {
			Assert.assertEquals(splitToTest[i].length(), lineSize);
			final int posIndexOfInexistentDivisorBar = splitToTest[i].substring(1, splitToTest[0].length() - 2)
					.indexOf("|");
			Assert.assertEquals(indexOfInexistentDivisorBar, posIndexOfInexistentDivisorBar);
		}
	}

	@Test
	public void convertDatesTest() {
		LocalDate date = LocalDate.of(2020, 4, 30);
		String asKey = ReportUtils.generateMonthKey(date);
		Assert.assertEquals(asKey, "2020-04");
		LocalDate convertedDate = ReportUtils.generateFromMonthKey(asKey);
		Assert.assertEquals(convertedDate, LocalDate.of(2020, 4, 1));

	}
}
