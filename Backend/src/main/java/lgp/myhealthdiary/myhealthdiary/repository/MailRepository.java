package lgp.myhealthdiary.myhealthdiary.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lgp.myhealthdiary.myhealthdiary.model.Mail;

@Repository
public interface MailRepository extends JpaRepository<Mail, Long> {
}
