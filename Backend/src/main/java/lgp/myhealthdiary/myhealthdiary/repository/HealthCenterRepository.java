package lgp.myhealthdiary.myhealthdiary.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lgp.myhealthdiary.myhealthdiary.model.HealthCenter;

@Repository
public interface HealthCenterRepository extends JpaRepository<HealthCenter, String> {

	@Query(value = "SELECT h FROM HealthCenter h WHERE h.hospital = false order by h.code")
	List<HealthCenter> getAllHealthCenters();
	
	@Query(value = "SELECT h FROM HealthCenter h WHERE h.hospital = true order by h.code")
	List<HealthCenter> getHospitals();
	
	@Override
	Optional<HealthCenter> findById(String code);
}