package lgp.myhealthdiary.myhealthdiary.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import lgp.myhealthdiary.myhealthdiary.dto.PatientDto;
import lgp.myhealthdiary.myhealthdiary.model.Patient;


@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
	
	@Override
	List<Patient> findAll();
	
	@Override
	Optional<Patient> findById(Long id);
	
	@Query(value = "SELECT p.id FROM Patient p WHERE p.code =:patientCode")
	Optional<Long> findIdByCode(String patientCode);

	Optional<Patient> findByCode(String code);
	
	Optional<Patient> findByEmail(String email);

	Optional<PatientDto> findProfileByCode(String code);

	@Query(nativeQuery = true, value = "SELECT count(*) FROM patient WHERE numero_utente =:numeroUtente")
	long checkIfNumeroUtenteExists(String numeroUtente);

	@Query(nativeQuery = true, value = "SELECT count(*) FROM patient WHERE email =:email")
	long checkIfEmailExists(String email);

	@Query(nativeQuery = true, value = "SELECT count(*) FROM patient WHERE num_processo_hospitalar =:numProcessoHospitalar")
	long checkIfNumProcessoHospitalarExists(String numProcessoHospitalar);

	@Query(nativeQuery = true, value = "SELECT MAX(SUBSTRING(code, 2)) FROM patient WHERE SUBSTRING(code, 1, 1) =:healthCenterCode")
	String lastRegistrationInHealthCenter(String healthCenterCode);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM professional_assigned_patients WHERE assigned_patients_id =:patientId")
	void deleteFromProfessionals(long patientId);
	
	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM prophylactic_treatment WHERE patient_id =:patientId")
	void deleteProphylacticTreatments(long patientId);

	List<PatientDto> findProfilesByProfessionalsId(long professionalId);
}
