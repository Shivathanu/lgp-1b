package lgp.myhealthdiary.myhealthdiary.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lgp.myhealthdiary.myhealthdiary.model.ProfessionalSpeciality;

@Repository
public interface ProfessionalSpecialityRepository extends JpaRepository<ProfessionalSpeciality, String> {

	@Override
	List<ProfessionalSpeciality> findAll();

	@Override
	Optional<ProfessionalSpeciality> findById(String speciality);
}