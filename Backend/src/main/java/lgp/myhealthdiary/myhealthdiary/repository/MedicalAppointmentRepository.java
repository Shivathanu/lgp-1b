package lgp.myhealthdiary.myhealthdiary.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lgp.myhealthdiary.myhealthdiary.model.MedicalAppointment;


@Repository
public interface MedicalAppointmentRepository extends JpaRepository<MedicalAppointment, Long> {
	
	@Override
	List<MedicalAppointment> findAll();
	
	@Override
	Optional<MedicalAppointment> findById(Long id);

	List<MedicalAppointment> findByPatientCode(String patientCode);

	@Query(nativeQuery = true, value = "SELECT COUNT(*) FROM medical_appointment WHERE patient_id =:patientId AND date_time >=:hourBefore AND date_time <=:hourAfter AND id<>:appointmentId")
	long hasAppointmentOnThatHour(long patientId, LocalDateTime hourBefore, LocalDateTime hourAfter,
			long appointmentId);
}