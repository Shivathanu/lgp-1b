package lgp.myhealthdiary.myhealthdiary.repository;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import lgp.myhealthdiary.myhealthdiary.model.Alert;

@Repository
public interface AlertRepository extends JpaRepository<Alert, Long> {

	@Override
	Optional<Alert> findById(Long id);

	// Patient alerts

	@Query(value = "SELECT a FROM Alert a WHERE a.dateTime >:startDate AND a.dateTime<=:actualDate AND a.professionalAlert = false AND a.patient.code=:patientCode ORDER BY a.dateTime DESC")
	List<Alert> getPatientAlertsHistory(String patientCode, LocalDateTime startDate, LocalDateTime actualDate);
	
	@Query(value = "SELECT a FROM Alert a WHERE a.dateTime >:actualDate AND a.professionalAlert = false AND a.patient.code=:patientCode ORDER BY a.dateTime")
	List<Alert> getPatientFutureAlerts(String patientCode, LocalDateTime actualDate);

	@Query(nativeQuery = true, value = "SELECT COUNT(*) FROM alert, patient WHERE alert.patient_id = patient.id AND alert.date_time >:lastDate AND alert.message LIKE :medication AND alert.helper_field >=:medicationDays AND patient.code =:patientCode AND alert.professional_alert = false")
	Long verifyIfAlertInDate(String medication, int medicationDays, LocalDateTime lastDate, String patientCode);

	@Query(value = "SELECT a FROM Alert a WHERE a.helperField=:appointmentId AND a.subject=:subject AND a.patient.code=:patientCode and a.professionalAlert = false")
	Optional<Alert> getAppointmentAlert(long appointmentId, String subject, String patientCode);

	@Query(value = "SELECT a FROM Alert a WHERE a.subject=:subject AND a.patient.code=:patientCode and a.professionalAlert = false")
	Optional<Alert> getDiaryAlert(String subject, String patientCode);

	// Professional alerts

	@Query(nativeQuery = true, value = "SELECT * FROM alert WHERE date_time >:startDate AND professional_alert = true AND patient_id IN (SELECT id FROM patient p, professional_assigned_patients list where p.id = list.assigned_patients_id and professionals_id =:professionalId) ORDER BY date_time desc")
	List<Alert> getAlertsOFFollowedPatients(long professionalId, LocalDateTime startDate);

	@Query(value = "SELECT a FROM Alert a WHERE a.patient.id =:patientId AND a.subject LIKE:subject AND a.dateTime >=:startDate AND a.dateTime<:endDate")
	Optional<Alert> getProPatientMonthAlert(long patientId, String subject, LocalDateTime startDate,
			LocalDateTime endDate);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "UPDATE alert SET read_by_recipient = true WHERE professional_alert = true AND read_by_recipient = false AND date_time <:nextMonthStart AND patient_id =:patientId")
	int markPastPatientProAlertsAsRead(long patientId, LocalDateTime nextMonthStart);
}