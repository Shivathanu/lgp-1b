package lgp.myhealthdiary.myhealthdiary.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lgp.myhealthdiary.myhealthdiary.model.ProphylacticTreatment;

@Repository
public interface ProphylacticTreatmentRepository extends JpaRepository<ProphylacticTreatment, Long> {

	@Query(value = "SELECT p.id FROM ProphylacticTreatment p WHERE p.patient.id=:patientId")
	List<Long> patientProphylacticTreatments(long patientId);

}