package lgp.myhealthdiary.myhealthdiary.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import lgp.myhealthdiary.myhealthdiary.dto.ProfessionalDto;
import lgp.myhealthdiary.myhealthdiary.model.Professional;

@Repository
public interface ProfessionalRepository extends JpaRepository<Professional, Long> {
    
	List<Professional> findAll();
	
	Optional<Professional> findById(Long id);
	
	Optional<Professional> findByEmail(String email);

	@Query(value = "SELECT p.id FROM Professional p WHERE p.email =:email")
	Optional<Long> findIdByEmail(String email);

	@Query(value = "SELECT p.id FROM Professional p WHERE p.username=:username")
	Optional<Long> findIdByUsername(String username);

	Optional<ProfessionalDto> findProfileById(Long id);

	Professional save(Professional professional);
	
	void delete(Professional id);
	
	@Query(nativeQuery = true, value = "SELECT COUNT(*) FROM professional_assigned_patients list, patient p WHERE list.assigned_patients_id = p.id AND p.code =:patientCode AND list.professionals_id =:professionalId")
	long isPatientOnProfessionalList(String patientCode, long professionalId);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM professional_assigned_patients WHERE professionals_id =:professionalId AND assigned_patients_id = (SELECT id FROM patient WHERE code =:patientCode)")
	void deletePatientFromProfessional(String patientCode, long professionalId);
	
	@Query(nativeQuery = true, value = "SELECT count(*) FROM professional WHERE email =:email")
	long checkIfEmailExists(String email);
	
	@Query(nativeQuery = true, value = "SELECT count(*) FROM professional WHERE username =:username")
	long checkIfUsernameExists(String username);
}