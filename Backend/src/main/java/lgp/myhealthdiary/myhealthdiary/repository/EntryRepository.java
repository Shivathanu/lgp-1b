package lgp.myhealthdiary.myhealthdiary.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import lgp.myhealthdiary.myhealthdiary.model.Entry;

@Repository
public interface EntryRepository extends JpaRepository<Entry, Long> {
	
	@Override
	List<Entry> findAll();
	
	@Override
	Optional<Entry> findById(Long id);

	@Query(nativeQuery = true, value = "SELECT * FROM entry WHERE YEAR(date)=:year AND MONTH(date)=:month AND DAY(date)=:day AND patient_id =(SELECT id FROM patient WHERE code=:patientCode)")
	Optional<Entry> findByDate(String patientCode, int year, int month, int day);

	@Query(value = "SELECT e FROM Entry e WHERE e.date>:startDate AND e.date<:endDate AND e.patient.code=:patientCode order by e.date")
	List<Entry> getBetweenDates(LocalDateTime startDate, LocalDateTime endDate, String patientCode);

	@Query(nativeQuery = true, value = "SELECT COUNT(*) FROM entry WHERE YEAR(date)=:year AND MONTH(date)=:month AND DAY(date)=:day AND patient_id = :patientId")
	long patientHasEntryInDate(long patientId, int year, int month, int day);

	// to calculate professional notifications

	@Query(nativeQuery = true, value = "SELECT * FROM entry WHERE date>=:startDate AND patient_id IN (SELECT assigned_patients_id FROM professional_assigned_patients WHERE professionals_id =:professionalId) ORDER BY patient_id, date;")
	List<Entry> entriesByProfessionalPatients(long professionalId, LocalDateTime startDate);
}