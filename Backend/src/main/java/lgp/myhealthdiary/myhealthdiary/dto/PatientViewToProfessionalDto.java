package lgp.myhealthdiary.myhealthdiary.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lgp.myhealthdiary.myhealthdiary.dto.report.MonthlyReport;
import lgp.myhealthdiary.myhealthdiary.dto.report.ReportUtils;
import lgp.myhealthdiary.myhealthdiary.dto.report.SummaryReport;
import lgp.myhealthdiary.myhealthdiary.model.Entry;
import lgp.myhealthdiary.myhealthdiary.model.MedicalAppointment;
import lgp.myhealthdiary.myhealthdiary.model.Patient;

/**
 * Object comprising all the info a Professional needs when opens the Patient's
 * page
 *
 */
public class PatientViewToProfessionalDto {

	private PatientDto patientProfile;

	private SummaryReport reportView;

	private Map<String, List<Entry>> calendarView = new LinkedHashMap<String, List<Entry>>();

	private Map<String, List<MedicalAppointmentDto>> appointmentsView = new LinkedHashMap<String, List<MedicalAppointmentDto>>();

	public PatientViewToProfessionalDto() {
	}

	public PatientViewToProfessionalDto(Patient patient) {
		this.patientProfile = new PatientDto(patient);
		reportView = processEntries(patient.getDiary());
		fillInEmptyMonths();
		processAppointments(patient.getMedicalAppointments());
	}

	public PatientDto getPatientProfile() {
		return patientProfile;
	}

	public SummaryReport getReportView() {
		return reportView;
	}

	public Map<String, List<Entry>> getCalendarView() {
		return calendarView;
	}

	public Map<String, List<MedicalAppointmentDto>> getAppointmentsView() {
		return appointmentsView;
	}

	public SummaryReport processEntries(List<Entry> diary) {
		Collections.sort(diary, Comparator.comparing(Entry::getDate));
		Map<String, MonthlyReport> monthlyReportsMap = new LinkedHashMap<String, MonthlyReport>();
		for (Entry entry : diary) {
			final String monthKey = ReportUtils.generateMonthKey(entry.getDate());
			if (!monthlyReportsMap.containsKey(monthKey)) {
				monthlyReportsMap.put(monthKey, new MonthlyReport(monthKey));
			}
			monthlyReportsMap.get(monthKey).processEntry(entry);
			if (!calendarView.containsKey(monthKey)) {
				calendarView.put(monthKey, new ArrayList<Entry>());
			}
			calendarView.get(monthKey).add(entry);
		}
		return new SummaryReport(new ArrayList(monthlyReportsMap.values()));
	}

	private void processAppointments(List<MedicalAppointment> appointments) {
		Collections.sort(appointments, Comparator.comparing(MedicalAppointment::getDateTime));
		for (MedicalAppointment appointment : appointments) {
			final String monthKey = ReportUtils.generateMonthKey(appointment.getDateTime().toLocalDate());
			if (!appointmentsView.containsKey(monthKey)) {
				appointmentsView.put(monthKey, new ArrayList<MedicalAppointmentDto>());
			}
			appointmentsView.get(monthKey).add(new MedicalAppointmentDto(appointment));
		}
	}

	private void fillInEmptyMonths() {
		List<String> emptyMonths = new ArrayList<String>();
		for (int i = 0; i < reportView.getMonths().size() - 1; ++i) {
			LocalDate firstDate = ReportUtils.generateFromMonthKey(reportView.getMonths().get(i).getMonth())
					.plusMonths(1);
			LocalDate nextDate = ReportUtils.generateFromMonthKey(reportView.getMonths().get(i + 1).getMonth());
			while (!firstDate.equals(nextDate)) {
				emptyMonths.add(ReportUtils.generateMonthKey(firstDate));
				firstDate = firstDate.plusMonths(1);
			}
		}
		for (String key : emptyMonths) {
			reportView.getMonths().add(new MonthlyReport(key).createEmpty());
			calendarView.put(key, new ArrayList<Entry>());
		}
		Collections.sort(reportView.getMonths(), Comparator.comparing(MonthlyReport::getMonth));
	}
}
