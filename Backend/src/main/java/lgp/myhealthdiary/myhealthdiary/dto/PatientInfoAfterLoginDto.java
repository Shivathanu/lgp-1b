package lgp.myhealthdiary.myhealthdiary.dto;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lgp.myhealthdiary.myhealthdiary.model.Alert;

/**
 * Object comprising all the info needed for a Patient after his/her login
 *
 */
public class PatientInfoAfterLoginDto {

	private PatientDto patientProfile;
	
	private Map<String, PatientDailyInfoDto> dailyInfo = new LinkedHashMap<String, PatientDailyInfoDto>();

	private List<Alert> alerts = new ArrayList<Alert>();

	private List<Alert> futureAlerts = new ArrayList<Alert>();

	public PatientInfoAfterLoginDto(PatientDto patientProfile, Map<String, PatientDailyInfoDto> dailyInfo,
			List<Alert> alerts, List<Alert> futureAlerts) {
		this.patientProfile = patientProfile;
		this.dailyInfo = dailyInfo;
		this.alerts = alerts;
		this.futureAlerts = futureAlerts;
	}

	public PatientInfoAfterLoginDto(PatientDto patientProfile) {
		this.patientProfile = patientProfile;
	}

	public PatientDto getPatientProfile() {
		return patientProfile;
	}

	public void setPatientProfile(PatientDto patientProfile) {
		this.patientProfile = patientProfile;
	}

	public Map<String, PatientDailyInfoDto> getDailyInfo() {
		return dailyInfo;
	}

	public void setDailyInfo(Map<String, PatientDailyInfoDto> dailyInfo) {
		this.dailyInfo = dailyInfo;
	}

	public List<Alert> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<Alert> alerts) {
		this.alerts = alerts;
	}

	public List<Alert> getFutureAlerts() {
		return futureAlerts;
	}

	public void setFutureAlerts(List<Alert> futureAlerts) {
		this.futureAlerts = futureAlerts;
	}
}