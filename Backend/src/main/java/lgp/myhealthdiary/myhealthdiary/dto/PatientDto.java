package lgp.myhealthdiary.myhealthdiary.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lgp.myhealthdiary.myhealthdiary.model.HealthCenter;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.model.ProphylacticTreatment;

public class PatientDto {

	// works as ID to frontend and mobile
    private String code;

    private String nameInitials;

	private String email;

	private String numProcessoHospitalar;

	private String numeroUtente;

    private LocalDate birthdate;

    private String gender;

	private Boolean migraineWithAura;

	private Boolean exclusivelyUnilateral;
    
    private HealthCenter healthCenter;

	private Boolean symptomaticTreatment;

	private String symptomaticTreatmentDetails;

	private List<ProphylacticTreatment> prophylacticTreatments = new ArrayList<>();

	private Boolean anxiety;

	private Boolean depression;

	private Boolean insomnia;

	private String otherMedicalConditions;

	private boolean active;

    public PatientDto() {
	}

	public PatientDto(String code, String email, String nameInitials, String numeroUtente, String numProcessoHospitalar,
			HealthCenter healthCenter) {
		this.code = code;
		this.email = email;
		this.nameInitials = nameInitials;
		this.numProcessoHospitalar = numProcessoHospitalar;
		this.numeroUtente = numeroUtente;
		this.healthCenter = healthCenter;
	}

	public PatientDto(Patient patient) {
		this.code = patient.getCode();
		this.nameInitials = patient.getNameInitials();
		this.email = patient.getEmail();
		this.numeroUtente = patient.getNumeroUtente();
		this.numProcessoHospitalar = patient.getNumProcessoHospitalar();
		this.birthdate = patient.getBirthdate();
		this.gender = patient.getGender();
		this.migraineWithAura = patient.isMigraineWithAura();
		this.exclusivelyUnilateral = patient.isExclusivelyUnilateral();
		this.healthCenter = patient.getHealthCenter();
		this.symptomaticTreatment = patient.getSymptomaticTreatment();
		this.symptomaticTreatmentDetails = patient.getSymptomaticTreatmentDetails();
		this.prophylacticTreatments = patient.getProphylacticTreatments();
		this.anxiety = patient.getAnxiety();
		this.depression = patient.getDepression();
		this.insomnia = patient.getInsomnia();
		this.otherMedicalConditions = patient.getOtherMedicalConditions();
		this.active = patient.hasActiveAccount();
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNameInitials() {
		return nameInitials;
	}

	public void setNameInitials(String nameInitials) {
		this.nameInitials = nameInitials;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumProcessoHospitalar() {
		return numProcessoHospitalar;
	}

	public void setNumProcessoHospitalar(String numProcessoHospitalar) {
		this.numProcessoHospitalar = numProcessoHospitalar;
	}

	public String getNumeroUtente() {
		return numeroUtente;
	}

	public void setNumeroUtente(String numeroUtente) {
		this.numeroUtente = numeroUtente;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Boolean isMigraineWithAura() {
		return migraineWithAura;
	}

	public void setMigraineWithAura(Boolean migraineWithAura) {
		this.migraineWithAura = migraineWithAura;
	}

	public Boolean isExclusivelyUnilateral() {
		return exclusivelyUnilateral;
	}

	public void setExclusivelyUnilateral(Boolean exclusivelyUnilateral) {
		this.exclusivelyUnilateral = exclusivelyUnilateral;
	}

	public HealthCenter getHealthCenter() {
		return healthCenter;
	}

	public void setHealthCenter(HealthCenter healthCenter) {
		this.healthCenter = healthCenter;
	}

	public Boolean getSymptomaticTreatment() {
		return symptomaticTreatment;
	}

	public void setSymptomaticTreatment(Boolean symptomaticTreatment) {
		this.symptomaticTreatment = symptomaticTreatment;
	}

	public String getSymptomaticTreatmentDetails() {
		return symptomaticTreatmentDetails;
	}

	public void setSymptomaticTreatmentDetails(String symptomaticTreatmentDetails) {
		this.symptomaticTreatmentDetails = symptomaticTreatmentDetails;
	}

	public List<ProphylacticTreatment> getProphylacticTreatments() {
		return prophylacticTreatments;
	}

	public void setProphylacticTreatments(List<ProphylacticTreatment> prophylacticTreatments) {
		this.prophylacticTreatments = prophylacticTreatments;
	}

	public Boolean getAnxiety() {
		return anxiety;
	}

	public void setAnxiety(Boolean anxiety) {
		this.anxiety = anxiety;
	}

	public Boolean getDepression() {
		return depression;
	}

	public void setDepression(Boolean depression) {
		this.depression = depression;
	}

	public Boolean getInsomnia() {
		return insomnia;
	}

	public void setInsomnia(Boolean insomnia) {
		this.insomnia = insomnia;
	}

	public String getOtherMedicalConditions() {
		return otherMedicalConditions;
	}

	public void setOtherMedicalConditions(String otherMedicalConditions) {
		this.otherMedicalConditions = otherMedicalConditions;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Patient build() {

        Patient patient = new Patient();
		patient.setCode(this.code);
        patient.setNameInitials(this.nameInitials);
		patient.setEmail(this.email);
		patient.setNumeroUtente(this.numeroUtente);
		patient.setNumProcessoHospitalar(this.numProcessoHospitalar);
        patient.setBirthdate(this.birthdate);
        patient.setGender(this.gender);
		patient.setMigraineWithAura(this.migraineWithAura);
		patient.setExclusivelyUnilateral(this.exclusivelyUnilateral);
        patient.setHealthCenter(this.healthCenter);
		patient.setSymptomaticTreatment(this.symptomaticTreatment);
		patient.setSymptomaticTreatmentDetails(this.symptomaticTreatmentDetails);
		patient.setProphylacticTreatments(this.prophylacticTreatments);
		patient.setAnxiety(this.anxiety);
		patient.setDepression(this.depression);
		patient.setInsomnia(this.insomnia);
		patient.setOtherMedicalConditions(this.otherMedicalConditions);
        return patient;
    }
	
	public Patient updatePatient(Patient patient) {

		patient.setNameInitials(this.nameInitials);
		patient.setBirthdate(this.birthdate);
        patient.setGender(this.gender);
		patient.setMigraineWithAura(this.migraineWithAura);
		patient.setExclusivelyUnilateral(this.exclusivelyUnilateral);
		patient.setSymptomaticTreatment(this.symptomaticTreatment);
		patient.setSymptomaticTreatmentDetails(this.symptomaticTreatmentDetails);
		patient.setProphylacticTreatments(this.prophylacticTreatments);
		patient.setAnxiety(this.anxiety);
		patient.setDepression(this.depression);
		patient.setInsomnia(this.insomnia);
		patient.setOtherMedicalConditions(this.otherMedicalConditions);
        return patient;
    }
	
}
