package lgp.myhealthdiary.myhealthdiary.dto;

import lgp.myhealthdiary.myhealthdiary.model.HealthCenter;
import lgp.myhealthdiary.myhealthdiary.model.Professional;
import lgp.myhealthdiary.myhealthdiary.model.ProfessionalSpeciality;

public class ProfessionalDto {
	
	private String clinicalName;

    private String speciality;

	private HealthCenter healthCenter;
	
	private String image;
	
	private String username;
	
	private String email;

	private String password;

    public ProfessionalDto() {
    }

    public ProfessionalDto(String clinicalName, String speciality, HealthCenter healthCenter, String password) {
        this.clinicalName = clinicalName;
        this.speciality = speciality;
        this.healthCenter = healthCenter;
        this.password = password;
    }

	public ProfessionalDto(Professional professional) {
		this.clinicalName = professional.getClinicalName();
		this.speciality = professional.getSpeciality().getSpecialityName();
		this.healthCenter = professional.getHealthCenter();
		this.image = professional.getImage();
		this.username = professional.getUsername();
		this.email = professional.getEmail();
	}

	public Professional build() {
        Professional professional = new Professional();
		professional.setClinicalName(this.clinicalName);
		professional.setSpeciality(new ProfessionalSpeciality(this.speciality));
		professional.setHealthCenter(this.healthCenter);
		professional.setImage(this.image);
		professional.setPassword(this.password);
		professional.setEmail(this.email);
		professional.setUsername(this.username);
		professional.setId(0);
        return professional;
    }

    public String getClinicalName() {
        return clinicalName;
    }

    public void setClinicalName(String clinicalName) {
        this.clinicalName = clinicalName;
    }

	public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

	public HealthCenter getHealthCenter() {
		return healthCenter;
	}

	public void setHealthCenter(HealthCenter healthCenter) {
		this.healthCenter = healthCenter;
	}
    
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
