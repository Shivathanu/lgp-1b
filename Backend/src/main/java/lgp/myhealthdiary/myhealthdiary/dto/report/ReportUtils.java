package lgp.myhealthdiary.myhealthdiary.dto.report;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Utility operations for grouping data into months
 *
 */
public class ReportUtils {

	private static final String DIVISOR = "-";
	// to generate table as String
	private static String[] rowNames = { "Mês", "Evolução (%) nº dias cefaleias", "Nº dias cefaleia",
			"Nº dias enxaqueca",
			"Nº dias med. analgésica", "Nº dias med. triptanos", "Intensidade máxima cefaleia", "Nº idas urgências",
			"Nº dias abstinência laboral" };
	private static final int MAX_MONTH_TO_EXPORT = 3;

	public static String generateMonthKey(LocalDate date) {
		return date.getYear() + DIVISOR + convertToDateNumber(date.getMonthValue());
	}

	public static String generateMonthKey(LocalDateTime date) {
		return generateMonthKey(date.toLocalDate());
	}

	public static String convertToDateNumber(int num) {
		final String asDateString = num < 10 ? "0" + num : "" + num;
		return asDateString;
	}

	public static LocalDate generateFromMonthKey(String monthKey) {
		String[] split = monthKey.split(DIVISOR);
		return LocalDate.of((int) Integer.valueOf(split[0]), (int) Integer.valueOf(split[1]), 1);
	}

	/**
	 * Receives a list of MonthlyReport and generates a table for the Professionals
	 * to export to another system
	 * 
	 * @param months the MonthlyReports to convert to table
	 * @return a String with the information for be copied to the clipboard
	 */
	protected static String generateTableAsString(List<MonthlyReport> months) {
		return convertMatrixToString(generateMatrix(months));
	}

	private static String[][] generateMatrix(List<MonthlyReport> months) {
		final int nCol = Math.min(MAX_MONTH_TO_EXPORT, months.size());
		String[][] matrix = new String[rowNames.length][1 + nCol];
		for (int i = 0; i < matrix.length; ++i) {
			matrix[i][0] = rowNames[i];
		}
		for (int i = 1, m = months.size() - nCol; m < months.size(); ++i, ++m) {
			matrix[0][i] = months.get(m).getMonth();
			matrix[1][i] = "" + (int) (months.get(m).getPercentEvolutionHeadacheDay() * 100) + "%";
			matrix[2][i] = "" + months.get(m).getHeadacheDays();
			matrix[3][i] = "" + months.get(m).getMigraineDays();
			matrix[4][i] = "" + months.get(m).getPainkillerDays();
			matrix[5][i] = "" + months.get(m).getTriptanDays();
			matrix[6][i] = "" + months.get(m).getMaximumHeadacheIntensity();
			matrix[7][i] = "" + months.get(m).getUrgencyServiceVisits();
			matrix[8][i] = "" + months.get(m).getWorkAbsentDays();
		}
		return matrix;
	}

	private static String convertMatrixToString(String[][] values) {
		int[] dimensions = calculateColumnsWidth(values);
		StringBuilder sb = new StringBuilder();
		for (int l = 0; l < values.length; ++l) {
			sb.append(lineAsString(values[l], dimensions));
		}
		return sb.toString();
	}

	private static String lineAsString(String[] line, int[] dimensions) {
		StringBuilder sb = new StringBuilder("|");
		for (int i = 0; i < line.length; ++i) {
			if (i == 0) {
				sb.append(rowHeaderCellAsString(dimensions[i], line[i]));
			} else {
				sb.append(cellAsString(dimensions[i], line[i]));
			}
		}
		sb.append("\n");
		return sb.toString();
	}

	private static String cellAsString(int size, String string) {
		return fillWithSpaces(size - string.length()) + string + " |";
	}

	private static String rowHeaderCellAsString(int size, String string) {
		return " " + string + fillWithSpaces(size - string.length()) + "|";
	}

	private static String fillWithSpaces(int times) {
		String space = "";
		while (times >= 0) {
			space += " ";
			--times;
		}
		return space;
	}

	private static int[] calculateColumnsWidth(String[][] values) {
		int[] dimensions = new int[values[0].length];
		for (int l = 0; l < values.length; ++l) {
			for (int i = 0; i < values[l].length; ++i) {
				dimensions[i] = Math.max(dimensions[i], values[l][i].length());
			}
		}
		return dimensions;
	}
}
