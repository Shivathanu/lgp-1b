package lgp.myhealthdiary.myhealthdiary.dto;

import lgp.myhealthdiary.myhealthdiary.model.Alert;

/**
 * To return the MedicalAppointment id and the associated Alert upon
 * MedicalAppointment creation
 *
 */
public class MedicalAppointmentAlertDto {

	private long medicalAppointmentId;

	private Alert alert;

	public MedicalAppointmentAlertDto() {
	}

	public MedicalAppointmentAlertDto(long medicalAppointmentId, Alert alert) {
		this.medicalAppointmentId = medicalAppointmentId;
		this.alert = alert;
	}

	public long getMedicalAppointmentId() {
		return medicalAppointmentId;
	}

	public void setMedicalAppointmentId(long medicalAppointmentId) {
		this.medicalAppointmentId = medicalAppointmentId;
	}

	public Alert getAlert() {
		return alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}
}
