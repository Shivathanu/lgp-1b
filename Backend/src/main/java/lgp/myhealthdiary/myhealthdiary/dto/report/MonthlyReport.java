package lgp.myhealthdiary.myhealthdiary.dto.report;

import java.util.List;

import lgp.myhealthdiary.myhealthdiary.model.Entry;

/**
 * This class is responsible for summarizing all Patient Entries from a month
 * into a monthly view
 *
 */
public class MonthlyReport {

	private String month;

	// normal headache + migraine
	private int headacheDays;

	private int migraineDays;

	// relative to previous month
	private double percentEvolutionHeadacheDay;

	private int painkillerDays;

	private int triptanDays;

	private int maximumHeadacheIntensity;

	private int urgencyServiceVisits;

	private int workAbsentDays;

	public MonthlyReport(String month) {
		this.month = month;
	}

	public MonthlyReport(String month, List<Entry> diaryEntriesOfMonth) {
		this(month);
		processMonthlyEntries(diaryEntriesOfMonth);
	}

	public String getMonth() {
		return month;
	}

	public int getHeadacheDays() {
		return headacheDays;
	}

	public int getMigraineDays() {
		return migraineDays;
	}

	public double getPercentEvolutionHeadacheDay() {
		return percentEvolutionHeadacheDay;
	}

	public void setPercentEvolutionHeadacheDay(double percentEvolutionHeadacheDay) {
		this.percentEvolutionHeadacheDay = percentEvolutionHeadacheDay;
	}

	public int getPainkillerDays() {
		return painkillerDays;
	}

	public int getTriptanDays() {
		return triptanDays;
	}

	public int getMaximumHeadacheIntensity() {
		return maximumHeadacheIntensity;
	}

	public int getWorkAbsentDays() {
		return workAbsentDays;
	}

	public int getUrgencyServiceVisits() {
		return urgencyServiceVisits;
	}

	/**
	 * This method creates an empty MonthlyReport with invalid info just to fill in
	 * the Professional table when there is no information for a month
	 * 
	 * @return a MonthlyReport with -1 in every field except
	 *         percentageEvolutionHeadacheDay
	 */
	public MonthlyReport createEmpty() {
		this.headacheDays = -1;
		this.migraineDays = -1;
		this.percentEvolutionHeadacheDay = 0;
		this.painkillerDays = -1;
		this.triptanDays = -1;
		this.maximumHeadacheIntensity = -1;
		this.urgencyServiceVisits = -1;
		this.workAbsentDays = -1;
		return this;
	}
	
	/**
	 * Receives an Entry and updates the object field according to that entry
	 * 
	 * @param entry an Entry
	 */
	public void processEntry(Entry entry) {
		maximumHeadacheIntensity = Math.max(maximumHeadacheIntensity, entry.getHeadacheIntensity());
		if (entry.getHeadacheIntensity() > 0) {
			++headacheDays;
			if (entry.isMigraine()) {
				++migraineDays;
			}
		}
		if (entry.isTookPainKillers()) {
			++painkillerDays;
		}
		if (entry.isTookTriptans()) {
			++triptanDays;
		}
		if (entry.isEmergencyServiceRecurrence()) {
			++urgencyServiceVisits;
		}
		if (entry.isWorkIncapacity()) {
			++workAbsentDays;
		}
	}

	/**
	 * Receives a list of Entries for a month and processes them to create the
	 * MonthlyReport
	 * 
	 * @param diaryEntries the Patient's diary entries for a desired month
	 */
	private void processMonthlyEntries(List<Entry> diaryEntries) {
		for (Entry entry : diaryEntries) {
			processEntry(entry);
		}
	}

	
	@Override
	public String toString() {
		return "MonthlySummary [month=" + month + ", headacheDays=" + headacheDays + ", migraineDays=" + migraineDays
				+ ", percentEvolutionHeadacheDay=" + percentEvolutionHeadacheDay + ", painkillerDays=" + painkillerDays
				+ ", triptanDays=" + triptanDays + ", maximumHeadacheIntensity=" + maximumHeadacheIntensity
				+ ", urgencyServiceVisits=" + urgencyServiceVisits + ", workAbsentDays=" + workAbsentDays + "]";
	}
	}
