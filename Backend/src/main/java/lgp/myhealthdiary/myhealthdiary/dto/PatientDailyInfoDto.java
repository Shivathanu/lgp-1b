package lgp.myhealthdiary.myhealthdiary.dto;

/**
 * To represent the daily view a Patient can have on the mobile app
 *
 */
public class PatientDailyInfoDto {

	private EntryDto entry;

	private MedicalAppointmentDto medicalAppointment;

	public PatientDailyInfoDto(EntryDto entry) {
		this.entry = entry;
	}

	public PatientDailyInfoDto(MedicalAppointmentDto medicalAppointment) {
		this.medicalAppointment = medicalAppointment;
	}

	public PatientDailyInfoDto(EntryDto entry, MedicalAppointmentDto medicalAppointment) {
		this.entry = entry;
		this.medicalAppointment = medicalAppointment;
	}

	public EntryDto getEntry() {
		return entry;
	}

	public void setEntry(EntryDto entry) {
		this.entry = entry;
	}

	public MedicalAppointmentDto getMedicalAppointment() {
		return medicalAppointment;
	}

	public void setMedicalAppointment(MedicalAppointmentDto medicalAppointment) {
		this.medicalAppointment = medicalAppointment;
	}

}
