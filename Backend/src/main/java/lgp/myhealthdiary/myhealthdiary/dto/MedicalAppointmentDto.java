package lgp.myhealthdiary.myhealthdiary.dto;

import java.time.LocalDateTime;

import lgp.myhealthdiary.myhealthdiary.model.MedicalAppointment;

public class MedicalAppointmentDto {
	
	private long id;

	private LocalDateTime dateTime;

	private String doctor;
	
	private String place;
	
	private String observations;
	
	private String type;

	public MedicalAppointmentDto(MedicalAppointment medicalAppointment) {
		this.id = medicalAppointment.getId();
		this.dateTime = medicalAppointment.getDateTime();
		this.doctor = medicalAppointment.getDoctor();
		this.place = medicalAppointment.getPlace();
		this.observations = medicalAppointment.getObservations();
		this.type = medicalAppointment.getType();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}
}
