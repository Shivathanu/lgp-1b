package lgp.myhealthdiary.myhealthdiary.dto;

import java.util.ArrayList;
import java.util.List;

import lgp.myhealthdiary.myhealthdiary.model.Alert;

/**
 * Object comprising all the info needed for a Professional after his/her login
 *
 */
public class ProfessionalInfoAfterLoginDto {

	private ProfessionalDto professionalProfile;
	
	private List<PatientDto> patientsList = new ArrayList<PatientDto>();

	private List<Alert> alerts = new ArrayList<Alert>();

	public ProfessionalInfoAfterLoginDto(ProfessionalDto professionalProfile, List<PatientDto> patientsList,
			List<Alert> alerts) {
		this.professionalProfile = professionalProfile;
		this.patientsList = patientsList;
		this.alerts = alerts;
	}

	public ProfessionalDto getProfessionalProfile() {
		return professionalProfile;
	}

	public List<PatientDto> getPatientsList() {
		return patientsList;
	}

	public List<Alert> getAlerts() {
		return alerts;
	}
	}