package lgp.myhealthdiary.myhealthdiary.dto;

import lgp.myhealthdiary.myhealthdiary.model.Entry;

public class EntryDto {
	
	private boolean migraine;
	
	//between 1 to 10
	private int headacheIntensity;
	
	
	//taken pain killer(s) or not
	private boolean tookPainKillers;
	
	
	//taken triptan or not
	private boolean tookTriptans;
	
	
	//appealed to the emergency service or not
	private boolean emergencyServiceRecurrence;
	
	
	//patient went to work or not
	private boolean workIncapacity;
	
	
	//extra important observations
	private String observations;
	
	
	//men cant have menstruation
	private boolean menstruation;
	
	public EntryDto(Entry entry) {
		this.setMigraine(entry.isMigraine());
		this.setHeadacheIntensity(entry.getHeadacheIntensity());
		this.setTookPainKillers(entry.isTookPainKillers());
		this.setTookTriptans(entry.isTookTriptans());
		this.setEmergencyServiceRecurrence(entry.isEmergencyServiceRecurrence());
		this.setWorkIncapacity(entry.isWorkIncapacity());
		this.setMenstruation(entry.isMenstruation());
		this.setObservations(entry.getObservations());
	}

	public boolean isMigraine() {
		return migraine;
	}

	public void setMigraine(boolean migraine) {
		this.migraine = migraine;
	}

	public int getHeadacheIntensity() {
		return headacheIntensity;
	}

	public void setHeadacheIntensity(int headacheIntensity) {
		this.headacheIntensity = headacheIntensity;
	}

	public boolean isTookPainKillers() {
		return tookPainKillers;
	}

	public void setTookPainKillers(boolean tookPainKillers) {
		this.tookPainKillers = tookPainKillers;
	}

	public boolean isTookTriptans() {
		return tookTriptans;
	}

	public void setTookTriptans(boolean tookTriptans) {
		this.tookTriptans = tookTriptans;
	}

	public boolean isEmergencyServiceRecurrence() {
		return emergencyServiceRecurrence;
	}

	public void setEmergencyServiceRecurrence(boolean emergencyServiceRecurrence) {
		this.emergencyServiceRecurrence = emergencyServiceRecurrence;
	}

	public boolean isWorkIncapacity() {
		return workIncapacity;
	}

	public void setWorkIncapacity(boolean workIncapacity) {
		this.workIncapacity = workIncapacity;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public boolean isMenstruation() {
		return menstruation;
	}

	public void setMenstruation(boolean menstruation) {
		this.menstruation = menstruation;
	}
}
