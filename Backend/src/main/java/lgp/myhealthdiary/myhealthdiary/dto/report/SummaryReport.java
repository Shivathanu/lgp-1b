package lgp.myhealthdiary.myhealthdiary.dto.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class groups the MonthlyReports and the table as String to the
 * Professionals
 *
 */
public class SummaryReport {

	private List<MonthlyReport> months = new ArrayList<MonthlyReport>();

	private String exportToString = "";

	public SummaryReport(List<MonthlyReport> months) {
		super();
		this.months = months;
		calculatePercentageEvolution();
		this.exportToString = ReportUtils.generateTableAsString(months);
	}

	public List<MonthlyReport> getMonths() {
		return months;
	}

	public String getExportToString() {
		return exportToString;
	}

	/**
	 * This method calculates the evolution of headache days from a month to the
	 * next
	 */
	private void calculatePercentageEvolution() {
		Collections.sort(months, Comparator.comparing(MonthlyReport::getMonth));
		for (int i = 1; i < months.size(); ++i) {
			final double evolution = calculateEvolution(months.get(i).getHeadacheDays(),
					months.get(i - 1).getHeadacheDays());
			months.get(i).setPercentEvolutionHeadacheDay(evolution);
		}
	}


	private double calculateEvolution(int actualValue, int previousValue) {
		return previousValue != 0 ? Math.round(((actualValue - previousValue) / (double) previousValue) * 100) / 100.
				: actualValue * 1.;
	}
}
