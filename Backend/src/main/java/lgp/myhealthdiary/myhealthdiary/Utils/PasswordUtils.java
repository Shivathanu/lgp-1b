package lgp.myhealthdiary.myhealthdiary.Utils;

import java.security.SecureRandom;

/**
 * To generate random passwords
 *
 */
public class PasswordUtils {

	private static final String ALLOWED_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	private static final int PASSWORD_LENGTH = 9;

	// code from
	// https://www.techiedelight.com/generate-random-alphanumeric-password-java/

	public static String generateRandomPassword() {
        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();
		for (int i = 0; i < PASSWORD_LENGTH; i++) {
            int randomIndex = random.nextInt(ALLOWED_CHARS.length());
            sb.append(ALLOWED_CHARS.charAt(randomIndex));
        }
        return sb.toString();
    }

	public static int getMinimumPasswordLength() {
		return PASSWORD_LENGTH;
	}

}
