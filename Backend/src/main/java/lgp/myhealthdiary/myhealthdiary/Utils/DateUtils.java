package lgp.myhealthdiary.myhealthdiary.Utils;

/**
 * To validate date fields
 *
 */
public class DateUtils {

	public static void validateDate(int year, int month, int day) {
		validateYearMonth(year, month);
		if (day > 31) {
			throw new RuntimeException("Data inválida");
		}
	}

	public static void validateYearMonth(int year, int month) {
		if (month < 1 || month > 12 || year < 2019) {
			throw new RuntimeException("Data inválida");
		}
	}

}
