package lgp.myhealthdiary.myhealthdiary.Utils;

public interface DTO<T> {

    public T toObject();
}
