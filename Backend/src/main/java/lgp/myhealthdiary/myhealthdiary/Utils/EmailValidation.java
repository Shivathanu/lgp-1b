package lgp.myhealthdiary.myhealthdiary.Utils;
import org.apache.commons.validator.routines.EmailValidator;

/**
 * To validate email addresses
 *
 */
public class EmailValidation {

	// to insert the hosts allowed to register Professionals with
	private static final String[] hosts = { "ulsm.min-saude.pt" };
	
	public static boolean isValidEmail(String email) {
	       EmailValidator validator = EmailValidator.getInstance();
	       return validator.isValid(email);
	   }

	public static boolean isValidProfessionalEmail(String email) {
		boolean isHospitalEmail = false;
		String[] split = email.split("@");
		if (isValidEmail(email) && split.length == 2) {
			for (int i = 0; i < hosts.length && !isHospitalEmail; ++i) {
				if (split[1].equals(hosts[i])) {
					isHospitalEmail = true;
				}
			}
		}
		return isHospitalEmail;
	}

}
