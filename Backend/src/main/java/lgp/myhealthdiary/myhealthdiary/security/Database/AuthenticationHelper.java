package lgp.myhealthdiary.myhealthdiary.security.Database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.repository.PatientRepository;
import lgp.myhealthdiary.myhealthdiary.repository.ProfessionalRepository;
import lgp.myhealthdiary.myhealthdiary.security.JwtTokenUtil;

/**
 * Methods to authorize the access into the system from the token
 *
 */
@Service
public class AuthenticationHelper {

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	ProfessionalRepository professionalRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	private final static String INVALIDITY_MESSAGE = "O token não é válido para o user ";

	/**
	 * returns the professional id of the Professional associated with the token
	 * 
	 * @param token the token
	 * @return the Professional id
	 */
	public long getProfessionalIdFromToken(String token) {
		final String username = jwtTokenUtil.getUsernameFromBearerToken(token);
		final long professionalId = professionalRepository.findIdByUsername(username)
				.orElseThrow(() -> new RuntimeException(INVALIDITY_MESSAGE + username));
		return professionalId;
	}

	/**
	 * checks if a token is associated with a given Patient code
	 * 
	 * @param token       the token
	 * @param patientCode the code to be compared with the code associated with the
	 *                    token
	 * @return true if the code is associated with the token, false otherwise
	 */
	public boolean isPatientCodeInToken(String token, String patientCode) {
		final String username = jwtTokenUtil.getUsernameFromBearerToken(token);
		boolean match = true;
		if (!patientCode.equals(username)) {
			match = false;
			throw new RuntimeException(INVALIDITY_MESSAGE + username);
		}
		return match;
	}

	}
