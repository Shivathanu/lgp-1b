package lgp.myhealthdiary.myhealthdiary.security.Database;
import java.util.ArrayList;

import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.model.Professional;
import lgp.myhealthdiary.myhealthdiary.repository.PatientRepository;
import lgp.myhealthdiary.myhealthdiary.repository.ProfessionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;

@Service
public class JWTUserDetailsService implements UserDetailsService {

    @Autowired
    private ProfessionalRepository professionalRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        if ("javainuse".equals(username)) { // debug

            return null;

        } else {

            for(Patient u : patientRepository.findAll()){
                if( u.getUsername().equals(username)){

                    return u;
                }
            }

            for(Professional u : professionalRepository.findAll()){
                if( u.getUsername().equals(username)){

                    return u;
                }
            }


            throw new UsernameNotFoundException("User not found with username: " + username);


        }
    }
}