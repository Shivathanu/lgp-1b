package lgp.myhealthdiary.myhealthdiary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyhealthdiaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyhealthdiaryApplication.class, args);
	}

}
