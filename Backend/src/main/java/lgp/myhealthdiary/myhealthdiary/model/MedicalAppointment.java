package lgp.myhealthdiary.myhealthdiary.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents the Patients' appointments. Used as a reminder, no connection to
 * Professionals
 *
 */
@Entity
public class MedicalAppointment {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
    
	private String doctor;
	
	
	private String place;
	
	
	private String observations;
	
	private LocalDateTime dateTime;
	
	
	//CSP appointment or neurology appointment
	private String type;
	
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    @JsonIgnore
	private Patient patient;
	
	
	/*
	@OneToOne
	private Alert alert;
	*/
	
	public MedicalAppointment() {
		
	}
	
	public MedicalAppointment(String doctor, String place, String observations, LocalDateTime dateTime, String type) {
		this.doctor = doctor;
		this.place = place;
		this.observations = observations;
		this.dateTime = dateTime;
		this.type = type;
	}

	
	public Long getId() {
		return id;
	}



	public String getPlace() {
		return place;
	}



	public void setPlace(String place) {
		this.place = place;
	}



	public String getObservations() {
		return observations;
	}



	public void setObservations(String observations) {
		this.observations = observations;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}


	public String getDoctor() {
		return doctor;
	}


	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}


	public LocalDateTime getDateTime() {
		return dateTime;
	}


	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	
	public Patient getPatient() {
		return patient;
	}


	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	
}
