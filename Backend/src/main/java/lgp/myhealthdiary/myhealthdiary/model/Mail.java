package lgp.myhealthdiary.myhealthdiary.model;

import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lgp.myhealthdiary.myhealthdiary.security.Database.AttributeEncryptor;

/**
 * Representing the emails sent by the system (GDPR)
 * 
 *
 */
@Entity
public class Mail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@Convert(converter = AttributeEncryptor.class)
    private String recipient;

    private String content;

    private LocalDateTime createTime = LocalDateTime.now();

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getDate() {
        return createTime;
    }

    public void setDate(LocalDateTime createTime) {
        this.createTime = createTime;
    }
}
