package lgp.myhealthdiary.myhealthdiary.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents alerts both for Patient and Professional
 * 
 *
 */
@Entity
public class Alert {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	private String subject;

	//message that will show up 
	private String message;
	
	//date and time of notification
	private LocalDateTime dateTime;

	// patient to whom the alert is related
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "patient_id")
	@JsonIgnore
	private Patient patient;

	private boolean readByRecipient = false;

	// if the alert is intended for the patient (F) or for the professional (T)
	@JsonIgnore
	private boolean professionalAlert;

	// to use in medication abuse notifications as a count
	// as appointment id in appointment alerts
	@JsonIgnore
	private Long helperField;

	public Alert() {
	}

	public Alert(String subject, String message, Patient patient, LocalDateTime dateTime, boolean professionalAlert) {
		this.subject = subject;
		this.message = message;
		this.patient = patient;
		this.professionalAlert = professionalAlert;
		setDateTime(dateTime);
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime.plusMinutes(1).withSecond(0).withNano(0);
		this.readByRecipient = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public boolean isReadByRecipient() {
		return readByRecipient;
	}

	public boolean changeReadStatus() {
		this.readByRecipient = !readByRecipient;
		return this.readByRecipient;
	}

	public boolean isProfessionalAlert() {
		return professionalAlert;
	}

	public Long getHelperField() {
		return helperField;
	}

	public void setHelperField(Long helperField) {
		this.helperField = helperField;
	}
}
