package lgp.myhealthdiary.myhealthdiary.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * The logs to be stored encrypted in the server about all accesses and
 * operations of the system (GDPR)
 *
 */
@Entity
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date create_time;

    private String logger;

    private String level;

    private String message;
}
