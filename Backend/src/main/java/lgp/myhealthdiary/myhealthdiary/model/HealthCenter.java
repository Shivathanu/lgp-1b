package lgp.myhealthdiary.myhealthdiary.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents the unidades de saúde from the system, also the hospital
 *
 */
@Entity
public class HealthCenter {
	
	@Id
	@Column(length = 1)
	private String code;
	
	private String name;
	
	private String location;

	private String centroSaude;
	
	// to distinguish the hospitals
	@JsonIgnore
	private boolean hospital;

	public HealthCenter() {
		
	}

	public HealthCenter(String code, String name, String location, String centroSaude) {
		this(code, name, location, centroSaude, false);
	}
	
	public HealthCenter(String code, String name, String location, String centroSaude, boolean hospital) {
		this.code = code;
		this.name = name;
		this.location = location;
		this.centroSaude = centroSaude;
		this.hospital = hospital;
	}
	
	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public String getCentroSaude() {
		return centroSaude;
	}

	public boolean isHospital() {
		return hospital;
	}
	}
