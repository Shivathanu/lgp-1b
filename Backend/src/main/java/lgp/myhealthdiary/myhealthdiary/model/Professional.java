package lgp.myhealthdiary.myhealthdiary.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lgp.myhealthdiary.myhealthdiary.security.Database.AttributeEncryptor;

/**
 * Represents a health Professional (neurologist, doctor or nurse) and all its
 * related Entities
 *
 */
@Entity
public class Professional implements UserDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Convert(converter = AttributeEncryptor.class)
	private String clinicalName;

	@ManyToOne
	private ProfessionalSpeciality speciality;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String image;

	@ManyToOne
	private HealthCenter healthCenter;

	@ManyToMany
	public List<Patient> assigned_patients = new ArrayList<>();

	@Column(nullable = false, unique = true)
	private String password;

	@Convert(converter = AttributeEncryptor.class)
	@Column(nullable = false, unique = true)
	private String username;
	
	@Convert(converter = AttributeEncryptor.class)
	@Column(nullable = false, unique = true)
	private String email;

	public Professional() {
	}

	public Professional(long id, String clinicalName, ProfessionalSpeciality speciality, HealthCenter healthCenter,
			String username) {
		this.id = id;
		this.clinicalName = clinicalName;
		this.speciality = speciality;
		this.healthCenter = healthCenter;
		this.username = username;
	}
	
	public Professional(
			long id, 
			String clinicalName, 
			ProfessionalSpeciality speciality,
			HealthCenter healthCenter, 
			String username,
			String email,
			String password) {
		this.id = id;
		this.clinicalName = clinicalName;
		this.speciality = speciality;
		this.healthCenter = healthCenter;
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public String getClinicalName() {
		return clinicalName;
	}

	public void setClinicalName(String clinicalName) {
		this.clinicalName = clinicalName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ProfessionalSpeciality getSpeciality() {
		return speciality;
	}

	public void setSpeciality(ProfessionalSpeciality speciality) {
		this.speciality = speciality;
	}

	public HealthCenter getHealthCenter() {
		return healthCenter;
	}

	public void setHealthCenter(HealthCenter healthCenter) {
		this.healthCenter = healthCenter;
	}

	public List<Patient> getAssigned_patients() {
		return assigned_patients;
	}

	public void setAssigned_patients(List<Patient> assigned_patients) {
		this.assigned_patients = assigned_patients;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public void setPassword(String encode) {
		this.password = encode;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
