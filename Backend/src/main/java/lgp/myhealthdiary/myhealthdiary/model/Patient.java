package lgp.myhealthdiary.myhealthdiary.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lgp.myhealthdiary.myhealthdiary.security.Database.AttributeEncryptor;

/**
 * Represents a Patient and all its related Entities
 *
 */
@Entity
public class Patient implements UserDetails {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    //health center letter plus id
    @Column(nullable = false, unique = true)
    private String code;

	@Convert(converter = AttributeEncryptor.class)
	private String nameInitials;

	@Convert(converter = AttributeEncryptor.class)
	@Column(nullable = false, unique = true)
	private String email;
	
	@Convert(converter = AttributeEncryptor.class)
	@Column(nullable = false, unique = true)
	private String numeroUtente;

	@Convert(converter = AttributeEncryptor.class)
	@Column(nullable = false, unique = true)
	private String numProcessoHospitalar;

	private LocalDate birthdate;
	
	private String gender;
    
	private Boolean migraineWithAura;
    
	private Boolean exclusivelyUnilateral;
	
	@ManyToOne
    private HealthCenter healthCenter;

	private Boolean symptomaticTreatment;

	private String symptomaticTreatmentDetails;

	@OneToMany(mappedBy = "patient")
	private List<ProphylacticTreatment> prophylacticTreatments = new ArrayList<>();
	
	private Boolean anxiety;
	
	private Boolean depression;
	
	private Boolean insomnia;
	
	private String otherMedicalConditions;
	
	private Boolean hasActiveAccount = false;

	@Column(nullable = false, unique = true)
	private String password;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", orphanRemoval = true)
	private List<Entry> diary = new ArrayList<>();

	@ManyToMany(mappedBy = "assigned_patients")
	@JsonIgnore
	private List<Professional> professionals = new ArrayList<>();
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", orphanRemoval = true)
	private List<MedicalAppointment> medicalAppointments = new ArrayList<>();

	// both alerts to and alerts about patient (to professionals)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", orphanRemoval = true)
	private List<Alert> alerts = new ArrayList<>();

	public Patient() {
	}

	public Patient(HealthCenter healthCenter, String nameInitials, LocalDate birthdate, String gender,
			Boolean migraineWithAura, Boolean exclusivelyUnilateral) {
		this.nameInitials = nameInitials;
		this.setBirthdate(birthdate);
		this.setGender(gender);
		this.migraineWithAura = migraineWithAura;
		this.exclusivelyUnilateral = exclusivelyUnilateral;
		this.healthCenter = healthCenter;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumProcessoHospitalar() {
		return numProcessoHospitalar;
	}

	public void setNumProcessoHospitalar(String numProcessoHospitalar) {
		this.numProcessoHospitalar = numProcessoHospitalar;
	}

	public String getNameInitials() {
		return nameInitials;
	}

	public void setNameInitials(String nameInitials) {
		this.nameInitials = nameInitials;
	}

	public String getNumeroUtente() {
		return numeroUtente;
	}

	public void setNumeroUtente(String numeroUtente) {
		this.numeroUtente = numeroUtente;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Boolean isMigraineWithAura() {
		return migraineWithAura;
	}

	public void setMigraineWithAura(Boolean migraineWithAura) {
		this.migraineWithAura = migraineWithAura;
	}

	public Boolean isExclusivelyUnilateral() {
		return exclusivelyUnilateral;
	}

	public void setExclusivelyUnilateral(Boolean exclusivelyUnilateral) {
		this.exclusivelyUnilateral = exclusivelyUnilateral;
	}

	public HealthCenter getHealthCenter() {
		return healthCenter;
	}

	public void setHealthCenter(HealthCenter healthCenter) {
		this.healthCenter = healthCenter;
	}

	public Boolean getSymptomaticTreatment() {
		return symptomaticTreatment;
	}

	public void setSymptomaticTreatment(Boolean symptomaticTreatment) {
		this.symptomaticTreatment = symptomaticTreatment;
	}

	public String getSymptomaticTreatmentDetails() {
		return symptomaticTreatmentDetails;
	}

	public void setSymptomaticTreatmentDetails(String symptomaticTreatmentDetails) {
		this.symptomaticTreatmentDetails = symptomaticTreatmentDetails;
	}

	public List<ProphylacticTreatment> getProphylacticTreatments() {
		return prophylacticTreatments;
	}

	public void setProphylacticTreatments(List<ProphylacticTreatment> prophylacticTreatments) {
		this.prophylacticTreatments = prophylacticTreatments;
	}

	public void addProphylacticTreatment(ProphylacticTreatment prophylacticTreatment) {
		this.prophylacticTreatments.add(prophylacticTreatment);
	}

	public Boolean getAnxiety() {
		return anxiety;
	}

	public void setAnxiety(Boolean anxiety) {
		this.anxiety = anxiety;
	}

	public Boolean getDepression() {
		return depression;
	}

	public void setDepression(Boolean depression) {
		this.depression = depression;
	}

	public Boolean getInsomnia() {
		return insomnia;
	}

	public void setInsomnia(Boolean insomnia) {
		this.insomnia = insomnia;
	}

	public String getOtherMedicalConditions() {
		return otherMedicalConditions;
	}

	public void setOtherMedicalConditions(String otherMedicalConditions) {
		this.otherMedicalConditions = otherMedicalConditions;
	}

	public List<Entry> getDiary() {
		return diary;
	}

	public void setDiary(List<Entry> diary) {
		this.diary = diary;
	}

	public void addEntry(Entry entry) {
		this.diary.add(entry);
	}

	public void setMedicalAppointments(List<MedicalAppointment> medicalAppointments) {
		this.medicalAppointments = medicalAppointments;
	}

	public List<MedicalAppointment> getMedicalAppointments() {
		return this.medicalAppointments;
	}
	
	public void addMedicalAppointment(MedicalAppointment medicalAppointment) {
		this.medicalAppointments.add(medicalAppointment);
	}
	
	public List<Alert> getAlerts() {
		return this.alerts;
	}

	public Boolean hasActiveAccount() {
		return this.hasActiveAccount;
	}
	
	public void activateAccount() {
		this.hasActiveAccount = true;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}	

	@Override
	public String getUsername() {
		return code;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
