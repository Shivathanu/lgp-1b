package lgp.myhealthdiary.myhealthdiary.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents the diary entries
 *
 */
@Entity
public class Entry {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
    
    //relatively to a patient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    @JsonIgnore
    public Patient patient;

	private LocalDateTime date;
    
// quality of headache: tension headache (false) or migraine (true)     
	private boolean migraine;
	
	//between 1 to 10
	private int headacheIntensity;
	
	
	//taken pain killer(s) or not
	private boolean tookPainKillers;
	
	
	//taken triptan or not
	private boolean tookTriptans;
	
	
	//appealed to the emergency service or not
	private boolean emergencyServiceRecurrence;
	
	
	//patient went to work or not
	private boolean workIncapacity;
	
	
	//extra important observations
	private String observations;
	
	
	//men cant have menstruation
	private boolean menstruation;
	
	
	public Entry() {
		
	}
	
	
	public Entry(Patient patient, LocalDate date, boolean migraine, int headacheIntensity, boolean tookPainKillers,
			boolean tookTriptans, boolean emergencyServiceRecurrence, boolean workIncapacity, String observations,
			boolean menstruation) {
		this.patient = patient;
		this.date = date.atStartOfDay().plusHours(12);
		this.migraine = migraine;
		this.headacheIntensity = headacheIntensity;
		this.tookTriptans = tookTriptans;
		this.tookPainKillers = tookPainKillers;
		this.emergencyServiceRecurrence = emergencyServiceRecurrence;
		this.workIncapacity = workIncapacity;
		this.observations = observations;
		this.menstruation = menstruation;
	}
	
	
	public Long getId() {
		return id;
	}
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	public LocalDateTime getDate() {
		return date;
	}



	public void setDate(LocalDateTime date) {
		this.date = date.withHour(12).withMinute(0).withSecond(0).withNano(0);
	}

	public boolean isMigraine() {
		return migraine;
	}


	public void setMigraine(boolean migraine) {
		this.migraine = migraine;
	}

	public int getHeadacheIntensity() {
		return headacheIntensity;
	}



	public void setHeadacheIntensity(int headacheIntensity) {
		this.headacheIntensity = headacheIntensity;
	}



	public boolean isTookTriptans() {
		return tookTriptans;
	}



	public void setTookTriptans(boolean tookTriptans) {
		this.tookTriptans = tookTriptans;
	}



	public boolean isEmergencyServiceRecurrence() {
		return emergencyServiceRecurrence;
	}



	public void setEmergencyServiceRecurrence(boolean emergencyServiceRecurrence) {
		this.emergencyServiceRecurrence = emergencyServiceRecurrence;
	}



	public boolean isWorkIncapacity() {
		return workIncapacity;
	}



	public void setWorkIncapacity(boolean workIncapacity) {
		this.workIncapacity = workIncapacity;
	}



	public String getObservations() {
		return observations;
	}



	public void setObservations(String observations) {
		this.observations = observations;
	}



	public boolean isMenstruation() {
		return menstruation;
	}



	public void setMenstruation(boolean menstruation) {
		this.menstruation = menstruation;
	}


	public boolean isTookPainKillers() {
		return tookPainKillers;
	}


	public void setTookPainKillers(boolean tookPainKillers) {
		this.tookPainKillers = tookPainKillers;
	}
}
