package lgp.myhealthdiary.myhealthdiary.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents the health Professionals' specialities, storing the endpoint
 * termination where they can be found
 *
 */
@Entity
public class ProfessionalSpeciality {
	
    @Id
	private String specialityName;
    
	@Column(nullable = false)
	private String workplacesEndpoint;

	@JsonIgnore
	private boolean hospitalAssociated;

	public ProfessionalSpeciality() {
	}

	public ProfessionalSpeciality(String specialityName) {
		this.specialityName = specialityName;
	}

	public ProfessionalSpeciality(String specialityName, String workplacesEndpoint, boolean hospitalAssociated) {
		this.specialityName = specialityName;
		this.workplacesEndpoint = workplacesEndpoint;
		this.hospitalAssociated = hospitalAssociated;
	}

	public String getSpecialityName() {
		return specialityName;
	}

	public String getWorkplacesEndpoint() {
		return workplacesEndpoint;
	}

	public boolean isHospitalAssociated() {
		return hospitalAssociated;
	}
}
