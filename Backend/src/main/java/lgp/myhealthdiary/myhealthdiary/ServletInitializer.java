package lgp.myhealthdiary.myhealthdiary;

import lgp.myhealthdiary.myhealthdiary.MyhealthdiaryApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MyhealthdiaryApplication.class);
	}

}
