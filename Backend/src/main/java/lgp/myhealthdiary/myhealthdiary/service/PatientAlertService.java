package lgp.myhealthdiary.myhealthdiary.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.dto.report.ReportUtils;
import lgp.myhealthdiary.myhealthdiary.model.Alert;
import lgp.myhealthdiary.myhealthdiary.model.Entry;
import lgp.myhealthdiary.myhealthdiary.model.MedicalAppointment;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.repository.AlertRepository;


/**
 * This class is responsible for processing the Patient Alerts
 *
 */
@Service
public class PatientAlertService {

	private static Logger logger = Logger.getLogger(PatientAlertService.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	AlertRepository alertRepository;

	@Autowired
	PatientServiceHelper patientServiceHelper;

	// Definition of days
	private static final int PAST_DAYS_ALLOWED = 7;
	private static final int TRIPTANS_THRESHOLD = 10;
	private static final int PAIN_KILLER_THRESHOLD = 15;
	private static final int ALERTS_RANGE = 31;
	private static final int APPOINTMENT_ALERT_ANTECIPATION = 7;
	private static final int DIARY_ALERT_FUTUROLOGY = 3;
	private static final int DIARY_ALERT_HOURS = 21;
	private static final int DIARY_ALERT_MINUTES = 29;

	// messages medication abuse:
	private static final String MEDICATION_ABUSE_SUBJECT = "Abuso de medicação";
	private static final String MEDICATION_ABUSE_START = "Tomou ";
	private static final String TRIPTANS = "triptanos em ";
	private static final String PAIN_KILLERS = "analgésicos em ";
	private static final String MEDICATION_ABUSE_END = " dos últimos " + (ALERTS_RANGE - 1) + " dias.";

	// messages appointment alerts
	private static final String APPOINTMENT_SUBJECT = "Consulta na próxima semana";
	private static final String APPOINTMENT_ALERT_MESSAGE_START = "Tem uma consulta agendada para o próximo dia ";
	private static final String DATE_DIVISOR = "/";
	private static final String APPOINTMENT_TIME = " pelas ";
	private static final String TIME_DIVISOR = ":";
	private static final String APPOINTMENT_ALERT_MESSAGE_END = ". Não se esqueça de preencher o seu diário até lá.";

	// messages diary alerts
	private static final String DIARY_SUBJECT = "Não preencheu o seu diário nos últimos 3 dias";
	private static final String DIARY_MESSAGE = "Preencha o seu diário sempre que for incomodado por uma cefaleia. Lembre-se que é pela sua saúde!";

	public PatientAlertService(AlertRepository alertRepository) {
		this.alertRepository = alertRepository;
	}

	/**
	 *gets all Patient past Alerts 
	 * @param patientCode the code of the Patient
	 * @return the List of Alerts in reversed chronological ordr
	 */
	public List<Alert> getPatientAlerts(String patientCode) {
		return alertRepository.getPatientAlertsHistory(patientCode, beginningDateOfSearch(),
				LocalDateTime.now().plusMinutes(1));
	}

	/**
	 *all scheduled Patient Alerts 
	 * @param patientCode the Patient code
	 * @return the List of the Patient's future Allerts
	 */
	public List<Alert> getPatientFutureAlerts(String patientCode) {
		return alertRepository.getPatientFutureAlerts(patientCode, LocalDateTime.now());
	}

	/**
	 *processes all new Patient Alerts and returns them 
	 * @param patientCode the code of the Patient
	 * @param entries the list of Entries from which to process Alerts
	 * @return the List of Alerts
	 */
	public List<Alert> processAndGetPatientAlerts(String patientCode, List<Entry> entries) {
		processMedicationAlerts(entries);
		return getPatientAlerts(patientCode);
	}

	/**
	 * mark a Patient Alert as read or unread
	 * 
	 * @param alertId     the id of the Allert
	 * @param patientCode the code of the Patient
	 * @return true if the Alert was marked as read, false if it was marked as
	 *         unread
	 */
	public Boolean changePatientAlertReadStatus(long alertId, String patientCode) {
		Boolean read = null;
		Alert alertToRead = alertRepository.findById(alertId)
				.orElseThrow(() -> new RuntimeException("O alerta não existe. " + alertId));
		if (alertToRead.getPatient().getCode().equals(patientCode)) {
			read = alertToRead.changeReadStatus();
			alertRepository.save(alertToRead);
		} else {
			logger.error(bcryptEncoder.encode("PatientAlertService Exception - O alerta não está associado ao paciente. " + patientCode + "."));
			throw new RuntimeException("O alerta não está associado ao paciente. " + patientCode);
		}
		return read;
	}

	/**
	 * process medication Alerts from a List of Entries
	 * 
	 * @param entries the most recent List of Entries
	 * @return the new generated Alerts, if generated, otherwise it will return {}
	 */
	public List<Alert> processMedicationAlerts(List<Entry> entries) {
		List<Alert> createdAlerts = new ArrayList<Alert>();
		List<Entry> recents = sortAndFilter(entries);
		int painkillerDays = 0;
		int triptanDays = 0;
		LocalDateTime lastTriptanDay = beginningDateOfSearch();
		LocalDateTime lastPainKIllerDay = beginningDateOfSearch();
		Patient patient = new Patient();
		if (recents != null && !recents.isEmpty()) {
			patient = recents.get(0).getPatient();
			for (Entry e : recents) {
				if (e.isTookTriptans()) {
					++triptanDays;
					lastTriptanDay = e.getDate();
				}
				if (e.isTookPainKillers()) {
					++painkillerDays;
						lastPainKIllerDay = e.getDate();
				}
			}
		}
		if (shouldCreateAlert(TRIPTANS_THRESHOLD, TRIPTANS, lastTriptanDay, triptanDays, patient.getCode())) {
			createdAlerts.add(createMedicationAlert(TRIPTANS, triptanDays, patient));
		}
		if (shouldCreateAlert(PAIN_KILLER_THRESHOLD, PAIN_KILLERS, lastPainKIllerDay, painkillerDays,
				patient.getCode())) {
			createdAlerts.add(createMedicationAlert(PAIN_KILLERS, painkillerDays, patient));
		}
		return createdAlerts;
	}

	/**
	 * creates a new MedicalAppointment Alert
	 * 
	 * @param appointment the MedicalAppointment
	 * @return the generated Alert
	 */
	public Alert createAppointmentAlert(MedicalAppointment appointment) {
		Alert alert = new Alert(APPOINTMENT_SUBJECT, generateAppointmentMessage(appointment.getDateTime()),
				appointment.getPatient(),
				appointment.getDateTime().minusMinutes(1).minusDays(APPOINTMENT_ALERT_ANTECIPATION), false);
		alert.setHelperField(appointment.getId());
		alert = alertRepository.save(alert);
		return alert;
	}

	/**
	 * updates a MedicalAppointment Alert
	 * 
	 * @param appointment the MedicalAppointment
	 * @return the updated Alert
	 */
	public Alert updateAppointmentAlert(MedicalAppointment appointment) {
		Alert alertToUpdate = alertRepository.getAppointmentAlert(appointment.getId(), APPOINTMENT_SUBJECT,
				appointment.getPatient().getCode()).get();
		alertToUpdate.setDateTime(appointment.getDateTime().minusMinutes(1).minusDays(APPOINTMENT_ALERT_ANTECIPATION));
		alertToUpdate.setMessage(generateAppointmentMessage(appointment.getDateTime()));
		alertRepository.save(alertToUpdate);
		return alertToUpdate;
	}

	/**
	 * deletes a MedicalAppointment Alert
	 * 
	 * @param appointment the MedicalAppointment
	 * @return the id of the deleted Alert
	 */
	public long deleteAppointmentAlert(MedicalAppointment appointment) {
		Alert alertToDelete = alertRepository
				.getAppointmentAlert(appointment.getId(), APPOINTMENT_SUBJECT, appointment.getPatient().getCode())
				.get();
		alertRepository.delete(alertToDelete);
		return alertToDelete.getId();
	}

	/**
	 * gets the Patient diary Alert, unique per Patient
	 * 
	 * @param patientCode the Patient code
	 * @return the diary Alert
	 */
	public Alert getPatientDiaryAlert(String patientCode) {
		Patient patient = patientServiceHelper.getPatientByCode(patientCode);
		return getPatientDiaryAlert(patient);
	}

	/**
	 * gets the Patient diary Alert, unique per Patient
	 * 
	 * @param patient the Patient object
	 * @return the diary Alert
	 */
	public Alert getPatientDiaryAlert(Patient patient) {
		return getOrUpdatePatientDiaryAlert(patient, false);
	}

	/**
	 * updates the diary Alert before returning it
	 * 
	 * @param patient the Patient
	 * @return the diary Alert
	 */
	public Alert updateAndGetPatientDiaryAlert(Patient patient) {
		return getOrUpdatePatientDiaryAlert(patient, true);
	}

	/**
	 * gets the updated Patient diary Alert
	 * 
	 * @param patient    the Patient object
	 * @param changeDate if the date must be changed forcefully (when diary is
	 *                   updated) or only if it has expired
	 * @return the diary Alert
	 */
	private Alert getOrUpdatePatientDiaryAlert(Patient patient, boolean changeDate) {
		Alert diaryAlert = alertRepository.getDiaryAlert(DIARY_SUBJECT, patient.getCode())
				.orElse(
				new Alert(DIARY_SUBJECT, DIARY_MESSAGE, patient, diaryAlertTime(), false));
		if (changeDate || diaryAlert.getDateTime().isBefore(LocalDateTime.now())) {
		diaryAlert.setDateTime(diaryAlertTime());
		}
		diaryAlert = alertRepository.save(diaryAlert);
		return diaryAlert;
	}

	/**
	 * creates a medication related Alert if there isn't any one with the same
	 * medication and medication days in the allowed Entry period
	 * 
	 * @param medication     the type of medication (triptan or painkiller)
	 * @param medicationDays the number of days with medication in the last period
	 *                       to consider
	 * @param patient        the Patient to whom the Alert is related
	 * @return the generated Alert
	 */
	private Alert createMedicationAlert(String medication, int medicationDays, Patient patient) {
		final String message = MEDICATION_ABUSE_START + medication + medicationDays + MEDICATION_ABUSE_END;
		Alert medicationAlert = new Alert(MEDICATION_ABUSE_SUBJECT, message, patient, LocalDateTime.now(), false);
		medicationAlert.setHelperField((long) medicationDays);
		alertRepository.save(medicationAlert);
		return medicationAlert;
	}

	/**
	 * verifiesif a medication Alert shall be created
	 * 
	 * @param threshold         the threshold allowed of the medication, in days
	 * @param medication        the type of medication
	 * @param lastMedicationDay the last day in which the medication was taken
	 * @param medicationDays    the number of days the Patient took the medication
	 *                          in the period in analysis
	 * @param patientCode       the code of the Patient
	 * @return true if a new Alert should be created, false otherwise
	 */
	private boolean shouldCreateAlert(int threshold, String medication, LocalDateTime lastMedicationDay,
			int medicationDays,
			String patientCode) {
		LocalDateTime allowedEntryPeriod = LocalDate.now().minusDays(EntryService.getPastDaysAllowed()).atStartOfDay();
		return medicationDays > threshold && !lastMedicationDay.isBefore(allowedEntryPeriod)
				&& (alertRepository.verifyIfAlertInDate(inSearchMode(medication), medicationDays, lastMedicationDay,
						patientCode) == 0);
	}

	/**
	 * sorts and filters the Entries in chronological order and in the allowed time
	 * period
	 * 
	 * @param entries the list of diary Entries
	 * @return the sorted and filtered List
	 */
	private List<Entry> sortAndFilter(List<Entry> entries) {
		Collections.sort(entries, Comparator.comparing(Entry::getDate));
		LocalDateTime startOfPeriodToFilter = beginningDateOfSearch();
		List<Entry> recent = entries.stream().filter(entry -> !entry.getDate().isBefore(startOfPeriodToFilter))
				.collect(Collectors.toList());
		return recent;
	}

	/**
	 * adds % in the begining and end of a String to search it on the database
	 * 
	 * @param string the original String
	 * @return the %String%
	 */
	private String inSearchMode(String string) {
		return "%" + string + "%";
	}

	/**
	 * generates the message to put on the MedicalAppointment Alert
	 * 
	 * @param date the date of the Appointment
	 * @return the message as a String
	 */
	private String generateAppointmentMessage(LocalDateTime date) {
		String message = APPOINTMENT_ALERT_MESSAGE_START + date.getDayOfMonth() + DATE_DIVISOR
				+ ReportUtils.convertToDateNumber(date.getMonthValue()) + DATE_DIVISOR + date.getYear()
				+ APPOINTMENT_TIME + date.getHour() + TIME_DIVISOR + ReportUtils.convertToDateNumber(date.getMinute())
				+ APPOINTMENT_ALERT_MESSAGE_END;
		return message;
	}

	/**
	 * returns the date from which the Alerts shall be selected
	 * 
	 * @return the start date for the search
	 */
	private LocalDateTime beginningDateOfSearch() {
		return LocalDate.now().minusDays(ALERTS_RANGE).atStartOfDay();
	}

	/**
	 * returns the date to set the diary allert to
	 * 
	 * @return the desired date for the Alert, with the predefined time
	 */
	private LocalDateTime diaryAlertTime() {
		return LocalDate.now().plusDays(DIARY_ALERT_FUTUROLOGY).atTime(DIARY_ALERT_HOURS, DIARY_ALERT_MINUTES);
	}
}