package lgp.myhealthdiary.myhealthdiary.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.dto.EntryDto;
import lgp.myhealthdiary.myhealthdiary.model.Alert;
import lgp.myhealthdiary.myhealthdiary.model.Entry;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.repository.EntryRepository;

/**
 * This service is responsible for all Diary operations of a Patient
 *
 */
@Service
public class EntryService {

	private static Logger logger = Logger.getLogger(EntryService.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	EntryRepository entryRepository;

	@Autowired
	PatientServiceHelper patientServiceHelper;

	@Autowired
	PatientAlertService alertService;

	// constants
	private static final int PAST_DAYS_ALLOWED = 7;
	private static final int HEADACHE_MIN = 0;
	private static final int HEADACHE_MAX = 10;
	private static final int NOON_HOURS = 12;
	
	public EntryService(EntryRepository entryRepository, PatientServiceHelper patientServiceHelper,
			PatientAlertService alertService) {
		this.entryRepository = entryRepository;
		this.patientServiceHelper = patientServiceHelper;
		this.alertService = alertService;
	}

	/**
	 * Gives all entries from a Patient in a map with each day as a key
	 * 
	 * @param patientCode the Patient code
	 * @return the Map with the day as key and the EntryDto as value
	 */
	public Map<LocalDate, EntryDto> getPatientEntries(String patientCode) {
		Patient patient = patientServiceHelper.getPatientByCode(patientCode);
		Map<LocalDate, EntryDto> entriesDate = new HashMap<>();
		for (Entry entry : patient.getDiary()) {
			EntryDto entryDto = new EntryDto(entry);
			entriesDate.put(entry.getDate().toLocalDate(), entryDto);
		}
		return entriesDate;
	}

	/**
	 * Method to register a new diary Entry
	 * 
	 * @param entry       the Entry to be created
	 * @param patientCode the code of the Patient that wants to create the Entry
	 * @return a ResponseEntity with the diary Alert updated and eventual medication
	 *         related Alerts
	 */
	public ResponseEntity<?> createEntry(Entry entry, String patientCode) {
		Patient patient = patientServiceHelper.getPatientByCode(patientCode);
		LocalDateTime today = LocalDateTime.now();

		if (!validateHeadacheIntensity(entry.getHeadacheIntensity())) {
			return new ResponseEntity<String>("A intensidade da cefaleia tem que respeitar a escala 0 - 10.",
					HttpStatus.CONFLICT);
		}
		if (entry.getDate().withHour(0).withMinute(0).isAfter(today)) {
			return new ResponseEntity<String>("Não é possível inserir um registo no futuro!", HttpStatus.CONFLICT);
		}

		if (entry.getDate().isBefore(allowedPeriod())) {
			return new ResponseEntity<String>(
					"Não é possível inserir um registo com mais de " + PAST_DAYS_ALLOWED + " dias de atraso!",
					HttpStatus.CONFLICT);
		}

if (hasEntryInDate(patient.getId(), entry.getDate())) {
			return new ResponseEntity<String>("Já existe um registo para essa data!", HttpStatus.CONFLICT);
		}

		entry.setPatient(patient);
		patient.getDiary().add(entry);
		entryRepository.save(entry);


		List<Alert> generatedAlerts = alertService.processMedicationAlerts(patient.getDiary());

		generatedAlerts.add(alertService.updateAndGetPatientDiaryAlert(patient));

		ResponseEntity response = new ResponseEntity(generatedAlerts, HttpStatus.OK);

		return response;
	}

	/**
	 * To update a previously entered diary Entry
	 * 
	 * @param entry       the updated Entry
	 * @param patientCode the Patient code
	 * @return the ResponseEntity with the updated diary Alert and eventual
	 *         medication related Alerts
	 */
	public ResponseEntity<?> updateEntry(Entry entry, String patientCode) {
		if (entry.getDate().isBefore(LocalDate.now().minusDays(PAST_DAYS_ALLOWED).atStartOfDay())) {
			return new ResponseEntity<String>(
					"Não é possível editar um registo anterior aos últimos " + PAST_DAYS_ALLOWED + " dias!",
					HttpStatus.CONFLICT);
		}
		if (!validateHeadacheIntensity(entry.getHeadacheIntensity())) {
			return new ResponseEntity<String>("A intensidade da cefaleia tem que respeitar a escala 0 - 10.",
					HttpStatus.CONFLICT);
		}
		Entry entryToUpdate = findEntryByDate(patientCode, entry.getDate());
		entryToUpdate.setEmergencyServiceRecurrence(entry.isEmergencyServiceRecurrence());
		entryToUpdate.setHeadacheIntensity(entry.getHeadacheIntensity());
		entryToUpdate.setMenstruation(entry.isMenstruation());
		entryToUpdate.setMigraine(entry.isMigraine());
		entryToUpdate.setObservations(entry.getObservations());
		entryToUpdate.setTookPainKillers(entry.isTookPainKillers());
		entryToUpdate.setTookTriptans(entry.isTookTriptans());
		entryToUpdate.setWorkIncapacity(entry.isWorkIncapacity());
		Patient patient = entryToUpdate.getPatient();

		entryRepository.save(entryToUpdate);

		List<Alert> generatedAlerts = alertService.processMedicationAlerts(patient.getDiary());

		generatedAlerts.add(alertService.updateAndGetPatientDiaryAlert(patient));

		ResponseEntity response = new ResponseEntity(generatedAlerts, HttpStatus.OK);

		return response;
	}

	/**
	 * deletes a previously registered entry
	 * 
	 * @param patientCode the Patient code, that along with the date constitutes a
	 *                    composit key
	 * @param date        the date of the entry, only one allowed per day per
	 *                    Patient
	 * @return true if deleted successfuly, false otherwise
	 */
	public boolean deleteEntry(String patientCode, LocalDate date) {
		Entry entryToDelete = findEntryByDate(patientCode, dateAtNoon(date));
		boolean success = false;
		// can only delete from last week
		if (patientCode.equals(entryToDelete.getPatient().getCode())
				&& entryToDelete.getDate().isAfter(allowedPeriod())) {
		entryRepository.delete(entryToDelete);
			success = true;
		}
		return success;
	}

	/**
	 * returns the diary Entry for a given Patient on a given day (only one per day
	 * allowed)
	 * 
	 * @param patientCode the code of the Patient
	 * @param date        the date of the Entry
	 * @return the diary Entry for that Patient in that day, if it exists
	 */
	public Entry findEntryByDate(String patientCode, LocalDateTime date) {
		return entryRepository.findByDate(patientCode, date.getYear(), date.getMonthValue(), date.getDayOfMonth())
				.orElseThrow(() -> new RuntimeException("Não há registo para a data indicada"));
	}

	/**
	 * returns a list of Entries of all followed Patients of a Professional, ordered
	 * by Patient
	 * 
	 * @param professionalId the id of the Professional
	 * @param startDate      the date after which the Entries should be fetched
	 * @return the List with all Entries ordered by Patient and in chronological
	 *         order
	 */
	public List<Entry> getFollowedPatientsEntries(long professionalId, LocalDateTime startDate) {
		return entryRepository.entriesByProfessionalPatients(professionalId, startDate);
	}

	/**
	 * Checks if the Patient already has an Entry on a given day
	 * 
	 * @param patientId the id of the Patient
	 * @param date      the date
	 * @return true if already exists an Entry in that day for the Patient, false
	 *         otherwise
	 */
	private boolean hasEntryInDate(long patientId, LocalDateTime date) {
		return entryRepository.patientHasEntryInDate(patientId, date.getYear(), date.getMonthValue(),
				date.getDayOfMonth()) > 0 ? true : false;
	}

	/**
	 * validates if the headache intesity is on the desired scale
	 * 
	 * @param headacheIntensity the given headache intensity
	 * @return true if the value is valid, false otherwise
	 */
	private boolean validateHeadacheIntensity(int headacheIntensity) {
		boolean isValid = false;
		if (headacheIntensity >= HEADACHE_MIN && headacheIntensity <= HEADACHE_MAX) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * the date from which it is possible to add new Entries
	 * 
	 * @return the last day where is possible to insert an Entry
	 */
	private LocalDateTime allowedPeriod() {
		return LocalDate.now().minusDays(PAST_DAYS_ALLOWED).atStartOfDay();
	}

	/**
	 * sets the date time to 12:00
	 */
	private LocalDateTime dateAtNoon (LocalDate date) {
		return date.atStartOfDay().plusHours(NOON_HOURS);
	}
	
	/**
	 * For other services to get the allowed period when an Entry can be created
	 * 
	 * @return the number of past days allowed
	 */
	public static int getPastDaysAllowed() {
		return PAST_DAYS_ALLOWED;
	}
}