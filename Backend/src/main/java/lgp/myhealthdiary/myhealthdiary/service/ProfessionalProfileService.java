package lgp.myhealthdiary.myhealthdiary.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.Utils.EmailValidation;
import lgp.myhealthdiary.myhealthdiary.Utils.PasswordUtils;
import lgp.myhealthdiary.myhealthdiary.dto.ProfessionalDto;
import lgp.myhealthdiary.myhealthdiary.model.HealthCenter;
import lgp.myhealthdiary.myhealthdiary.model.Professional;
import lgp.myhealthdiary.myhealthdiary.model.ProfessionalSpeciality;
import lgp.myhealthdiary.myhealthdiary.repository.HealthCenterRepository;
import lgp.myhealthdiary.myhealthdiary.repository.ProfessionalRepository;
import lgp.myhealthdiary.myhealthdiary.repository.ProfessionalSpecialityRepository;
import lgp.myhealthdiary.myhealthdiary.security.Database.AttributeEncryptor;

/**
 *This service is responsible for all logic related to the Professional profile 
 *
 */
@Service
public class ProfessionalProfileService {

	private static Logger logger = Logger.getLogger(ProfessionalProfileService.class.getName());

	@Autowired
	private MailService mailService;

	@Autowired
	private ProfessionalRepository professionalRepository;

	@Autowired
	private ProfessionalSpecialityRepository specialityRepository;

	@Autowired
	private HealthCenterRepository healthCenterRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	private AttributeEncryptor attributeEncryptor;

	// hardcoded specialities and respective endpoints
	private static final String[] specialityNames = { "Médico de medicina geral e familiar", "Enfermeiro de família",
			"Neurologista" };
	private static final String healthCentersEndpoint = "/healthcenters";
	private static final String hospitalsEndpoint = "/professionals/hospitals";

	/**
	 *all health centers in the system 
	 * @return a list of all available health centers
	 */
	public List<HealthCenter> getAllHealthCenters() {
		return healthCenterRepository.getAllHealthCenters();
	}

	/**
	 *all hospitals available in the system 
	 * @return the list of all hospitals
	 */
	public List<HealthCenter> getAllHospitals() {
		return healthCenterRepository.getHospitals();
	}

	/**
	 *all professional specialities 
	 * @return list of all ProfessionalSpecialities
	 */
	public List<ProfessionalSpeciality> getSpecialities() {
		List<ProfessionalSpeciality> specialities = specialityRepository.findAll();
		if (specialities.isEmpty()) {
			specialities.add(new ProfessionalSpeciality(specialityNames[0], healthCentersEndpoint, false));
			specialities.add(new ProfessionalSpeciality(specialityNames[1], healthCentersEndpoint, false));
			specialities.add(new ProfessionalSpeciality(specialityNames[2], hospitalsEndpoint, true));
			specialityRepository.saveAll(specialities);
		}
		return specialities;
	}

/**
 * returns a map with the speciality as the key and the endpoint termination to get the associated HealthCenters as the value	
 * @return the Map with all information for the web app about specialities: endpoints
 */
	public Map<String, String> specialitiesEndPoint() {
		List<ProfessionalSpeciality> specialities = getSpecialities();
		Map<String, String> toEndpoint = new LinkedHashMap<String, String>();
		specialities.forEach(
				speciality -> toEndpoint.put(speciality.getSpecialityName(), speciality.getWorkplacesEndpoint()));
		return toEndpoint;
	}

	/**
	 *returns the profile of the Professional 
	 * @param id the id of the Professional
	 * @return the profile of the Professional
	 */
	public ProfessionalDto getProfessionalProfileById(Long id) {
		return professionalRepository.findProfileById(id)
				.orElseThrow(() -> new RuntimeException("Profissional não existe: " + id));
	}

	/**
	 *creates a new Professional in the system 
	 * @param professional the information about the Professional
	 * @return the created Professional
	 */
	public Professional createProfessional(Professional professional) {

		if (UsernameExists(professional.getUsername())) {
			throw new RuntimeException("Username já existe.");
		}
		if (!EmailValidation.isValidProfessionalEmail(professional.getEmail())) {
			throw new RuntimeException("Digite email válido aceite pelo sistema.");
		}
		if (EmailExists(professional.getEmail())) {
			throw new RuntimeException("Email já existe.");
		}
		if (professional.getPassword() == null || professional.getPassword().isEmpty()
				|| professional.getPassword().length() < PasswordUtils.getMinimumPasswordLength()) {
			throw new RuntimeException("Digite uma Palavra-passe que tenha no mínimo "
					+ PasswordUtils.getMinimumPasswordLength() + " caracteres.");
		}

		ProfessionalSpeciality speciality = specialityRepository
				.findById(professional.getSpeciality().getSpecialityName())
				.orElseThrow(() -> new RuntimeException("A especialidade não é suportada pelo sistema"));
		professional.setSpeciality(speciality);

		final String healthCenterCode = professional.getHealthCenter().getCode();
		HealthCenter healthCenter = healthCenterRepository.findById(healthCenterCode)
				.orElseThrow(() -> new RuntimeException("Centro Saúde não existe: " + healthCenterCode));
		if (professional.getSpeciality().isHospitalAssociated() == healthCenter.isHospital()) {
			professional.setHealthCenter(healthCenter);
		} else {
			final String possibility = professional.getSpeciality().isHospitalAssociated() ? "só" : "não";
			final String specialityError = "A especialidade do profissional " + possibility
					+ " pode ser associada a um hospital.";
			throw new RuntimeException(specialityError);
		}
		professional.setPassword(bcryptEncoder.encode(professional.getPassword()));

		return professionalRepository.save(professional);
	}

	/**
	 *updates the information about a previously created Professional 
	 * @param professionalId the id of the Professional
	 * @param professional the updated Professional info
	 */
	public void updateProfessional(Long professionalId, Professional professional) {
		try {
			Optional<Professional> professionalToUpdate = professionalRepository.findById(professionalId);
			HealthCenter healthCenter = healthCenterRepository.findById(professional.getHealthCenter().getCode())
					.orElseThrow(() -> new RuntimeException(
							"Centro Saúde não existe: " + professional.getHealthCenter().getCode()));
			professionalToUpdate.get().setClinicalName(professional.getClinicalName());
			if (professionalToUpdate.get().getSpeciality().isHospitalAssociated() == healthCenter.isHospital()) {
				professionalToUpdate.get().setHealthCenter(healthCenter);
			} else {
				throw new RuntimeException(
						"A especialidade do profissional não pode ser associada ao centro de saúde seleccionado.");
			}
			professionalToUpdate.get().setImage(professional.getImage());

			save(professionalToUpdate.get());
		} catch (RuntimeException e) {
			logger.error(
					bcryptEncoder.encode("ProfessionalService Exception in updateProfessional()" + e.toString() + "."));
			throw (e);
		}
	}

	/**
	 *changes the Professional password 
	 * @param professionalId the id of the Professional 
	 * @param params the old and new passwords
	 */
	public void changePassword(Long professionalId, Map<String, String> params) {
		String oldPassword = params.get("oldPassword");
		String newPassword = params.get("newPassword");
		String repeatedNewPassword = params.get("repeatedNewPassword");

		Professional professionalToUpdate = this.getProfessionalById(professionalId);

		if (!bcryptEncoder.matches(oldPassword, professionalToUpdate.getPassword())) {
			throw new RuntimeException("A palavra-passe antiga está incorreta!");
		}

		if (newPassword == null || newPassword.length() < PasswordUtils.getMinimumPasswordLength()) {
			throw new RuntimeException("A palavra-passe tem de ter no mínimo "
					+ PasswordUtils.getMinimumPasswordLength() + " caracteres!");
		}

		if (!repeatedNewPassword.equals(newPassword)) {
			throw new RuntimeException("A nova palavra-passe e a repetida não correspondem!");
		}

		professionalToUpdate.setPassword(bcryptEncoder.encode(newPassword));
		this.save(professionalToUpdate);

	}

	/**
	 *deletes the Professional 
	 * @param professionalId the id of the Professional
	 */
	public void deleteProfessional(Long professionalId) {
		professionalRepository.delete(professionalRepository.findById(professionalId)
				.orElseThrow(() -> new RuntimeException("Profissional não existe: " + professionalId)));
	}

	/**
	 *checks if a Professional email already exists 
	 * @param email the plaintext email, to be encrypted in the query
	 * @return true if the email already exists in the database, false otherwise
	 */
	public boolean EmailExists(String email) {
		final String encryptedEmail = attributeEncryptor.convertToDatabaseColumn(email);
		return professionalRepository.checkIfEmailExists(encryptedEmail) == 0 ? false : true;
	}

	/**
	 *checks if a Professional username already exists 
	 * @param username the plaintext username, to be encrypted in the query
	 * @return true if the username already exists in the database, false otherwise
	 */
	public boolean UsernameExists(String username) {
		final String encryptedUsername = attributeEncryptor.convertToDatabaseColumn(username);
		return professionalRepository.checkIfUsernameExists(encryptedUsername) == 0 ? false : true;
	}

	/**
	 * finds a Professional by the email address
	 * 
	 * @param email the email address
	 * @return the Professional, if exists
	 */
	public Professional getProfessionalByEmail(String email) {
		return professionalRepository.findByEmail(email)
				.orElseThrow(() -> new RuntimeException("Professional com email " + email + " não existe"));
	}

	/**
	 * resets the Professional password, generating a random one and sending it to
	 * the Professional email adress
	 * 
	 * @param email the Professional's email address
	 * @return a ResponseEntity with the success or failure of the process
	 */
	public ResponseEntity<String> resetProfessionalPassword(String email) {

		Professional professional = getProfessionalByEmail(email);

		String username = professional.getUsername();

		final String newPassword = PasswordUtils.generateRandomPassword();
		professional.setPassword(bcryptEncoder.encode(newPassword));

		professionalRepository.save(professional);

		try {
			mailService.sendCredentialsEmail("professionalPasswordReset", email, username, newPassword);
		} catch (Exception e) {
			logger.error(bcryptEncoder
					.encode("ProfessionalService Exception in resetProfessionalPassword()" + e.toString() + "."));
			return new ResponseEntity<String>("Ocorreu um problema ao enviar a nova palavra-passe!",
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>("A nova palavra-passe foi enviada!", HttpStatus.OK);
	}

	/**
	 * gets a Professional by the id
	 * 
	 * @param id the professional id
	 * @return the Professional, if exists
	 */
	private Professional getProfessionalById(Long id) {
		return professionalRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("Profissional não existe: " + id));
	}

	/**
	 * saves the Professional in the database
	 * 
	 * @param professional the Professional info
	 * @return the saved Professional
	 */
	private Professional save(Professional professional) {
		return professionalRepository.save(professional);
	}
}
