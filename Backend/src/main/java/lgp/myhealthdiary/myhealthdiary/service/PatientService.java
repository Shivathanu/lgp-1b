package lgp.myhealthdiary.myhealthdiary.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.Utils.PasswordUtils;
import lgp.myhealthdiary.myhealthdiary.dto.EntryDto;
import lgp.myhealthdiary.myhealthdiary.dto.MedicalAppointmentDto;
import lgp.myhealthdiary.myhealthdiary.dto.PatientDailyInfoDto;
import lgp.myhealthdiary.myhealthdiary.dto.PatientDto;
import lgp.myhealthdiary.myhealthdiary.dto.PatientInfoAfterLoginDto;
import lgp.myhealthdiary.myhealthdiary.model.Alert;
import lgp.myhealthdiary.myhealthdiary.model.Entry;
import lgp.myhealthdiary.myhealthdiary.model.MedicalAppointment;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.repository.EntryRepository;
import lgp.myhealthdiary.myhealthdiary.repository.MedicalAppointmentRepository;
import lgp.myhealthdiary.myhealthdiary.repository.PatientRepository;


/**
 * this service is responsible for all the generic logic only applicable to the
 * Patient. Other services related to specific Patient funcionalities and shared
 * Patient related funcionalities can also be found.
 */
@Service
public class PatientService {

	private static Logger logger = Logger.getLogger(PatientService.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	PatientServiceHelper patientServiceHelper;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	EntryRepository entryRepository;

	@Autowired
	MedicalAppointmentRepository appointmentRepository;

	@Autowired
	PatientAlertService alertService;

	@Autowired
	private MailService mailService;

	private static final int NUM_PAST_MONTH_FOR_DIARY = 1;

	public PatientService(PatientRepository patientRepository) {
		this.patientRepository = patientRepository;
	}

	/**
	 * gets the Patient profile information
	 * 
	 * @param code the Patient code
	 * @return the Patient profile
	 */
	public PatientDto getPatientProfile(String code) {
		return patientRepository.findProfileByCode(code)
				.orElseThrow(() -> new RuntimeException("Paciente não existe: " + code));
	}

	/**
	 * returns the diary information for a Patient in a given month (and the month
	 * before) and all future appointments
	 * 
	 * @param patientCode the Patient code
	 * @param year        the year of the desired date
	 * @param month       the month of the desired date
	 * @return a map with the date as key and the diary Entry and eventual
	 *         MedicalAppointments as its value
	 */
	public Map<String, PatientDailyInfoDto> getPatientInfoForMonth(String patientCode, int year, int month) {
		List<Entry> entries = findEntriesForTimePeriod(patientCode, year, month);
		return createDailyMap(patientCode, entries);
	}

	/**
	 * returns all the information the mobile app needs to show to the Patient after
	 * login
	 * 
	 * @param patientCode the Patient code
	 * @return an Object with the Patient profile, the diary and MedicalAppoinments
	 *         map from the current and previous months and the past and scheduled
	 *         Alerts
	 */
	public PatientInfoAfterLoginDto getPatientInfoAfterLogin(String patientCode) {
		PatientDto patientProfile = getPatientProfile(patientCode);
		LocalDate today = LocalDate.now();
		List<Entry> entries = findEntriesForTimePeriod(patientCode, today.getYear(),
				today.getMonthValue());
		Map<String, PatientDailyInfoDto> diary = createDailyMap(patientCode, entries);
		List<Alert> alerts = alertService.processAndGetPatientAlerts(patientCode, entries);
		List<Alert> futureAlerts = alertService.getPatientFutureAlerts(patientCode);
		return new PatientInfoAfterLoginDto(patientProfile, diary, alerts, futureAlerts);
	}

	/**
	 * updates the Patient profile information
	 * 
	 * @param patient the Patient information
	 * @return the updated Patient
	 */
	public PatientDto updatePatient(PatientDto patient) {
		try {
			Patient patientToUpdate = patientRepository.findByCode(patient.getCode())
					.orElseThrow(() -> new RuntimeException("Paciente não existe: " + patient.getCode()));
			LocalDate today = LocalDate.now();
			if (patient.getBirthdate() == null || patient.getBirthdate().isAfter(today)) {
				throw new RuntimeException("A data de nascimento é inválida.");
			}

			patientServiceHelper.validatePatientProphylactic(patient);
			patientServiceHelper.validateGender(patient.getGender());
			patientServiceHelper.validateInitialsLength(patient.getNameInitials());

			patientToUpdate = patient.updatePatient(patientToUpdate);

			patientToUpdate.setProphylacticTreatments(patientServiceHelper.saveProphylacticTreatments(patientToUpdate));

			patientToUpdate = save(patientToUpdate);
			
			return new PatientDto(patientToUpdate);
		} catch (RuntimeException e) {
			logger.error(bcryptEncoder.encode("PatientService Exception in updatePatient" + e.toString() + "."));
			throw (e);
		}
	}

	/**
	 * changes the Patient password
	 * 
	 * @param patientCode the Patient code
	 * @param params      the old and new passwords
	 */
	public void changePassword(String patientCode, Map<String, String> params) {
		String oldPassword = params.get("oldPassword");
		String newPassword = params.get("newPassword");
		String repeatedNewPassword = params.get("repeatedNewPassword");

		Patient patientToUpdate = this.getPatientByCode(patientCode);

		if (!bcryptEncoder.matches(oldPassword, patientToUpdate.getPassword())) {
			throw new RuntimeException("A palavra-passe antiga está incorreta!");
		}

		if (newPassword.length() < PasswordUtils.getMinimumPasswordLength()) {
			throw new RuntimeException("A palavra-passe tem de ter no mínimo "
					+ PasswordUtils.getMinimumPasswordLength() + " caracteres!");
		}

		if (!repeatedNewPassword.equals(newPassword)) {
			throw new RuntimeException("A nova palavra-passe e a repetida não correspondem!");
		}

		patientToUpdate.setPassword(bcryptEncoder.encode(newPassword));
		this.save(patientToUpdate);

	}

	/**
	 * deletes Patient from the database, with all associated Entries,
	 * MedicalAppointments, ProphylacticTreatments and removing him/her from all
	 * Professional lists in which is associated
	 * 
	 * @param patientCode the Patient code
	 */
	public void deletePatient(String patientCode) {
		Patient patientToDelete = getPatientByCode(patientCode);
		patientRepository.deleteProphylacticTreatments(patientToDelete.getId());
		patientRepository.deleteFromProfessionals(patientToDelete.getId());
		patientRepository.delete(patientToDelete);
	}

	/**
	 *activates the Patient account after accepting the GPDR policy 
	 * @param patientCode the code of the Patient
	 * @param params the new passwords
	 */
	public void activatePatientAccount(String patientCode, Map<String, String> params) {

		Patient patient = getPatientByCode(patientCode);

		String password = params.get("password");
		String repeatedPassword = params.get("repeatedPassword");

		if (password.length() < PasswordUtils.getMinimumPasswordLength()) {
			throw new RuntimeException("A palavra-passe tem de ter no mínimo "
					+ PasswordUtils.getMinimumPasswordLength() + " caracteres!");
		}

		if (!password.equals(repeatedPassword)) {
			throw new RuntimeException("A nova palavra-passe e a repetida não correspondem!");
		}

		patient.setPassword(bcryptEncoder.encode(password));
		patient.activateAccount();

		patientRepository.save(patient);

	}

	/**
	 *resets the forgotten Patient password, generating a new random one and sending it via email alongside the code 
	 * @param email the Patient's email address
	 */
	public void resetPatientPassword(String email) {

		Patient patient = getPatientByEmail(email);

		String code = patient.getCode();

		final String newPassword = PasswordUtils.generateRandomPassword();
		patient.setPassword(bcryptEncoder.encode(newPassword));

		patientRepository.save(patient);

		try {
			mailService.sendCredentialsEmail("patientPasswordReset", email, code, newPassword);
		} catch (Exception e) {
			logger.error(bcryptEncoder.encode("PatientService Exception in recoverPatientPassword()" + e.toString() + "."));
			throw new RuntimeException(e.toString());
		}

	}

	/**
	 *gets a Patient by his/her email 
	 * @param email the Patient's email
	 * @return the Patient that has the email address associated, if exists
	 */
	public Patient getPatientByEmail(String email) {
		return patientRepository.findByEmail(email)
				.orElseThrow(() -> new RuntimeException("Paciente com email " + email + " não existe"));
	}

	/**
	 * finds the Patient id from his/her code
	 * 
	 * @param code the Patient code
	 * @return the Patient id
	 */
	public long getPatientIdByCode(String code) {
		return patientRepository.findIdByCode(code)
				.orElseThrow(() -> new RuntimeException("Paciente não existe: " + code));
	}

	/**
	 * gets a Patient by his/her code
	 * 
	 * @param code the Patient code
	 * @return the Patient, if exists
	 */
	public Patient getPatientByCode(String code) {
		return patientServiceHelper.getPatientByCode(code);
	}


	/**
	 * gets a Patient by his/her id
	 * 
	 * @param id the Patient id
	 * @return the Patient, if exists
	 */
	private Patient getPatientById(Long id) {
		return patientRepository.findById(id).orElseThrow(() -> new RuntimeException("Paciente não existe: " + id));
	}

	/**
	 * saves a Patient in the database
	 * 
	 * @param patient the Patient to be saved
	 * @return the saved Patient
	 */
	public Patient save(Patient patient) {
		return patientServiceHelper.save(patient);
	}

	/**
	 * finds all entries for a Patient from the month before the desired month and
	 * until the start of the following month
	 * 
	 * @param patientCode the code of the Patient
	 * @param year        the year of the desired month
	 * @param month       the desired month
	 * @return the list of Entries in the desired time period
	 */
	private List<Entry> findEntriesForTimePeriod(String patientCode, int year, int month) {
		LocalDateTime actualMonth = LocalDate.of(year, month, 1).atStartOfDay();
		LocalDateTime queryStartDate = actualMonth.minusMonths(1).minusDays(1);
		LocalDateTime queryEndDate = actualMonth.plusMonths(1);
		List<Entry> entries = entryRepository.getBetweenDates(queryStartDate, queryEndDate, patientCode);
		return entries;
	}

	/**
	 * creates a Map that represents the diary of a Patient, with Entries and
	 * MedicalAppointments per day
	 * 
	 * @param patientCode the Patient code
	 * @param entries     the list of Entries
	 * @return a Map with the date as key and an oject composed of the Entry and
	 *         MedicalAppointment as value
	 */
	private Map<String, PatientDailyInfoDto> createDailyMap(String patientCode, List<Entry> entries) {
		List<MedicalAppointment> appointments = appointmentRepository.findByPatientCode(patientCode);
		Map<String, PatientDailyInfoDto> dailyMap = new LinkedHashMap<String, PatientDailyInfoDto>();
		for (Entry entry : entries) {
			String dateKey = entry.getDate().toString();
			dailyMap.put(dateKey, new PatientDailyInfoDto(new EntryDto(entry)));
		}
		LocalDateTime today = LocalDateTime.now();
		for (MedicalAppointment medicalAppointment : appointments) {
			MedicalAppointmentDto appointmentDto = new MedicalAppointmentDto(medicalAppointment);
			String dateKey = appointmentDto.getDateTime().toLocalDate().toString();
			// put if: in past and day has entry ;or in future
			if (medicalAppointment.getDateTime().isBefore(today) && dailyMap.containsKey(dateKey)) {
				dailyMap.get(dateKey).setMedicalAppointment(appointmentDto);
			} else if (medicalAppointment.getDateTime().isAfter(today)) {
				dailyMap.put(dateKey, new PatientDailyInfoDto(appointmentDto));
			}
		}
		return dailyMap;
	}
}
