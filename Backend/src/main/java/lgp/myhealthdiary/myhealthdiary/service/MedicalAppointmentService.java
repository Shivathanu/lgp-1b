package lgp.myhealthdiary.myhealthdiary.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.dto.MedicalAppointmentAlertDto;
import lgp.myhealthdiary.myhealthdiary.dto.MedicalAppointmentDto;
import lgp.myhealthdiary.myhealthdiary.model.Alert;
import lgp.myhealthdiary.myhealthdiary.model.MedicalAppointment;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.repository.MedicalAppointmentRepository;

/**
 *This service is responsible for all the MedicalAppointment operations of a Patient 
 *
 */
@Service
public class MedicalAppointmentService {

	private static Logger logger = Logger.getLogger(MedicalAppointmentService.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	MedicalAppointmentRepository medicalAppointmentRepository;

	@Autowired
	PatientServiceHelper patientServiceHelper;

	@Autowired
	PatientAlertService alertService;

	private static final int MINIMUM_APPOINTMENT_DURATION = 59; // minutes

	public MedicalAppointmentService(MedicalAppointmentRepository medicalAppointmentRepository) {
		this.medicalAppointmentRepository = medicalAppointmentRepository;
	}

/**
 *Generates a Map of MedicalAppointments by date of a Patient
 * @param patientCode the code of the Patient
 * @return the Map with the days as key and MedicalAppointment as value
 */
	public Map<String, MedicalAppointmentDto> getPatientMedicalAppointments(String patientCode) {
		Patient patient = patientServiceHelper.getPatientByCode(patientCode);
		Map<String, MedicalAppointmentDto> medicalAppointmentsDateTime = new HashMap<>();
		for (MedicalAppointment medicalAppointment : patient.getMedicalAppointments()) {
			MedicalAppointmentDto medicalAppointmentDto = new MedicalAppointmentDto(medicalAppointment);
			String formatted = medicalAppointment.getDateTime().toLocalDate().toString();
			medicalAppointmentsDateTime.put(formatted, medicalAppointmentDto);
		}
		return medicalAppointmentsDateTime;
	}

	/**
	 * creates a new MedicalAppointment for the Patient
	 * 
	 * @param medicalAppointment the MedicalAppointment to be created
	 * @param patientCode        the code of the Patient that wants to create it
	 * @return a ResponseEntity with the id of the created MedicalAppointment and
	 *         the new created Alert associated to it, to be sent to the mobile app
	 */
	public ResponseEntity<?> createMedicalAppointment(MedicalAppointment medicalAppointment, String patientCode) {
		Patient patient = patientServiceHelper.getPatientByCode(patientCode);
		if (LocalDateTime.now().isAfter(medicalAppointment.getDateTime())) {
			return new ResponseEntity("Só é possivel agendar consultas no futuro.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (checkIfScheduleIsClear(medicalAppointment.getDateTime(), patient.getId(), 0)) {
			medicalAppointment.setPatient(patient);

			medicalAppointment = medicalAppointmentRepository.save(medicalAppointment);

			Alert alert = alertService.createAppointmentAlert(medicalAppointment);

			return new ResponseEntity(new MedicalAppointmentAlertDto(medicalAppointment.getId(), alert), HttpStatus.OK);
		}
		logger.error(bcryptEncoder.encode("MedicalAppointmentService - A hora não é válida. Verifique se não tem consultas na hora indicada."));
		return new ResponseEntity("A hora não é válida. Verifique se não tem consultas na hora indicada.",
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * updates a previously created MedicalAppointment
	 * 
	 * @param appointmentId      the id of the MedicalAppointment to update
	 * @param medicalAppointment the new information to update the
	 *                           MedicalAppointment
	 * @param patientCode        the code of the Patient who created the Medical
	 *                           Appointment
	 * @return a ResponseEntity with the new generated Allert for the updated
	 *         MedicalAppointment, to be updated on the mobile app
	 */
	public ResponseEntity<?> updateMedicalAppointment(long appointmentId, MedicalAppointment medicalAppointment,
			String patientCode) {
		MedicalAppointment appointmentToUpdate = getMedicalAppointmentById(appointmentId);

		if (!patientCode.equals(appointmentToUpdate.getPatient().getCode())) {
			logger.error(bcryptEncoder.encode("MedicalAppointmentService - A consulta não pertence ao paciente com o código"));
			return new ResponseEntity("A consulta não pertence ao paciente com o código " + patientCode,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (LocalDateTime.now().isAfter(medicalAppointment.getDateTime())) {
			logger.error(bcryptEncoder.encode("MedicalAppointmentService - Só é possivel agendar consultas no futuro."));
			return new ResponseEntity("Só é possivel agendar consultas no futuro.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (checkIfScheduleIsClear(medicalAppointment.getDateTime(), appointmentToUpdate.getPatient().getId(),
				appointmentToUpdate.getId())) {
			appointmentToUpdate.setDateTime(medicalAppointment.getDateTime());
			appointmentToUpdate.setDoctor(medicalAppointment.getDoctor());
			appointmentToUpdate.setObservations(medicalAppointment.getObservations());
			appointmentToUpdate.setPlace(medicalAppointment.getPlace());
			appointmentToUpdate.setType(medicalAppointment.getType());
			medicalAppointmentRepository.save(appointmentToUpdate);

			Alert alert = alertService.updateAppointmentAlert(appointmentToUpdate);

			return new ResponseEntity(alert, HttpStatus.OK);
		}
		logger.error(bcryptEncoder.encode("MedicalAppointmentService - A hora não é válida. Verifique se não tem consultas na hora indicada."));
		return new ResponseEntity("A hora não é válida. Verifique se não tem consultas na hora indicada.",
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Deletes a MedicalAppointment
	 * 
	 * @param patientCode the code of the Patient
	 * @param id          the id of the MedicalAppointment
	 * @return a ResponseEntity with the id of the deleted Alert associated with the
	 *         deleted MedicalAppointment, to be deleted on the mobile app
	 */
	public ResponseEntity deleteMedicalAppointment(String patientCode, Long id) {
		ResponseEntity response = null;
		MedicalAppointment appointment = getMedicalAppointmentById(id);
		if (appointment.getPatient().getCode().equals(patientCode)) {
			long deletedAlertId = alertService.deleteAppointmentAlert(appointment);
			medicalAppointmentRepository.delete(appointment);

			response = new ResponseEntity("Consulta apagada com sucesso. Apagado alerta com id=" + deletedAlertId,
					HttpStatus.OK);
		} else {
			logger.error(bcryptEncoder.encode("MedicalAppointmentService - A consulta não é do paciente"));
			response = new ResponseEntity("A consulta não é do paciente " + patientCode,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	private MedicalAppointment getMedicalAppointmentById(Long id) {
		return medicalAppointmentRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("Consulta não existe: " + id));
	}

	/**
	 * checks if a time slot is free in order to schedule or update a
	 * MedicalAppointment
	 * 
	 * @param appointmentDateTime the date and time to be checked
	 * @param patientId           the id of the Patient
	 * @param appointmentId       the id of the MedicalAppointment to be
	 *                            (re)scheduled
	 * @return true if the MedicalAppointment can be scheduled, false otherwise
	 */
	private boolean checkIfScheduleIsClear(LocalDateTime appointmentDateTime, long patientId, long appointmentId) {
		LocalDateTime hourBefore = appointmentDateTime.minusMinutes(MINIMUM_APPOINTMENT_DURATION);
		LocalDateTime hourAfter = appointmentDateTime.plusMinutes(MINIMUM_APPOINTMENT_DURATION);
		return medicalAppointmentRepository.hasAppointmentOnThatHour(patientId, hourBefore, hourAfter,
						appointmentId) > 0 ? false
						: true;
	}
}
