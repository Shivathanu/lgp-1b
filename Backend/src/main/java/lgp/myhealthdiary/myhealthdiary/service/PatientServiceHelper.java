package lgp.myhealthdiary.myhealthdiary.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.dto.PatientDto;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.model.ProphylacticTreatment;
import lgp.myhealthdiary.myhealthdiary.repository.PatientRepository;
import lgp.myhealthdiary.myhealthdiary.repository.ProphylacticTreatmentRepository;
import lgp.myhealthdiary.myhealthdiary.security.Database.AttributeEncryptor;

/**
 * this service comprises all Patient related methods needed in other services
 *
 */
@Service
public class PatientServiceHelper {

	private static final int CODE_NUMERIC_SIZE = 6;
	private static final int INITIALS_MAZ_LENGTH = 10;

	private static final String MALE_GENDER = "Masculino";
	private static final String FEMALE_GENDER = "Feminino";

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	ProphylacticTreatmentRepository prophylacticRepository;

	@Autowired
	private AttributeEncryptor attributeEncryptor;

	public PatientServiceHelper(PatientRepository patientRepository) {
		this.patientRepository = patientRepository;
	}

	/**
	 * gets a Patient by his/her code
	 * 
	 * @param code the Patient's code
	 * @return the Patient, if exists
	 */
	public Patient getPatientByCode(String code) {
		return patientRepository.findByCode(code).orElseThrow(() -> new RuntimeException("Paciente não existe: " + code));
	}

	/**
	 * saves the Patient in the database
	 * 
	 * @param patient the Patient info to be saved
	 * @return the updated Patient
	 */
	public Patient save(Patient patient) {
		return patientRepository.save(patient);
	}

	/**
	 * returns the List of Patient's followed by the Professional
	 * 
	 * @param professionalId the id of the Professional
	 * @return the list of Patients associated to that Professional
	 */
	public List<PatientDto> findPatientsOfProfessional(long professionalId) {
		return patientRepository.findProfilesByProfessionalsId(professionalId);
	}

	/**
	 * verifies if a numero utente already exists in the database
	 * 
	 * @param numeroUtente the numero utente in plaintext, to be encrypted before
	 *                     the query
	 * @return true if numero utente already exists, false if not
	 */
	public boolean numeroUtentExists(String numeroUtente) {
		final String encryptedNumeroUtente = attributeEncryptor.convertToDatabaseColumn(numeroUtente);
		return patientRepository.checkIfNumeroUtenteExists(encryptedNumeroUtente) == 0 ? false : true;
	}

	/**
	 * verifies if an email already exists in the database
	 * 
	 * @param email the emailin plaintext, to be encrypted before the query
	 * @return true if email already exists, false if not
	 */
	public boolean EmailExists(String email) {
		final String encryptedEmail = attributeEncryptor.convertToDatabaseColumn(email);
		return patientRepository.checkIfEmailExists(encryptedEmail) == 0 ? false : true;
	}

	/**
	 * verifies if a numProcessoHospitalar already exists in the database
	 * 
	 * @param numProcessoHospitalar the numero processo hospitalar in plaintext, to
	 *                              be encrypted before the query
	 * @return true if numero processo hospitalar already exists, false if not
	 */
	public boolean numProcessoHospitalarExists(String numProcessoHospitalar) {
		final String encryptedNumProcHospitalar = attributeEncryptor.convertToDatabaseColumn(numProcessoHospitalar);
		return patientRepository.checkIfNumProcessoHospitalarExists(encryptedNumProcHospitalar) == 0 ? false : true;
	}

	/**
	 *generates a new Patient code from the HealthCenter code, by finding the last code registered with that HealthCenter code and incrementing it. 
	 * @param healthCenterCode the HealthCenter code that will be incorporated in the Patient code
	 * @return the newly generated Patient code, that will identify the Patient in the system
	 */
	public String generatePatientCode(String healthCenterCode) {
		int newCode = 0;
		// finds last registration in the given health center
		String lastHealthCenterRegister = patientRepository.lastRegistrationInHealthCenter(healthCenterCode);
		if (lastHealthCenterRegister != null && !lastHealthCenterRegister.isEmpty()) {
			newCode = Integer.parseInt(lastHealthCenterRegister);
		}
		// increases the last found register in 1 unit - to 1 if inexistent
		newCode++;
		String codeGenerated = "" + newCode;
		// pads code with 0s to reach the desired length 
		for (int i = CODE_NUMERIC_SIZE - codeGenerated.length(); i > 0; --i) {
			codeGenerated = "0" + codeGenerated;
		}
		// concatenates the HealthCenter code with the generated numeric part 
		return healthCenterCode + codeGenerated;
	}

	/**
	 * saves the multiple ProphylacticTreatments in the Patient profile
	 * 
	 * @param patient the Patient with the new ProphylacticTreatments already
	 *                associated
	 * @return the list of all the saved Prophylactic treatments for that Patient
	 */
	public List<ProphylacticTreatment> saveProphylacticTreatments(Patient patient) {
		List<ProphylacticTreatment> treatments = new ArrayList<ProphylacticTreatment>();
		List<Long> idsToKeep = new ArrayList<Long>();
		// get the ids of this patient's treatments to only allow edit of those
		List<Long> patientTreatmentIds = prophylacticRepository.patientProphylacticTreatments(patient.getId());

		for (ProphylacticTreatment treatment : patient.getProphylacticTreatments()) {
			// verifies if treatment already exists and can be edited by that Patient
			if (treatment.getId() != null && treatment.getId() > 0
					&& !patientTreatmentIds.contains(treatment.getId())) {
				throw new RuntimeException("O tratamento profilático com id " + treatment.getId()
						+ " não pertence ao paciente " + patient.getCode());
			}
			// saves the treatment
			treatment.setPatient(patient);
			ProphylacticTreatment savedTreatment = prophylacticRepository.save(treatment);
			treatments.add(savedTreatment);
			idsToKeep.add(savedTreatment.getId());
		}
		// to delete treatments left behind
			List<Long> idsToDelete = new ArrayList<Long>();
			for (Long id : patientTreatmentIds) {
			// if id not present in actual list, it shall be deleted
				if (!idsToKeep.contains(id)) {
					idsToDelete.add(id);
				}
			}
			idsToDelete.forEach(id -> prophylacticRepository.deleteById(id));
		return treatments;
	}

	/**
	 * validates the ProphylacticTreatments info
	 * 
	 * @param patient the Patient with the associated ProphylacticTreatments
	 */
	public void validatePatientProphylactic(PatientDto patient) {
		LocalDate today = LocalDate.now();
		if (patient.getProphylacticTreatments() != null) {
			for (ProphylacticTreatment prophylactic : patient.getProphylacticTreatments()) {
				if (prophylactic.getStartDate().isAfter(today)) {
					throw new RuntimeException(
							"Introduza uma data anterior a hoje para o inicio do tratamento profilático "
									+ prophylactic.getTreatmentDetails());
				}
			}
		}
	}

	/**
	 * validates the gender of the Patient
	 * 
	 * @param gender the Patient's gender
	 * @return true if the gender is valid for the system, false otherwise
	 */
	public boolean validateGender(String gender) {
		boolean success = false;
		if (gender != null && (MALE_GENDER.equals(gender) || FEMALE_GENDER.equals(gender))) {
			success = true;
		} else {
			throw new RuntimeException("O género não é válido.");
		}
		return success;
	}

	/**
	 * validates if the Patient inicials meet the admited maximum length
	 * 
	 * @param initials the name initials
	 * @return true if the inicials are valid, false otherwise
	 */
	public boolean validateInitialsLength(String initials) {
		boolean valid = true;
		if (initials == null || initials.length() > INITIALS_MAZ_LENGTH) {
			valid = false;
			throw new RuntimeException("As iniciais do nome não podem ter mais de " + INITIALS_MAZ_LENGTH + " caracteres.");
		}
		return valid;
	}
}
