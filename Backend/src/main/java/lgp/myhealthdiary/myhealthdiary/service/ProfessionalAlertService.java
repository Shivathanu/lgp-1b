package lgp.myhealthdiary.myhealthdiary.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.dto.PatientViewToProfessionalDto;
import lgp.myhealthdiary.myhealthdiary.dto.report.MonthlyReport;
import lgp.myhealthdiary.myhealthdiary.dto.report.ReportUtils;
import lgp.myhealthdiary.myhealthdiary.dto.report.SummaryReport;
import lgp.myhealthdiary.myhealthdiary.model.Alert;
import lgp.myhealthdiary.myhealthdiary.model.Entry;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.repository.AlertRepository;

/**
 * This class is responsible for processing the Professional Alerts
 *
 */
@Service
public class ProfessionalAlertService {
	@Autowired
	AlertRepository alertRepository;

	@Autowired
	EntryService entryService;

	// important thresholds
	private static final int ALERTS_RANGE = 3; // months
	private static final int RANGE_TO_PROCESS_ENTRIES = 1; // months
	private static final int SIZE_TO_COMPARE = 2; // has to have 2 months to allow comparison
	private static final double PERCENTAGE_EVOLUTION_THRESHOLD = 0.2;
	private static final int NOON_HOURS = 12;

	// messages:
	private static final String URGENCY_SUBJECT = "Urgências";
	private static final String URGENCY_MESSAGE = "Recorreu %s vezes ao serviço de urgências em %s, mais do que no mês anterior (%s).";
	private static final String INCAPACITY_SUBJECT = "Incapacidade";
	private static final String INCAPACITY_MESSAGE = "Faltou %s vezes ao trabalho em %s, mais do que no mês anterior (%s).";
	private static final String PERCENTAGE_SUBJECT = "Aumento de cefaleias mensais";
	private static final String PERCENTAGE_MESSAGE = "Aumento de %s%% no número de dias com cefaleia em %s, tendo registado %s dia%s com cefaleia.";

	public ProfessionalAlertService(AlertRepository alertRepository) {
		this.alertRepository = alertRepository;
	}

	/**
	 * the list of all alerts of a Professional
	 * 
	 * @param professionalId the id of the Professional
	 * @return the List of the Professional Alerts
	 */
	public List<Alert> getProfessionalAlerts(long professionalId) {
		generateNewProfessionalsAlerts(professionalId);
		return alertRepository.getAlertsOFFollowedPatients(professionalId, alertsStartDate());
	}

	/**
	 * generates/updates Alerts related to the Patients followed by the Professional
	 * 
	 * @param professionalId the professional id
	 */
	private void generateNewProfessionalsAlerts(long professionalId) {
		Map<String, SummaryReport> reports = generatePatientsReports(professionalId);
		for (Map.Entry<String, SummaryReport> mapEntry : reports.entrySet()) {
			generatePatientRelatedAlerts(mapEntry.getKey(), professionalId, mapEntry.getValue().getMonths());
		}
	}

	/**
	 * generates/updates Professional realted Alerts for a given Patient
	 * 
	 * @param patientKey     the Patient info to the Alert subject with \n + id
	 *                       appended
	 * @param professionalId the id of the Professional
	 * @param months         the list of MonthlyReport from whose info the Alerts
	 *                       will be generated
	 */
	private void generatePatientRelatedAlerts(String patientKey, long professionalId, List<MonthlyReport> months) {
		if (months.size() == SIZE_TO_COMPARE) {
			final String lastMonth = months.get(1).getMonth();
			final int lastMonthIncapacity = months.get(1).getWorkAbsentDays();
			if (lastMonthIncapacity > months.get(0).getWorkAbsentDays()) {
				final String message = String.format(INCAPACITY_MESSAGE, lastMonthIncapacity,
						lastMonth, months.get(0).getWorkAbsentDays());
				createOrUpdateMonthlyAlert(INCAPACITY_SUBJECT, message, lastMonth, patientKey,
						(long) lastMonthIncapacity);
			}
			int lastMonthUrgencies = months.get(1).getUrgencyServiceVisits();
			if (lastMonthUrgencies > months.get(0).getUrgencyServiceVisits()) {
				final String message = String.format(URGENCY_MESSAGE, lastMonthUrgencies,
						lastMonth, months.get(0).getUrgencyServiceVisits());
				createOrUpdateMonthlyAlert(URGENCY_SUBJECT, message, lastMonth, patientKey, (long) lastMonthUrgencies);
			}
			if (months.get(1).getPercentEvolutionHeadacheDay() > PERCENTAGE_EVOLUTION_THRESHOLD) {
				final long evolutionFormatted = (long) (months.get(1).getPercentEvolutionHeadacheDay() * 100);
				final int headacheDays = months.get(1).getHeadacheDays();
				final String plural = headacheDays > 1 ? "s" : "";
				final String message = String.format(PERCENTAGE_MESSAGE,
						evolutionFormatted, lastMonth, headacheDays, plural);
				createOrUpdateMonthlyAlert(PERCENTAGE_SUBJECT, message, lastMonth, patientKey, evolutionFormatted);
			}
		}
	}

	/**
	 * method to create or update a Professional related Patient Alert
	 * 
	 * @param subject     the subject of the Alert
	 * @param message     the message of the Alert
	 * @param month       the month to which the Alert is related
	 * @param patientInfo the Patient info to the Alert subject with \n + id
	 *                    appended
	 * @param number      the number of days or percent of evolution to
	 *                    create/update the Alert with
	 */
	private void createOrUpdateMonthlyAlert(String subject, String message, String month, String patientInfo,
			long number) {
		final long patientId = extractPatientId(patientInfo);
		Patient patient = new Patient();
		patient.setId(patientId);
		LocalDateTime startOfMonth = ReportUtils.generateFromMonthKey(month).atStartOfDay();
		LocalDateTime endOfMonth = startOfMonth.plusMonths(1).minusNanos(1);
		Alert alert = alertRepository
				.getProPatientMonthAlert(patientId, inSearchMode(subject), startOfMonth, endOfMonth)
				.orElse(new Alert(extractPatientDisplayInfo(patientInfo) + subject, message, patient,
						startOfMonth,
						true));
		alert.setMessage(message);
		alert.setHelperField(number);
		LocalDateTime today = LocalDate.now().atStartOfDay().plusHours(NOON_HOURS);
		if (today.isBefore(endOfMonth)) {
			alert.setDateTime(today);
		}
		alertRepository.save(alert);
		alertRepository.markPastPatientProAlertsAsRead(patientId, startOfMonth);
	}

	/**
	 * generates a List of Patient's SummaryReports from all Patients associated
	 * with a given Professional
	 * 
	 * @param professionalId the id of the Professional
	 * @return a List of SummaryReport of all followed Patients
	 */
	private Map<String, SummaryReport> generatePatientsReports(long professionalId) {
		List<Entry> entriesToProcess = entryService.getFollowedPatientsEntries(professionalId, entriesStartDate());
		Map<String, List<Entry>> entriesByPatient = new LinkedHashMap<String, List<Entry>>();
		for (Entry e : entriesToProcess) {
			final String patientKey = generatePatientKey(e.getPatient());
			if (!entriesByPatient.containsKey(patientKey)) {
				entriesByPatient.put(patientKey, new ArrayList<Entry>());
			}
			entriesByPatient.get(patientKey).add(e);
		}
		Map<String, SummaryReport> reports = new LinkedHashMap<String, SummaryReport>();
		for (Map.Entry<String, List<Entry>> mapEntry : entriesByPatient.entrySet()) {
			PatientViewToProfessionalDto temp = new PatientViewToProfessionalDto();
			reports.put(mapEntry.getKey(), temp.processEntries(mapEntry.getValue()));
		}
		return reports;
	}

	/**
	 * creates the String to associate with each Patient Alert 's subject
	 * 
	 * @param patient the Patient
	 * @return a String with the initials, code and the id appended for search
	 *         reasons
	 */
	private String generatePatientKey(Patient patient) {
		return patient.getNameInitials() + " (" + patient.getCode() + "): \n" + patient.getId();
	}

	/**
	 * extracts the info to display in the Alert from the agregated Patient info
	 * 
	 * @param patientKey the concatenated String
	 * @return the info to insert in the Alert subject
	 */
	private String extractPatientDisplayInfo(String patientKey) {
		String[] split = patientKey.split("\n");
		return split[0];
	}

	/**
	 * extracts the Patient id from the agregated Patient info
	 * 
	 * @param patientKey the concatenated String
	 * @return the Patient id
	 */
	private long extractPatientId(String patientKey) {
		String[] split = patientKey.split("\n");
		return Long.parseLong(split[1]);
	}

	/**
	 * adds % in the begining and end of a String to search it on the database
	 * 
	 * @param string the original String
	 * @return the %String%
	 */
	private String inSearchMode(String string) {
		return "%" + string + "%";
	}

	/**
	 * the start day from which to find Alerts
	 * 
	 * @return the start date from which to find Alerts
	 */
	private LocalDateTime alertsStartDate() {
		return LocalDate.now().minusMonths(ALERTS_RANGE).atStartOfDay();
	}

	/**
	 * returns a date from which to process Patient Entries to generate the Alerts
	 * 
	 * @return a date in the begining of the month from which to process the Entries
	 *         from
	 */
	private LocalDateTime entriesStartDate() {
		LocalDate aMonthAgo = LocalDate.now().minusMonths(RANGE_TO_PROCESS_ENTRIES);
		return aMonthAgo.minusDays(aMonthAgo.getDayOfMonth()).atStartOfDay();
	}

}