package lgp.myhealthdiary.myhealthdiary.service;

import java.time.LocalDate;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lgp.myhealthdiary.myhealthdiary.Utils.EmailValidation;
import lgp.myhealthdiary.myhealthdiary.Utils.PasswordUtils;
import lgp.myhealthdiary.myhealthdiary.dto.PatientDto;
import lgp.myhealthdiary.myhealthdiary.dto.PatientViewToProfessionalDto;
import lgp.myhealthdiary.myhealthdiary.dto.ProfessionalDto;
import lgp.myhealthdiary.myhealthdiary.dto.ProfessionalInfoAfterLoginDto;
import lgp.myhealthdiary.myhealthdiary.model.Alert;
import lgp.myhealthdiary.myhealthdiary.model.HealthCenter;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.model.Professional;
import lgp.myhealthdiary.myhealthdiary.repository.HealthCenterRepository;
import lgp.myhealthdiary.myhealthdiary.repository.ProfessionalRepository;

/**
 * this Service handles the logic of the Patient list of the Professionals
 *
 */
@Service
public class ProfessionalService {

	private static Logger logger = Logger.getLogger(ProfessionalService.class.getName());

	@Autowired
	private PatientServiceHelper patientService;
	
	@Autowired
	private ProfessionalProfileService professionalProfileService;

	@Autowired
	private ProfessionalAlertService alertService;

	@Autowired
	private MailService mailService;

	@Autowired
    private ProfessionalRepository professionalRepository;

	@Autowired
	private HealthCenterRepository healthCenterRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	public ProfessionalService(PatientServiceHelper patientService) {
		this.patientService = patientService;
	}

	/**
	 * returns all the information a Professional needs in the web page after login
	 * 
	 * @param professionalId the Professional id
	 * @return an object with the Professional profile, list of patients and Alerts
	 */
	public ProfessionalInfoAfterLoginDto professionalLoginInfo(long professionalId) {
		ProfessionalDto profile = professionalProfileService.getProfessionalProfileById(professionalId);
		List<PatientDto> patientsList = getPatientsFromProfessional(professionalId);
		List<Alert> alerts = alertService.getProfessionalAlerts(professionalId);
		return new ProfessionalInfoAfterLoginDto(profile, patientsList, alerts);
	}

	/**
	 * creates a new Patient into the system, generating his/her code automatically,
	 * creating a random password and sending it via email to the Patient
	 * 
	 * @param professionalId the id of the Professional who wants to create the
	 *                       Patient, that will be associated to his/her list
	 * @param patient        the information of the new Patient to be created
	 * @return the generated code of the Patient, that will also be emailed to the
	 *         new Patient
	 */
	public String createPatient(Long professionalId, PatientDto patient) {
		Professional professional = professionalRepository.findById(professionalId)
			.orElseThrow(() -> new RuntimeException("Profissional não existe: " + professionalId));
		if (patientService.numeroUtentExists(patient.getNumeroUtente())) {
			logger.error(bcryptEncoder.encode("ProfessionalService - O número de utente já existe"));
			throw new RuntimeException("O número de utente já existe");
		}
		if (patientService.numProcessoHospitalarExists(patient.getNumProcessoHospitalar())) {
			logger.error(bcryptEncoder.encode("ProfessionalService - O número de processo hospitalar já existe"));
			throw new RuntimeException("O número de processo hospitalar já existe");
		}
		if (patientService.EmailExists(patient.getEmail())) {
			logger.error(bcryptEncoder.encode("ProfessionalService - Email já existe."));
			throw new RuntimeException("Email já existe.");
		}
		if (!EmailValidation.isValidEmail(patient.getEmail())) {
			logger.error(bcryptEncoder.encode("ProfessionalService - Digite email correto."));
			throw new RuntimeException("Digite email correto.");
		}
		patientService.validateInitialsLength(patient.getNameInitials());
		validatePatientGender(patient.getGender());
		validatePatientDates(patient);

		Patient patientToCreate = patient.build();

		final String healthCenterCode = patient.getHealthCenter().getCode();
		HealthCenter healthCenter = healthCenterRepository.findById(healthCenterCode)
			.orElseThrow(() -> new RuntimeException("Centro Saúde não existe: " + healthCenterCode));
		if (healthCenter.isHospital()) {
			logger.error(bcryptEncoder.encode("ProfessionalService - Não é possivel associar o paciente a um hospital."));
			throw new RuntimeException("Não é possivel associar o paciente a um hospital.");
		}

		// generate new code based on the HealthCenter
		final String code = patientService.generatePatientCode(healthCenterCode);
		patientToCreate.setCode(code);

		patientToCreate.setHealthCenter(healthCenter);

		// create random password
		final String newPassword = PasswordUtils.generateRandomPassword();
		patientToCreate.setPassword(bcryptEncoder.encode(newPassword));

		patientToCreate = patientService.save(patientToCreate);

		patientService.saveProphylacticTreatments(patientToCreate);
				
		// associate newly created Patient to Professional list
		professional.getAssigned_patients().add(patientToCreate);
		professionalRepository.save(professional);
		try {
			// send credentials via email
			mailService.sendCredentialsEmail("registration",patientToCreate.getEmail(),
				patientToCreate.getCode(), newPassword);
		} catch (Exception e) {
			logger.error(bcryptEncoder.encode("ProfessionalService Exception in createPatient()" + e.toString() + "."));
			e.printStackTrace();
			throw new RuntimeException("Problemas no envio do email");
		}
		return code;
	}

	/**
	 * get the list of Patients associated with the Professional
	 * 
	 * @param professionalId the id of the Professional
	 * @return the list of Patients
	 */
    public List<PatientDto> getPatientsFromProfessional(Long professionalId){
		return patientService.findPatientsOfProfessional(professionalId);
	}

	/**
	 * gets all the information of a Patient, namely the summarized view per month
	 * and all diary Entries and all MedicalAppointments
	 * 
	 * @param professionalId the id of the Professional
	 * @param patientCode    the code of the Patient
	 * @return the object with all the information related to that Patient
	 */
    public PatientViewToProfessionalDto getPatientViewByCode(Long professionalId, String patientCode) {

		if(isPatientOnProfessionalList(patientCode, professionalId)) {
			Patient patient = new Patient();
			patient = patientService.getPatientByCode(patientCode);
			PatientViewToProfessionalDto patientView = new PatientViewToProfessionalDto(patient);

			return patientView;
		} else {
			logger.error(bcryptEncoder.encode("ProfessionalService - Paciente não faz parte da lista do Profissional!"));
			throw new RuntimeException("Paciente não faz parte da lista do Profissional!");
		}
	}

	/**
	 * adds a Patient to the Professional list
	 * 
	 * @param professionalId the id of the Professional
	 * @param patientCode    the code of the Patient
	 * @return the newly associated Patient
	 */
	public PatientDto addPatientToProfessional(Long professionalId, String patientCode) {
		boolean found = false;

		Patient patient = patientService.getPatientByCode(patientCode);

		Professional professional = professionalRepository.findById(professionalId)
				.orElseThrow(() -> new RuntimeException("Profissional não existe: " + professionalId));

		if(professional.getAssigned_patients() != null){

			for (Patient p : professional.getAssigned_patients()) {
				if(p.getCode().equals(patientCode)) {
					found = true;
					break;
				}
			}
		}

		if (found) {
			logger.error(bcryptEncoder.encode("ProfessionalService - Paciente já foi adicionado à lista do Profissional!"));
			throw new RuntimeException("Paciente já foi adicionado à lista do Profissional!");
		} else {
			professional.getAssigned_patients().add(patient);
			professionalRepository.save(professional);
		}

		return new PatientDto(patient);
	}

	/**
	 * allows a Professional to update the Patient's profile
	 * 
	 * @param professionalId the id of the Professional
	 * @param patient        the Patient info
	 * @return the updated Patient
	 */
	public PatientDto editPatientToProfessional(Long professionalId, PatientDto patient) {
    	if(isPatientOnProfessionalList(patient.getCode(), professionalId)) {

			validatePatientDates(patient);

			patientService.validateInitialsLength(patient.getNameInitials());

    		Patient patientToUpdate = patientService.getPatientByCode(patient.getCode());

    		patientToUpdate = patient.updatePatient(patientToUpdate);

			patientService.saveProphylacticTreatments(patientToUpdate);

    		patientService.save(patientToUpdate);

    		return new PatientDto(patientToUpdate);

		} else {
			logger.error(bcryptEncoder
					.encode("ProfessionalService Exception - Paciente não faz parte da lista do Profissional!"));
			throw new RuntimeException("Paciente não faz parte da lista do Profissional!");
		}
    }

	/**
	 * removes the Patient from the Professional's List
	 * 
	 * @param professionalId the id of the Professional
	 * @param patientCode    the code of the Patient to be removed from the List
	 * @return true if the Patient was successfully deleted from the List, false
	 *         otherwise
	 */
    public boolean removePatientFromProfessional(Long professionalId, String patientCode) {
    	boolean deleted = false;

		if (isPatientOnProfessionalList(patientCode, professionalId)) {
			professionalRepository.deletePatientFromProfessional(patientCode, professionalId);
			if (!isPatientOnProfessionalList(patientCode, professionalId)) {
				deleted = true;
			}
		}

    	return deleted;
	}

	/**
	 * verifies if a Patient is on the Professional's list
	 * 
	 * @param patientCode    the code of the Patient
	 * @param professionalId the id of the Professional
	 * @return true if the Patient is on the list, false otherwise
	 */
	public boolean isPatientOnProfessionalList(String patientCode, long professionalId) {
		return professionalRepository.isPatientOnProfessionalList(patientCode, professionalId) == 0 ? false : true;
	}

	/**
	 * verifies if the Patient days ar valid
	 * 
	 * @param patient the Patient info
	 */
	private void validatePatientDates(PatientDto patient) {
		LocalDate today = LocalDate.now();
		if (patient.getBirthdate() != null && patient.getBirthdate().isAfter(today)) {
			throw new RuntimeException("A data de nascimento é inválida.");
		}
		patientService.validatePatientProphylactic(patient);
			}

	/**
	 * validates the Patient gender
	 * 
	 * @param gender the Patient gender
	 */
	public void validatePatientGender(String gender) {
		// only validates if it is not null, because Professionals can opt not to enter
		// the field
		if (gender != null) {
			patientService.validateGender(gender);
		}
	}
	}
