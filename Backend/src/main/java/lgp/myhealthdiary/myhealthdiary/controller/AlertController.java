package lgp.myhealthdiary.myhealthdiary.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import lgp.myhealthdiary.myhealthdiary.model.Alert;
import lgp.myhealthdiary.myhealthdiary.security.Database.AuthenticationHelper;
import lgp.myhealthdiary.myhealthdiary.service.PatientAlertService;
import lgp.myhealthdiary.myhealthdiary.service.ProfessionalAlertService;

/**
 * This controller has the endpoints to both Patient and Professional Alerts. It
 * controls the identity using the "Authorization" token
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AlertController {

	private static Logger logger = Logger.getLogger(AlertController.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	PatientAlertService patientAlertService;

	@Autowired
	ProfessionalAlertService professionalAlertService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@GetMapping(value = "/api/professionals/alerts", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Alert> getProfessionalAlerts(@RequestHeader("Authorization") String token) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
		logger.info(bcryptEncoder.encode("AlertController - getProfessionalAlerts() " + professionalId));
		return professionalAlertService.getProfessionalAlerts(professionalId);
	}


	@GetMapping(value = "/api/patients/{patientCode}/alerts", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Alert> getPatientAlerts(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("AlertController - getPatientAlerts() " + patientCode));
		return patientAlertService.getPatientAlerts(patientCode);
	}

	@GetMapping(value = "/api/patients/{patientCode}/alerts/future", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Alert> getPatientFutureAlerts(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("AlertController - getPatientFutureAlerts() " + patientCode));
		return patientAlertService.getPatientFutureAlerts(patientCode);
	}

	@GetMapping(value = "/api/patients/{patientCode}/alert/diary", produces = MediaType.APPLICATION_JSON_VALUE)
	public Alert getPatientDiaryAlert(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("AlertController - getPatientDiaryAlert() " + patientCode));
		return patientAlertService.getPatientDiaryAlert(patientCode);

	}

	@PutMapping(value = "/api/patients/{patientCode}/alerts/{alertId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Boolean changeAlertReadStatus(@PathVariable("alertId") long alertId,
			@PathVariable("patientCode") String patientCode, @RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("AlertController - changeAlertReadStatus() " + patientCode));
		return patientAlertService.changePatientAlertReadStatus(alertId, patientCode);
	}
}