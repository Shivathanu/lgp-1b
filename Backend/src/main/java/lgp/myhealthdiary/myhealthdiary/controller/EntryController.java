package lgp.myhealthdiary.myhealthdiary.controller;

import java.time.LocalDate;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import lgp.myhealthdiary.myhealthdiary.Utils.DateUtils;
import lgp.myhealthdiary.myhealthdiary.dto.EntryDto;
import lgp.myhealthdiary.myhealthdiary.model.Entry;
import lgp.myhealthdiary.myhealthdiary.security.Database.AuthenticationHelper;
import lgp.myhealthdiary.myhealthdiary.service.EntryService;

/**
 * This Controller is responsible for the Patient diary management. It controls
 * the identity using the "Authorization" token
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EntryController {

	private static Logger logger = Logger.getLogger(EntryController.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	EntryService entryService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@GetMapping(value = "/api/patients/{patientCode}/entries", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<LocalDate, EntryDto> getPatientEntries(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("EntryController - getPatientEntries() " + patientCode));
		return entryService.getPatientEntries(patientCode);
	}

	@PostMapping(value = "/api/patients/{patientCode}/entries", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createEntry(@RequestBody Entry entry, @PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("Calling POST:/entries - createEntry" + patientCode));
		return entryService.createEntry(entry, patientCode);
	}

	@PutMapping(value = "/api/patients/{patientCode}/entries/edit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateEntry(@RequestBody Entry entry, @PathVariable("patientCode") String patientCode, @RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("Calling PUT:/entries - updateEntry" + patientCode));
		return entryService.updateEntry(entry, patientCode);
	}

	@DeleteMapping(value = "/api/patients/{patientCode}/entries/delete/{year}/{month}/{day}", produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean deleteEntrys(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token,
			@PathVariable("year") int year, @PathVariable("month") int month, @PathVariable("day") int day) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("EntryController - deleteEntrys() " + patientCode));
		DateUtils.validateDate(year, month, day);
		LocalDate date = LocalDate.of(year, month, day);
		return entryService.deleteEntry(patientCode, date);
	}
}