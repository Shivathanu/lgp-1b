package lgp.myhealthdiary.myhealthdiary.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import lgp.myhealthdiary.myhealthdiary.dto.MedicalAppointmentDto;
import lgp.myhealthdiary.myhealthdiary.model.MedicalAppointment;
import lgp.myhealthdiary.myhealthdiary.security.Database.AuthenticationHelper;
import lgp.myhealthdiary.myhealthdiary.service.MedicalAppointmentService;

/**
 * This controller is responsible for managing the Patient's appointments. It
 * controls the identity using the "Authorization" token
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MedicalAppointmentController {

	private static Logger logger = Logger.getLogger(MedicalAppointmentController.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	MedicalAppointmentService medicalAppointmentService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@GetMapping(value = "/api/patients/{patientCode}/appointments", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, MedicalAppointmentDto> getPatientAppointments(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("MedicalAppointmentController - getPatientAppointments() " + patientCode));
		return medicalAppointmentService.getPatientMedicalAppointments(patientCode);
	}

	@PostMapping(value = "/api/patients/{patientCode}/appointments", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveMedicalAppointment(@RequestBody MedicalAppointment medicalAppointment,
			@PathVariable("patientCode") String patientCode, @RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("MedicalAppointmentController - saveMedicalAppointment() " + patientCode));
		return medicalAppointmentService.createMedicalAppointment(medicalAppointment, patientCode);
	}

	@PutMapping(value = "/api/patients/{patientCode}/appointments/{appointmentId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveMedicalAppointment(@RequestBody MedicalAppointment medicalAppointment,
			@PathVariable("patientCode") String patientCode, @PathVariable("appointmentId") long appointmentId,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("MedicalAppointmentController - saveMedicalAppointment() " + patientCode));
		return medicalAppointmentService.updateMedicalAppointment(appointmentId, medicalAppointment, patientCode);
	}

	@DeleteMapping(value = "/api/patients/{patientCode}/appointments/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity deleteMedicalAppointments(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token, @PathVariable("id") Long id) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("MedicalAppointmentController - deleteMedicalAppointments() " + patientCode));
		return medicalAppointmentService.deleteMedicalAppointment(patientCode, id);
	}
}