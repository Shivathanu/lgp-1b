package lgp.myhealthdiary.myhealthdiary.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import lgp.myhealthdiary.myhealthdiary.Utils.DateUtils;
import lgp.myhealthdiary.myhealthdiary.dto.PatientDailyInfoDto;
import lgp.myhealthdiary.myhealthdiary.dto.PatientDto;
import lgp.myhealthdiary.myhealthdiary.dto.PatientInfoAfterLoginDto;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.security.Database.AuthenticationHelper;
import lgp.myhealthdiary.myhealthdiary.service.PatientService;

/**
 * This Controller is responsible for all the Patient profile related endpoints.
 * It controls the identity using the "Authorization" token
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PatientController {

	private static Logger logger = Logger.getLogger(PatientController.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	private PatientService patientsService;

	@Autowired
	private AuthenticationHelper authenticationHelper;

	@GetMapping(value = "/api/patients/{patientCode}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Patient getPatient(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("PatientController -  getPatient() " + patientCode));
		return this.patientsService.getPatientByCode(patientCode);
	}

	@GetMapping(value = "/api/patients/{patientCode}/profile", produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientDto getPatientProfile(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("PatientController -  getPatientProfile() " + patientCode));
		return this.patientsService.getPatientProfile(patientCode);
	}

	@GetMapping(value = "/api/patients/{patientCode}/diary/{year}/{month}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, PatientDailyInfoDto> getPatientEventsForMonth(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token,
			@PathVariable("year") int year,
			@PathVariable("month") int month) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("PatientController -  getPatientEventsForMonth() " + patientCode));
		DateUtils.validateYearMonth(year, month);
		return this.patientsService.getPatientInfoForMonth(patientCode, year, month);
	}

	@GetMapping(value = "/api/patients/{patientCode}/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientInfoAfterLoginDto getPatientInfoAfterLogin(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("PatientController -  getPatientInfoAfterLogin() " + patientCode));

		return this.patientsService.getPatientInfoAfterLogin(patientCode);
	}

	@PutMapping(value = "/api/patients", consumes = MediaType.APPLICATION_JSON_VALUE)
	public PatientDto updatePatients(@RequestHeader("Authorization") String token, @RequestBody PatientDto patientDto) {
		authenticationHelper.isPatientCodeInToken(token, patientDto.getCode());
		logger.info(bcryptEncoder.encode("PatientController - updatePatients() " + patientDto.getCode()));
		return patientsService.updatePatient(patientDto);
	}
	
	@PutMapping(value = "/api/patients/{patientCode}/changepassword", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void changePassword(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token, @RequestBody Map<String, String> params) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("PatientController -  changePassword() " + patientCode));
		patientsService.changePassword(patientCode, params);
	}

	@PutMapping(value = "/api/patients/{patientCode}/activateaccount", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void activatePatientAccount(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token, @RequestBody Map<String, String> params) {
		authenticationHelper.isPatientCodeInToken(token, patientCode);
		logger.info(bcryptEncoder.encode("PatientController - activatePatientAccount() " + token));
		this.patientsService.activatePatientAccount(patientCode, params);
	}
	
	@PutMapping(value = "/api/patients/passwordreset")
	public void resetPatientPassword(@RequestBody String email) {
		logger.info(bcryptEncoder.encode("PatientController - resetPatientPassword() " + email));
		this.patientsService.resetPatientPassword(email);
	}
	
	@DeleteMapping(value = "/api/patients/{patientCode}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deletePatients(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {
		try {
			authenticationHelper.isPatientCodeInToken(token, patientCode);
			logger.info(bcryptEncoder.encode("PatientController - deletePatients() " + patientCode));
			this.patientsService.deletePatient(patientCode);
			return new ResponseEntity<String>("Paciente eliminado com sucesso", HttpStatus.OK);
		} catch (RuntimeException e) {
			return new ResponseEntity<String>("Pedido sem sucesso", HttpStatus.BAD_REQUEST);
		}
	}
}