package lgp.myhealthdiary.myhealthdiary.controller;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import lgp.myhealthdiary.myhealthdiary.dto.PatientDto;
import lgp.myhealthdiary.myhealthdiary.dto.PatientViewToProfessionalDto;
import lgp.myhealthdiary.myhealthdiary.dto.ProfessionalDto;
import lgp.myhealthdiary.myhealthdiary.dto.ProfessionalInfoAfterLoginDto;
import lgp.myhealthdiary.myhealthdiary.exception.NotFoundException;
import lgp.myhealthdiary.myhealthdiary.model.HealthCenter;
import lgp.myhealthdiary.myhealthdiary.security.Database.AuthenticationHelper;
import lgp.myhealthdiary.myhealthdiary.service.ProfessionalProfileService;
import lgp.myhealthdiary.myhealthdiary.service.ProfessionalService;

/**
 * This Controller is responsible for all Professional Profile and Professional
 * list of Patients endpoints. It controls the identity using the
 * "Authorization" token
 *
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProfessionalController {

	private static Logger logger = Logger.getLogger(ProfessionalController.class.getName());

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	private ProfessionalService professionalService;


	@Autowired
	private ProfessionalProfileService professionalProfileService;
	
	@Autowired
	private AuthenticationHelper authenticationHelper;

	@GetMapping(value = "/api/healthcenters", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<HealthCenter> getAllHealthCenters() {
		logger.info(bcryptEncoder.encode("ProfessionalController -  getAllHealthCenters() "));
		return professionalProfileService.getAllHealthCenters();
	}

	@GetMapping(value = "/api/professionals/hospitals", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<HealthCenter> getAllHospitals() {
		logger.info(bcryptEncoder.encode("ProfessionalController -  getAllHospitals() "));
		return professionalProfileService.getAllHospitals();
	}

	@GetMapping(value = "/api/professionals/specialities", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, String> getSpecialities() {
		logger.info(bcryptEncoder.encode("ProfessionalController -  getSpecialities() "));
		return professionalProfileService.specialitiesEndPoint();
	}

	@GetMapping(value = "/api/professionals/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProfessionalInfoAfterLoginDto getInfoAfterLogin(@RequestHeader("Authorization") String token) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
		logger.info(bcryptEncoder.encode("ProfessionalController -  getProfessionalProfile() " + professionalId));
		return professionalService.professionalLoginInfo(professionalId);
	}

	@GetMapping(value = "/api/professionals/profile", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProfessionalDto getProfessionalProfile(@RequestHeader("Authorization") String token) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
		logger.info(bcryptEncoder.encode("ProfessionalController -  getProfessionalProfile() " + professionalId));
		return professionalProfileService.getProfessionalProfileById(professionalId);
	}

	@GetMapping(value = "/api/professionals/mypatients/{patientCode}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientViewToProfessionalDto getPatient(@PathVariable("patientCode") String patientCode,
			@RequestHeader("Authorization") String token) {

		try {
			final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
			logger.info(bcryptEncoder.encode("ProfessionalController -  getPatient() " + professionalId));
			return professionalService.getPatientViewByCode(professionalId, patientCode);

		} catch(NoSuchElementException e) {
			throw new NotFoundException("Código Paciente " + patientCode + " não existe!");
		}
	}

	@GetMapping(value = "/api/professionals/mypatients", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PatientDto> getPatientsFromProfessional(@RequestHeader("Authorization") String token) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
			logger.info(bcryptEncoder.encode("ProfessionalController -  getPatientsFromProfessional() "+ professionalId));
			return professionalService.getPatientsFromProfessional(professionalId);
	}

	@PostMapping(value = "/api/register/professionals", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createProfessionals(@RequestBody ProfessionalDto professionalDto) {
		ProfessionalDto professional = new ProfessionalDto(
				this.professionalProfileService.createProfessional(professionalDto.build()));
		return ResponseEntity.ok(professional);
	}

	@PostMapping(value = "/api/professionals/newpatient", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String createPatient(@RequestHeader("Authorization") String token,
			@RequestBody PatientDto patient) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
		logger.info(bcryptEncoder.encode("ProfessionalController -  createPatient() " + professionalId));
		String patientCode = this.professionalService.createPatient(professionalId, patient);
		return patientCode;
	}

	@PutMapping(value = "/api/professionals/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateProfessionals(@RequestBody ProfessionalDto professionalDto,
			@RequestHeader("Authorization") String token) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
		logger.info(bcryptEncoder.encode("ProfessionalController -  updateProfessionals() " + professionalId));
		professionalProfileService.updateProfessional(professionalId, professionalDto.build());
	}
	
	@PutMapping(value = "/api/professionals/changepassword", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void changePassword(@RequestHeader("Authorization") String token,
			@RequestBody Map<String, String> params) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
		logger.info(bcryptEncoder.encode("ProfessionalController -  changePassword() " + professionalId));
		professionalProfileService.changePassword(professionalId, params);
	}
	

	@PutMapping(value = "/api/professionals/mypatients", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientDto addPatientToProfessional(@RequestHeader("Authorization") String token,
			@RequestHeader String patientCode) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
		logger.info(bcryptEncoder.encode("ProfessionalController -  addPatientToProfessional() " + professionalId));
		PatientDto patientDto = this.professionalService.addPatientToProfessional(professionalId, patientCode);
		return patientDto;
	}

	@PutMapping(value = "/api/professionals/editpatient", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public PatientDto editPatientToProfessional(@RequestHeader("Authorization") String token,
			@RequestBody PatientDto patient) {
		final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
		logger.info(bcryptEncoder.encode("ProfessionalController -  editPatientToProfessional() " + professionalId));
		PatientDto patientDto = this.professionalService.editPatientToProfessional(professionalId, patient);
		return patientDto;
	}
	
	@PutMapping(value = "/api/professionals/passwordreset")
	public ResponseEntity<String> resetProfessionalPassword(@RequestBody String email) {
		logger.info(bcryptEncoder.encode("ProfessionalController - resetProfessionalPassword() " + email));
		return this.professionalProfileService.resetProfessionalPassword(email);
	}

	@DeleteMapping(value = "/api/professionals/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteProfessionals(@RequestHeader("Authorization") String token) {
		try {
			final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
			logger.info(bcryptEncoder.encode("ProfessionalController -  deleteProfessional() " + professionalId));
			professionalProfileService.deleteProfessional(professionalId);
			return new ResponseEntity<String>("Profissional apagado com sucesso", HttpStatus.OK);
		} catch (RuntimeException e) {
			return new ResponseEntity<String>("Pedido sem sucesso", HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping(value = "/api/professionals/deletemypatient", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity removePatientFromProfessional(@RequestHeader("Authorization") String token,
			@RequestHeader String patientCode) {
		try {
			final long professionalId = authenticationHelper.getProfessionalIdFromToken(token);
			logger.info(bcryptEncoder.encode("ProfessionalController -  removePatientFromProfessional() " + professionalId));
			if(professionalService.removePatientFromProfessional(professionalId, patientCode)) {
				return new ResponseEntity<String>("Paciente removido com sucesso", HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("Paciente não existe na lista", HttpStatus.NOT_FOUND);
			}
		} catch (RuntimeException e) {
			return new ResponseEntity<String>("Pedido sem sucesso", HttpStatus.BAD_REQUEST);
		}
	}
}
