package lgp.myhealthdiary.myhealthdiary.controller;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestHeader;

import lgp.myhealthdiary.myhealthdiary.dto.PatientDto;
import lgp.myhealthdiary.myhealthdiary.model.Entry;
import lgp.myhealthdiary.myhealthdiary.model.HealthCenter;
import lgp.myhealthdiary.myhealthdiary.model.MedicalAppointment;
import lgp.myhealthdiary.myhealthdiary.model.Patient;
import lgp.myhealthdiary.myhealthdiary.model.Professional;
import lgp.myhealthdiary.myhealthdiary.model.ProfessionalSpeciality;
import lgp.myhealthdiary.myhealthdiary.model.ProphylacticTreatment;
import lgp.myhealthdiary.myhealthdiary.repository.HealthCenterRepository;
import lgp.myhealthdiary.myhealthdiary.service.EntryService;
import lgp.myhealthdiary.myhealthdiary.service.MedicalAppointmentService;
import lgp.myhealthdiary.myhealthdiary.service.PatientService;
import lgp.myhealthdiary.myhealthdiary.service.ProfessionalProfileService;
import lgp.myhealthdiary.myhealthdiary.service.ProfessionalService;

/**
 * This Controller is used only during development to test main functionality
 * and to insert data for testing and demoing purposes
 */
//@RestController
//@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FeedDataController {

	private static Logger logger = Logger.getLogger(FeedDataController.class.getName());

	// to only allow to use one at runtime - to avoid repeated registrations
	private boolean isMethodAllowed = true;

	// password to protect the endpoint
	private static final String PASSWORD = "INSERT PASSWORD HERE";


	// email pole to randomly select 5 to create testing accounts
	private static final String[] emailsPole = { "insert", "5 or more", "valid", "email", "addresses" };

	private List<String> emailsList = new ArrayList<String>();

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	@Qualifier("professionalService")
	private ProfessionalService professionalService;

	@Autowired
	private PatientService patientService;

	@Autowired
	private EntryService entryService;

	@Autowired
	private MedicalAppointmentService appointmentService;

	@Autowired
	private HealthCenterRepository healthCenterRepository;

	@Autowired
	private ProfessionalProfileService professionalProfileService;


	// @PostMapping(value = "/internal/feeddata", consumes =
	// MediaType.APPLICATION_JSON_VALUE, produces =
	// MediaType.APPLICATION_JSON_VALUE)
	public String feedData(@RequestHeader String password) {
		logger.info(bcryptEncoder.encode("FeedDataController - feedData() "));
		if (password.equals(PASSWORD) && isMethodAllowed) {
		createMockData();
			isMethodAllowed = false;
			return "Test data generated";
		} else if (!isMethodAllowed) {
			return "already created before, please re-run the project to try again";
		} else {
			return "wrong password";
		}
	}


	private void createMockData() {

		// K isn't in the official health center List, used here to avoid conflicts
		HealthCenter healthCenter = new HealthCenter("K", "UCF to test", "undefined", "undefined");
		healthCenterRepository.save(healthCenter);
		List<ProfessionalSpeciality> specialities = professionalProfileService.getSpecialities();
ProfessionalSpeciality specialityToUse = new ProfessionalSpeciality();
for (ProfessionalSpeciality speciality: specialities) {
			// can't be a neurologist to be associated with the K health center
			if (!speciality.isHospitalAssociated()) {
		specialityToUse = speciality;
		 break;
	}
}

		Professional doctor1 = new Professional(0, "Médico de teste 1", specialityToUse, healthCenter,
				"hivehealthdiary1@ulsm.min-saude.pt", "hivehealthdiary1@ulsm.min-saude.pt", "testingpassword1");
		Professional doctor2 = new Professional(0, "Médico de teste 2", specialityToUse, healthCenter,
				"hivehealthdiary2@ulsm.min-saude.pt", "hivehealthdiary2@ulsm.min-saude.pt", "testingpassword2");

		doctor1 = professionalProfileService.createProfessional(doctor1);
		doctor2 = professionalProfileService.createProfessional(doctor2);

		// add predefined emails to the list to being randomized
		emailsList.addAll(Arrays.asList(emailsPole));

		PatientDto patient1 = new PatientDto("", emailRoulette(), "ONE", "fake1", "fake1", healthCenter);
		PatientDto patient2 = new PatientDto("", emailRoulette(), "TWO", "fake2", "fake2", healthCenter);
		PatientDto patient3 = new PatientDto("", emailRoulette(), "THREE", "fake3", "fake3", healthCenter);
		PatientDto patient4 = new PatientDto("", emailRoulette(), "FOUR", "fake4", "fake4", healthCenter);
		PatientDto patient5 = new PatientDto("", emailRoulette(), "FIVE", "fake5", "fake5", healthCenter);

		String code1 = generateProphylacticAndCreate(doctor1.getId(), patient1);
		String code2 = generateProphylacticAndCreate(doctor1.getId(), patient2);
		String code3 = generateProphylacticAndCreate(doctor1.getId(), patient3);
		String code4 = generateProphylacticAndCreate(doctor2.getId(), patient4);
		String code5 = generateProphylacticAndCreate(doctor2.getId(), patient5);

		mockEntries(patient1.build(), code1);
		mockEntries(patient2.build(), code2);
		mockEntries(patient3.build(), code3);
		mockEntries(patient4.build(), code4);
		mockEntries(patient5.build(), code5);
	}

	private String generateProphylacticAndCreate (long professionalId, PatientDto patient) {
		patient.getProphylacticTreatments().add(new ProphylacticTreatment("Tratamento", LocalDate.of(2020, 1, 1)));
		patient.getProphylacticTreatments().add(new ProphylacticTreatment("Profilático", LocalDate.of(2020, 3, 15)));
		return professionalService.createPatient(professionalId, patient);
	}

	private void mockEntries(Patient patient, String patientCode) {

		LocalDate dateToInsert= LocalDate.now().minusDays(entryService.getPastDaysAllowed());
		LocalDate dateToStop = LocalDate.now().minusDays(2);
		while (dateToInsert.isBefore(dateToStop)) {
			entryService.createEntry(
					new Entry(patient, dateToInsert, true, 8, true, true, true, true, "", false),
					patientCode);
			dateToInsert = dateToInsert.plusDays(1);
		}

		LocalDateTime dateToAppointment = dateToStop.plusMonths(1).atStartOfDay().withHour(12);
		appointmentService.createMedicalAppointment(
				new MedicalAppointment("Nome do médico", "Hospital", "Observation", dateToAppointment, "Neurologia"),
				patientCode);
		appointmentService.createMedicalAppointment(
				new MedicalAppointment("Nome do médico", "Hospital", "Observações", dateToAppointment.plusMonths(1),
						"Neurologia"),
				patientCode);
		appointmentService.createMedicalAppointment(new MedicalAppointment("Nome do médico", "Centro de saude", "",
				dateToAppointment.plusMonths(2), "Rotina"), patientCode);
	}

	// to randomly select a email from the list of emails to create an account and
	// send email with credentials. To divide spam for whole team.
	private String emailRoulette () {
		SecureRandom random = new SecureRandom();
		int randomIndex = random.nextInt(emailsList.size());
		String lucky = emailsList.get(randomIndex);
		emailsList.remove(randomIndex);
		return lucky;
	}
}
