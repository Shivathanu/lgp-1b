import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/load_info.dart';
import 'package:headache_diary/screens_component/notifications/notificationsPage.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) => runApp(MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      title: 'Flutter Demo',
      theme: new ThemeData(
        fontFamily: 'sevenOneEight',
        primaryColor: Color(0xff3f7380),
        accentColor: Color(0xff3f7380),
      ),
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        // GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('pt', 'PT'), // Portugues
      ],
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class _MyHomePageState extends State<MyHomePage> {
  Future<User> u;

  DataBase db = DataBase();
  User user = User();

  @override
  void initState() {
    super.initState();

    var initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    //Function from future builder
    u = getInit();
  }

  Future<User> getInit() async {
    await db.getLoggedUser();
    if (user.code != null) {
      String expireTokenString = await getExpireToken();
      DateTime expireToken = DateTime.parse(expireTokenString);
      if (expireToken.isAfter(DateTime.now())) {
        bool isConnected = await checkConnection();
        if (isConnected) {
          await getUserByCode(user.code);

          if (user != null) {
            db.getUserLoggedIn(user.code).then((val) {
              user.id = val;
              db.updateUserState(user);
            });
          }
          return User();
        } else {
          Toast.show(
              "Ligue-se à internet para poder finalizar a operação!", context,
              duration: 5, gravity: Toast.BOTTOM);
        }
      } else {
        await flutterLocalNotificationsPlugin.cancelAll();
      }
    }
    return null;
  }

  Future onSelectNotification(String payload) async {
    /* if (payload != null) {
      print("notification payload: ");
    } */
    await Navigator.push(
        context, MaterialPageRoute(builder: (context) => NotificationsPage()));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
      future: u,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Container(
              color: Colors.grey.shade100,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      "assets/images/Untitled-1.png",
                      width: 200.0,
                    ),
                    Image.asset(
                      "assets/images/HIVE_health.png",
                      width: 70.0,
                    ),
                  ],
                ),
              ),
            );
            break;
          default:
            if (snapshot.hasError) {
              return Scaffold(
                  body: Center(
                child: Container(
                  child: Text("app fail"),
                ),
              ));
            } else {
              if (snapshot.data != null) {
                return LoadInfo();
              }
              return SignInPage();
            }
        }
      },
    );
  }
}
