//import 'dart:convert';

class Headache {
  int id;
  String date;
  String obs;
  bool migraine;
  int headacheIntensity;
  bool tookPainKillers;
  bool tookTriptans;
  bool menstruation;
  bool workIncapacity;
  bool emergencyServiceRecurrence;

  Headache();

  Map headacheToMap() {
    return {
      "date": date ?? "",
      "obs": obs ?? "",
      "migraine": migraine ?? true,
      "headacheIntensity": headacheIntensity ?? 1,
      "tookPainKillers": tookPainKillers ?? false,
      "tookTriptans": tookTriptans ?? false,
      "menstruation": menstruation ?? false,
      "workIncapacity": workIncapacity ?? false,
      "emergencyServiceRecurrence": emergencyServiceRecurrence ?? false,
    };
  }

  @override
  String toString() {
    return "$id ### $date , $migraine,  $headacheIntensity, $tookPainKillers, $tookTriptans, $menstruation, $workIncapacity, $emergencyServiceRecurrence";
  }
}
