import 'dart:convert';

import 'package:headache_diary/utils/database.dart';
import 'package:intl/intl.dart';

class User {
  int id;
  int logged;
  String code;
  String name;
  String numUtente;
  String email;
  String numProcess;
  String healthCenter;
  String gender;
  bool migraineWithAura;
  bool exclusivelyUnilateral;
  bool symptomaticTreatment;
  String symptomaticTreatmentDetails;
  List prophylacticTreatments;
  bool anxiety;
  bool depression;
  bool insomnia;
  String otherMedicalConditions;
  String dayBirth;
  DateTime birthdate;
  List pastNotifications;
  List futureNotifications;
  int diaryNotificationID;
  int readByRecipient;
  bool active;

  Map<DateTime, List> allEvents;
  Map<DateTime, List> allHolidays;

  final f = new DateFormat('yyyy-MM-dd');

  static final User _instance = User.internal();
  factory User() => _instance;
  User.internal();

  static User get instance => _instance;

  User.fromJson(Map<String, dynamic> json) {
    _instance.code = json["patientProfile"]['code'] ?? "";
    _instance.numUtente = json["patientProfile"]['numeroUtente'] ?? "";
    _instance.name = json["patientProfile"]['nameInitials'] ?? "";
    _instance.email = json["patientProfile"]['email'] ?? "";
    _instance.numProcess =
        json["patientProfile"]['numProcessoHospitalar'] ?? "";
    _instance.healthCenter =
        json["patientProfile"]["healthCenter"]["name"] ?? "";
    _instance.gender = json["patientProfile"]['gender'] ?? "";
    _instance.migraineWithAura =
        json["patientProfile"]['migraineWithAura'] ?? false;
    _instance.exclusivelyUnilateral =
        json["patientProfile"]['exclusivelyUnilateral'] ?? false;
    _instance.symptomaticTreatment =
        json["patientProfile"]['symptomaticTreatment'] ?? false;
    _instance.symptomaticTreatmentDetails =
        json["patientProfile"]['symptomaticTreatmentDetails'];
    _instance.anxiety = json["patientProfile"]['anxiety'] ?? false;
    _instance.depression = json["patientProfile"]['depression'] ?? false;
    _instance.insomnia = json["patientProfile"]['insomnia'] ?? false;
    _instance.otherMedicalConditions =
        json["patientProfile"]['otherMedicalConditions'] ?? "";

    if (json["patientProfile"]["birthdate"] == null) {
      _instance.birthdate = new DateTime(1, 1, 1);
    } else {
      _instance.birthdate = new DateTime(
          json["patientProfile"]['birthdate'][0],
          json["patientProfile"]['birthdate'][1],
          json["patientProfile"]['birthdate'][2]);
    }

    _instance.prophylacticTreatments =
        json["patientProfile"]['prophylacticTreatments'] ?? [];
    _instance.pastNotifications = json["alerts"];
    _instance.futureNotifications = json["futureAlerts"];
    _instance.active = json["patientProfile"]["active"] ?? false;
  }

  String toJSON() {
    return json.encode({
      "code": code,
      "initials": name,
      "processNumber": numProcess,
      "healthCenter": healthCenter,
      "email": email,
      "numeroUtente": numUtente,
      "gender": gender,
      "birthdate": f.format(birthdate),
      "migraineWithAura": migraineWithAura,
      "exclusivelyUnilateral": exclusivelyUnilateral,
      "symptomaticTreatment": symptomaticTreatment,
      "symptomaticTreatmentDetails": symptomaticTreatmentDetails,
      "prophylacticTreatments": prophylacticTreatments,
      "anxiety": anxiety,
      "depression": depression,
      "insomnia": insomnia,
      "otherMedicalConditions": otherMedicalConditions,
      "diaryNotificationID": diaryNotificationID ?? -1,
      "readByRecipient": readByRecipient ?? 0
    });
  }

  String toJSONExport() {
    return json.encode({
      "code": code,
      "initials": name,
      "processNumber": numProcess,
      "healthCenter": healthCenter,
      "email": email,
      "numeroUtente": numUtente,
      "gender": gender,
      "birthdate": f.format(birthdate),
      "migraineWithAura": migraineWithAura,
      "exclusivelyUnilateral": exclusivelyUnilateral,
      "symptomaticTreatment": symptomaticTreatment,
      "symptomaticTreatmentDetails": symptomaticTreatmentDetails,
      "prophylacticTreatments": prophylacticTreatments,
      "anxiety": anxiety,
      "depression": depression,
      "insomnia": insomnia,
      "otherMedicalConditions": otherMedicalConditions,
      "calendar": allEvents,
    });
  }

  User.map(dynamic obj) {
    this.code = obj["code"];
    this.numUtente = obj["numeroUtente"];
    this.gender = obj["gender"];
    this.anxiety = obj["anxiety"];
  }

  User fromMap(Map map) {
    var data = json.decode(map[userColumn]);
    _instance.id = map[idColumn];
    _instance.logged = map[loggedUserColumn];

    _instance.code = data["code"];
    _instance.name = data["initials"];
    _instance.numProcess = data["processNumber"];
    _instance.email = data["email"];
    _instance.healthCenter = data["healthCenter"];
    _instance.numUtente = data["numeroUtente"];
    _instance.gender = data["gender"];
    _instance.birthdate = f.parse(data["birthdate"]);
    _instance.migraineWithAura = data['migraineWithAura'];
    _instance.exclusivelyUnilateral = data['exclusivelyUnilateral'];
    _instance.symptomaticTreatment = data['symptomaticTreatment'];
    _instance.symptomaticTreatmentDetails = data['symptomaticTreatmentDetails'];
    _instance.prophylacticTreatments = data['prophylacticTreatments'];
    _instance.anxiety = data['anxiety'];
    _instance.depression = data['depression'];
    _instance.insomnia = data['insomnia'];
    _instance.otherMedicalConditions = data['otherMedicalConditions'];
    _instance.diaryNotificationID = data['diaryNotificationID'] ?? -1;
    _instance.readByRecipient = data['readByRecipient'] ?? 0;

    return this;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      userColumn: this.toJSON(),
      loggedUserColumn: logged
    };

    if (id != null) map[idColumn] = id;

    return map;
  }

  @override
  String toString() {
    return "$code " +
        "$name " +
        "$gender " +
        "$birthdate " +
        "$migraineWithAura " +
        "$exclusivelyUnilateral " +
        "$symptomaticTreatment " +
        "$symptomaticTreatmentDetails " +
        "$prophylacticTreatments " +
        "$anxiety " +
        "$depression " +
        "$insomnia " +
        "$otherMedicalConditions ";
  }
}
