import 'dart:convert';
import 'dart:io';
import 'package:headache_diary/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

String urlBase =
    /* "http://192.168.1.66:8080/"; */ "https://hive-health-diary.herokuapp.com/";

Future<bool> setToken(String value) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.setString('token', value);
}

Future<String> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('token');
}

Future<bool> setExpireToken(String value) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.setString('expireToken', value);
}

Future<String> getExpireToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('expireToken');
}

Future<User> getUserByCode(String code) async {
  var url = urlBase + "api/patients/$code/login";
  try {
    String token = await getToken();

    var response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'password': '{{password}}',
        'Authorization': 'Bearer $token'
      },
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400) {
      return null;
    }
    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        User.fromJson(json.decode(utf8.decode(response.bodyBytes)));
        return User();
      }
      return null;
    }

    //If fails to return a state, returns the error code
    return null;
  } catch (e) {
    print(e);
    return null;
  }
}

Future<String> changePassword(String code, String oldPassword,
    String newPassword, String repeatedNewPassword) async {
  var url = urlBase + "api/patients/$code/changepassword";
  try {
    String token = await getToken();

    var response = await http
        .put(url,
            headers: {
              'Content-type': 'application/json',
              'Authorization': 'Bearer $token'
            },
            body: json.encode({
              'oldPassword': '$oldPassword',
              'newPassword': '$newPassword',
              'repeatedNewPassword': '$repeatedNewPassword',
            }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    //Timeout
    if (response == null) return "FAIL - resp=null";

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400)
      return " ${response.statusCode} , ${json.decode(utf8.decode(response.bodyBytes))["message"]}";

    return response.body;
  } catch (e) {
    print("catch $e");
    return "FAIL";
  }
}

Future<String> deleteUser(String code) async {
  var url = urlBase + "api/patients/$code";
  try {
    String token = await getToken();

    var response = await http.delete(
      url,
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    );
    return utf8.decode(response.bodyBytes);
  } catch (e) {
    return e;
  }
}

Future<String> authenticate(String code, String password) async {
  var url = urlBase + "authenticate";
  try {
    var response = await http
        .post(url,
            headers: {'Content-type': 'application/json'},
            body: json.encode({
              'username': '$code',
              'password': '$password',
            }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return "FAIL - resp=null";

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400)
      return "${response.statusCode}";
    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return response.body;
      }
      return "FAIL - application/json";
    }

    //If fails to return a state, returns the error code
    return "FAIL - content-type";
  } catch (e) {
    print("catch $e");
    return "FAIL";
  }
}

Future<String> accountActivation(
    String code, String password, String repeatedPassword) async {
  var url = urlBase + "api/patients/$code/activateaccount";
  try {
    String token = await getToken();
    var response = await http
        .put(url,
            headers: {
              'Content-type': 'application/json',
              'Authorization': 'Bearer $token'
            },
            body: json.encode({
              'password': '$password',
              'repeatedPassword': '$repeatedPassword',
            }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return "FAIL - resp=null";
    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400)
      return "${response.statusCode} , ${json.decode(utf8.decode(response.bodyBytes))["message"]}";

    return response.body;
  } catch (e) {
    print("catch $e");
    return "FAIL";
  }
}

Future<String> forgotPassword(String email) async {
  var url = urlBase + "api/patients/passwordreset";
  try {
    var response = await http
        .put(url, headers: {'Content-type': 'text/plain'}, body: email)
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return "FAIL - resp=null";

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400)
      return " ${response.statusCode} , ${json.decode(utf8.decode(response.bodyBytes))["message"]}";

    return response.body;
  } catch (e) {
    print("catch $e");
    return "FAIL";
  }
}

Future<String> editUserProfile(
    String code,
    String name,
    DateTime birthdate,
    String gender,
    bool migraineWithAura,
    bool exclusivelyUnilateral,
    bool symptomaticTreatment,
    String symptomaticTreatmentDetails,
    List prophylacticTreatment,
    bool insomnia,
    bool anxiety,
    bool depression,
    String otherMedicalConditions) async {
  var url = urlBase + "api/patients";

  try {
    String token = await getToken();

    var response = await http
        .put(url,
            headers: {
              'Content-type': 'application/json',
              'Authorization': 'Bearer $token'
            },
            body: json.encode({
              'code': '$code',
              'nameInitials': "$name",
              'birthdate': [birthdate.year, birthdate.month, birthdate.day],
              'gender': '$gender',
              'migraineWithAura': migraineWithAura,
              'exclusivelyUnilateral': exclusivelyUnilateral,
              'symptomaticTreatment': symptomaticTreatment,
              'symptomaticTreatmentDetails': '$symptomaticTreatmentDetails',
              'prophylacticTreatments': prophylacticTreatment,
              'anxiety': anxiety,
              'depression': depression,
              'insomnia': insomnia,
              'otherMedicalConditions': '$otherMedicalConditions',
            }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400) return null;

    //verify if the response is in JSON format
    /*  if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) { */
    return utf8.decode(response.bodyBytes);
    /*  }
      return null;
    }

    return null; */
  } catch (e) {
    print("catch $e");
    return null;
  }
}

Future<String> createEntryOnCalendar(
    String patient,
    List date,
    bool migraine,
    int headacheIntensity,
    bool tookPainKillers,
    bool tookTriptans,
    bool emergencyServiceRecurrence,
    bool workIncapacity,
    String observations,
    bool menstruation) async {
  var url = urlBase + "api/patients/$patient/entries";

  try {
    String token = await getToken();

    var response = await http
        .post(url,
            headers: {
              'Content-type': 'application/json',
              'Authorization': 'Bearer $token'
            },
            body: json.encode({
              'patient': '$patient',
              'date': date,
              'migraine': migraine,
              'headacheIntensity': headacheIntensity,
              'tookPainKillers': tookPainKillers,
              'tookTriptans': tookTriptans,
              'emergencyServiceRecurrence': emergencyServiceRecurrence,
              'workIncapacity': workIncapacity,
              'observations': "$observations",
              'menstruation': menstruation,
            }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    //Timeout
    if (response == null) return "FAIL - resp=null";

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400)
      return "${response.statusCode}";

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return utf8.decode(response.bodyBytes);
      }
      return "FAIL - application/json";
    }

    //If fails to return a state, returns the error code
    return "FAIL - content-type";
  } catch (e) {
    print("catch $e");
    return "FAIL";
  }
}

Future<String> editEntryOnCalendar(
    String patient,
    List date,
    bool migraine,
    int headacheIntensity,
    bool tookPainKillers,
    bool tookTriptans,
    bool emergencyServiceRecurrence,
    bool workIncapacity,
    String observations,
    bool menstruation) async {
  var url = urlBase + "api/patients/$patient/entries/edit";

  try {
    String token = await getToken();

    var response = await http
        .put(url,
            headers: {
              'Content-type': 'application/json',
              'Authorization': 'Bearer $token'
            },
            body: json.encode({
              'date': date,
              'migraine': migraine,
              'headacheIntensity': headacheIntensity,
              'tookPainKillers': tookPainKillers,
              'tookTriptans': tookTriptans,
              'emergencyServiceRecurrence': emergencyServiceRecurrence,
              'workIncapacity': workIncapacity,
              'observations': "$observations",
              'menstruation': menstruation,
            }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    //Timeout
    if (response == null) return "FAIL - resp=null";

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400)
      return "${response.statusCode}";

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return utf8.decode(response.bodyBytes);
      }
      return "FAIL - application/json";
    }

    //If fails to return a state, returns the error code
    return "FAIL - content-type";
  } catch (e) {
    print("catch $e");
    return "FAIL";
  }
}

Future<String> deleteEntryOnCalendar(
    int id, String patient, DateTime date) async {
  var url = urlBase +
      "api/patients/$patient/entries/delete/${date.year}/${date.month}/${date.day}";

  try {
    String token = await getToken();

    var response = await http.delete(
      url,
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode >= 400) return null;

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return utf8.decode(response.bodyBytes);
      }
      return null;
    }

    //If fails to return a state, returns the error code
    return null;
  } catch (e) {
    return null;
  }
}

Future<Map> createAppointmentOnCalendar(String patient, String type,
    List dateTime, String doctor, String place, String observations) async {
  var url = urlBase + "api/patients/$patient/appointments/";

  try {
    String token = await getToken();

    var response = await http
        .post(url,
            headers: {
              'Content-type': 'application/json',
              'Authorization': 'Bearer $token'
            },
            body: json.encode({
              'type': "$type",
              'dateTime': dateTime,
              'doctor': "$doctor",
              'place': "$place",
              'observations': "$observations",
            }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400) return null;

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return json.decode(utf8.decode(response.bodyBytes));
      }
      return null;
    }

    //If fails to return a state, returns the error code
    return null;
  } catch (e) {
    return null;
  }
}

Future<Map> editAppointmentOnCalendar(int id, String patient, String type,
    List dateTime, String doctor, String place, String observations) async {
  var url = urlBase + "api/patients/$patient/appointments/$id";

  try {
    String token = await getToken();

    var response = await http
        .put(url,
            headers: {
              'Content-type': 'application/json',
              'Authorization': 'Bearer $token'
            },
            body: json.encode({
              'type': "$type",
              'dateTime': dateTime,
              'doctor': "$doctor",
              'place': "$place",
              'observations': "$observations",
            }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400) return null;

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return json.decode(utf8.decode(response.bodyBytes));
      }
      return null;
    }

    //If fails to return a state, returns the error code
    return null;
  } catch (e) {
    return null;
  }
}

Future<String> deleteAppointmentOnCalendar(int id, String patient) async {
  var url = urlBase + "api/patients/$patient/appointments/$id";

  try {
    String token = await getToken();

    var response = await http.delete(
      url,
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode >= 400) return null;

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return utf8.decode(response.bodyBytes);
      }
      return null;
    }

    //If fails to return a state, returns the error code
    return null;
  } catch (e) {
    return null;
  }
}

Future<Map> get2MonthsEntries(String patient, int year, int month) async {
  var url = urlBase + "api/patients/$patient/diary/$year/$month";

  try {
    String token = await getToken();

    var response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400) return null;

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return json.decode(utf8.decode(response.bodyBytes));
      }
      return null;
    }
    return null;
  } catch (e) {
    print("catch $e");
    return null;
  }
}

Future<int> getDiaryNotification(String patient) async {
  var url = urlBase + "api/patients/$patient/alert/diary";

  try {
    String token = await getToken();

    var response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400) return null;

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return json.decode(utf8.decode(response.bodyBytes))["id"] ?? -1;
      }
      return null;
    }
    return null;
  } catch (e) {
    return null;
  }
}

Future<List> getPastNotifications(String patient) async {
  var url = urlBase + "api/patients/$patient/alerts";

  try {
    String token = await getToken();

    var response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400) return null;

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return json.decode(utf8.decode(response.bodyBytes));
      }
      return null;
    }
    return null;
  } catch (e) {
    return null;
  }
}

Future<bool> updateNotificationStatus(String patient, String alertID) async {
  var url = urlBase + "api/patients/$patient/alerts/$alertID";

  try {
    String token = await getToken();

    var response = await http.put(
      url,
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer $token'
      },
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });

    //Timeout
    if (response == null) return null;

    //Possible Server problem?
    if (response.statusCode < 200 || response.statusCode > 400) return null;

    //verify if the response is in JSON format
    if (response.headers.containsKey('content-type')) {
      if (response.headers['content-type'].contains("application/json")) {
        return response.body == "true";
      }
      return null;
    }
    return null;
  } catch (e) {
    return null;
  }
}

Future<bool> checkConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('connected');
      return true;
    }
    return false;
  } on SocketException catch (_) {
    print('not connected');
    return false;
  }
}
