import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class UserManual extends StatefulWidget {
  @override
  _UserManualState createState() => _UserManualState();
}

class _UserManualState extends State<UserManual> {
  YoutubePlayerController _controller;

  PlayerState _playerState;
  YoutubeMetaData _videoMetaData;
  bool _muted = false;
  bool _isPlayerReady = false;
  int value = 0;

  List a = [
    "https://www.youtube.com/watch?v=FXnKtpTAh04&feature=youtu.be",
    "https://www.youtube.com/watch?v=RrfwWa2pK9k&feature=youtu.be",
    "https://www.youtube.com/watch?v=P1DysS_6F9Y&feature=youtu.be",
    "https://www.youtube.com/watch?v=nb6LNvGwQyc&feature=youtu.be",
    "https://www.youtube.com/watch?v=SpGuX3u_ctc&feature=youtu.be",
    "https://www.youtube.com/watch?v=J7vYO9ML97Q&feature=youtu.be",
    "https://www.youtube.com/watch?v=Drkmabd87aM&feature=youtu.be",
    "https://www.youtube.com/watch?v=ZfBqyJilOBE&feature=youtu.be",
    "https://www.youtube.com/watch?v=yTHP_ZKtduE&feature=youtu.be",
  ];

  String ids(int ind) {
    String videoId = "";

    videoId = YoutubePlayer.convertUrlToId(a[ind]);
    return videoId;
  }

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: ids(0),
      flags: YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: true,
      ),
    )..addListener(listener);
    _videoMetaData = YoutubeMetaData();
    _playerState = PlayerState.unknown;
  }

  void listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(),
            Center(
              child: Text(
                "MY HEALTH DIARY",
                style: TextStyle(color: Color(0xff3f7380)),
              ),
            ),
          ],
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/guia.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Guia de Utilização",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            YoutubePlayer(
              controller: _controller,
              showVideoProgressIndicator: true,
              progressIndicatorColor: Colors.blueAccent,
              topActions: <Widget>[
                SizedBox(width: 8.0),
                Expanded(
                  child: Text(
                    _controller.metadata.title,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                ),
              ],
              onReady: () {
                _isPlayerReady = true;
              },
              onEnded: (data) {
                _controller.load(ids(value + 1));
                setState(() {
                  value += 1;
                });
              },
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      IconButton(
                        icon: Icon(Icons.skip_previous),
                        onPressed: _isPlayerReady
                            ? () {
                                _controller.load(ids(value - 1));
                                setState(() {
                                  value -= 1;
                                });
                              }
                            : null,
                      ),
                      IconButton(
                        icon: Icon(_muted ? Icons.volume_off : Icons.volume_up),
                        onPressed: _isPlayerReady
                            ? () {
                                _muted
                                    ? _controller.unMute()
                                    : _controller.mute();
                                setState(() {
                                  _muted = !_muted;
                                });
                              }
                            : null,
                      ),
                      IconButton(
                        icon: Icon(Icons.skip_next),
                        onPressed: _isPlayerReady
                            ? () {
                                _controller.load(ids(value + 1));
                                setState(() {
                                  value += 1;
                                });
                              }
                            : null,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              color: Color(0xffdbeded),
              child: ListTile(
                title: Text("Como posso ativar a minha conta pessoal?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(0));
                        setState(() {
                          value = 0;
                        });
                      }
                    : null,
                trailing: value == 0
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
            Container(
              color: Colors.white,
              child: ListTile(
                title: Text(
                    "Onde posso rever e consultar os termos e política de privacidade de dados?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(1));
                        setState(() {
                          value = 1;
                        });
                      }
                    : null,
                trailing: value == 1
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
            Container(
              color: Color(0xffdbeded),
              child: ListTile(
                title: Text("Como posso preencher o meu diário?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(2));
                        setState(() {
                          value = 2;
                        });
                      }
                    : null,
                trailing: value == 2
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
            Container(
              color: Colors.white,
              child: ListTile(
                title: Text("Como posso marcar uma consulta?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(3));
                        setState(() {
                          value = 3;
                        });
                      }
                    : null,
                trailing: value == 3
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
            Container(
              color: Color(0xffdbeded),
              child: ListTile(
                title: Text("Como posso editar o meu diário?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(4));
                        setState(() {
                          value = 4;
                        });
                      }
                    : null,
                trailing: value == 4
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
            Container(
              color: Colors.white,
              child: ListTile(
                title: Text("Onde posso consultar as minha notificações?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(5));
                        setState(() {
                          value = 5;
                        });
                      }
                    : null,
                trailing: value == 5
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
            Container(
              color: Color(0xffdbeded),
              child: ListTile(
                title: Text("Como posso alterar os meus dados pessoais?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(6));
                        setState(() {
                          value = 6;
                        });
                      }
                    : null,
                trailing: value == 6
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
            Container(
              color: Colors.white,
              child: ListTile(
                title: Text("Como posso alterar a minha palavra-passe?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(7));
                        setState(() {
                          value = 7;
                        });
                      }
                    : null,
                trailing: value == 7
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
            Container(
              color: Color(0xffdbeded),
              child: ListTile(
                title: Text("Como posso apagar a minha conta?"),
                onTap: _isPlayerReady
                    ? () {
                        _controller.load(ids(8));
                        setState(() {
                          value = 8;
                        });
                      }
                    : null,
                trailing: value == 8
                    ? Icon(Icons.play_arrow)
                    : Container(
                        width: 0.0,
                        height: 0.0,
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
