import 'package:flutter/material.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/daily_report/daily_report.dart';
import 'package:headache_diary/screens_component/read_diary/remove_diary_dialog.dart';

class ReadDairy extends StatefulWidget {
  final String currentDate;
  final DateTime chosenDate;
  final Map entry;
  final Function() removeDiarySuccessfully;
  ReadDairy(this.currentDate, this.chosenDate, this.entry,
      this.removeDiarySuccessfully);

  @override
  _ReadDairyState createState() => _ReadDairyState();
}

class _ReadDairyState extends State<ReadDairy> {
  final User user = User();

  Map current;
  @override
  void initState() {
    super.initState();
    current = widget.entry;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(
              color: Color(0xff3f7380),
            ),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/diary.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Diário",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30.0),
              child: Text(
                widget.currentDate,
                style: TextStyle(color: Color(0xff3f7380), fontSize: 17.0),
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              width: MediaQuery.of(context).size.width * 0.85,
              height: 300.0,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff3f7380), width: 2.0),
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        "Qualidade da cefaleia: ",
                        style: TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff3f7380),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Text("${current["migraine"] ? "Enxaqueca" : "Tensão"}"),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Intensidade da dor: ",
                        style: TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff3f7380),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Text("${current["headacheIntensity"]}"),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Tomou analgésico: ",
                        style: TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff3f7380),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Text("${current["tookPainKillers"] ? "Sim" : "Não"}"),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Tomou triptano: ",
                        style: TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff3f7380),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Text("${current["tookTriptans"] ? "Sim" : "Não"}"),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Faltou ao trabalho: ",
                        style: TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff3f7380),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Text("${current["workIncapacity"] ? "Sim" : "Não"}"),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Recorreu a um serviço de urgência: ",
                        style: TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff3f7380),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Text(
                          "${current["emergencyServiceRecurrence"] ? "Sim" : "Não"}"),
                    ],
                  ),
                  user.gender.startsWith("f") || user.gender.startsWith("F")
                      ? Row(
                          children: <Widget>[
                            Text(
                              "Menstruação: ",
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff3f7380),
                              ),
                            ),
                            SizedBox(width: 5.0),
                            Text("${current["menstruation"] ? "Sim" : "Não"}"),
                          ],
                        )
                      : Container(),
                ],
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            widget.chosenDate
                    .isAfter(DateTime.now().subtract(Duration(days: 8)))
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      RaisedButton(
                        child: Text(
                          "Apagar",
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                          ),
                        ),
                        color: Colors.white,
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context1) {
                                // retorna um objeto do tipo Dialog
                                return RemoveDiaryDialog(
                                    widget.chosenDate,
                                    widget.entry,
                                    widget.removeDiarySuccessfully);
                              });
                        },
                      ),
                      RaisedButton(
                        child: Text(
                          "Editar",
                          style: TextStyle(
                            color: Color(0xff3f7380),
                            fontSize: 15,
                          ),
                        ),
                        color: Colors.white,
                        onPressed: () async {
                          List a = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      DailyReport(
                                          widget.currentDate, widget.chosenDate,
                                          entry: current)));

                          setState(() {
                            current = a[0];
                          });
                        },
                      ),
                    ],
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
