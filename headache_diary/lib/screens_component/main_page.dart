import 'package:flutter/material.dart';

import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/consult/consult.dart';
import 'package:headache_diary/screens_component/consult/read_consult.dart';
import 'package:headache_diary/screens_component/daily_report/daily_report.dart';
import 'package:headache_diary/screens_component/drawer.dart';
import 'package:headache_diary/screens_component/read_diary/read_diary.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';

import 'package:intl/intl.dart';

import 'package:table_calendar/table_calendar.dart';
import 'package:toast/toast.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  CalendarController _calendarController;

  User user = User();
  DataBase db = DataBase();

  final f = new DateFormat('yyyy-MM-dd');

  bool loadingEvents = false;
  bool consult = false;
  bool diary = true;
  bool hasDiary = false;
  bool hasConsult = false;
  bool is7Days = true;

  DateTime currentSearch;
  DateTime chosenDate;
  String todayDate;
  List entry;

  void addDiarySuccessfully() {
    setState(() {
      diary = false;
      hasDiary = true;
    });
  }

  void addConsultSuccessfully() {
    setState(() {
      consult = false;
      hasConsult = true;
    });
  }

  void removeConsultSuccessfully() {
    setState(() {
      consult = true;
      hasConsult = false;
    });
  }

  void removeDiarySuccessfully() {
    setState(() {
      diary = true;
      hasDiary = false;
    });
  }

  @override
  void initState() {
    super.initState();
    currentSearch = DateTime.now();
    chosenDate =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

    todayDate = buildDate(chosenDate);
    _calendarController = CalendarController();

    if (user.allEvents[f.parse(f.format(DateTime.now()))] != null) {
      if (user.allEvents[f.parse(f.format(DateTime.now()))][0]["migraine"] !=
          null) {
        setState(() {
          hasDiary = true;
        });
        if (user.allEvents[f.parse(f.format(DateTime.now()))].length == 2 &&
            user.allEvents[f.parse(f.format(DateTime.now()))][1]["doctor"] !=
                null) {
          setState(() {
            hasConsult = true;
          });
        }
      } else if (user.allEvents[f.parse(f.format(DateTime.now()))][0]
              ["doctor"] !=
          null) {
        setState(() {
          hasConsult = true;
        });
      }
      entry = user.allEvents[f.parse(f.format(DateTime.now()))];
    } else {
      setState(() {
        hasDiary = false;
        hasConsult = false;
      });
    }
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey.shade100,
      drawer: DrawerMenu(),
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            color: Color(0xff707070),
          ),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 45.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/calendario.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Calendário",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            RefreshIndicator(
              color: Color(0xff3f7380),
              onRefresh: () async {
                String expireTokenString = await getExpireToken();
                DateTime expireToken = DateTime.parse(expireTokenString);
                if (expireToken.isAfter(DateTime.now())) {
                  bool isConnected = await checkConnection();
                  if (isConnected) {
                    Map entries = await get2MonthsEntries(
                        user.code, DateTime.now().year, DateTime.now().month);
                    if (entries != null) {
                      entries.forEach((key, value) {
                        List a = List();

                        if (value["entry"] != null) {
                          a.add(value["entry"]);
                        }
                        if (value["medicalAppointment"] != null) {
                          a.add(value["medicalAppointment"]);
                        }

                        user.allEvents[f.parse(key)] = a;
                      });

                      DateTime twoMonths = DateTime(
                          DateTime.now().year, DateTime.now().month - 1, 1);

                      while (twoMonths.isBefore(DateTime.now())) {
                        if (user.allEvents[f.parse(f.format(twoMonths))] ==
                            null) {
                          user.allHolidays[f.parse(f.format(twoMonths))] = [
                            "sem preenchimento"
                          ];
                        }
                        twoMonths = twoMonths.add(Duration(days: 1));
                      }

                      if (user.allEvents[f.parse(f.format(chosenDate))] !=
                          null) {
                        if (user.allEvents[f.parse(f.format(chosenDate))][0]
                                ["migraine"] !=
                            null) {
                          setState(() {
                            hasDiary = true;
                          });
                          if (user.allEvents[f.parse(f.format(chosenDate))]
                                      .length ==
                                  2 &&
                              user.allEvents[f.parse(f.format(chosenDate))][1]
                                      ["doctor"] !=
                                  null) {
                            setState(() {
                              hasConsult = true;
                            });
                          }
                        } else if (user.allEvents[f.parse(f.format(chosenDate))]
                                [0]["doctor"] !=
                            null) {
                          setState(() {
                            hasConsult = true;
                          });
                        }
                        entry = user.allEvents[f.parse(f.format(chosenDate))];
                      } else {
                        setState(() {
                          hasDiary = false;
                          hasConsult = false;
                        });
                      }
                    }
                  } else {
                    Toast.show(
                        "Não está ligado à internet! \nPor esse motivo, não serão apresentadas as suas informações!",
                        context,
                        duration: 8,
                        gravity: Toast.BOTTOM);
                  }
                } else {
                  Toast.show("Login expirou! Por favor inicie sessão.", context,
                      duration: 5, gravity: Toast.BOTTOM);
                  user.logged = 0;
                  db.updateUserState(user).then((val) async {
                    user.id = null;

                    await flutterLocalNotificationsPlugin.cancelAll();

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => SignInPage()),
                        (Route<dynamic> route) => false);
                  });
                }
              },
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0, top: 20.0),
                        child: Text(
                          "Bem-vindo",
                          style: TextStyle(fontSize: 15.0),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 150.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Text(
                          todayDate,
                          style: TextStyle(
                              color: Color(0xff3f7380), fontSize: 17.0),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Color(0xff92cdd3), width: 2.0),
                            // color: Colors.teal.shade50,
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(10.0),
                              bottom: Radius.circular(
                                  !hasDiary && !hasConsult && !is7Days
                                      ? 10.0
                                      : 0.0),
                            ),
                          ),
                          child: TableCalendar(
                            onVisibleDaysChanged: (a, b, r) async {
                              if (b.isBefore(DateTime(currentSearch.year,
                                  currentSearch.month - 1, 1))) {
                                setState(() {
                                  loadingEvents = true;
                                });
                                String expireTokenString =
                                    await getExpireToken();
                                DateTime expireToken =
                                    DateTime.parse(expireTokenString);
                                if (expireToken.isAfter(DateTime.now())) {
                                  bool isConnected = await checkConnection();
                                  if (isConnected) {
                                    Map entries = await get2MonthsEntries(
                                        user.code,
                                        currentSearch.year,
                                        (currentSearch.month - 2) % 12 == 0
                                            ? 12
                                            : (currentSearch.month - 2) % 12);
                                    currentSearch = DateTime(currentSearch.year,
                                        currentSearch.month - 1, 1);

                                    entries.forEach((key, value) {
                                      List a = List();

                                      if (value["entry"] != null) {
                                        a.add(value["entry"]);
                                      }
                                      if (value["medicalAppointment"] != null) {
                                        a.add(value["medicalAppointment"]);
                                      }
                                      setState(() {
                                        user.allEvents[f.parse(key)] = a;
                                      });
                                    });

                                    DateTime twoMonths = DateTime(
                                        currentSearch.year,
                                        currentSearch.month - 1,
                                        1);

                                    while (twoMonths.isBefore(currentSearch)) {
                                      if (user.allEvents[
                                              f.parse(f.format(twoMonths))] ==
                                          null) {
                                        setState(() {
                                          user.allHolidays[
                                              f.parse(f.format(twoMonths))] = [
                                            "sem preenchimento"
                                          ];
                                        });
                                      }
                                      twoMonths =
                                          twoMonths.add(Duration(days: 1));
                                    }
                                  } else {
                                    Toast.show(
                                        "Não está ligado à internet! \nPor esse motivo, não serão apresentadas as suas informações!",
                                        context,
                                        duration: 8,
                                        gravity: Toast.BOTTOM);
                                  }
                                } else {
                                  Toast.show(
                                      "Login expirou! Por favor inicie sessão.",
                                      context,
                                      duration: 5,
                                      gravity: Toast.BOTTOM);
                                  user.logged = 0;
                                  db.updateUserState(user).then((val) async {
                                    user.id = null;

                                    await flutterLocalNotificationsPlugin
                                        .cancelAll();

                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                SignInPage()),
                                        (Route<dynamic> route) => false);
                                  });
                                }
                              }
                            },
                            onDaySelected: (d, eventsOfDay) =>
                                pickDay(f.parse(f.format(d)), eventsOfDay),
                            calendarStyle: CalendarStyle(
                              outsideDaysVisible: false,
                              selectedColor: Color(0xff92cdd3),
                              markersColor: Color(0xff3f7380),
                              todayColor: Color(0xffdbeded),
                              todayStyle: TextStyle(
                                color: Color(0xff3f7380),
                              ),
                              weekdayStyle: TextStyle(
                                color: Color(0xff3f7380),
                              ),
                              weekendStyle: TextStyle(
                                color: Color(0xff3f7380).withOpacity(0.7),
                              ),
                            ),
                            daysOfWeekStyle: DaysOfWeekStyle(
                              weekdayStyle: TextStyle(
                                  color: Color(0xff3f7380),
                                  fontWeight: FontWeight.bold),
                              weekendStyle: TextStyle(
                                  color: Color(0xff3f7380).withOpacity(0.7),
                                  fontWeight: FontWeight.bold),
                              dowTextBuilder: (date, locale) =>
                                  DateFormat.E(locale)
                                      .format(date)
                                      .substring(0, 3),
                            ),
                            startingDayOfWeek: StartingDayOfWeek.monday,
                            rowHeight: 50.0,
                            availableCalendarFormats: {
                              CalendarFormat.month: 'Month'
                            },
                            startDay: DateTime(2019),
                            endDay: DateTime(2030),
                            locale: 'pt_PT',
                            calendarController: _calendarController,
                            events: user.allEvents,
                            holidays: user.allHolidays,
                          ),
                        ),
                      ),
                      !hasDiary && !hasConsult && is7Days
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Color(0xff92cdd3), width: 2.0),
                                  //color: Color(0xff92cdd3),
                                  borderRadius: BorderRadius.vertical(
                                    bottom: Radius.circular(
                                        hasDiary || hasConsult ? 0.0 : 10.0),
                                  ),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    diary
                                        ? FlatButton(
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.add,
                                                  color: Color(0xff3f7380),
                                                ),
                                                SizedBox(width: 10.0),
                                                Text(
                                                  "Diário",
                                                  style: TextStyle(
                                                    fontSize: 16.0,
                                                    color: Color(0xff3f7380),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            onPressed: () async {
                                              entry = await Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (BuildContext
                                                              context) =>
                                                          DailyReport(todayDate,
                                                              chosenDate,
                                                              addDiarySuccessfully:
                                                                  addDiarySuccessfully)));
                                            },
                                          )
                                        : Container(),
                                    consult && diary
                                        ? Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 15.0),
                                            child: Container(
                                              width: 1,
                                              height: 30.0,
                                              color: Colors.grey,
                                            ))
                                        : Container(),
                                    consult
                                        ? FlatButton(
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.add,
                                                  color: Color(0xff3f7380),
                                                ),
                                                SizedBox(width: 10.0),
                                                Text(
                                                  "Consulta",
                                                  style: TextStyle(
                                                    fontSize: 16.0,
                                                    color: Color(0xff3f7380),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            onPressed: () async {
                                              entry = await Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (BuildContext
                                                              context) =>
                                                          Consult(todayDate,
                                                              chosenDate,
                                                              addConsultSuccessfully:
                                                                  addConsultSuccessfully)));
                                            },
                                          )
                                        : Container(),
                                  ],
                                ),
                              ),
                            )
                          : Container(),
                      hasDiary || hasConsult
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Color(0xff92cdd3), width: 2.0),
                                  color: Color(0xffdbeded),
                                  borderRadius: BorderRadius.vertical(
                                    bottom: Radius.circular(10.0),
                                  ),
                                ),
                                child: Column(
                                  children: <Widget>[
                                    hasDiary
                                        ? Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: InkWell(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                    "Ver diário ",
                                                    style: TextStyle(
                                                      fontSize: 16.0,
                                                      color: Color(0xff3f7380),
                                                    ),
                                                  ),
                                                  Icon(Icons.arrow_right),
                                                ],
                                              ),
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (BuildContext
                                                                context) =>
                                                            ReadDairy(
                                                                todayDate,
                                                                chosenDate,
                                                                entry[0],
                                                                removeDiarySuccessfully)));
                                              },
                                            ),
                                          )
                                        : Container(),
                                    hasConsult
                                        ? Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: InkWell(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                    "Ver consulta ",
                                                    style: TextStyle(
                                                      fontSize: 16.0,
                                                      color: Color(0xff3f7380),
                                                    ),
                                                  ),
                                                  Icon(Icons.arrow_right),
                                                ],
                                              ),
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (BuildContext
                                                                context) =>
                                                            ReadConsult(
                                                                todayDate,
                                                                chosenDate,
                                                                hasDiary
                                                                    ? entry[1]
                                                                    : entry[0],
                                                                removeConsultSuccessfully)));
                                              },
                                            ),
                                          )
                                        : Container(),
                                  ],
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void pickDay(d, event) {
    setState(() {
      chosenDate = d;
      todayDate = buildDate(d);
    });

    if (chosenDate.isAfter(f.parse(f.format(DateTime.now())))) {
      if (event.length != 0) {
        setState(() {
          hasConsult = true;
          hasDiary = false;
        });
        entry = event;
      } else {
        setState(() {
          consult = true;
          diary = false;
          hasDiary = false;
          hasConsult = false;
          is7Days = true;
        });
      }
    } else {
      setState(() {
        consult = false;
        diary = true;
      });

      if (event.length != 0) {
        if (event[0]["migraine"] != null) {
          setState(() {
            hasDiary = true;
            hasConsult = false;
          });
          if (event.length == 2 && event[1]["doctor"] != null) {
            setState(() {
              hasConsult = true;
            });
          }
        } else if (event[0]["doctor"] != null) {
          setState(() {
            hasConsult = true;
            hasDiary = false;
          });
        }
        entry = event;
      } else {
        if (chosenDate.isBefore(
            f.parse(f.format(DateTime.now().subtract(Duration(days: 7)))))) {
          setState(() {
            hasDiary = false;
            hasConsult = false;
            is7Days = false;
          });
        } else {
          setState(() {
            hasDiary = false;
            hasConsult = false;
            is7Days = true;
          });
        }
      }
    }
  }

  String buildDate(DateTime dt) {
    int day = dt.day;
    int month = dt.month;
    int year = dt.year;
    int weekday = dt.weekday;
    String weekdayDesc = "";
    String monthDesc = "";

    switch (weekday) {
      case 1:
        weekdayDesc = "Segunda-feira";
        break;
      case 2:
        weekdayDesc = "Terça-feira";
        break;
      case 3:
        weekdayDesc = "Quarta-feira";
        break;
      case 4:
        weekdayDesc = "Quinta-feira";
        break;
      case 5:
        weekdayDesc = "Sexta-feira";
        break;
      case 6:
        weekdayDesc = "Sábado";
        break;
      case 7:
        weekdayDesc = "Domingo";
        break;
      default:
        break;
    }
    switch (month) {
      case 1:
        monthDesc = "Janeiro";
        break;
      case 2:
        monthDesc = "Fevereiro";
        break;
      case 3:
        monthDesc = "Março";
        break;
      case 4:
        monthDesc = "Abril";
        break;
      case 5:
        monthDesc = "Maio";
        break;
      case 6:
        monthDesc = "Junho";
        break;
      case 7:
        monthDesc = "Julho";
        break;
      case 8:
        monthDesc = "Agosto";
        break;
      case 9:
        monthDesc = "Setembro";
        break;
      case 10:
        monthDesc = "Outubro";
        break;
      case 11:
        monthDesc = "Novembro";
        break;
      case 12:
        monthDesc = "Dezembro";
        break;
      default:
        break;
    }

    return "$weekdayDesc, $day de $monthDesc de $year";
  }
}
