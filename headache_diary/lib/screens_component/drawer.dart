import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/about_us.dart';
import 'package:headache_diary/screens_component/notifications/notificationsPage.dart';
import 'package:headache_diary/screens_component/profile/baseProfile.dart';
import 'package:headache_diary/screens_component/rgpd_consent.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/screens_component/user_manual.dart';
import 'package:headache_diary/utils/database.dart';

class DrawerMenu extends StatefulWidget {
  @override
  _DrawerMenuState createState() => _DrawerMenuState();
}

class _DrawerMenuState extends State<DrawerMenu> {
  User user = User();
  DataBase db = DataBase();
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Container(
              child: Image.asset(
            "assets/images/Untitled-1.png",
            height: 130.0,
          )),
          Container(
            color: Color(0xffdbeded),
            child: ListTile(
              leading: Image.asset(
                "assets/images/perfil.png",
                width: 25.0,
              ),
              title: Text(
                "Perfil",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => ProfileBase()));
              },
            ),
          ),
          Container(
            color: Colors.grey.shade100,
            child: ListTile(
              leading: Image.asset(
                "assets/images/notifica.png",
                width: 25.0,
              ),
              title: Text(
                "Notificações",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            NotificationsPage()));
              },
            ),
          ),
          Container(
            color: Color(0xffdbeded),
            child: ListTile(
              leading: Image.asset(
                "assets/images/guia.png",
                width: 25.0,
              ),
              title: Text(
                "Guia de Utilização",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => UserManual()));
              },
            ),
          ),
          Container(
            color: Colors.grey.shade100,
            child: ListTile(
              leading: Image.asset(
                "assets/images/info.png",
                width: 25.0,
              ),
              title: Text(
                "Sobre Nós",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => AboutUs()));
              },
            ),
          ),
          Container(
            color: Color(0xffdbeded),
            child: ListTile(
              leading: Image.asset(
                "assets/images/verified.png",
                width: 25.0,
              ),
              title: Text(
                "RGPD",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => RGPDConsent()));
              },
            ),
          ),
          Container(
            color: Colors.grey.shade100,
            child: ListTile(
              leading: Image.asset(
                "assets/images/login.png",
                width: 25.0,
              ),
              title: Text(
                "Terminar Sessão",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                user.logged = 0;
                db.updateUserState(user).then((val) async {
                  user.id = null;

                  await flutterLocalNotificationsPlugin.cancelAll();

                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => SignInPage()),
                      (Route<dynamic> route) => false);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
