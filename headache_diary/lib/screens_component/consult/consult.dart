import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class Consult extends StatefulWidget {
  final String currentDate;
  final DateTime chosenDate;
  final Map consult;
  final Function() addConsultSuccessfully;

  Consult(this.currentDate, this.chosenDate,
      {this.consult, this.addConsultSuccessfully});
  @override
  _ConsultState createState() => _ConsultState();
}

class _ConsultState extends State<Consult> {
  final _typeController = TextEditingController();
  final _doctorController = TextEditingController();
  final _placeController = TextEditingController();
  final _obsController = TextEditingController();

  User user = User();
  DataBase db = DataBase();

  DateTime consultDateWithTime = DateTime.now();

  final f = new DateFormat('yyyy-MM-dd');
  bool emergency = false;
  String time = "";
  TimeOfDay tod;

  bool buttonPressed = false;

  @override
  void initState() {
    super.initState();
    if (widget.consult != null) {
      _typeController.text = widget.consult["type"];
      _doctorController.text = widget.consult["doctor"];
      _placeController.text = widget.consult["place"];
      _obsController.text = widget.consult["observations"];
      time =
          "${widget.consult["dateTime"][3]}h${widget.consult["dateTime"][4] == 0 ? "" : widget.consult["dateTime"][4]}";
      tod = TimeOfDay(
          hour: widget.consult["dateTime"][3],
          minute: widget.consult["dateTime"][4]);
    } else {
      tod = TimeOfDay(hour: 10, minute: 0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(
              color: Color(0xff3f7380),
            ),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/consulta.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Consulta",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30.0),
              child: Text(
                widget.currentDate,
                style: TextStyle(color: Color(0xff3f7380), fontSize: 17.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  width: MediaQuery.of(context).size.width * 0.85,
                  height: 300.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xff3f7380), width: 2.0),
                    color: Color(0xffdbeded),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            "Consulta",
                            style: TextStyle(
                              color: Color(0xff3f7380),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(width: 10.0),
                          Expanded(
                            child: TextField(
                              controller: _typeController,
                              keyboardType: TextInputType.text,
                              cursorColor: Color(0xff3f7380),
                              style: TextStyle(fontSize: 14.0),
                              decoration: InputDecoration(
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xff3f7380),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xff3f7380),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 5.0),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Hora",
                            style: TextStyle(
                              color: Color(0xff3f7380),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: Theme(
                              data: Theme.of(context).copyWith(
                                primaryColor: Color(
                                    0xff92cdd3), //color you want at header

                                buttonTheme: ButtonTheme.of(context).copyWith(
                                  colorScheme: ColorScheme.light(
                                    primary: Color(
                                        0xff3f7380), // Color you want for action buttons (CANCEL and OK)
                                  ),
                                ),
                              ),
                              child: Builder(
                                builder: (context) => IconButton(
                                    icon: Icon(
                                      Icons.access_alarm,
                                      color: Color(0xff3f7380),
                                    ),
                                    onPressed: () {
                                      showTimePicker(
                                              context: context,
                                              initialTime: TimeOfDay(
                                                  hour: tod.hour,
                                                  minute: tod.minute))
                                          .then((t) {
                                        setState(() {
                                          tod = t ??
                                              TimeOfDay(hour: 10, minute: 0);
                                          time =
                                              "${tod.hour}h${tod.minute == 0 ? "" : tod.minute}";
                                          consultDateWithTime = DateTime(
                                              widget.chosenDate.year,
                                              widget.chosenDate.month,
                                              widget.chosenDate.day,
                                              tod.hour,
                                              tod.minute);
                                        });
                                      });
                                    }),
                              ),
                            ),
                          ),
                          Text(time),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Médico",
                            style: TextStyle(
                              color: Color(0xff3f7380),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(width: 10.0),
                          Expanded(
                            child: TextField(
                              controller: _doctorController,
                              keyboardType: TextInputType.text,
                              cursorColor: Color(0xff3f7380),
                              style: TextStyle(fontSize: 14.0),
                              decoration: InputDecoration(
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xff3f7380),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xff3f7380),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 5.0),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Local",
                            style: TextStyle(
                              color: Color(0xff3f7380),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(width: 10.0),
                          Expanded(
                            child: TextField(
                              controller: _placeController,
                              keyboardType: TextInputType.text,
                              cursorColor: Color(0xff3f7380),
                              style: TextStyle(fontSize: 14.0),
                              decoration: InputDecoration(
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xff3f7380),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xff3f7380),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 5.0),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Obs",
                            style: TextStyle(
                              color: Color(0xff3f7380),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(width: 10.0),
                          Expanded(
                            child: TextField(
                              controller: _obsController,
                              keyboardType: TextInputType.text,
                              cursorColor: Color(0xff3f7380),
                              style: TextStyle(fontSize: 14.0),
                              decoration: InputDecoration(
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xff3f7380),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xff3f7380),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 5.0),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          buttonPressed
                              ? Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Container(
                                    height: 20.0,
                                    width: 20.0,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2.0,
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Color(0xff3f7380)),
                                    ),
                                  ),
                                )
                              : FlatButton(
                                  child: Text(
                                    "Finalizar",
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      color: Color(0xff3f7380),
                                    ),
                                  ),
                                  onPressed: () async {
                                    if (!buttonPressed) {
                                      setState(() {
                                        buttonPressed = true;
                                      });
                                      String expireTokenString =
                                          await getExpireToken();
                                      DateTime expireToken =
                                          DateTime.parse(expireTokenString);
                                      if (expireToken.isAfter(DateTime.now())) {
                                        bool isConnected =
                                            await checkConnection();
                                        if (isConnected) {
                                          if (tod == null) {
                                            tod =
                                                TimeOfDay(hour: 10, minute: 0);
                                          }
                                          if (widget.addConsultSuccessfully !=
                                              null) {
                                            Map resp =
                                                await createAppointmentOnCalendar(
                                              user.code,
                                              _typeController.text ?? "",
                                              [
                                                widget.chosenDate.year,
                                                widget.chosenDate.month,
                                                widget.chosenDate.day,
                                                tod.hour,
                                                tod.minute,
                                                0,
                                                0
                                              ],
                                              _doctorController.text ?? "",
                                              _placeController.text ?? "",
                                              _obsController.text ?? "",
                                            );

                                            if (resp != null) {
                                              user.allEvents[
                                                  widget.chosenDate] = [
                                                {
                                                  "id": resp[
                                                      "medicalAppointmentId"],
                                                  "dateTime": [
                                                    widget.chosenDate.year,
                                                    widget.chosenDate.month,
                                                    widget.chosenDate.day,
                                                    tod.hour ?? 0,
                                                    tod.minute ?? 0,
                                                    0,
                                                    0
                                                  ],
                                                  "doctor":
                                                      _doctorController.text ??
                                                          "",
                                                  "place":
                                                      _placeController.text ??
                                                          "",
                                                  "observations":
                                                      _obsController.text ?? "",
                                                  "type":
                                                      _typeController.text ?? ""
                                                }
                                              ];

                                              widget.addConsultSuccessfully();
                                              _scheduleNotification(
                                                  resp["alert"]["id"],
                                                  DateTime(
                                                      resp["alert"]["dateTime"]
                                                          [0],
                                                      resp["alert"]["dateTime"]
                                                          [1],
                                                      resp["alert"]["dateTime"]
                                                          [2],
                                                      resp["alert"]["dateTime"]
                                                          [3],
                                                      resp["alert"]["dateTime"]
                                                          [4]),
                                                  resp["alert"]["subject"],
                                                  resp["alert"]["message"]);
                                              Toast.show(
                                                  "Consulta adicionada com sucesso!",
                                                  context,
                                                  duration: 5,
                                                  gravity: Toast.BOTTOM);
                                            }
                                          } else if (widget.consult != null) {
                                            Map resp =
                                                await editAppointmentOnCalendar(
                                              widget.consult["id"],
                                              user.code,
                                              _typeController.text ?? "",
                                              [
                                                widget.chosenDate.year,
                                                widget.chosenDate.month,
                                                widget.chosenDate.day,
                                                tod.hour,
                                                tod.minute,
                                                0,
                                                0
                                              ],
                                              _doctorController.text ?? "",
                                              _placeController.text ?? "",
                                              _obsController.text ?? "",
                                            );
                                            if (resp != null) {
                                              user.allEvents[
                                                  widget.chosenDate] = [
                                                {
                                                  "id": widget.consult["id"],
                                                  "dateTime": [
                                                    widget.chosenDate.year,
                                                    widget.chosenDate.month,
                                                    widget.chosenDate.day,
                                                    tod.hour ?? 0,
                                                    tod.minute ?? 0,
                                                    0,
                                                    0
                                                  ],
                                                  "doctor":
                                                      _doctorController.text ??
                                                          "",
                                                  "place":
                                                      _placeController.text ??
                                                          "",
                                                  "observations":
                                                      _obsController.text ?? "",
                                                  "type":
                                                      _typeController.text ?? ""
                                                }
                                              ];

                                              await flutterLocalNotificationsPlugin
                                                  .cancel(resp["id"]);
                                              _scheduleNotification(
                                                  resp["id"],
                                                  DateTime(
                                                      resp["dateTime"][0],
                                                      resp["dateTime"][1],
                                                      resp["dateTime"][2],
                                                      resp["dateTime"][3],
                                                      resp["dateTime"][4]),
                                                  resp["subject"],
                                                  resp["message"]);
                                            }
                                          }

                                          Navigator.pop(
                                              context,
                                              user.allEvents[
                                                  widget.chosenDate]);
                                        } else {
                                          Toast.show(
                                              "Ligue-se à internet para poder finalizar a operação!",
                                              context,
                                              duration: 5,
                                              gravity: Toast.BOTTOM);
                                        }
                                      } else {
                                        Toast.show(
                                            "Login expirou! Por favor inicie sessão.",
                                            context,
                                            duration: 5,
                                            gravity: Toast.BOTTOM);
                                        user.logged = 0;
                                        db
                                            .updateUserState(user)
                                            .then((val) async {
                                          user.id = null;

                                          await flutterLocalNotificationsPlugin
                                              .cancelAll();

                                          Navigator.pushAndRemoveUntil(
                                              context,
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          SignInPage()),
                                              (Route<dynamic> route) => false);
                                        });
                                      }
                                      setState(() {
                                        buttonPressed = false;
                                      });
                                    }
                                  },
                                ),
                        ],
                      )
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Future _scheduleNotification(
      int id, DateTime scheduledDateTime, String title, String body) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        "your channel id", "your channel name", "your channel description",
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        id, title, body, scheduledDateTime, platformChannelSpecifics,
        payload: "item x", androidAllowWhileIdle: true);
  }
}
