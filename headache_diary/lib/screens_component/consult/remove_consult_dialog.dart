import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/main_page.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

class RemoveConsultDialog extends StatefulWidget {
  final DateTime chosenDate;
  final Map consult;
  final Function() removeConsultSuccessfully;

  RemoveConsultDialog(
      this.chosenDate, this.consult, this.removeConsultSuccessfully);

  @override
  _RemoveConsultDialogState createState() => _RemoveConsultDialogState();
}

class _RemoveConsultDialogState extends State<RemoveConsultDialog> {
  DataBase db = DataBase();
  User user = User();

  bool dialogRemoveIsPressed = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: dialogRemoveIsPressed
          ? Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xff3f7380)),
                  ),
                ),
              ),
            )
          : Text("Tem a certeza que pretende eliminar esta consulta?"),
      actions: dialogRemoveIsPressed
          ? [Container()]
          : <Widget>[
              // define os botões na base do dialogo

              new FlatButton(
                child: new Text("Cancelar",
                    style: TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              new FlatButton(
                child: new Text("Apagar",
                    style: TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                onPressed: () async {
                  if (!dialogRemoveIsPressed) {
                    setState(() {
                      dialogRemoveIsPressed = true;
                    });
                    String expireTokenString = await getExpireToken();
                    DateTime expireToken = DateTime.parse(expireTokenString);
                    if (expireToken.isAfter(DateTime.now())) {
                      bool isConnected = await checkConnection();
                      if (isConnected) {
                        String resp = await deleteAppointmentOnCalendar(
                            widget.consult["id"], user.code);
                        if (resp != null) {
                          List a = resp.split(new RegExp('='));
                          await flutterLocalNotificationsPlugin
                              .cancel(int.tryParse(a[1]));
                          user.allEvents.removeWhere(
                              (key, value) => key == widget.chosenDate);
                          widget.removeConsultSuccessfully();
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      MainPage()),
                              (Route<dynamic> route) => false);
                        } else {
                          Toast.show("Servidor falhou!", context,
                              duration: 7, gravity: Toast.BOTTOM);
                        }
                      } else {
                        Toast.show(
                            "Não está ligado à internet! \nPor esse motivo, não poderá eliminar a consulta!",
                            context,
                            duration: 7,
                            gravity: Toast.BOTTOM);
                      }
                    } else {
                      Toast.show(
                          "Login expirou! Por favor inicie sessão.", context,
                          duration: 5, gravity: Toast.BOTTOM);
                      user.logged = 0;
                      db.updateUserState(user).then((val) async {
                        user.id = null;

                        await flutterLocalNotificationsPlugin.cancelAll();

                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    SignInPage()),
                            (Route<dynamic> route) => false);
                      });
                    }
                  }
                  setState(() {
                    dialogRemoveIsPressed = false;
                  });
                },
              ),
            ],
    );
  }
}
