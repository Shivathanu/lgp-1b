import 'package:flutter/material.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/consult/consult.dart';
import 'package:headache_diary/screens_component/consult/remove_consult_dialog.dart';
import 'package:intl/intl.dart';

class ReadConsult extends StatefulWidget {
  final String currentDate;
  final DateTime chosenDate;
  final Map consult;
  final Function() removeConsultSuccessfully;

  ReadConsult(this.currentDate, this.chosenDate, this.consult,
      this.removeConsultSuccessfully);

  @override
  _ReadConsultState createState() => _ReadConsultState();
}

class _ReadConsultState extends State<ReadConsult> {
  final f = new DateFormat('yyyy-MM-dd');
  User user = User();

  Map current;
  @override
  void initState() {
    super.initState();
    current = widget.consult;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(
              color: Color(0xff3f7380),
            ),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/consulta.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Consulta",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30.0),
              child: Text(
                widget.currentDate,
                style: TextStyle(color: Color(0xff3f7380), fontSize: 17.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                width: MediaQuery.of(context).size.width * 0.85,
                height: 300.0,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff3f7380), width: 2.0),
                  color: Color(0xffdbeded),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Consulta: ",
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff3f7380),
                          ),
                        ),
                        SizedBox(width: 5.0),
                        Text(
                            "${current["type"] == "" ? " - " : current["type"]}"),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          "Hora ",
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff3f7380),
                          ),
                        ),
                        SizedBox(width: 5.0),
                        Text(
                            "${current["dateTime"][3]}h${current["dateTime"][4] == 0 ? "" : current["dateTime"][4]}"),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          "Médico: ",
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff3f7380),
                          ),
                        ),
                        SizedBox(width: 5.0),
                        Text(
                            "${current["doctor"] == "" ? " - " : current["doctor"]}"),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          "Local: ",
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff3f7380),
                          ),
                        ),
                        SizedBox(width: 5.0),
                        Text(
                            "${current["place"] == "" ? " - " : current["place"]}"),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          "Obs: ",
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff3f7380),
                          ),
                        ),
                        SizedBox(width: 5.0),
                        Text(
                            "${current["observations"] == "" ? " - " : current["observations"]}"),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 30.0),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  widget.chosenDate.isAfter(DateTime.now())
                      ? RaisedButton(
                          child: Text(
                            "Apagar",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 15,
                            ),
                          ),
                          color: Colors.white,
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context1) {
                                  // retorna um objeto do tipo Dialog
                                  return RemoveConsultDialog(
                                      widget.chosenDate,
                                      widget.consult,
                                      widget.removeConsultSuccessfully);
                                });
                          },
                        )
                      : Container(),
                  RaisedButton(
                    child: Text(
                      "Editar",
                      style: TextStyle(
                        color: Color(0xff3f7380),
                        fontSize: 15,
                      ),
                    ),
                    color: Colors.white,
                    onPressed: () async {
                      List a = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => Consult(
                                    widget.currentDate,
                                    widget.chosenDate,
                                    consult: current,
                                  )));
                      setState(() {
                        current = a[0];
                      });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
