import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/info.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Sobre Nós",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 40.0,
                left: 35.0,
                right: 35.0,
              ),
              child: Container(
                padding: EdgeInsets.all(15.0),
                height: 300.0,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff3f7380), width: 2.0),
                  color: Color(0xffdbeded),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                child: Center(
                  child: Text(
                    "O presente projeto foi desenvolvido no âmbito académico, em parceria " +
                        "com a Faculdade de Engenharia da Universidade do Porto. \nO Hospital Pedro " +
                        "Hispano junta-se à Hive Health para a criação de uma aplicação que visa " +
                        "melhorar a qualidade de vida e rotina dos doentes com cefaleias. \nA plataforma " +
                        "propõe uma navegação intuitiva e prática, quer para o utente, quer para o médico.",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color(0xff707070), fontSize: 15.0),
                  ),
                ),
              ),
            ),
            Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Image.asset(
                      "assets/images/HIVE_health.png",
                      width: 100.0,
                    ),
                    Image.asset(
                      "assets/images/Untitled-1.png",
                      width: 100.0,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Image.asset(
                      "assets/images/feup.png",
                      width: 120.0,
                    ),
                  ],
                ),
              ],
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: Text(
                "Para mais informações, por favor consulte as nossas redes sociais e as restantes plataformas",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black, fontSize: 10.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
