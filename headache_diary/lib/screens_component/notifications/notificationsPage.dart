import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/loading_page.dart';
import 'package:headache_diary/screens_component/notifications/notificationItem.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  Future<int> notif;
  User user = User();
  DataBase db = DataBase();

  List notifications = [];
  @override
  void initState() {
    super.initState();
    notif = getPastNotif();
  }

  Future<int> getPastNotif() async {
    await db.getLoggedUser();
    if (user.code != null) {
      String expireTokenString = await getExpireToken();
      DateTime expireToken = DateTime.parse(expireTokenString);
      if (expireToken.isAfter(DateTime.now())) {
        bool isConnected = await checkConnection();
        if (isConnected) {
          notifications = await getPastNotifications(user.code);
          if (notifications == null) {
            notifications = [];
          }
          return 1;
        } else {
          Toast.show(
              "Não está ligado à internet! \nPor esse motivo, não serão apresentadas as suas informações!",
              context,
              duration: 7,
              gravity: Toast.BOTTOM);
        }
      } else {
        Toast.show("Login expirou! Por favor inicie sessão.", context,
            duration: 5, gravity: Toast.BOTTOM);
        user.logged = 0;
        db.updateUserState(user).then((val) async {
          user.id = null;

          await flutterLocalNotificationsPlugin.cancelAll();

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => SignInPage()),
              (Route<dynamic> route) => false);
        });
      }
    }
    notifications = [];
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<int>(
      future: notif,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return LoadingPage();
            break;
          default:
            if (snapshot.hasError) {
              return Scaffold(
                body: Center(
                  child: Container(
                    child: Text("app failed"),
                  ),
                ),
              );
            } else {
              return Scaffold(
                backgroundColor: Colors.grey.shade100,
                appBar: AppBar(
                  backgroundColor: Color(0xffe1e1e1),
                  leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Color(0xff707070),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  title: Center(
                    child: Text(
                      "MY HEALTH DIARY",
                      style: TextStyle(color: Color(0xff3f7380)),
                    ),
                  ),
                  actions: <Widget>[
                    Image.asset("assets/images/Untitled-1.png")
                  ],
                ),
                body: SingleChildScrollView(
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50.0,
                        decoration: BoxDecoration(
                          color: Color(0xffdbeded),
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10.0),
                            bottomRight: Radius.circular(10.0),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              "assets/images/notifica.png",
                              width: 20.0,
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text(
                              "Notificações",
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Color(0xff3f7380),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 30.0,
                          left: 25.0,
                          right: 25.0,
                        ),
                        child: RefreshIndicator(
                          color: Color(0xff3f7380),
                          onRefresh: () async {
                            await db.getLoggedUser();
                            if (user.code != null) {
                              String expireTokenString = await getExpireToken();
                              DateTime expireToken =
                                  DateTime.parse(expireTokenString);
                              if (expireToken.isAfter(DateTime.now())) {
                                bool isConnected = await checkConnection();
                                if (isConnected) {
                                  List result =
                                      await getPastNotifications(user.code);
                                  setState(() {
                                    notifications = result;
                                  });
                                } else {
                                  Toast.show(
                                      "Não está ligado à internet! \nPor esse motivo, não serão apresentadas as suas informações!",
                                      context,
                                      duration: 7,
                                      gravity: Toast.BOTTOM);
                                  setState(() {
                                    notifications = [];
                                  });
                                }
                              } else {
                                Toast.show(
                                    "Login expirou! Por favor inicie sessão.",
                                    context,
                                    duration: 5,
                                    gravity: Toast.BOTTOM);
                                user.logged = 0;
                                db.updateUserState(user).then((val) async {
                                  user.id = null;

                                  await flutterLocalNotificationsPlugin
                                      .cancelAll();

                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              SignInPage()),
                                      (Route<dynamic> route) => false);
                                });
                              }
                            } else {
                              setState(() {
                                notifications = [];
                              });
                            }
                          },
                          child: SingleChildScrollView(
                            physics: AlwaysScrollableScrollPhysics(),
                            child: Container(
                              child: Column(
                                children: [
                                  Container(
                                      padding: EdgeInsets.all(10.0),
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Color(0xff92cdd3),
                                            width: 2.0),
                                        color: Color(0xff92cdd3),
                                        borderRadius: BorderRadius.vertical(
                                          top: Radius.circular(10.0),
                                        ),
                                      ),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/images/alert.png",
                                            width: 20.0,
                                          ),
                                          SizedBox(
                                            width: 15.0,
                                          ),
                                          Text(
                                            "Notificações",
                                            style: TextStyle(
                                              fontSize: 18.0,
                                              color: Color(0xff3f7380),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Container(
                                    constraints: BoxConstraints(
                                      minHeight:
                                          MediaQuery.of(context).size.height *
                                              0.45,
                                      maxHeight:
                                          MediaQuery.of(context).size.height *
                                              0.65,
                                    ),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(0xff92cdd3), width: 2.0),
                                      borderRadius: BorderRadius.vertical(
                                        bottom: Radius.circular(10.0),
                                      ),
                                    ),
                                    child: notifications.isEmpty
                                        ? Container(
                                            color: Color(0xffdbeded),
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Center(
                                                  child: Text(
                                                    "Sem notificações",
                                                    style: TextStyle(
                                                        color: Colors
                                                            .grey.shade600),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        : ListView.separated(
                                            shrinkWrap: true,
                                            itemCount: notifications.length,
                                            separatorBuilder:
                                                (context, index) => Divider(
                                              height: 0,
                                              thickness: 2,
                                            ),
                                            itemBuilder: (context, item) {
                                              return NotificationItem(
                                                  item, notifications[item]);
                                            },
                                          ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
        }
      },
    );
  }
}
