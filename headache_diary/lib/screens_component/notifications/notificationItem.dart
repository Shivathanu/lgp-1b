import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

class NotificationItem extends StatefulWidget {
  final int index;
  final Map item;
  NotificationItem(this.index, this.item);
  @override
  _NotificationItemState createState() => _NotificationItemState();
}

class _NotificationItemState extends State<NotificationItem> {
  User user = User();
  DataBase db = DataBase();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.index % 2 == 0 ? Color(0xffdbeded) : Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "${widget.item["dateTime"][2]}/" +
                    "${widget.item["dateTime"][1]}/" +
                    "${widget.item["dateTime"][0]} - " +
                    "${widget.item["dateTime"][3]}h" +
                    "${widget.item["dateTime"][4]}",
                style: TextStyle(color: Color(0xff3f7380), fontSize: 13.0),
              ),
            ],
          ),
          ListTile(
            trailing: Checkbox(
              activeColor: Color(0xff3f7380),
              value: widget.item["readByRecipient"],
              onChanged: (t) async {
                String expireTokenString = await getExpireToken();
                DateTime expireToken = DateTime.parse(expireTokenString);
                if (expireToken.isAfter(DateTime.now())) {
                  bool isConnected = await checkConnection();
                  if (isConnected) {
                    bool res = await updateNotificationStatus(
                        user.code, "${widget.item["id"]}");
                    setState(() {
                      widget.item["readByRecipient"] = res;
                    });
                  } else {
                    Toast.show(
                        "Não está ligado à internet! \nPor esse motivo, não serão apresentadas as suas informações!",
                        context,
                        duration: 7,
                        gravity: Toast.BOTTOM);
                  }
                } else {
                  Toast.show("Login expirou! Por favor inicie sessão.", context,
                      duration: 5, gravity: Toast.BOTTOM);
                  user.logged = 0;
                  db.updateUserState(user).then((val) async {
                    user.id = null;

                    await flutterLocalNotificationsPlugin.cancelAll();

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => SignInPage()),
                        (Route<dynamic> route) => false);
                  });
                }
              },
            ),
            title: Text(
              widget.item["subject"],
              style: TextStyle(fontSize: 15.0),
            ),
            subtitle: Text(widget.item["message"]),
          )
        ]),
      ),
    );
  }
}
