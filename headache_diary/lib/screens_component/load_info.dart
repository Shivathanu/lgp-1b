import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/loading_page.dart';
import 'package:headache_diary/screens_component/main_page.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

class LoadInfo extends StatefulWidget {
  @override
  _LoadInfoState createState() => _LoadInfoState();
}

class _LoadInfoState extends State<LoadInfo> {
  Future<int> entries;
  User user = User();
  DataBase db = DataBase();

  final f = new DateFormat('yyyy-MM-dd');

  @override
  void initState() {
    super.initState();
    user.allEvents = Map<DateTime, List>();
    user.allHolidays = Map<DateTime, List>();
    entries = getInitInfo();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<int>(
      future: entries,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return LoadingPage();
            break;
          default:
            if (snapshot.hasError) {
              return Scaffold(
                body: Center(
                  child: Container(
                    child: Text("app failed"),
                  ),
                ),
              );
            } else
              return MainPage();
        }
      },
    );
  }

  Future<int> getInitInfo() async {
    Future<int> a;

    /* var pendingNotificationRequests =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();

    print(pendingNotificationRequests); */
    String expireTokenString = await getExpireToken();
    DateTime expireToken = DateTime.parse(expireTokenString);
    if (expireToken.isAfter(DateTime.now())) {
      bool isConnected = await checkConnection();
      if (isConnected) {
        Map entries = await get2MonthsEntries(
            user.code, DateTime.now().year, DateTime.now().month);
        if (entries != null) {
          entries.forEach((key, value) {
            List a = List();

            if (value["entry"] != null) {
              a.add(value["entry"]);
            }
            if (value["medicalAppointment"] != null) {
              a.add(value["medicalAppointment"]);
            }

            user.allEvents[f.parse(key)] = a;
          });

          DateTime twoMonths =
              DateTime(DateTime.now().year, DateTime.now().month - 1, 1);

          while (twoMonths.isBefore(DateTime.now())) {
            if (user.allEvents[f.parse(f.format(twoMonths))] == null) {
              user.allHolidays[f.parse(f.format(twoMonths))] = [
                "sem preenchimento"
              ];
            }
            twoMonths = twoMonths.add(Duration(days: 1));
          }
        } else {
          Toast.show("Servidor falhou!", context,
              duration: 7, gravity: Toast.BOTTOM);
        }
      } else {
        Toast.show(
            "Não está ligado à internet! \nPor esse motivo, não serão apresentadas as suas informações!",
            context,
            duration: 7,
            gravity: Toast.BOTTOM);
      }
    } else {
      Toast.show("Login expirou! Por favor inicie sessão.", context,
          duration: 5, gravity: Toast.BOTTOM);
      user.logged = 0;
      db.updateUserState(user).then((val) async {
        user.id = null;

        await flutterLocalNotificationsPlugin.cancelAll();

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => SignInPage()),
            (Route<dynamic> route) => false);
      });
    }

    return a;
  }
}
