import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/headache.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/daily_report/daily_report.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

class ThirdPageReport extends StatefulWidget {
  final Function() update;
  final Headache diary;
  final DateTime chosenDate;
  final Function() addDiarySuccessfully;
  final bool isNew;
  ThirdPageReport(this.update, this.diary, this.chosenDate,
      this.addDiarySuccessfully, this.isNew);
  @override
  _ThirdPageReportState createState() => _ThirdPageReportState();
}

class _ThirdPageReportState extends State<ThirdPageReport> {
  int pain = 1;
  User user = User();
  DataBase db = DataBase();
  bool buttonPressed = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(height: 15.0),
        Text("Faltei ao trabalho"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0),
          child: Container(
            height: 35.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Sim",
                    style: widget.diary.workIncapacity
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    if (!buttonPressed)
                      setState(() {
                        widget.diary.workIncapacity = true;
                      });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Não",
                    style: !widget.diary.workIncapacity
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    if (!buttonPressed)
                      setState(() {
                        widget.diary.workIncapacity = false;
                      });
                  },
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Text("Fui às urgências"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0),
          child: Container(
            height: 35.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Sim",
                    style: widget.diary.emergencyServiceRecurrence
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    if (!buttonPressed)
                      setState(() {
                        widget.diary.emergencyServiceRecurrence = true;
                      });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Não",
                    style: !widget.diary.emergencyServiceRecurrence
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    if (!buttonPressed)
                      setState(() {
                        widget.diary.emergencyServiceRecurrence = false;
                      });
                  },
                ),
              ],
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FlatButton(
              child: Text(
                "Anterior",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onPressed: () {
                if (!buttonPressed) {
                  page = 2;
                  widget.update();
                }
              },
            ),
            buttonPressed
                ? Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      height: 20.0,
                      width: 20.0,
                      child: CircularProgressIndicator(
                        strokeWidth: 2.0,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Color(0xff3f7380)),
                      ),
                    ),
                  )
                : FlatButton(
                    child: Text(
                      "Finalizar",
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Color(0xff3f7380),
                      ),
                    ),
                    onPressed: () async {
                      if (!buttonPressed) {
                        setState(() {
                          buttonPressed = true;
                        });
                        String expireTokenString = await getExpireToken();
                        DateTime expireToken =
                            DateTime.parse(expireTokenString);
                        if (expireToken.isAfter(DateTime.now())) {
                          bool isConnected = await checkConnection();
                          if (isConnected) {
                            if (widget.isNew) {
                              String resp = await createEntryOnCalendar(
                                  user.code,
                                  [
                                    widget.chosenDate.year,
                                    widget.chosenDate.month,
                                    widget.chosenDate.day,
                                    12,
                                    0,
                                    0,
                                    0
                                  ],
                                  widget.diary.migraine,
                                  widget.diary.headacheIntensity,
                                  widget.diary.tookPainKillers,
                                  widget.diary.tookTriptans,
                                  widget.diary.emergencyServiceRecurrence,
                                  widget.diary.workIncapacity,
                                  widget.diary.obs,
                                  widget.diary.menstruation);
                              if (resp.startsWith("[")) {
                                await flutterLocalNotificationsPlugin
                                    .cancel(user.diaryNotificationID);
                                List a = json.decode(resp);
                                user.allEvents[widget.chosenDate] = [
                                  widget.diary.headacheToMap()
                                ];

                                user.allHolidays.remove(widget.chosenDate);
                                widget.addDiarySuccessfully();
                                a.forEach((element) {
                                  if (element["subject"] ==
                                      "Abuso de medicação") {
                                    _showNotification(element["id"],
                                        element["subject"], element["message"]);
                                  } else {
                                    _scheduleNotification(
                                        DateTime(
                                            element["dateTime"][0],
                                            element["dateTime"][1],
                                            element["dateTime"][2],
                                            element["dateTime"][3],
                                            element["dateTime"][4]),
                                        element["subject"],
                                        element["message"]);
                                  }
                                });
                                Toast.show(
                                    "Registo adicionado com sucesso!", context,
                                    duration: 5, gravity: Toast.BOTTOM);
                              } else if (resp == "409") {
                                Toast.show(
                                    "Este dia já tem um diário associado!",
                                    context,
                                    duration: 5,
                                    gravity: Toast.BOTTOM);
                              }
                            } else {
                              String resp = await editEntryOnCalendar(
                                  user.code,
                                  [
                                    widget.chosenDate.year,
                                    widget.chosenDate.month,
                                    widget.chosenDate.day,
                                    12,
                                    0,
                                    0,
                                    0
                                  ],
                                  widget.diary.migraine,
                                  widget.diary.headacheIntensity,
                                  widget.diary.tookPainKillers,
                                  widget.diary.tookTriptans,
                                  widget.diary.emergencyServiceRecurrence,
                                  widget.diary.workIncapacity,
                                  widget.diary.obs,
                                  widget.diary.menstruation);
                              if (resp.startsWith("[")) {
                                await flutterLocalNotificationsPlugin
                                    .cancel(user.diaryNotificationID);
                                List a = json.decode(resp);

                                user.allEvents[widget.chosenDate] = [
                                  widget.diary.headacheToMap()
                                ];

                                a.forEach((element) {
                                  if (element["subject"] ==
                                      "Abuso de medicação") {
                                    _showNotification(element["id"],
                                        element["subject"], element["message"]);
                                  } else {
                                    _scheduleNotification(
                                        DateTime(
                                            element["dateTime"][0],
                                            element["dateTime"][1],
                                            element["dateTime"][2],
                                            element["dateTime"][3],
                                            element["dateTime"][4]),
                                        element["subject"],
                                        element["message"]);
                                  }
                                });
                                Toast.show(
                                    "Registo editado com sucesso!", context,
                                    duration: 5, gravity: Toast.BOTTOM);
                              } else if (resp == "409") {
                                Toast.show(
                                    "Este dia já tem um diário associado!",
                                    context,
                                    duration: 5,
                                    gravity: Toast.BOTTOM);
                              }
                            }
                            Navigator.pop(
                                context, user.allEvents[widget.chosenDate]);
                          } else {
                            Toast.show(
                                "Ligue-se à internet para poder finalizar a operação!",
                                context,
                                duration: 5,
                                gravity: Toast.BOTTOM);
                          }
                        } else {
                          Toast.show("Login expirou! Por favor inicie sessão.",
                              context,
                              duration: 5, gravity: Toast.BOTTOM);
                          user.logged = 0;
                          db.updateUserState(user).then((val) async {
                            user.id = null;

                            await flutterLocalNotificationsPlugin.cancelAll();

                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        SignInPage()),
                                (Route<dynamic> route) => false);
                          });
                        }
                        setState(() {
                          buttonPressed = false;
                        });
                      }
                    },
                  ),
          ],
        )
      ],
    );
  }

  Future _scheduleNotification(
      DateTime scheduledDateTime, String title, String body) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        "your channel id", "your channel name", "your channel description",
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(user.diaryNotificationID,
        title, body, scheduledDateTime, platformChannelSpecifics,
        payload: "item x", androidAllowWhileIdle: true);

    await flutterLocalNotificationsPlugin.periodicallyShow(
        user.diaryNotificationID,
        "Não preencheu o seu diário na última semana",
        "Preencha o diário sempre que for incomodado por uma cefaleia. Lembre-se que é pela sua saúde!",
        RepeatInterval.Weekly,
        platformChannelSpecifics,
        payload: "item x");
  }

  Future _showNotification(int id, String title, String body) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        "your channel id", "your channel name", "your channel description",
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin
        .show(id, title, body, platformChannelSpecifics, payload: "item x");
  }
}
