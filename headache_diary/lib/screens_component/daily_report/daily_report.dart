import 'package:flutter/material.dart';
import 'package:headache_diary/model/headache.dart';
import 'package:headache_diary/screens_component/daily_report/daily_report_1st.dart';
import 'package:headache_diary/screens_component/daily_report/daily_report_2nd.dart';
import 'package:headache_diary/screens_component/daily_report/daily_report_3rd.dart';
import 'package:intl/intl.dart';

class DailyReport extends StatefulWidget {
  final String todayDate;
  final DateTime chosenDate;
  final Function() addDiarySuccessfully;
  final Map entry;
  DailyReport(this.todayDate, this.chosenDate,
      {this.addDiarySuccessfully, this.entry});
  @override
  _DailyReportState createState() => _DailyReportState();
}

int page;

class _DailyReportState extends State<DailyReport> {
  final f = DateFormat('yyyy-MM-dd');
  Headache diary = Headache();

  bool isNew;

  @override
  void initState() {
    super.initState();
    page = 1;
    if (widget.entry != null) {
      isNew = false;
      diary.migraine = widget.entry["migraine"];
      diary.headacheIntensity = widget.entry["headacheIntensity"];
      diary.tookPainKillers = widget.entry["tookPainKillers"];
      diary.tookTriptans = widget.entry["tookTriptans"];
      diary.menstruation = widget.entry["menstruation"];
      diary.workIncapacity = widget.entry["workIncapacity"];
      diary.emergencyServiceRecurrence =
          widget.entry["emergencyServiceRecurrence"];
    } else {
      isNew = true;
      diary.migraine = true;
      diary.headacheIntensity = 1;
      diary.tookPainKillers = false;
      diary.tookTriptans = false;
      diary.menstruation = false;
      diary.workIncapacity = false;
      diary.emergencyServiceRecurrence = false;
    }
  }

  update() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(
              color: Color(0xff3f7380),
            ),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/diary.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Diário",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30.0),
              child: Text(
                widget.todayDate,
                style: TextStyle(color: Color(0xff3f7380), fontSize: 17.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: Container(
                  width: MediaQuery.of(context).size.width * 0.85,
                  height: 300.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xff3f7380), width: 2.0),
                    color: Color(0xffdbeded),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: page == 1
                      ? FirstPageReport(update, diary)
                      : page == 2
                          ? SecondPageReport(update, diary)
                          : ThirdPageReport(update, diary, widget.chosenDate,
                              widget.addDiarySuccessfully, isNew)),
            ),
            SizedBox(height: 15.0),
            RaisedButton(
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.grey, width: 1.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              color: Colors.white,
              child: Text(
                "Regressar ao Calendário",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
      ),
    );
  }
}
