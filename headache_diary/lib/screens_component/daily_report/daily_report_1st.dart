import 'package:flutter/material.dart';
import 'package:headache_diary/model/headache.dart';
import 'package:headache_diary/screens_component/daily_report/daily_report.dart';

class FirstPageReport extends StatefulWidget {
  final Function() update;
  final Headache diary;
  FirstPageReport(this.update, this.diary);
  @override
  _FirstPageReportState createState() => _FirstPageReportState();
}

class _FirstPageReportState extends State<FirstPageReport> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(height: 15.0),
        Text("Qualidade de Cefaleia"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0),
          child: Container(
            height: 35.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Enxaqueca",
                    style: widget.diary.migraine
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.diary.migraine = true;
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Tensão",
                    style: !widget.diary.migraine
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.diary.migraine = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Text("Intensidade"),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              child: Image.asset(
                "assets/images/1.png",
                width: widget.diary.headacheIntensity == 1 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 1;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/2.png",
                width: widget.diary.headacheIntensity == 2 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 2;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/3.png",
                width: widget.diary.headacheIntensity == 3 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 3;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/4.png",
                width: widget.diary.headacheIntensity == 4 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 4;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/5.png",
                width: widget.diary.headacheIntensity == 5 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 5;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/6.png",
                width: widget.diary.headacheIntensity == 6 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 6;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/7.png",
                width: widget.diary.headacheIntensity == 7 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 7;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/8.png",
                width: widget.diary.headacheIntensity == 8 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 8;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/9.png",
                width: widget.diary.headacheIntensity == 9 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 9;
                });
              },
            ),
            GestureDetector(
              child: Image.asset(
                "assets/images/10.png",
                width: widget.diary.headacheIntensity == 10 ? 50.0 : 30.0,
              ),
              onTap: () {
                setState(() {
                  widget.diary.headacheIntensity = 10;
                });
              },
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FlatButton(
              child: Text(
                "Seguinte",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onPressed: () {
                page = 2;
                widget.update();
              },
            ),
          ],
        )
      ],
    );
  }
}
