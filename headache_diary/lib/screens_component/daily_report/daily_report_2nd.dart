import 'package:flutter/material.dart';
import 'package:headache_diary/model/headache.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/daily_report/daily_report.dart';

class SecondPageReport extends StatefulWidget {
  final Function() update;
  final Headache diary;
  SecondPageReport(this.update, this.diary);
  @override
  _SecondPageReportState createState() => _SecondPageReportState();
}

class _SecondPageReportState extends State<SecondPageReport> {
  User user = User();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(height: 15.0),
        Text("Tomei Analgésico"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0),
          child: Container(
            height: 35.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Sim",
                    style: widget.diary.tookPainKillers
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.diary.tookPainKillers = true;
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Não",
                    style: !widget.diary.tookPainKillers
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.diary.tookPainKillers = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 10.0),
        Text("Tomei Triptano"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0),
          child: Container(
            height: 35.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Sim",
                    style: widget.diary.tookTriptans
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.diary.tookTriptans = true;
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Não",
                    style: !widget.diary.tookTriptans
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.diary.tookTriptans = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 10.0),
        user.gender.startsWith("f") || user.gender.startsWith("F")
            ? Column(
                children: [
                  Text("Estou na Menstruação"),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50.0),
                    child: Container(
                      height: 35.0,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 2.0),
                        color: Colors.grey.shade50,
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FlatButton(
                            child: Text(
                              "Sim",
                              style: widget.diary.menstruation
                                  ? TextStyle(
                                      fontSize: 18.0,
                                      color: Color(0xff3f7380),
                                    )
                                  : TextStyle(
                                      fontSize: 15.0,
                                      color: Color(0xff3f7380).withOpacity(0.7),
                                    ),
                            ),
                            onPressed: () {
                              setState(() {
                                widget.diary.menstruation = true;
                              });
                            },
                          ),
                          FlatButton(
                            child: Text(
                              "Não",
                              style: !widget.diary.menstruation
                                  ? TextStyle(
                                      fontSize: 18.0,
                                      color: Color(0xff3f7380),
                                    )
                                  : TextStyle(
                                      fontSize: 15.0,
                                      color: Color(0xff3f7380).withOpacity(0.7),
                                    ),
                            ),
                            onPressed: () {
                              setState(() {
                                widget.diary.menstruation = false;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : Container(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FlatButton(
              child: Text(
                "Anterior",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onPressed: () {
                page = 1;
                widget.update();
              },
            ),
            FlatButton(
              child: Text(
                "Seguinte",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onPressed: () {
                page = 3;
                widget.update();
              },
            ),
          ],
        )
      ],
    );
  }
}
