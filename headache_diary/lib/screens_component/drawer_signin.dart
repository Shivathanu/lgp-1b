import 'package:flutter/material.dart';
import 'package:headache_diary/screens_component/about_us.dart';
import 'package:headache_diary/screens_component/rgpd_consent.dart';
import 'package:headache_diary/screens_component/user_manual.dart';

class DrawerSignin extends StatefulWidget {
  @override
  _DrawerSigninState createState() => _DrawerSigninState();
}

class _DrawerSigninState extends State<DrawerSignin> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Container(
              child: Image.asset(
            "assets/images/Untitled-1.png",
            height: 130.0,
          )),
          Container(
            color: Color(0xffdbeded),
            child: ListTile(
              leading: Image.asset(
                "assets/images/guia.png",
                width: 25.0,
              ),
              title: Text(
                "Guia de Utilização",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => UserManual()));
              },
            ),
          ),
          Container(
            color: Colors.grey.shade100,
            child: ListTile(
              leading: Image.asset(
                "assets/images/info.png",
                width: 25.0,
              ),
              title: Text(
                "Sobre Nós",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => AboutUs()));
              },
            ),
          ),
          Container(
            color: Color(0xffdbeded),
            child: ListTile(
              leading: Image.asset(
                "assets/images/verified.png",
                width: 25.0,
              ),
              title: Text(
                "RGPD",
                style: TextStyle(
                  color: Color(0xff3f7380),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => RGPDConsent()));
              },
            ),
          ),
        ],
      ),
    );
  }
}
