import 'package:flutter/material.dart';

class RGPDConsent extends StatefulWidget {
  @override
  _RGPDConsentState createState() => _RGPDConsentState();
}

class _RGPDConsentState extends State<RGPDConsent> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/info.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Consentimento RGPD",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 30.0,
                left: 25.0,
                right: 25.0,
              ),
              child: Container(
                padding: EdgeInsets.all(15.0),
                height: MediaQuery.of(context).size.height * 0.7,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff3f7380), width: 2.0),
                  color: Color(0xffdbeded),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                child: SingleChildScrollView(
                  child: Center(
                    child: Column(
                      children: [
                        Text("Política de Privacidade e Termos e Condições\n",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xff707070),
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold)),
                        Text(
                          "1. Proteção de dados de acordo com o RGPD\n\n" +
                              "A aplicação My Health Diary respeita e preocupa-se com a privacidade dos utilizadores/pacientes, por isso processa os " +
                              "seus dados pessoais de acordo com o regulamento de proteção de dados em vigor - RGPD.\n" +
                              "O Hospital Pedro Hispano reserva-se no direito de atualizar ou modificar a sua Política de Privacidade a qualquer momento, " +
                              "nomeadamente, de forma a adaptá-la a alterações legislativas.\n\n" +
                              "2. Recolha de Dados\n\n" +
                              "A aplicação My Health Diary recolhe dados dos utilizadores/pacientes ao abrigo do interesse legítimo. Os dados podem " +
                              "incluir iniciais do nome, email  e registo de informação relativa a cefaleias ou consultas que são preenchidos diariamente.\n" +
                              "Os dados recolhidos destinam-se à monitorização da informação de registo de diários dos utilizadores/pacientes.\n" +
                              "A utilização da aplicação, está sujeita a consentimento prévio por parte do titular dos dados.\n\n" +
                              "3. Acesso aos dados pessoais do Utilizador\n\n" +
                              "Têm acesso aos dados os profissionais de saúde, médicos ou enfermeiros, caso e apenas, quando precisem desse acesso para " +
                              "prestarem o serviço acordado.\n" +
                              "Não serão divulgados a terceiros quaisquer dados pessoais dos nossos pacientes e utilizadores, sem o seu consentimento, " +
                              "exceto quando tal for exigido por lei.\n\n" +
                              "4. Tempo em que são guardados os dados do utilizador \n\n" +
                              "Os dados recolhidos pela aplicação My Health Diary são guardados pelo tempo tido como necessário durante o processo " +
                              "hospitalar entre o Hospital Pedro Hispano e o utilizador/paciente. Nos restantes casos os dados serão eliminados ao fim " +
                              "de 5 anos.\n\n" +
                              "5. Onde são guardados os dados pessoais recolhidos\n\n" +
                              "Os dados recolhidos pela aplicação My Health Diary são guardados na intranet do Hospital Pedro Hispano pelo tempo tido como " +
                              "necessário durante o processo hospitalar entre o hospital e o utilizador/paciente. Nos restantes casos os dados serão " +
                              "eliminados ao fim de 5 anos.\n\n" +
                              "6. Como pedir acesso aos seus dados pessoais\n\n" +
                              "A qualquer momento um utilizador/paciente pode ter acesso, alterar ou apagar os seus dados pessoais. Caso deseje ser " +
                              "removido da nossa base de dados, poderá exercer esse direito, a partir da aplicação no menu de perfil.\n\n" +
                              "Versão atualizada a 29 de maio de 2020.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color(0xff707070), fontSize: 14.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
