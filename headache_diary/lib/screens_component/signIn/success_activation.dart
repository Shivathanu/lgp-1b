import 'package:flutter/material.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/signIn/activated.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:intl/intl.dart';

class SuccessActivation extends StatefulWidget {
  final String message;
  SuccessActivation(this.message);
  @override
  _SuccessActivationState createState() => _SuccessActivationState();
}

class _SuccessActivationState extends State<SuccessActivation> {
  Future<int> wait;
  User user = User();

  final f = new DateFormat('yyyy-MM-dd');

  @override
  void initState() {
    super.initState();
    wait = getInitInfo();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<int>(
      future: wait,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Activated(widget.message);
            break;
          default:
            if (snapshot.hasError) {
              return Scaffold(
                body: Center(
                  child: Container(
                    child: Text("app failed"),
                  ),
                ),
              );
            } else
              return SignInPage();
        }
      },
    );
  }

  Future<int> getInitInfo() async {
    await Future.delayed(Duration(seconds: 3));
    return 1;
  }
}
