import 'package:flutter/material.dart';

class Activated extends StatelessWidget {
  final String message;
  Activated(this.message);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xffdbeded),
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        leading: Container(),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "$message",
                textAlign: TextAlign.center,
                style: TextStyle(color: Color(0xff3f7380), fontSize: 18.0),
              ),
              SizedBox(height: 15.0),
              Image.asset("assets/images/seen.png"),
              SizedBox(height: 50.0),
            ],
          ),
        ),
      ),
    );
  }
}
