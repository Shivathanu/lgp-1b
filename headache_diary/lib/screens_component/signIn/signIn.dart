import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/drawer_signin.dart';
import 'package:headache_diary/screens_component/load_info.dart';
import 'package:headache_diary/screens_component/signIn/accountAtivation.dart';
import 'package:headache_diary/screens_component/signIn/forgot_password.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  TextEditingController idController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  DataBase db = DataBase();
  User user = User();

  bool buttonPressed = false;
  bool passwordVisible = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      drawer: DrawerSignin(),
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            color: Color(0xff707070),
          ),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(),
            Center(
              child: Text(
                "MY HEALTH DIARY",
                style: TextStyle(color: Color(0xff3f7380)),
              ),
            ),
          ],
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset(
                        "assets/images/iniciarsessao.png",
                        width: 20.0,
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text(
                        "Iniciar Sessão",
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Color(0xff3f7380),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 40.0,
              decoration: BoxDecoration(
                color: Color(0xff92cdd3),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                      child: Text(
                        "Ativar Conta",
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    AccountAtivation()));
                      }),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 50.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 60.0),
                  TextFormField(
                      controller: idController,
                      keyboardType: TextInputType.text,
                      cursorColor: Color(0xff3f7380),
                      decoration: InputDecoration(
                        labelText: "ID",
                        labelStyle: TextStyle(color: Color(0xff3f7380)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            borderSide: BorderSide(color: Color(0xff3f7380))),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      )),
                  SizedBox(height: 40.0),
                  TextFormField(
                    controller: passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: !passwordVisible,
                    cursorColor: Color(0xff3f7380),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                          icon: Icon(
                            passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.grey,
                          ),
                          onPressed: () {
                            setState(() {
                              passwordVisible = !passwordVisible;
                            });
                          }),
                      labelText: "Palavra-passe",
                      labelStyle: TextStyle(color: Color(0xff3f7380)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  FlatButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  ForgotPassword()));
                    },
                    child: Text(
                      "Recuperar Palavra-passe",
                      style: TextStyle(
                        color: Color(0xff3f7380),
                        fontSize: 14.0,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  RaisedButton(
                    child: buttonPressed
                        ? Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  Color(0xff3f7380)),
                            ),
                          )
                        : Text("Iniciar Sessão",
                            style: TextStyle(
                                color: Color(0xff3f7380), fontSize: 15.0)),
                    onPressed: () async {
                      if (!buttonPressed) {
                        setState(() {
                          buttonPressed = true;
                        });
                        FocusScope.of(context).requestFocus(FocusNode());

                        if (idController.text == "" ||
                            passwordController.text == "") {
                          Toast.show("Campos vazios!", context,
                              duration: 5, gravity: Toast.BOTTOM);
                        } else {
                          bool isConnected = await checkConnection();
                          if (isConnected) {
                            String resp = await authenticate(
                              idController.text,
                              passwordController.text,
                            );
                            if (resp == "401") {
                              Toast.show("Credenciais incorretas!", context,
                                  duration: 5, gravity: Toast.BOTTOM);
                            } else {
                              var decodeSucceeded = false;
                              String token = "";
                              try {
                                Map<String, dynamic> jsondecoded =
                                    jsonDecode(resp);
                                decodeSucceeded = true;
                                token = jsondecoded['token'];
                              } on FormatException catch (e) {
                                print(
                                    'The provided string is not valid JSON: $e');
                              }
                              if (decodeSucceeded) {
                                await setToken(token);
                                await setExpireToken(DateTime.now()
                                    .add(Duration(hours: 4, minutes: 45))
                                    .toString());
                                user = await getUserByCode(idController.text);
                                if (user != null) {
                                  if (!user.active) {
                                    Toast.show("Conta não ativada!", context,
                                        duration: Toast.LENGTH_LONG,
                                        gravity: Toast.BOTTOM);
                                  } else {
                                    user.logged = 1;

                                    user.readByRecipient = user
                                        .pastNotifications
                                        ?.where((element) =>
                                            element["readByRecipient"] == false)
                                        ?.length;

                                    user.diaryNotificationID =
                                        await getDiaryNotification(user.code);
                                    db.getUserLoggedIn(user.code).then((val) {
                                      if (val == -1) {
                                        db.addUser(user);
                                      } else {
                                        user.id = val;
                                        db.updateUserState(user);
                                      }

                                      user.futureNotifications
                                          ?.forEach((element) {
                                        _scheduleNotification(
                                            element["id"],
                                            DateTime(
                                                element["dateTime"][0],
                                                element["dateTime"][1],
                                                element["dateTime"][2],
                                                element["dateTime"][3],
                                                element["dateTime"][4]),
                                            element["subject"],
                                            element["message"]);
                                      });

                                      Toast.show("Bem vindo!", context,
                                          duration: Toast.LENGTH_LONG,
                                          gravity: Toast.BOTTOM);

                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  LoadInfo()),
                                          (Route<dynamic> route) => false);
                                    });
                                  }
                                }
                              }
                            }

                            //Navigator.pop(context);
                          } else {
                            Toast.show(
                                "Ligue-se à internet para poder finalizar a operação!",
                                context,
                                duration: 5,
                                gravity: Toast.BOTTOM);
                          }
                        }
                        setState(() {
                          buttonPressed = false;
                        });
                      }
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                    color: Colors.white,
                  ),
                  SizedBox(height: 40.0),
                  Image.asset(
                    "assets/images/HIVE_health.png",
                    width: 70.0,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future _scheduleNotification(
      int id, DateTime scheduledDateTime, String title, String body) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        "your channel id", "your channel name", "your channel description",
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        id, title, body, scheduledDateTime, platformChannelSpecifics,
        payload: "item x", androidAllowWhileIdle: true);
  }
}
