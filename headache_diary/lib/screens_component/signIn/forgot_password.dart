import 'package:flutter/material.dart';
import 'package:headache_diary/screens_component/signIn/success_activation.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:toast/toast.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController _emailController = new TextEditingController();

  bool buttonPressed = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(),
            Center(
              child: Text(
                "MY HEALTH DIARY",
                style: TextStyle(color: Color(0xff3f7380)),
              ),
            ),
          ],
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: 60.0,
            decoration: BoxDecoration(
              color: Color(0xffdbeded),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Image.asset(
                      "assets/images/register.png",
                      width: 20.0,
                    ),
                    SizedBox(
                      width: 15.0,
                    ),
                    Text(
                      "Recuperar Palavra-passe",
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Color(0xff3f7380),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 50.0),
            child: Column(
              children: <Widget>[
                SizedBox(height: 60.0),
                Text(
                    "Introduza o email com o qual o seu médico o registou, para poder voltar a aceder à aplicação.",
                    style: TextStyle(fontSize: 15.0)),
                SizedBox(height: 35.0),
                TextFormField(
                    controller: _emailController,
                    keyboardType: TextInputType.text,
                    cursorColor: Color(0xff3f7380),
                    decoration: InputDecoration(
                      labelText: "Email",
                      labelStyle: TextStyle(color: Color(0xff3f7380)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    )),
                SizedBox(height: 40.0),
                RaisedButton(
                  child: buttonPressed
                      ? Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Color(0xff3f7380)),
                          ),
                        )
                      : Text("Continuar",
                          style: TextStyle(
                              color: Color(0xff3f7380), fontSize: 15.0)),
                  onPressed: () async {
                    if (!buttonPressed) {
                      setState(() {
                        buttonPressed = true;
                      });
                      bool isConnected = await checkConnection();
                      if (isConnected) {
                        String resp =
                            await forgotPassword(_emailController.text);
                        if (resp == "") {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SuccessActivation(
                                          "Email de reposição de palavra-passe\n enviado com sucesso!")),
                              (Route<dynamic> route) => false);
                        } else {
                          Toast.show("Servidor falhou!", context,
                              duration: 7, gravity: Toast.BOTTOM);
                        }
                      } else {
                        Toast.show(
                            "Não está ligado à internet! \nPor esse motivo, não poderá eliminar a consulta!",
                            context,
                            duration: 7,
                            gravity: Toast.BOTTOM);
                      }
                    }
                    setState(() {
                      buttonPressed = false;
                    });
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  color: Colors.white,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
