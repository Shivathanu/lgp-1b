import 'package:flutter/material.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/screens_component/signIn/success_activation.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:toast/toast.dart';

class AccountAtivationPassword extends StatefulWidget {
  final String code;
  AccountAtivationPassword(this.code);
  @override
  _AccountAtivationPasswordState createState() =>
      _AccountAtivationPasswordState();
}

class _AccountAtivationPasswordState extends State<AccountAtivationPassword> {
  TextEditingController passwordController = new TextEditingController();
  TextEditingController passwordConfirmController = new TextEditingController();

  bool passwordVisible = false;
  bool repeatedPasswordVisible = false;
  bool buttonPressed = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: new Container(),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset(
                        "assets/images/register.png",
                        width: 20.0,
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text(
                        "Alterar palavra-passe temporária",
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Color(0xff3f7380),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Center(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 50.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 40.0),
                    TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: !passwordVisible,
                      cursorColor: Color(0xff3f7380),
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                            icon: Icon(
                              passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.grey,
                            ),
                            onPressed: () {
                              setState(() {
                                passwordVisible = !passwordVisible;
                              });
                            }),
                        labelText: "Nova Palavra-passe",
                        labelStyle: TextStyle(color: Color(0xff3f7380)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            borderSide: BorderSide(color: Color(0xff3f7380))),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            borderSide: BorderSide(color: Color(0xff3f7380))),
                      ),
                    ),
                    SizedBox(height: 40.0),
                    TextFormField(
                      controller: passwordConfirmController,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: !repeatedPasswordVisible,
                      cursorColor: Color(0xff3f7380),
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                            icon: Icon(
                              repeatedPasswordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.grey,
                            ),
                            onPressed: () {
                              setState(() {
                                repeatedPasswordVisible =
                                    !repeatedPasswordVisible;
                              });
                            }),
                        labelText: "Confirmar Palavra-passe",
                        labelStyle: TextStyle(color: Color(0xff3f7380)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            borderSide: BorderSide(color: Color(0xff3f7380))),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            borderSide: BorderSide(color: Color(0xff3f7380))),
                      ),
                    ),
                    SizedBox(height: 40.0),
                    RaisedButton(
                      child: buttonPressed
                          ? Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Color(0xff3f7380)),
                              ),
                            )
                          : Text("Continuar",
                              style: TextStyle(
                                  color: Color(0xff3f7380), fontSize: 15.0)),
                      onPressed: () async {
                        if (!buttonPressed) {
                          setState(() {
                            buttonPressed = true;
                          });
                          FocusScope.of(context).requestFocus(FocusNode());
                          String expireTokenString = await getExpireToken();
                          DateTime expireToken =
                              DateTime.parse(expireTokenString);
                          if (expireToken.isAfter(DateTime.now())) {
                            bool isConnected = await checkConnection();
                            if (isConnected) {
                              String resp = await accountActivation(
                                widget.code,
                                passwordController.text,
                                passwordConfirmController.text,
                              );
                              if (resp != null) {
                                if (resp.contains("500")) {
                                  if (resp.contains(("9 caracteres"))) {
                                    Toast.show(
                                        "A palavra-passe tem de ter, no mínimo, 9 caracteres!",
                                        context,
                                        duration: 5,
                                        gravity: Toast.BOTTOM);
                                  } else if (resp.contains(("correspondem"))) {
                                    Toast.show(
                                        "A nova palavra-passe e a repetida não correspondem!",
                                        context,
                                        duration: 5,
                                        gravity: Toast.BOTTOM);
                                  }
                                } else if (resp.contains("401")) {
                                  Toast.show(
                                      "Login expirou! Por favor inicie sessão.",
                                      context,
                                      duration: 5,
                                      gravity: Toast.BOTTOM);

                                  Navigator.pop(context);
                                } else {
                                  Toast.show("Conta Ativada!", context,
                                      duration: 5, gravity: Toast.BOTTOM);
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              SuccessActivation(
                                                "Conta Ativada com Sucesso",
                                              )),
                                      (Route<dynamic> route) => false);
                                }
                              } else {
                                Toast.show("Servidor falhou!", context,
                                    duration: 7, gravity: Toast.BOTTOM);
                              }
                            } else {
                              Toast.show(
                                  "Ligue-se à internet para poder finalizar a operação!",
                                  context,
                                  duration: 5,
                                  gravity: Toast.BOTTOM);
                            }
                          } else {
                            Toast.show(
                                "Inicie novamente o processo de ativação de conta.",
                                context,
                                duration: 5,
                                gravity: Toast.BOTTOM);
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        SignInPage()),
                                (Route<dynamic> route) => false);
                          }
                        }
                        setState(() {
                          buttonPressed = false;
                        });
                      },
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
