import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:headache_diary/screens_component/signIn/accountActivationPassword.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:toast/toast.dart';

class AccountAtivation extends StatefulWidget {
  @override
  _AccountAtivationState createState() => _AccountAtivationState();
}

class _AccountAtivationState extends State<AccountAtivation> {
  TextEditingController idController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool _termosUtilizacao = false;
  bool _politicaPrvacidade = false;
  bool _tratamentoDados = false;
  bool passwordVisible = false;
  bool buttonPressed = false;

  void _value1Changed(bool value) => setState(() => _termosUtilizacao = value);

  void _value2Changed(bool value) =>
      setState(() => _politicaPrvacidade = value);

  void _value3Changed(bool value) => setState(() => _tratamentoDados = value);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset(
                        "assets/images/register.png",
                        width: 20.0,
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text(
                        "Ativação de conta",
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Color(0xff3f7380),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 50.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 40.0),
                  TextFormField(
                      controller: idController,
                      keyboardType: TextInputType.text,
                      cursorColor: Color(0xff3f7380),
                      decoration: InputDecoration(
                        labelText: "ID do Utilizador",
                        labelStyle: TextStyle(color: Color(0xff3f7380)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                            borderSide: BorderSide(color: Color(0xff3f7380))),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      )),
                  SizedBox(height: 50.0),
                  TextFormField(
                    controller: passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: !passwordVisible,
                    cursorColor: Color(0xff3f7380),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                          icon: Icon(
                            passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.grey,
                          ),
                          onPressed: () {
                            setState(() {
                              passwordVisible = !passwordVisible;
                            });
                          }),
                      labelText: "Palavra-passe",
                      labelStyle: TextStyle(color: Color(0xff3f7380)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                    ),
                  ),
                  SizedBox(height: 60.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 0.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 2.0),
                        color: Colors.grey.shade50,
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                      ),
                      child: Column(
                        children: <Widget>[
                          CheckboxListTile(
                              value: _termosUtilizacao,
                              onChanged: _value1Changed,
                              title: new InkWell(
                                  child: new Text(
                                    'Aceito os termos de utilização.',
                                    style: TextStyle(
                                      fontSize: 15,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                  onTap: () => _showDialogTherms()),
                              controlAffinity: ListTileControlAffinity.leading,
                              activeColor: Color(0xff3f7380)),
                          CheckboxListTile(
                              value: _politicaPrvacidade,
                              onChanged: _value2Changed,
                              title: new InkWell(
                                  child: new Text(
                                    'Tenho conhecimento e aceito a Política de Privacidade.',
                                    style: TextStyle(
                                      fontSize: 15,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                  onTap: () => _showDialogPrivacy()),
                              controlAffinity: ListTileControlAffinity.leading,
                              activeColor: Color(0xff3f7380)),
                          CheckboxListTile(
                              value: _tratamentoDados,
                              onChanged: _value3Changed,
                              title: new Text(
                                'Autorizo o tratamento de dados pessoais para marketing de produtos e serviços.',
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                              controlAffinity: ListTileControlAffinity.leading,
                              activeColor: Color(0xff3f7380)),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  RaisedButton(
                    child: buttonPressed
                        ? Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  Color(0xff3f7380)),
                            ),
                          )
                        : Text("Ativar Conta",
                            style: TextStyle(
                                color: Color(0xff3f7380), fontSize: 15.0)),
                    onPressed: () async {
                      if (!_termosUtilizacao || !_politicaPrvacidade) {
                        showWarning();
                      } else {
                        if (!buttonPressed) {
                          setState(() {
                            buttonPressed = true;
                          });
                          FocusScope.of(context).requestFocus(FocusNode());

                          bool isConnected = await checkConnection();
                          if (isConnected) {
                            String resp = await authenticate(
                              idController.text,
                              passwordController.text,
                            );
                            if (resp != null) {
                              if (resp == "401") {
                                Toast.show("Credenciais incorretas!", context,
                                    duration: 5, gravity: Toast.BOTTOM);
                              } else {
                                var decodeSucceeded = false;
                                String token = "";
                                try {
                                  Map<String, dynamic> jsondecoded =
                                      jsonDecode(resp);
                                  decodeSucceeded = true;
                                  token = jsondecoded['token'];
                                } on FormatException catch (e) {
                                  print(
                                      'The provided string is not valid JSON: $e');
                                }
                                if (decodeSucceeded) {
                                  await setToken(token);
                                  await setExpireToken(DateTime.now()
                                      .add(Duration(hours: 4, minutes: 45))
                                      .toString());

                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              AccountAtivationPassword(
                                                  idController.text)));
                                }
                              }
                            } else {
                              Toast.show("Servidor falhou!", context,
                                  duration: 7, gravity: Toast.BOTTOM);
                            }
                          } else {
                            Toast.show(
                                "Ligue-se à internet para poder finalizar a operação!",
                                context,
                                duration: 5,
                                gravity: Toast.BOTTOM);
                          }
                        }
                        setState(() {
                          buttonPressed = false;
                        });
                      }
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showDialogPrivacy() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                AlertDialog(
                  title: new Text("Política de privacidade"),
                  content: new Text(
                      "1. Proteção de dados de acordo com o RGPD\n\n" +
                          "A aplicação My Health Diary respeita e preocupa-se com a privacidade dos utilizadores/pacientes, por isso processa os " +
                          "seus dados pessoais de acordo com o regulamento de proteção de dados em vigor - RGPD.\n" +
                          "O Hospital Pedro Hispano reserva-se no direito de atualizar ou modificar a sua Política de Privacidade a qualquer momento, " +
                          "nomeadamente, de forma a adaptá-la a alterações legislativas.\n\n" +
                          "2. Recolha de Dados\n\n" +
                          "A aplicação My Health Diary recolhe dados dos utilizadores/pacientes ao abrigo do interesse legítimo. Os dados podem " +
                          "incluir iniciais do nome, email  e registo de informação relativa a cefaleias ou consultas que são preenchidos diariamente.\n" +
                          "Os dados recolhidos destinam-se à monitorização da informação de registo de diários dos utilizadores/pacientes.\n" +
                          "A utilização da aplicação, está sujeita a consentimento prévio por parte do titular dos dados.\n\n" +
                          "3. Acesso aos dados pessoais do Utilizador\n\n" +
                          "Têm acesso aos dados os profissionais de saúde, médicos ou enfermeiros, caso e apenas, quando precisem desse acesso para " +
                          "prestarem o serviço acordado.\n" +
                          "Não serão divulgados a terceiros quaisquer dados pessoais dos nossos pacientes e utilizadores, sem o seu consentimento, " +
                          "exceto quando tal for exigido por lei.\n\n" +
                          "4. Tempo em que são guardados os dados do utilizador\n\n" +
                          "Os dados recolhidos pela aplicação My Health Diary são guardados pelo tempo tido como necessário durante o processo " +
                          "hospitalar entre o Hospital Pedro Hispano e o utilizador/paciente. Nos restantes casos os dados serão eliminados ao fim " +
                          "de 5 anos.\n\n" +
                          "5. Onde são guardados os dados pessoais recolhidos\n\n" +
                          "Os dados recolhidos pela aplicação My Health Diary são guardados na intranet do Hospital Pedro Hispano pelo tempo tido como " +
                          "necessário durante o processo hospitalar entre o hospital e o utilizador/paciente. Nos restantes casos os dados serão " +
                          "eliminados ao fim de 5 anos.\n\n" +
                          "6. Como pedir acesso aos seus dados pessoais\n\n" +
                          "A qualquer momento um utilizador/paciente pode ter acesso, alterar ou apagar os seus dados pessoais. Caso deseje ser " +
                          "removido da nossa base de dados, poderá exercer esse direito, a partir da aplicação no menu de perfil.\n\n" +
                          "Versão atualizada a 29 de maio de 2020.",
                      style: TextStyle(fontSize: 10.0)),
                  actions: <Widget>[
                    // define os botões na base do dialogo
                    new FlatButton(
                      child: new Text(
                        "Ok",
                        style: TextStyle(color: Color(0xff3f7380)),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  void _showDialogTherms() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                AlertDialog(
                  title: new Text("Termos de utilização"),
                  content: new Text(
                      "MY HEALTH DIARY – Hive está empenhada no cumprimento do Regulamento Geral sobre a Proteção de Dados (RGPD)," +
                          " assegurando a proteção de dados pessoais e reforçando a relação de confiança que a compromete com o cliente." +
                          "\n\nEsta Política de Privacidade e Proteção de Dados Pessoais consiste em prestar as informações que recolhemos " +
                          "e da forma como as podemos utilizar, bem como das normas de segurança adotadas para proteger a sua identidade e " +
                          "dados pessoais.\n\n" +
                          "MY HEALTH DIARY – Hive reserva-se o direito de alterar a Política de Privacidade e Proteção de Dados Pessoais, " +
                          "pelo que o convidamos à consulta regular do presente documento.\n\n" +
                          "Por favor note que a presente Política de Privacidade e Proteção de Dados Pessoais aplica-se a informação colhida " +
                          "pela MY HEALTH DIARY – Hive e pela Unidade Local de Saúde de Matosinhos apenas, e não se estende a informação que " +
                          "possa ter submetido em outros sites ou plataformas sociais geridas for outras entidades ou organizações.\n\n" +
                          "1. Responsável pelo tratamento de dados\n\n" +
                          "MY HEALTH DIARY – Hive e Unidade Local de Saúde de Matosinhos, Rua Dr. Eduardo Torres / 4464-513 Senhora da Hora.\n\n" +
                          "2. Dados pessoais\n\nNos termos do RGPD, são considerados dados pessoais todas as informações relativas a uma pessoa singular " +
                          "identificada ou identificável (denominado o «titular dos dados»). É considerada identificável uma pessoa singular que " +
                          "possa ser identificada, direta ou indiretamente, em especial por referência a um identificador, como, por exemplo, " +
                          "nome, número de identificação, dados de localização, endereço IP (protocolo de internet), cookies, identificadores " +
                          "por via eletrónica ou a um ou mais elementos específicos da identidade física, fisiológica, genética, mental, económica, " +
                          "cultural ou social dessa pessoa singular.\n\n" +
                          "Também constituem dados pessoais o conjunto de informações distintas que podem levar à identificação de uma determinada " +
                          "pessoa.\n\n3. Titular de dados pessoais.\n\n" +
                          "O titular de dados pessoais é a pessoa singular a quem os dados dizem respeito e que utilizou a aplicação MY HEALTH DIARY " +
                          "– Hive.\n\n4. Categorias de dados pessoais e Finalidades\n\n" +
                          "MY HEALTH DIARY – Hive, recolhe diferentes categorias de dados:\n\nNome;\n\nEndereço de correio eletrónico;\n\nGénero;\n\n" +
                          "Data de nascimento;\n\nCódigo de paciente.",
                      style: TextStyle(fontSize: 10.0)),
                  actions: <Widget>[
                    // define os botões na base do dialogo
                    new FlatButton(
                      child: new Text(
                        "Ok",
                        style: TextStyle(color: Color(0xff3f7380)),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  void showWarning() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return AlertDialog(
            title: new Text("Termos em falta"),
            content: new Text(
                "Para continuar necessita de concordar com a política de privacidade e termos de utilização"),
            actions: <Widget>[
              // define os botões na base do dialogo
              new FlatButton(
                child: new Text(
                  "Ok",
                  style: TextStyle(color: Color(0xff3f7380)),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
}
