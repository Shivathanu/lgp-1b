import 'package:flutter/material.dart';
import 'package:headache_diary/screens_component/profile/prophylactic_treatment.dart';

class Tab2 extends StatefulWidget {
  final TabController controller;
  final Map<String, dynamic> userAttributes;

  Tab2(this.controller, this.userAttributes);

  @override
  _Tab2State createState() => _Tab2State();
}

class _Tab2State extends State<Tab2> with AutomaticKeepAliveClientMixin<Tab2> {
  TextEditingController _symptomaticDetailsController = TextEditingController();
  TextEditingController _prophylacticTreatmentDetailsController =
      TextEditingController();

  String newProphylacticTreatmentDetails;
  DateTime newProphylacticTreatmentDate;

  void update() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return SingleChildScrollView(
      child: Column(children: <Widget>[
        SizedBox(height: 10.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Tratamento sintomático: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        SizedBox(height: 10.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40.0),
          child: Container(
            height: 30.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Sim",
                    style: widget.userAttributes["symptomaticTreatment"]
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.userAttributes["symptomaticTreatment"] = true;
                      widget.userAttributes["symptomaticTreatmentDetails"] = "";
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Não",
                    style: !widget.userAttributes["symptomaticTreatment"]
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.userAttributes["symptomaticTreatment"] = false;
                      widget.userAttributes["symptomaticTreatmentDetails"] = "";
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        widget.userAttributes["symptomaticTreatment"]
            ? Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text("Se sim. Qual? "),
                  Flexible(
                    child: SizedBox(
                      width: 200.0,
                      child: TextFormField(
                        cursorColor: Color(0xff3f7380),
                        controller: _symptomaticDetailsController,
                        onChanged: (text) {
                          setState(() {
                            widget.userAttributes[
                                    "symptomaticTreatmentDetails"] =
                                _symptomaticDetailsController.text;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              )
            : Container(
                height: 45.0,
              ),
        SizedBox(height: 40.0),
        Row(
          children: [
            Text(
              "Tratamento profilático: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Qual? "),
                Flexible(
                  child: TextFormField(
                    cursorColor: Color(0xff3f7380),
                    controller: _prophylacticTreatmentDetailsController,
                    onChanged: (text) {
                      setState(() {
                        newProphylacticTreatmentDetails = text;
                      });
                    },
                  ),
                ),
                SizedBox(width: 15.0),
                Container(
                  height: 30.0,
                  width: 90.0,
                  child: RaisedButton(
                    color: Colors.white,
                    child: Text(
                      "Novo",
                      style:
                          TextStyle(fontSize: 13.0, color: Color(0xff3f7380)),
                    ),
                    onPressed: () async {
                      if (newProphylacticTreatmentDetails != "" &&
                          newProphylacticTreatmentDate != null) {
                        Map newProphylacticTreatment = {
                          "id": null,
                          "treatmentDetails": newProphylacticTreatmentDetails,
                          "startDate": [
                            newProphylacticTreatmentDate.year,
                            newProphylacticTreatmentDate.month,
                            newProphylacticTreatmentDate.day
                          ],
                        };
                        FocusScope.of(context).requestFocus(FocusNode());

                        setState(() {
                          widget.userAttributes["prophylacticTreatments"]
                              .add(newProphylacticTreatment);
                          _prophylacticTreatmentDetailsController.text = "";
                          newProphylacticTreatmentDate = null;
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: 10.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Quando começou? "),
                GestureDetector(
                    child: new Icon(
                      Icons.calendar_today,
                      color: Color(0xff3f7380),
                      size: 22.0,
                    ),
                    onTap: () async {
                      final datePick = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1900),
                          lastDate: DateTime.now());
                      if (datePick != null) {
                        setState(() {
                          newProphylacticTreatmentDate = datePick;
                        });
                      }
                    }),
                SizedBox(width: 10.0),
                Text(
                  newProphylacticTreatmentDate == null
                      ? ""
                      : "${newProphylacticTreatmentDate.day}/" +
                          "${newProphylacticTreatmentDate.month}/" +
                          "${newProphylacticTreatmentDate.year}",
                ),
              ],
            )
          ],
        ),
        Divider(
          color: Colors.grey.shade700,
          thickness: 1.0,
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.2,
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: widget.userAttributes["prophylacticTreatments"].length,
            itemBuilder: (context, index) {
              return Row(
                children: [
                  Flexible(
                    child: ProphylacticTreatment(
                        widget.userAttributes["prophylacticTreatments"][index],
                        widget.userAttributes["prophylacticTreatments"],
                        index),
                  ),
                  SizedBox(width: 5.0),
                  GestureDetector(
                      child: new Icon(
                        Icons.remove_circle,
                        color: Colors.grey,
                        size: 17.0,
                      ),
                      onTap: () async {
                        setState(() {
                          widget.userAttributes["prophylacticTreatments"]
                              .removeAt(index);
                        });
                      }),
                ],
              );
            },
          ),
        ),
        Divider(
          color: Colors.grey.shade700,
          height: 2.0,
          thickness: 1.0,
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                  onPressed: () {
                    widget.controller.animateTo(0);
                  },
                  child: Text("Anterior")),
              FlatButton(
                  onPressed: () {
                    widget.controller.animateTo(2);
                  },
                  child: Text("Próximo"))
            ]),
      ]),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
