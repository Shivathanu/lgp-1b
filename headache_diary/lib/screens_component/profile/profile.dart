import 'package:flutter/material.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/profile/baseEdit.dart';

import 'package:headache_diary/utils/utilities.dart';

class Profile extends StatefulWidget {
  final TabController controller;
  Profile(this.controller);
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("Editar",
                      style:
                          TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => Base()));
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  color: Colors.white,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 5.0,
                left: 35.0,
                right: 35.0,
              ),
              child: Container(
                padding: EdgeInsets.all(15.0),
                height: 500.0,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff3f7380), width: 2.0),
                  //color: Color(0xffdbeded),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Iniciais do nome: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(User().name),
                        ],
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Nº de utente: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(User().numUtente),
                        ],
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Nº de Processo: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(User().numProcess),
                        ],
                      ),
                      SizedBox(height: 20.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Data de nascimento: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text((() {
                            if (User().birthdate.day == 1 &&
                                User().birthdate.month == 1 &&
                                User().birthdate.year == 1) {
                              return "";
                            }
                            return Utilities.convertDateToString(
                                User().birthdate);
                          })()),
                        ],
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Sexo: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(User().gender.startsWith("f") ||
                                  User().gender.startsWith("F")
                              ? "Feminino"
                              : "Masculino"),
                        ],
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Email: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(User().email),
                        ],
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Centro de saúde: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(User().healthCenter),
                        ],
                      ),
                      SizedBox(height: 20.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Enxaqueca: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(convertBoolAura(User().migraineWithAura)),
                        ],
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Exclusivamente unilateral: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(convertBoolString(User().exclusivelyUnilateral)),
                        ],
                      ),
                      SizedBox(height: 20.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Tratamento sintomático: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(convertBoolString(User().symptomaticTreatment)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text("Qual?"),
                          Text(User().symptomaticTreatment
                              ? User().symptomaticTreatmentDetails
                              : "_________"),
                        ],
                      ),
                      SizedBox(height: 10.0),
                      User().prophylacticTreatments == null ||
                              User().prophylacticTreatments.isEmpty
                          ? Row(
                              children: [
                                Text(
                                  "Tratamento profilático: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("Não"),
                              ],
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Tratamento profilático: ",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  itemCount:
                                      User().prophylacticTreatments.length,
                                  itemBuilder: (context, index) {
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            //SizedBox(width: 20.0),
                                            Text(
                                              "${index + 1}. Qual? ",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                                "${User().prophylacticTreatments[index]["treatmentDetails"]}"),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            SizedBox(width: 20.0),
                                            Text(
                                              "Data de início: ",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text("${User().prophylacticTreatments[index]["startDate"][2]}" +
                                                "/${User().prophylacticTreatments[index]["startDate"][1]}" +
                                                "/${User().prophylacticTreatments[index]["startDate"][0]}"),
                                          ],
                                        )
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                      SizedBox(height: 20.0),
                      Row(
                        children: <Widget>[
                          Text("Comorbilidades:"),
                        ],
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text(
                            "Depressão: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(convertBoolString(User().depression)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Ansiedade: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(convertBoolString(User().anxiety)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Insónias: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(convertBoolString(User().insomnia)),
                        ],
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: <Widget>[
                          Text("Outras doenças relevantes: "),
                          Text(User().otherMedicalConditions),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String convertBoolString(bool b) {
    if (b) {
      return "Sim";
    } else {
      return "Não";
    }
  }

  String convertBoolAura(bool b) {
    if (b) {
      return "Com aura";
    } else {
      return "Sem aura";
    }
  }
}
