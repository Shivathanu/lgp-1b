import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController oldPasswordController = new TextEditingController();
  TextEditingController newPasswordConfirmController =
      new TextEditingController();
  TextEditingController newConfirmedPasswordConfirmController =
      new TextEditingController();

  User user = User();
  DataBase db = DataBase();

  bool oldPasswordVisible = false;
  bool passwordVisible = false;
  bool repeatedPasswordVisible = false;

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/perfil.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Perfil",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.symmetric(horizontal: 50.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 40.0),
                  TextFormField(
                    controller: oldPasswordController,
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: !oldPasswordVisible,
                    cursorColor: Color(0xff3f7380),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                          icon: Icon(
                            oldPasswordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.grey,
                          ),
                          onPressed: () {
                            setState(() {
                              oldPasswordVisible = !oldPasswordVisible;
                            });
                          }),
                      labelText: "Antiga Palavra-passe",
                      labelStyle: TextStyle(color: Color(0xff3f7380)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                    ),
                  ),
                  SizedBox(height: 40.0),
                  TextFormField(
                    controller: newPasswordConfirmController,
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: !passwordVisible,
                    cursorColor: Color(0xff3f7380),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                          icon: Icon(
                            passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.grey,
                          ),
                          onPressed: () {
                            setState(() {
                              passwordVisible = !passwordVisible;
                            });
                          }),
                      labelText: "Nova Palavra-passe",
                      labelStyle: TextStyle(color: Color(0xff3f7380)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                    ),
                  ),
                  SizedBox(height: 40.0),
                  TextFormField(
                    controller: newConfirmedPasswordConfirmController,
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: !repeatedPasswordVisible,
                    cursorColor: Color(0xff3f7380),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                          icon: Icon(
                            repeatedPasswordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.grey,
                          ),
                          onPressed: () {
                            setState(() {
                              repeatedPasswordVisible =
                                  !repeatedPasswordVisible;
                            });
                          }),
                      labelText: "Confirmar nova Palavra-passe",
                      labelStyle: TextStyle(color: Color(0xff3f7380)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Color(0xff3f7380))),
                    ),
                  ),
                  SizedBox(height: 40.0),
                  RaisedButton(
                    child: Text("Alterar Password",
                        style: TextStyle(
                            color: Color(0xff3f7380), fontSize: 15.0)),
                    onPressed: () async {
                      String expireTokenString = await getExpireToken();
                      DateTime expireToken = DateTime.parse(expireTokenString);
                      if (expireToken.isAfter(DateTime.now())) {
                        bool isConnected = await checkConnection();
                        if (isConnected) {
                          String resp = await changePassword(
                            User().code,
                            oldPasswordController.text,
                            newPasswordConfirmController.text,
                            newConfirmedPasswordConfirmController.text,
                          );

                          if (resp.contains("500")) {
                            if (resp.contains(("antiga"))) {
                              Toast.show(
                                  "A palavra-passe antiga está incorreta!",
                                  context,
                                  duration: 5,
                                  gravity: Toast.BOTTOM);
                            } else if (resp.contains(("correspondem"))) {
                              Toast.show(
                                  "A nova palavra-passe e a repetida não correspondem!",
                                  context,
                                  duration: 5,
                                  gravity: Toast.BOTTOM);
                            } else if (resp.contains("9")) {
                              Toast.show(
                                  "A palavra-passe tem de ter, no mínimo, 9 caracteres!",
                                  context,
                                  duration: 5,
                                  gravity: Toast.BOTTOM);
                            }
                          } else if (resp == "") {
                            Toast.show("Palavra-passe guardada!", context,
                                duration: Toast.LENGTH_LONG,
                                gravity: Toast.BOTTOM);
                            Navigator.pop(context);
                          }
                        }
                      } else {
                        Toast.show(
                            "Login expirou! Por favor inicie sessão.", context,
                            duration: 5, gravity: Toast.BOTTOM);
                        user.logged = 0;
                        db.updateUserState(user).then((val) async {
                          user.id = null;

                          await flutterLocalNotificationsPlugin.cancelAll();

                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SignInPage()),
                              (Route<dynamic> route) => false);
                        });
                      }
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
