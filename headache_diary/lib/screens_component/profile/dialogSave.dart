import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

class SaveEditProfile extends StatefulWidget {
  final Map<String, dynamic> userAttributes;
  SaveEditProfile(this.userAttributes);
  @override
  _SaveEditProfileState createState() => _SaveEditProfileState();
}

class _SaveEditProfileState extends State<SaveEditProfile> {
  DataBase db = DataBase();

  bool dialogSaveIsPressed = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: dialogSaveIsPressed
          ? Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xff3f7380)),
                  ),
                ),
              ),
            )
          : Text("Tem a certeza que quer guardar esta alteração?"),
      actions: dialogSaveIsPressed
          ? [Container()]
          : <Widget>[
              // define os botões na base do dialogo

              new FlatButton(
                child: new Text("Cancelar",
                    style: TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text("Guardar",
                    style: TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                onPressed: () async {
                  if (!dialogSaveIsPressed) {
                    setState(() {
                      dialogSaveIsPressed = true;
                    });
                    //EDIT REQUEST - SAVE USER IN BACKEND
                    String expireTokenString = await getExpireToken();
                    DateTime expireToken = DateTime.parse(expireTokenString);
                    if (expireToken.isAfter(DateTime.now())) {
                      bool isConnected = await checkConnection();
                      if (isConnected) {
                        String resp = await editUserProfile(
                            User().code,
                            widget.userAttributes["initials"],
                            widget.userAttributes["birthdate"],
                            widget.userAttributes["isWoman"]
                                ? "Feminino"
                                : "Masculino",
                            widget.userAttributes["withAura"],
                            widget.userAttributes["unilateral"],
                            widget.userAttributes["symptomaticTreatment"],
                            widget
                                .userAttributes["symptomaticTreatmentDetails"],
                            widget.userAttributes["prophylacticTreatments"],
                            widget.userAttributes["insomnia"],
                            widget.userAttributes["anxiety"],
                            widget.userAttributes["depression"],
                            widget.userAttributes["otherMedicalConditions"]);
                        Map userEdited = json.decode(resp);

                        if (resp != null) {
                          // EDIT USER LOCAL
                          User().name = userEdited["nameInitials"];
                          User().birthdate = DateTime(
                              userEdited["birthdate"][0],
                              userEdited["birthdate"][1],
                              userEdited["birthdate"][2]);
                          User().gender = userEdited["gender"];
                          User().migraineWithAura =
                              userEdited["migraineWithAura"];
                          User().exclusivelyUnilateral =
                              userEdited["exclusivelyUnilateral"];
                          User().symptomaticTreatment =
                              userEdited["symptomaticTreatment"];
                          User().symptomaticTreatmentDetails =
                              userEdited["symptomaticTreatmentDetails"];
                          User().prophylacticTreatments =
                              userEdited["prophylacticTreatments"];
                          User().insomnia = userEdited["insomnia"];
                          User().anxiety = userEdited["anxiety"];
                          User().depression = userEdited["depression"];
                          User().otherMedicalConditions =
                              userEdited["otherMedicalConditions"];

                          // SAVE USER LOCAL DB
                          await db.updateUser(User());
                          Toast.show("Alterações guardadas!", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.BOTTOM);
                          Navigator.pop(context);
                          Navigator.pop(context);
                        } else {
                          Toast.show("Servidor falhou!", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.BOTTOM);
                        }
                      } else {
                        Toast.show(
                            "Ligue-se à internet para poder finalizar a operação!",
                            context,
                            duration: 5,
                            gravity: Toast.BOTTOM);
                      }
                    } else {
                      Toast.show(
                          "Login expirou! Por favor inicie sessão.", context,
                          duration: 5, gravity: Toast.BOTTOM);
                      User().logged = 0;
                      db.updateUserState(User()).then((val) async {
                        User().id = null;

                        await flutterLocalNotificationsPlugin.cancelAll();

                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    SignInPage()),
                            (Route<dynamic> route) => false);
                      });
                    }
                  }
                  setState(() {
                    dialogSaveIsPressed = false;
                  });
                },
              ),
            ],
    );
  }
}
