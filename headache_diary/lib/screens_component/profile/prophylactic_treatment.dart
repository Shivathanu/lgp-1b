import 'package:flutter/material.dart';

class ProphylacticTreatment extends StatefulWidget {
  final Map prophTreatment;
  final List allProphTreatments;
  final int index;

  ProphylacticTreatment(
      this.prophTreatment, this.allProphTreatments, this.index);
  @override
  _ProphylacticTreatmentState createState() => _ProphylacticTreatmentState();
}

class _ProphylacticTreatmentState extends State<ProphylacticTreatment> {
  TextEditingController _prophylacticTreatmentDetailsController =
      TextEditingController();

  @override
  void initState() {
    super.initState();

    _prophylacticTreatmentDetailsController.text =
        widget.prophTreatment["treatmentDetails"];
  }

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget != widget) {
      setState(() {
        _prophylacticTreatmentDetailsController.text =
            widget.prophTreatment["treatmentDetails"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text("${widget.index + 1}. Qual? "),
        Flexible(
          child: TextField(
            cursorColor: Color(0xff3f7380),
            controller: _prophylacticTreatmentDetailsController,
            onChanged: (text) {
              widget.prophTreatment["treatmentDetails"] = text;
            },
          ),
        ),
        SizedBox(width: 20.0),
        Text("Início: "),
        GestureDetector(
            child: new Icon(
              Icons.calendar_today,
              color: Color(0xff3f7380),
              size: 20.0,
            ),
            onTap: () async {
              final datePick = await showDatePicker(
                  context: context,
                  initialDate: DateTime(
                    widget.prophTreatment["startDate"][0],
                    widget.prophTreatment["startDate"][1],
                    widget.prophTreatment["startDate"][2],
                  ),
                  firstDate: DateTime(1900),
                  lastDate: DateTime.now());
              if (datePick != null &&
                  datePick != widget.prophTreatment["startDate"]) {
                setState(() {
                  widget.prophTreatment["startDate"] = [
                    datePick.year,
                    datePick.month,
                    datePick.day
                  ];
                });
              }
            }),
        SizedBox(width: 5.0),
        Text(
          "${widget.prophTreatment["startDate"][2]}/" +
              "${widget.prophTreatment["startDate"][1]}/" +
              "${widget.prophTreatment["startDate"][0]}",
        ),
      ],
    );
  }
}
