import 'package:flutter/material.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/utils/utilities.dart';

class Tab1 extends StatefulWidget {
  final TabController controller;
  final Map<String, dynamic> userAttributes;

  Tab1(this.controller, this.userAttributes);

  @override
  _Tab1State createState() => _Tab1State();
}

class _Tab1State extends State<Tab1> with AutomaticKeepAliveClientMixin<Tab1> {
  TextEditingController _initialsController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _initialsController.text = widget.userAttributes["initials"];
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return SingleChildScrollView(
      child: Column(children: <Widget>[
        SizedBox(height: 10.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Número de Utente: " + User().numUtente,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Número de Processo: " + User().numProcess,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Iniciais: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Container(
              width: 150.0,
              child: TextFormField(
                cursorColor: Color(0xff3f7380),
                controller: _initialsController,
                onChanged: (text) {
                  setState(() {
                    widget.userAttributes["initials"] = text;
                  });
                },
              ),
            ),
          ],
        ),
        SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Data de Nascimento: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            GestureDetector(
                child: new Icon(
                  Icons.calendar_today,
                  color: Color(0xff3f7380),
                ),
                onTap: () async {
                  final datePick = await showDatePicker(
                      context: context,
                      locale: Localizations.localeOf(context),
                      initialDate: widget.userAttributes["birthdate"],
                      firstDate: DateTime(1900),
                      lastDate: DateTime.now());
                  if (datePick != null &&
                      datePick != widget.userAttributes["birthdate"]) {
                    setState(() {
                      widget.userAttributes["birthdate"] = datePick;
                    });
                  }
                }),
            SizedBox(width: 10.0),
            Text((() {
              if (User().birthdate.day == 1 &&
                  User().birthdate.month == 1 &&
                  User().birthdate.year == 1) {
                return "";
              }
              return Utilities.convertDateToString(
                  widget.userAttributes["birthdate"]);
            })()),
          ],
        ),
        SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Sexo: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        SizedBox(height: 10.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40.0),
          child: Container(
            height: 30.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Feminino",
                    style: widget.userAttributes["isWoman"]
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.userAttributes["isWoman"] = true;
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Masculino",
                    style: !widget.userAttributes["isWoman"]
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.userAttributes["isWoman"] = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Enxaqueca: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        SizedBox(height: 10.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40.0),
          child: Container(
            height: 30.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Com aura",
                    style: widget.userAttributes["withAura"]
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.userAttributes["withAura"] = true;
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Sem aura",
                    style: !widget.userAttributes["withAura"]
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.userAttributes["withAura"] = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Exclusivamente unilateral: ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        SizedBox(height: 10.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40.0),
          child: Container(
            height: 30.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2.0),
              color: Colors.grey.shade50,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text(
                    "Sim",
                    style: widget.userAttributes["unilateral"]
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.userAttributes["unilateral"] = true;
                    });
                  },
                ),
                FlatButton(
                  child: Text(
                    "Não",
                    style: !widget.userAttributes["unilateral"]
                        ? TextStyle(
                            fontSize: 18.0,
                            color: Color(0xff3f7380),
                          )
                        : TextStyle(
                            fontSize: 15.0,
                            color: Color(0xff3f7380).withOpacity(0.7),
                          ),
                  ),
                  onPressed: () {
                    setState(() {
                      widget.userAttributes["unilateral"] = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 30.0),
        Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          FlatButton(
              onPressed: () {
                widget.controller.animateTo(1);
              },
              child: Text("Próximo"))
        ]),
      ]),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
