import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:headache_diary/screens_component/profile/account.dart';
import 'package:headache_diary/screens_component/profile/profile.dart';

class ProfileBase extends StatefulWidget {
  @override
  _ProfileBaseState createState() => _ProfileBaseState();
}

class _ProfileBaseState extends State<ProfileBase>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, initialIndex: 0, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              decoration: BoxDecoration(
                color: Color(0xffdbeded),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/images/perfil.png",
                    width: 20.0,
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    "Perfil",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Color(0xff3f7380),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  //Tabs
                  Container(
                      decoration: BoxDecoration(
                        color: Color(0xff92cdd3),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0),
                        ),
                      ),
                      child: TabBar(
                        indicatorColor: Color(0xff3f7380),
                        controller: _tabController,
                        labelStyle: TextStyle(
                          color: Color(0xffdbeded),
                          fontWeight: FontWeight.bold,
                        ),
                        tabs: <Tab>[
                          Tab(text: "Dados Pessoais"),
                          Tab(text: "Dados de Conta"),
                        ],
                      )),

                  //Views
                  Container(
                      height: MediaQuery.of(context).size.height * 0.75,
                      width: 500.0,
                      child: TabBarView(
                          controller: _tabController,
                          children: <Widget>[
                            Profile(_tabController),
                            Account(
                              _tabController,
                            ),
                          ])),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
