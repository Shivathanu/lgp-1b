import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/profile/comorbilidades.dart';
import 'package:headache_diary/screens_component/profile/dialogSave.dart';
import 'package:headache_diary/screens_component/profile/pessoalInfo.dart';
import 'package:headache_diary/screens_component/profile/treatment.dart';
import 'package:headache_diary/utils/database.dart';

class Base extends StatefulWidget {
  @override
  _BaseState createState() => _BaseState();
}

class _BaseState extends State<Base> with SingleTickerProviderStateMixin {
  TabController _tabController;

  Map<String, dynamic> userAttributes;
  DataBase db = DataBase();

  bool dialogSaveIsPressed = false;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this, initialIndex: 0);

    userAttributes = new HashMap();
    userAttributes.putIfAbsent("initials", () => User().name);
    userAttributes.putIfAbsent("birthdate", () => User().birthdate);
    userAttributes.putIfAbsent("isWoman",
        () => User().gender.startsWith("f") || User().gender.startsWith("F"));
    userAttributes.putIfAbsent("withAura", () => User().migraineWithAura);
    userAttributes.putIfAbsent(
        "unilateral", () => User().exclusivelyUnilateral);

    userAttributes.putIfAbsent(
        "symptomaticTreatment", () => User().symptomaticTreatment);
    userAttributes.putIfAbsent("symptomaticTreatmentDetails",
        () => User().symptomaticTreatmentDetails);
    userAttributes.putIfAbsent("prophylacticTreatments",
        () => User().prophylacticTreatments.map((e) => {}..addAll(e)).toList());

    userAttributes.putIfAbsent("insomnia", () => User().insomnia);
    userAttributes.putIfAbsent("anxiety", () => User().anxiety);
    userAttributes.putIfAbsent("depression", () => User().depression);
    userAttributes.putIfAbsent(
        "otherMedicalConditions", () => User().otherMedicalConditions);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Color(0xff707070),
          ),
          onPressed: () {
            showWarning();
          },
        ),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Color(0xff3f7380), width: 1.5),
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Column(
                children: <Widget>[
                  //Tabs
                  Container(
                      child: TabBar(
                    indicatorColor: Color(0xff3f7380),
                    controller: _tabController,
                    labelColor: Color(0xff3f7380),
                    tabs: <Tab>[
                      Tab(text: "Pessoal"),
                      Tab(text: "Tratamento"),
                      Tab(text: "Comorbilidades")
                    ],
                  )),
                  //Views
                  Container(
                      height: MediaQuery.of(context).size.height * 0.7,
                      width: 350.0,
                      padding: EdgeInsets.all(10.0),
                      child: TabBarView(
                          controller: _tabController,
                          children: <Widget>[
                            Tab1(_tabController, userAttributes),
                            Tab2(_tabController, userAttributes),
                            Tab3(_tabController, userAttributes),
                          ])),
                ],
              ),
            ),
            ButtonTheme(
              minWidth: MediaQuery.of(context).size.width,
              height: 50.0,
              child: RaisedButton(
                child: Text("Guardar",
                    style: TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                onPressed: () {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (BuildContext context) {
                        // retorna um objeto do tipo Dialog
                        return SaveEditProfile(userAttributes);
                      });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showWarning() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return AlertDialog(
            title: new Text("Aviso"),
            content: new Text(
              "As suas alterações não serão guardadas.",
            ),
            actions: <Widget>[
              // define os botões na base do dialogo
              new FlatButton(
                child: new Text("Cancelar",
                    style: TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              new FlatButton(
                child: new Text("Continuar",
                    style: TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
}
