import 'package:flutter/material.dart';

class Tab3 extends StatefulWidget {
  final TabController controller;
  final Map<String, dynamic> userAttributes;

  Tab3(this.controller, this.userAttributes);

  @override
  _Tab3State createState() => _Tab3State();
}

class _Tab3State extends State<Tab3> with AutomaticKeepAliveClientMixin<Tab3> {
  TextEditingController _otherMedicalConditionsController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return SingleChildScrollView(
      child: SizedBox(
        height: 470,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Depressão: ",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              SizedBox(height: 10.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Container(
                  height: 30.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 2.0),
                    color: Colors.grey.shade50,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          "Sim",
                          style: widget.userAttributes["depression"]
                              ? TextStyle(
                                  fontSize: 18.0,
                                  color: Color(0xff3f7380),
                                )
                              : TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xff3f7380).withOpacity(0.7),
                                ),
                        ),
                        onPressed: () {
                          setState(() {
                            widget.userAttributes["depression"] = true;
                          });
                        },
                      ),
                      FlatButton(
                        child: Text(
                          "Não",
                          style: !widget.userAttributes["depression"]
                              ? TextStyle(
                                  fontSize: 18.0,
                                  color: Color(0xff3f7380),
                                )
                              : TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xff3f7380).withOpacity(0.7),
                                ),
                        ),
                        onPressed: () {
                          setState(() {
                            widget.userAttributes["depression"] = false;
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Ansiedade: ",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              SizedBox(height: 10.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Container(
                  height: 30.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 2.0),
                    color: Colors.grey.shade50,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          "Sim",
                          style: widget.userAttributes["anxiety"]
                              ? TextStyle(
                                  fontSize: 18.0,
                                  color: Color(0xff3f7380),
                                )
                              : TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xff3f7380).withOpacity(0.7),
                                ),
                        ),
                        onPressed: () {
                          setState(() {
                            widget.userAttributes["anxiety"] = true;
                          });
                        },
                      ),
                      FlatButton(
                        child: Text(
                          "Não",
                          style: !widget.userAttributes["anxiety"]
                              ? TextStyle(
                                  fontSize: 18.0,
                                  color: Color(0xff3f7380),
                                )
                              : TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xff3f7380).withOpacity(0.7),
                                ),
                        ),
                        onPressed: () {
                          setState(() {
                            widget.userAttributes["anxiety"] = false;
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Insónias: ",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              SizedBox(height: 10.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Container(
                  height: 30.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 2.0),
                    color: Colors.grey.shade50,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        child: Text(
                          "Sim",
                          style: widget.userAttributes["insomnia"]
                              ? TextStyle(
                                  fontSize: 18.0,
                                  color: Color(0xff3f7380),
                                )
                              : TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xff3f7380).withOpacity(0.7),
                                ),
                        ),
                        onPressed: () {
                          setState(() {
                            widget.userAttributes["insomnia"] = true;
                          });
                        },
                      ),
                      FlatButton(
                        child: Text(
                          "Não",
                          style: !widget.userAttributes["insomnia"]
                              ? TextStyle(
                                  fontSize: 18.0,
                                  color: Color(0xff3f7380),
                                )
                              : TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xff3f7380).withOpacity(0.7),
                                ),
                        ),
                        onPressed: () {
                          setState(() {
                            widget.userAttributes["insomnia"] = false;
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 40.0),
              Row(
                children: <Widget>[
                  Text("Outras doenças relevantes: "),
                ],
              ),
              Flexible(
                child: TextFormField(
                  controller: _otherMedicalConditionsController,
                  onChanged: (text) {
                    setState(() {
                      widget.userAttributes["otherMedicalConditions"] =
                          _otherMedicalConditionsController.text;
                    });
                  },
                ),
              ),
              SizedBox(height: 50.0),
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    FlatButton(
                        onPressed: () {
                          widget.controller.animateTo(1);
                        },
                        child: Text("Anterior"))
                  ]),
            ]),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
