import 'package:flutter/material.dart';
import 'package:headache_diary/main.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/signIn/signIn.dart';
import 'package:headache_diary/service/http_requests.dart';
import 'package:headache_diary/utils/database.dart';
import 'package:toast/toast.dart';

class DeleteAccount extends StatefulWidget {
  @override
  _DeleteAccountState createState() => _DeleteAccountState();
}

class _DeleteAccountState extends State<DeleteAccount> {
  bool buttonPressed = false;
  User user = User();
  DataBase db = DataBase();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xffdbeded),
      appBar: AppBar(
        backgroundColor: Color(0xffe1e1e1),
        title: Center(
          child: Text(
            "MY HEALTH DIARY",
            style: TextStyle(color: Color(0xff3f7380)),
          ),
        ),
        leading: Container(),
        actions: <Widget>[Image.asset("assets/images/Untitled-1.png")],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(50.0),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Apagar a sua conta significa que todos os seus dados serão eliminados da base de dados da aplicação e do Hospital correspondente. \nDeseja continuar?",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Color(0xff3f7380), fontSize: 18.0),
                ),
                SizedBox(height: 15.0),
                buttonPressed
                    ? Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              Color(0xff3f7380)),
                        ),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              height: 50.0,
                              width: 50.0,
                              child: Image.asset("assets/images/alert.png")),
                          SizedBox(height: 50.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              RaisedButton(
                                child: Text(
                                  "Apagar Conta",
                                  style: TextStyle(color: Colors.grey),
                                ),
                                color: Colors.white,
                                onPressed: () async {
                                  if (!buttonPressed) {
                                    setState(() {
                                      buttonPressed = true;
                                    });
                                    String expireTokenString =
                                        await getExpireToken();
                                    DateTime expireToken =
                                        DateTime.parse(expireTokenString);
                                    if (expireToken.isAfter(DateTime.now())) {
                                      bool isConnected =
                                          await checkConnection();
                                      if (isConnected) {
                                        String value =
                                            await deleteUser(user.code);
                                        if (value == "DELETE Response") {
                                          user.logged = 0;
                                          db.deleteUser(user).then((val) async {
                                            user.id = null;

                                            await flutterLocalNotificationsPlugin
                                                .cancelAll();
                                          });

                                          Toast.show("Conta Apagada!", context,
                                              duration: Toast.LENGTH_LONG,
                                              gravity: Toast.BOTTOM);
                                          Navigator.pushAndRemoveUntil(
                                              context,
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          SignInPage()),
                                              (Route<dynamic> route) => false);
                                        } else {
                                          Toast.show(
                                              "Servidor falhou!", context,
                                              duration: 7,
                                              gravity: Toast.BOTTOM);
                                        }
                                      } else {
                                        Toast.show(
                                            "Não está ligado à internet! \nPor esse motivo, a sua conta não será apagada!",
                                            context,
                                            duration: 7,
                                            gravity: Toast.BOTTOM);
                                      }
                                    } else {
                                      Toast.show(
                                          "Login expirou! Por favor inicie sessão.",
                                          context,
                                          duration: 5,
                                          gravity: Toast.BOTTOM);
                                      user.logged = 0;
                                      db
                                          .updateUserState(user)
                                          .then((val) async {
                                        user.id = null;

                                        await flutterLocalNotificationsPlugin
                                            .cancelAll();

                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        SignInPage()),
                                            (Route<dynamic> route) => false);
                                      });
                                    }
                                  }
                                  setState(() {
                                    buttonPressed = false;
                                  });
                                },
                              ),
                              RaisedButton(
                                color: Colors.white,
                                child: Text(
                                  "Cancelar",
                                  style: TextStyle(color: Color(0xff3f7380)),
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                SizedBox(height: 50.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
