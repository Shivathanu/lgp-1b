import 'package:flutter/material.dart';
import 'package:headache_diary/model/user.dart';
import 'package:headache_diary/screens_component/profile/changePassword.dart';
import 'package:headache_diary/screens_component/profile/deleteAccount.dart';
import 'package:headache_diary/utils/exportfile.dart';
import 'package:open_file/open_file.dart';

class Account extends StatefulWidget {
  final TabController controller;
  Account(this.controller);

  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 20.0),
            Padding(
              padding: const EdgeInsets.only(
                top: 5.0,
                left: 35.0,
                right: 35.0,
              ),
              child: Container(
                padding: EdgeInsets.all(15.0),
                height: 200.0,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff3f7380), width: 2.0),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 5.0),
                    Row(
                      children: <Widget>[
                        Text(
                          "Iniciais do nome: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(User().name),
                      ],
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      children: <Widget>[
                        Text(
                          "Nº de utente: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(User().numUtente),
                      ],
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      children: <Widget>[
                        Text(
                          "Nº de Processo: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(User().numProcess),
                      ],
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      children: <Widget>[
                        Text(
                          "Email: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(User().email),
                      ],
                    ),
                    SizedBox(height: 20.0),
                    RaisedButton(
                        color: Colors.white,
                        child: Text("Alterar password",
                            style: TextStyle(
                                color: Color(0xff3f7380), fontSize: 15.0)),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      ChangePassword()));
                        }),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("Exportar dados",
                      style:
                          TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                  onPressed: () {
                    writeToFile(User().toJSONExport())
                        .then((value) => OpenFile.open(value.path));
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  color: Colors.white,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("Apagar conta",
                      style:
                          TextStyle(color: Color(0xff3f7380), fontSize: 15.0)),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                DeleteAccount()));
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  color: Colors.white,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
