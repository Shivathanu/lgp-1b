class Utilities {
  static String convertDateToString(DateTime d) {
    return d.day.toString() +
        "/" +
        d.month.toString() +
        "/" +
        d.year.toString();
  }
}
