import 'dart:convert';

import 'package:headache_diary/model/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

final String userTable = "userTable";

final String idColumn = "idColumn";
final String userColumn = "userColumn";
final String loggedUserColumn = "loggedUserColumn";

class DataBase {
  //Singleton - só existe uma instância desta classe. Sempre que for instanciado (new DataBase) é chamada esta instância.
  static final DataBase _instance = DataBase.internal();
  factory DataBase() => _instance;
  DataBase.internal();

  Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    } else {
      _db = await initDB();
      return _db;
    }
  }

  Future<Database> initDB() async {
    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, "signals.db");
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int newerVersion) async {
      await db.execute("CREATE TABLE $userTable(" +
          "$idColumn INTEGER PRIMARY KEY, " +
          "$userColumn TEXT," +
          "$loggedUserColumn INTEGER);");
    });
  }

  Future close() async {
    Database database = await db;
    database.close();
  }

  Future<int> addUser(User user) async {
    Database database = await db;
    user.id = await database.insert(userTable, user.toMap());
    return user.id;
  }

  Future<int> updateUserState(User user) async {
    Database database = await db;
    int a = await database.rawUpdate(
        "UPDATE $userTable SET $loggedUserColumn = ? WHERE $idColumn = ?",
        [user.logged, user.id]);
    //getAllUser();
    return a;
  }

  Future<int> updateUser(User user) async {
    Database database = await db;
    return await database.update(userTable, user.toMap(),
        where: "$idColumn = ?", whereArgs: [user.id]);
  }

  Future<int> deleteUser(User user) async {
    Database database = await db;
    return await database
        .delete(userTable, where: "$idColumn = ?", whereArgs: [user.id]);
  }

  Future<int> deleteUsers() async {
    Database database = await db;
    return await database.delete(userTable);
  }

  Future<User> getLoggedUser() async {
    Database database = await db;
    User user = User();
    List listMap = await database
        .rawQuery("SELECT * FROM $userTable " + "WHERE $loggedUserColumn = 1");

    if (listMap.length == 1) {
      return user.fromMap(listMap[0]);
    } else if (listMap.length > 1) {
      return user.fromMap(listMap[0]);
    } else {
      return null;
    }
  }

  Future<int> getUserLoggedIn(String code) async {
    Database database = await db;
    List listMap = await database.rawQuery("SELECT * FROM $userTable");
    for (Map m in listMap) {
      if (code == json.decode(m[userColumn])["code"]) {
        return m[idColumn];
      }
    }
    return -1;
  }

  void getAllUser() async {
    Database database = await db;
    List listMap = await database.rawQuery("SELECT * FROM $userTable");
    for (Map m in listMap) {
      print("${m[idColumn]} &&& ${m[loggedUserColumn]}");
    }
  }
}
