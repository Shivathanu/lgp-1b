import React, { Component } from "react";
import './ErrorBoundary.css';

class ErrorBoundary extends Component {
    state = {
        errorMessage: ''
    }

    static getDerivedStateFromError(error) {
        return { errorMessage: error.toString() }
    }

    componentDidCatch(error, info) {
        this.logErrorToServices(error.toString(), info.componentStack)
    }


    logErrorToServices = console.log

    render() {
        if (this.state.errorMessage) {
            return (
                <div id="notfound">
                    <div class="notfound">
                        <div class="notfound-404"></div>
                        <h2>Oops! Algo correu mal</h2>
                        <p>Desculpe mas algo correu mal ao carregar a página desejada ou a mesma está temporariamente indisponível.</p>
                        <a href="/">Voltar ao início</a>
                    </div>
                </div>
            )
        }
        return this.props.children
    }
}

export default ErrorBoundary;