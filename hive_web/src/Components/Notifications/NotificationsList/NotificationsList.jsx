import React, { useEffect, useState, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Badge from '@material-ui/core/Badge';
import { UserInfoContext } from '../../UserInfo/UserInfoContext';
import * as API from '../../../Configs/API';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import { ThemeProvider } from '@material-ui/styles';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import { createMuiTheme } from '@material-ui/core/styles';

import './NotificationsList.css';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(1),
            width: theme.spacing(16),
            height: theme.spacing(16),
        },
    },
}));

/**
 * OVERRIDE STYLES FROM MATERIAL-UI
 */

const theme = createMuiTheme({
    palette: {
        secondary: {
            // This is green.A700 as hex.
            main: '#0f7387',
        },
    },
});

export default function NotificationsList() {
    const [notifications, setNotifications] = useState({
        notificationsToDisplay: [],
        everyNotifications: []
    });

    const [userData, setUserData] = useContext(UserInfoContext);
    const classes = useStyles();
    
    useEffect(() => {
        async function getNotifications() {

            setUserData({
                ...userData,
                loadingOpen: true
            })

            const servicePath = `${API.apiPath}api/professionals/alerts`;
            const data = await fetch(servicePath, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${sessionStorage.getItem('sessionToken')}`
                }
            });

            const notificationsItemsFromService = await data.json();

            if (data.status === 200) {
                var notificationItems = [];
                for (var i = 0; i < notificationsItemsFromService.length; i++) {
                    var newNotification = {}
                    for (var key in notificationsItemsFromService[i]) {
                        var item = notificationsItemsFromService[i][key];
                        if (key === 'dateTime') {
                            var month = (item[1].toString().length === 1) ? '0' + item[1] : item[1];
                            var date = item[2] + '-' + month + '-' + item[0];
                            newNotification.date = date;
                        } else {
                            newNotification[key] = item;
                        }
                    }
                    var result = Object.values(newNotification).join()
                    newNotification['toQuery'] = result;
                    notificationItems.push(newNotification);
                }
                console.log(notificationItems);
                setNotifications({
                    ...notifications,
                    notificationsToDisplay: notificationItems,
                    everyNotifications: notificationItems
                })

                setUserData({
                    ...userData,
                    loadingOpen: false
                })

            } else {

                alert(notificationsItemsFromService.message);
                setUserData({
                    ...userData,
                    loadingOpen: false
                })

            }

        }

        getNotifications();
    }, []);

    function searchBarChange(e) {
        setNotifications({
            ...notifications,
            notificationsToDisplay: notifications.everyNotifications.filter(x => x.toQuery.toUpperCase().includes(e.target.value.toUpperCase()))
        })
    }

    return (
        <div>
            <h2>Notificações dos doentes</h2>
            <Grid container spacing={4}>
                <Grid item xs={12} md={9} lg={9}>
                    <div className="searchBarWrapper">
                        <form>
                            <FormControl fullWidth variant="outlined">
                                <ThemeProvider theme={theme}>
                                    <InputLabel color="secondary" htmlFor="search-bar">Pesquisar...</InputLabel>
                                    <OutlinedInput
                                        id='search-bar'
                                        name='search-bar'
                                        type='search'
                                        onChange={searchBarChange}
                                        labelWidth={130}
                                        style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                        color="secondary"
                                        endAdornment={<InputAdornment position="start"><SearchIcon /></InputAdornment>}
                                    />
                                </ThemeProvider>
                            </FormControl>
                        </form>
                    </div>
                </Grid>
            </Grid>
            {
                notifications.notificationsToDisplay.length > 0 ?
                    <Grid container spacing={1} id="notificationsContainer">
                        {notifications.notificationsToDisplay.map((notification) => (
                            <Grid className="notificationsItem" item xs={12} md={9} md={9}>
                                <Badge color="error" variant="dot" invisible={notification.readByRecipient}>
                                    <Paper className="paperContainer">
                                        <div>
                                            <span id="dateNot">{notification.date}</span>
                                        </div>
                                        <h3>{notification.subject}</h3>
                                        <span>{notification.message}</span>
                                    </Paper>
                                </Badge>
                            </Grid>
                        ))}
                    </Grid>
                    :
                    <Grid container>
                        <Grid item xs={12} md={9} md={9}>
                            <Paper style={{ height: '265px' }}><p style={{ textAlign: 'center', paddingTop: '120px' }}>Sem notificações</p></Paper>
                        </Grid>
                    </Grid>


            }
        </div>
    );
}