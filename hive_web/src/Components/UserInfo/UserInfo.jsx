import React, { useContext } from 'react';
import './UserInfo.css';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import { UserInfoContext } from './UserInfoContext';
import { useEffect } from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as API from '../../Configs/API';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
    height: '100%',
  },
}));

export default function UserInfo(props) {
  const [data, setData] = useContext(UserInfoContext);
  const classes = useStyles();

  useEffect(() => {

    async function setUserInfoByRequest() {
      const loginServicePath = `${API.apiPath}api/professionals/login`;

      const loginServiceData = await fetch(loginServicePath, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization' : `Bearer ${sessionStorage.getItem('sessionToken')}`,
        }
      });

      const loginDataItems = await loginServiceData.json();
      if (loginServiceData.status === 200) {
        setUserInfo(loginDataItems.professionalProfile);
      } else {
        alert(loginDataItems.message)
      }
    }

    function setUserInfo(professional) {
      if (Object.keys(data.professional).length === 1 && data.professional.constructor === Object) {
        const newValues = { ...data };

        //Set professional
        newValues.professional = professional
        newValues.professional.tempImage = '';
        if(!sessionStorage.getItem("professionalEmail")){
          sessionStorage.setItem("professionalId", professional.id);
        }
        if(!sessionStorage.getItem("professionalEmail")){
          sessionStorage.setItem("professionalEmail", professional.email)
        }
        
        //Set date to display
        var today = new Date();
        newValues.dateToDisplay = newValues.weekDays[today.getDay()] + ', ' + today.getDate() + ' de ' + newValues.months[today.getMonth()] + ' ' + today.getFullYear();

        //set new state
        setData(newValues);
      }
    }


    if (props.currrentState.professionalContext === null && Object.keys(data.professional).length === 1 && data.professional.constructor === Object) {
      setUserInfoByRequest();
    } else {
      setUserInfo(props.currrentState.professionalContext);
    }


  }, [props.currrentState, data, setData]);

  function transform() {
    var profilePic = "/broken-image.jpg";
    if (data.professional.image !== '' || data.professional.image !== null || data.professional.image !== undefined) {
      profilePic = 'data:image/jpeg;base64,' + data.professional.image;
    }

    return profilePic;
  }

  return (
    <div className="contextRoot">
      <div>
        <span>
          <Avatar src={transform()} className={classes.large} />
        </span>
        <span>
          <p>Dr(a). {data.professional.clinicalName}</p>
        </span>
      </div>
      <p>{data.dateToDisplay}</p>
      <Backdrop id="backDrop" className={classes.backdrop} open={data.loadingOpen} >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}