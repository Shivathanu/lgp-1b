import React, { useState, useEffect } from 'react';
import './ProfessionalAuth.css';
import * as API from '../../../Configs/API';

import axios from 'axios';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MuiAlert from '@material-ui/lab/Alert';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Snackbar from '@material-ui/core/Snackbar';
import TextField from '@material-ui/core/TextField';

import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { makeStyles } from '@material-ui/core/styles';

//Image Resources
import login_registo from '../../../Resources/login_registo.png';
import usl_matosinhos from '../../../Resources/usl_matosinhos.png';
import hive_health from '../../../Resources/HIVE_health.png';
import reset_pass from '../../../Resources/reset_pass.png';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
        height: '100%',
    },
}));

const theme = createMuiTheme({
    palette: {
        secondary: {
            // This is green.A700 as hex.
            main: '#0f7484',
        },
    },
});

export default function ProfessionalAuth({ changeSessionState }) {
    const [screenData, setScreenData] = useState({
        recoverPass: false,
        loginScreen: true,
        buttonContent: 'Criar Conta',
        registrationData: {
            clinicalName: '',
            email: '',
            speciality: {},
            healthCenter: {},
            username: '',
            password: '',
            confirmPassword: '',
            passwordIsValid: true,
            passwordWarningText: '',
            healthCenterDisable: true,
            emailIsValid: true,
            emailWarningText: ''
        },
        loginData: {
            username: '',
            password: '',
            loginSuccess: true,
            loginWarning: 'O e-mail ou a palavra-passe é inválido'
        },
        changePassData: {
            email: ''
        },
        healthCenterOptions: [],
        specialityOptions: []
    })
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const classes = useStyles();

    function onRegistrationChange(newRegState) {
        setScreenData({
            ...screenData,
            registrationData: newRegState
        })
    }

    function onLoginChange(newLoginState) {
        setScreenData({
            ...screenData,
            loginData: newLoginState
        })
    }

    function onRecoverPassChange(newRecoverState) {
        setScreenData({
            ...screenData,
            changePassData: newRecoverState
        })
    }

    function setHealthCenterOptions(options) {
        setScreenData({
            ...screenData,
            healthCenterOptions: options
        })
    }

    function setSpecialityOptions(options) {
        setScreenData({
            ...screenData,
            specialityOptions: options
        })
    }

    function switchView(switchedPass = false) {
        var buttonContentOptions = ['Criar Conta', 'Iniciar Sessão', 'Voltar'];
        var newObject = { ...screenData };

        if (switchedPass === true) {
            setOpen(true);
        }

        if (screenData.buttonContent === buttonContentOptions[2] || screenData.buttonContent === buttonContentOptions[1]) {
            newObject.buttonContent = buttonContentOptions[0];
            newObject.loginScreen = true;
            newObject.recoverPass = false;
        } else {
            newObject.buttonContent = buttonContentOptions[1];
            newObject.loginScreen = false;
        }

        newObject.registrationData = {
            clinicalName: '',
            email: '',
            speciality: {},
            healthCenter: {},
            username: '',
            password: '',
            confirmPassword: '',
            passwordIsValid: true,
            passwordWarningText: '',
            healthCenterDisable: true,
            emailIsValid: true,
            emailWarningText: ''
        };
        newObject.loginData = {
            username: '',
            password: '',
            loginSuccess: true,
            loginWarning: 'O e-mail ou a palavra-passe é inválido'
        };

        newObject.changePassData = {
            email: ''
        };

        setScreenData(newObject);
    }

    function changeState(newState) {
        setScreenData(newState);
    }

    function enterOnApp(professionalProfile) {
        changeSessionState({
            isLoggedIn: true,
            email: professionalProfile.email,
            professionalContext: professionalProfile
        })
    }

    function changePass() {
        setScreenData({
            ...screenData,
            buttonContent: 'Voltar',
            recoverPass: !screenData.recoverPass
        })
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <div id="authContent">
            <div id="navBar">
                <div className="logoText" id="authLogoText">
                    <h2>My <br />Health <br />Diary</h2>
                </div>
                <div className="regLogButtonCont">
                    <ThemeProvider theme={theme}>
                        <Button onClick={switchView} className="regLogButton" variant="contained" size="large">{screenData.buttonContent}</Button>
                    </ThemeProvider>
                </div>
            </div>
            <div id="imageAndForm">
                <Grid container >
                    <Grid item xs={6} >
                        <img className="image_to" src={login_registo} alt="doctors" />
                    </Grid>
                    <Grid item xs={6}>
                        <Grid container id="forms">
                            <Grid item xs={12} md={9} lg={9}>
                                {
                                    screenData.recoverPass === false ?
                                        screenData.loginScreen === true ?
                                            <LoginForm loginData={screenData.loginData} changeLoginData={onLoginChange} enterOnApp={enterOnApp} setLoading={setLoading} changePass={changePass} />
                                            :
                                            <RegistrationForm registrationData={screenData.registrationData}
                                                healthCenterOptions={screenData.healthCenterOptions}
                                                specialityOptions={screenData.specialityOptions}
                                                changeRegistrationData={onRegistrationChange}
                                                setHealthCenterOptions={setHealthCenterOptions}
                                                setSpecialityOptions={setSpecialityOptions}
                                                switchView={switchView}
                                                screenData={screenData}
                                                changeState={changeState}
                                                setLoading={setLoading} />
                                        :
                                        <RecoverPassword
                                            changePassData={screenData.changePassData}
                                            setRecoverPassData={onRecoverPassChange}
                                            setLoading={setLoading}
                                            switchView={switchView} />
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
            <div id="footerContainer">
                <div className="footer">
                    <img className="footerImage" src={usl_matosinhos} alt="usl matosinhos" />
                    <img className="footerImage" src={hive_health} alt="hive health" />
                </div>
            </div>
            <Backdrop id="backDrop" className={classes.backdrop} open={loading} >
                <CircularProgress color="inherit" />
            </Backdrop>
            <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    Nova password enviada via email!
                </Alert>
            </Snackbar>
        </div >
    )
}

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function LoginForm({ loginData, changeLoginData, enterOnApp, setLoading, changePass }) {

    async function handleSubmit(e) {
        e.preventDefault();
        const loginToSend = Object.assign({}, loginData);
        delete loginToSend.loginSuccess;
        delete loginToSend.loginWarning;
        setLoading(true);
        const servicePath = `${API.apiPath}authenticate`;

        const data = await fetch(servicePath, {
            method: 'POST',
            body: JSON.stringify(loginToSend),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        var authObj = await data.json()
        if (data.status === 200) {
            const loginServicePath = `${API.apiPath}api/professionals/login`;

            const loginServiceData = await fetch(loginServicePath, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${authObj.token}`,
                }
            });

            const loginDataItems = await loginServiceData.json();
            if (loginServiceData.status === 200) {
                sessionStorage.setItem('sessionToken', authObj.token);
                enterOnApp(loginDataItems.professionalProfile);
                setLoading(false);
            } else {
                alert(loginDataItems.message)
                setLoading(false);
            }

        } else {
            changeLoginData({
                ...loginData,
                loginSuccess: false
            })
            setLoading(false);
        }
    }

    function handleChange(e) {
        changeLoginData({
            ...loginData,
            [e.target.name]: e.target.value
        })
    }

    return (
        <div>
            <form onSubmit={handleSubmit} validate='true'>
                <ThemeProvider theme={theme}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <FormControl error={!loginData.loginSuccess} size="small" fullWidth variant="outlined">
                                <InputLabel color="secondary" htmlFor="clinicalName" required>Email</InputLabel>
                                <OutlinedInput
                                    required
                                    value={loginData.username}
                                    name='username'
                                    type='text'
                                    onChange={handleChange}
                                    labelWidth={65}
                                    style={{ borderRadius: '20px', backgroundColor: "#fff" }}
                                    color="secondary" />
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl error={!loginData.loginSuccess} size="small" fullWidth variant="outlined">
                                <InputLabel color="secondary" htmlFor="password" required>Palavra-passe</InputLabel>
                                <OutlinedInput
                                    required
                                    value={loginData.password}
                                    name='password'
                                    type='password'
                                    onChange={handleChange}
                                    labelWidth={120}
                                    style={{ borderRadius: '20px', backgroundColor: "#fff" }}
                                    color="secondary" />
                                <FormHelperText
                                    id="password-error"
                                    style={!loginData.loginSuccess ? { visibility: 'visible' } : { visibility: 'hidden' }}
                                >{loginData.loginWarning}</FormHelperText>
                            </FormControl>
                            <div className="recoverPass">
                                <a onClick={changePass}>Recuperar palavra-passe</a>
                            </div>
                        </Grid>
                    </Grid>
                    <div className="buttonWrapper">
                        <Button type="submit"
                            variant="contained"
                            size="small"
                            color="secondary"
                            className="registerProfessional">INICIAR SESSÃO</Button>
                    </div>
                </ThemeProvider>
            </form>
        </div>
    )
}

function RegistrationForm({ registrationData,
    healthCenterOptions,
    specialityOptions,
    changeRegistrationData,
    setHealthCenterOptions,
    setSpecialityOptions,
    switchView,
    screenData,
    changeState,
    setLoading }) {

    var classes = useStyles();
    var EMAIL_DOMAIN = '@ulsm.min-saude.pt';

    useEffect(() => {
        async function fetchSpeciality() {
            setLoading(true);
            const servicePath = `${API.apiPath}api/professionals/specialities`;
            const data = await fetch(servicePath);
            const specialityItems = await data.json();
            if (data.status === 200) {
                var specialityItemsFinal = [];
                for (var key in specialityItems) {
                    specialityItemsFinal.push({
                        name: key,
                        healthCenterProvider: specialityItems[key]
                    })
                }
                setSpecialityOptions(specialityItemsFinal);
                setLoading(false);
            } else {
                alert(specialityItems.message);
                setLoading(false);
            }

        }

        fetchSpeciality();
    }, [])

    async function handleSubmit(e) {
        e.preventDefault();

        const registerUser = Object.assign({}, registrationData);
        var emailIsValid = validateEmail(registerUser.email);

        if (emailIsValid) {
            if (registerUser.confirmPassword === registerUser.password) {
                if (registerUser.password.length >= 9) {
                    registrationData.passwordIsValid = true;

                    var userToSend = Object.assign({}, registerUser);
                    userToSend.username = registerUser.email;
                    userToSend.speciality = registerUser.speciality.name;
                    delete userToSend.confirmPassword;
                    delete userToSend.passwordIsValid;
                    delete userToSend.passwordWarningText;
                    delete userToSend.healthCenterDisable;
                    delete userToSend.emailWarningText;
                    delete userToSend.emailIsValid;

                    setLoading(true);
                    var servicePath = `${API.apiPath}api/register/professionals`;

                    const data = await fetch(servicePath, {
                        method: 'POST',
                        body: JSON.stringify(userToSend),
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    });

                    if (data.status === 200) {
                        alert("Registo efetuado com sucesso")
                        switchView();
                        setLoading(false);
                    } else {
                        const result = await data.json();
                        alert(result.message);
                        setLoading(false);
                    }
                } else {
                    changeRegistrationData({
                        ...registrationData,
                        passwordWarningText: "Defina uma palavra-passe com pelo menos 9 caracteres",
                        passwordIsValid: false
                    })
                }

            } else {
                changeRegistrationData({
                    ...registrationData,
                    passwordWarningText: "A palavra-passe definida não corresponde à confirmação",
                    passwordIsValid: false
                })
            }
        } else {
            changeRegistrationData({
                ...registrationData,
                emailWarningText: "O email inserido não é válido",
                emailIsValid: false
            })
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(email)) {
            if (email.indexOf(EMAIL_DOMAIN, email.length - EMAIL_DOMAIN.length) !== -1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    async function handleChange(e) {
        if (e.target.id.includes('healthCenter')) {

            var splitedArray = e.target.id.split('-');
            changeRegistrationData({
                ...registrationData,
                healthCenter: healthCenterOptions[splitedArray[splitedArray.length - 1]]
            });

        } else if (e.target.id.includes('speciality')) {

            var splitedArray = e.target.id.split('-');
            var healthCenterApi = specialityOptions[splitedArray[splitedArray.length - 1]].healthCenterProvider;

            var newState = { ...screenData };
            newState.registrationData.speciality = specialityOptions[splitedArray[splitedArray.length - 1]];
            newState.registrationData.healthCenter = {};

            if (newState.registrationData.speciality !== undefined && newState.registrationData.speciality !== null && newState.registrationData.speciality !== {}) {
                newState.registrationData.healthCenterDisable = false;
            } else {
                newState.registrationData.healthCenterDisable = true;
            }
            setLoading(true);
            const servicePath = `${API.apiPath + 'api' + healthCenterApi}`;
            const data = await fetch(servicePath);
            const healthCenterItems = await data.json();

            if (data.status === 200) {
                newState.healthCenterOptions = healthCenterItems;
                changeState(newState);
                setLoading(false);
            } else {
                alert(healthCenterItems.message);
                setLoading(false);
            }

        } else {
            changeRegistrationData({
                ...registrationData,
                [e.target.name]: e.target.value,
                emailIsValid: true,
                emailWarningText: '',
                passwordIsValid: true,
                passwordWarningText: ''
            });
        }
    }

    return (
        <div>
            <h2 className="registoTitle">Registo</h2>
            <form onSubmit={handleSubmit} validate='true'>
                <ThemeProvider theme={theme}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <FormControl fullWidth size="small" variant="outlined">
                                <InputLabel color="secondary" htmlFor="clinicalName" required>Nome</InputLabel>
                                <OutlinedInput
                                    required
                                    value={registrationData.clinicalName}
                                    name='clinicalName'
                                    type='text'
                                    onChange={handleChange}
                                    labelWidth={75}
                                    style={{ borderRadius: '20px' }}
                                    color="secondary" />
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl error={!registrationData.emailIsValid} fullWidth size="small" variant="outlined">
                                <InputLabel htmlFor="email" color="secondary" required>Email</InputLabel>
                                <OutlinedInput
                                    required
                                    value={registrationData.email}
                                    name='email'
                                    type='email'
                                    onChange={handleChange}
                                    labelWidth={65}
                                    style={{ borderRadius: '20px' }}
                                    color="secondary"
                                    aria-describedby="email-error" />
                                <FormHelperText
                                    id="email-error"
                                    style={!registrationData.emailIsValid ? { visibility: 'visible' } : { visibility: 'hidden' }}
                                >{registrationData.emailWarningText}</FormHelperText>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl fullWidth size="small" variant="outlined">
                                <div className="dateField">
                                    <Autocomplete
                                        size="small"
                                        id="speciality"
                                        onChange={handleChange}
                                        options={specialityOptions}
                                        value={registrationData.speciality}
                                        getOptionLabel={(option) => option.name ? option.name : ''}
                                        color="secondary"
                                        style={{ borderRadius: '20px' }}
                                        renderInput={(params) => <TextField required {...params} color="secondary" label="Especialidade" variant="outlined" />}
                                    />
                                </div>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl fullWidth variant="outlined">
                                <div className="dateField">
                                    <Autocomplete
                                        disabled={registrationData.healthCenterDisable}
                                        size="small"
                                        id="healthCenter"
                                        onChange={handleChange}
                                        options={healthCenterOptions}
                                        value={registrationData.healthCenter}
                                        getOptionLabel={(option) => option.name ? option.name : ''}
                                        color="secondary"
                                        style={{ borderRadius: '20px' }}
                                        renderInput={(params) => <TextField required {...params} color="secondary" label="Local de Trabalho" variant="outlined" />}
                                    />
                                </div>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl error={!registrationData.passwordIsValid} fullWidth size="small" variant="outlined">
                                <InputLabel htmlFor="password" color="secondary" required>Palavra-passe</InputLabel>
                                <OutlinedInput
                                    required
                                    value={registrationData.password}
                                    type='password'
                                    name='password'
                                    onChange={handleChange}
                                    labelWidth={120}
                                    style={{ borderRadius: '20px' }}
                                    color="secondary" />
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl error={!registrationData.passwordIsValid} fullWidth size="small" variant="outlined">
                                <InputLabel htmlFor="confirmPassword" color="secondary" required>Confirmar palavra-passe</InputLabel>
                                <OutlinedInput
                                    required
                                    value={registrationData.confirmPassword}
                                    type='password'
                                    name='confirmPassword'
                                    onChange={handleChange}
                                    labelWidth={200}
                                    style={{ borderRadius: '20px' }}
                                    aria-describedby="password-error"
                                    color="secondary" />
                                <FormHelperText
                                    id="password-error"
                                    style={!registrationData.passwordIsValid ? { visibility: 'visible' } : { visibility: 'hidden' }}
                                >{registrationData.passwordWarningText}</FormHelperText>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <div className="buttonWrapper">
                        <Button type="submit"
                            variant="contained"
                            size="small"
                            color="secondary"
                            className="registerProfessional">CRIAR</Button>
                    </div>
                </ThemeProvider>
            </form>
        </div >
    )
}

function RecoverPassword({ changePassData, setRecoverPassData, setLoading, switchView }) {

    async function handleSubmit(e) {
        e.preventDefault();
        setLoading(true);

        const resetPassword = `${API.apiPath}api/professionals/passwordreset`;

        const resetPasswordData = await fetch(resetPassword, {
            method: 'PUT',
            body: changePassData.email
        });


        if (resetPasswordData.status === 200) {
            setLoading(false);
            switchView(true);
        } else {
            const result = await resetPasswordData.json();
            alert(result.message)
            setLoading(false);
        }
    }

    function handleChange(e) {
        setRecoverPassData({
            ...changePassData,
            [e.target.name]: e.target.value
        })
    }

    return (
        <div>
            <form onSubmit={handleSubmit} validate='true' className="recoverForm">
                <ThemeProvider theme={theme}>
                    <Grid container spacing={2}>
                        <Grid className="resetImgCont" item xs={12}>
                            <img id="reset_pass_img" src={reset_pass} />
                            <h3>Recuperar a Palavra-passe</h3>
                            <p>Introduza o email com o qual se registou nesta plataforma para alterar o seu acesso à Área Pessoal</p>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl size="small" fullWidth variant="outlined">
                                <InputLabel color="secondary" htmlFor="email" required>Email</InputLabel>
                                <OutlinedInput
                                    required
                                    value={changePassData.email}
                                    name='email'
                                    type='email'
                                    onChange={handleChange}
                                    labelWidth={70}
                                    style={{ borderRadius: '20px', backgroundColor: "#fff" }}
                                    color="secondary" />
                            </FormControl>
                        </Grid>
                    </Grid>
                    <div className="recoverButtonWrapper">
                        <Button type="submit"
                            variant="contained"
                            size="small"
                            color="secondary"
                            className="registerProfessional">REDEFINIR PALAVRA-PASSE</Button>
                    </div>
                </ThemeProvider>
            </form>
        </div>
    )
}
