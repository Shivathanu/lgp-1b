import React, { useContext, useEffect } from 'react';
import './PatientCRUD.css';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { ThemeProvider } from '@material-ui/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { createMuiTheme } from '@material-ui/core/styles';
import { PatientCRUDContext } from './PatientCRUDContext';
import { PatientDashboardContext } from '../PatientDashboard/PatientDashboardContext'
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import { useHistory } from 'react-router-dom';
import * as API from '../../../Configs/API';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { UserInfoContext } from '../../UserInfo/UserInfoContext';

/* teste */
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import IconButton from '@material-ui/core/IconButton';
/* teste */


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        height: "50%"
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,

    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        backgroundColor: "#fff"
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    textFieldDate: {
        width: 150,
    }
}));

const theme = createMuiTheme({
    palette: {
        secondary: {
            // This is green.A700 as hex.
            main: '#0f7387',
        },
        primary: {
            main: '#626263',
        }
    },
});


export default function PatientCRUD(props) {
    const isEditing = props.location.state.isEditing;
    const [patientForm, setForm] = useContext(PatientCRUDContext);
    const [screenMode, setScreenMode] = useState({
        enteredOnEditMode: isEditing,
        isEditing: isEditing,
        patientProfile: {}
    });

    const onCRUDContextChange = (newState) => {
        setForm(newState);
    }

    const onScreenModeChange = (newState) => {
        setScreenMode(newState);
    }

    return (
        <div>
            {screenMode.isEditing === true ?
                <ReadPatientForm patientForm={patientForm} changePatientState={onCRUDContextChange} screenMode={screenMode} changeScreenMode={onScreenModeChange} /> :
                <CreatePatientForm patientForm={patientForm} changePatientState={onCRUDContextChange} screenMode={screenMode} changeScreenMode={onScreenModeChange} />}
        </div>
    );
}

function CreatePatientForm({ patientForm, changePatientState, screenMode, changeScreenMode }) {

    const [radioValues, setRadioValues] = useState({
        migraineWithAura: false,
        exclusivelyUnilateral: false,
        symptomaticTreatment: false,
        prophylacticTreatments: false,
        depression: false,
        anxiety: false,
        insomnia: false,
        healthCenterOptions: []
    });

    const [userData, setUserData] = useContext(UserInfoContext);
    const [prophylacticTreatmentList, setProphylacticTreatmentList] = useState(false);

    const classes = useStyles();
    const history = useHistory();

    useEffect(() => {
        async function fetchHealthCenters() {
            setUserData({
                ...userData,
                loadingOpen: true,
            })
            const servicePath = `${API.apiPath}api/healthcenters`;
            const data = await fetch(servicePath);


            const healthCenterItems = await data.json();
            if (data.status === 200) {
                setRadioValues({
                    ...radioValues,
                    healthCenterOptions: healthCenterItems
                });

                if (screenMode.enteredOnEditMode) {
                    const newValues = { ...radioValues };
                    newValues.healthCenterOptions = healthCenterItems;
                    for (const key in radioValues) {
                        if (key != 'healthCenterOptions' && key != 'prophylacticTreatments') {
                            newValues[key] = (patientForm[key] !== null) ? patientForm[key] : radioValues[key];
                        } else if (key != 'healthCenterOptions') {
                            newValues[key] = (patientForm[key].length > 0) ? true : false;
                        }
                    }
                    setRadioValues(newValues);
                    setProphylacticTreatmentList(newValues.prophylacticTreatments);
                } else {
                    changePatientState({
                        ...patientForm,
                        prophylacticTreatments: patientForm.prophylacticTreatments.concat({
                            id: null,
                            treatmentDetails: '',
                            startDate: ''
                        })
                    })
                }

                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            } else {
                alert(healthCenterItems.message);
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            }

        }

        fetchHealthCenters();

    }, []);

    const handleChange = (event) => {
        const name = event.target.name !== undefined ? event.target.name : (event.target.id.includes('gender') ? 'gender' : (event.target.id.includes('healthCenter') ? 'healthCenter' : ''));

        if (name === "gender" || name === "healthCenter") {
            if (name === "gender") {
                changePatientState({
                    ...patientForm,
                    [name]: event.target.textContent,
                });
            } else {
                var splitedArray = event.target.id.split('-');
                changePatientState({
                    ...patientForm,
                    [name]: radioValues.healthCenterOptions[splitedArray[splitedArray.length - 1]]
                });

            }

        } else if (!radioValues.hasOwnProperty(name)) {
            changePatientState({
                ...patientForm,
                [name]: event.target.value,
            });
        }



        if (radioValues.hasOwnProperty(name)) {
            let typeOfData = typeof event.target.value;

            setRadioValues({
                ...radioValues,
                [name]: (typeOfData === "boolean") ? event.target.value : (event.target.value == "true") ? true : false
            })

            if (name === 'prophylacticTreatments') {
                var radioValue = (typeOfData === "boolean") ? event.target.value : (event.target.value == "true") ? true : false;
                setProphylacticTreatmentList(radioValue);
            }

            if (patientForm.hasOwnProperty(name + "StartDate")) {
                changePatientState({
                    ...patientForm,
                    [name]: (typeOfData === "boolean") ? event.target.value : (event.target.value == "true") ? true : false,
                    [name + 'Details']: radioValues[name] === false ? '' : patientForm[name + 'Details'],
                    [name + 'StartDate']: radioValues[name] === false ? '' : patientForm[name + 'StartDate']
                })
            } else if (patientForm.hasOwnProperty(name + "Details")) {
                changePatientState({
                    ...patientForm,
                    [name]: (typeOfData === "boolean") ? event.target.value : (event.target.value == "true") ? true : false,
                    [name + 'Details']: radioValues[name] === false ? '' : patientForm[name + 'Details']
                })
            } else if (name !== 'prophylacticTreatments') {
                changePatientState({
                    ...patientForm,
                    [name]: (typeOfData === "boolean") ? event.target.value : (event.target.value == "true") ? true : false
                });
            } 
        }
    }

    function handleProphylacticTreatmentChange(event) {
        var changedField = event.target.id.split('-');
        var newProphylacticTreatmentsArray = patientForm.prophylacticTreatments;

        newProphylacticTreatmentsArray[changedField[1]] = {
            ...newProphylacticTreatmentsArray[changedField[1]],
            [event.target.name]: event.target.value
        };

        changePatientState({
            ...patientForm,
            prophylacticTreatments: newProphylacticTreatmentsArray
        });

    }

    async function handleSubmit(e) {
        e.preventDefault();
        setUserData({
            ...userData,
            loadingOpen: true,
        })

        const patientToSend = Object.assign({}, patientForm);
        if(radioValues.prophylacticTreatments === false){
            changePatientState({
                ...patientForm,
                prophylacticTreatments: []
            }); 
            patientToSend.prophylacticTreatments = [];
        }

        if (screenMode.enteredOnEditMode) {
            const servicePath = `${API.apiPath}api/professionals/editpatient`;
            const data = await fetch(servicePath, {
                method: 'PUT',
                body: JSON.stringify(patientToSend),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${sessionStorage.getItem('sessionToken')}`
                }
            });

            if (data.status === 200) {
                changeScreenMode({
                    isEditing: true,
                    enteredOnEditMode: true,
                    patientProfile: patientToSend
                })
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            } else {
                let jsonObject = await data.json();
                alert(jsonObject.message);
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            }

        } else {
            const servicePath = `${API.apiPath}api/professionals/newpatient`;

            const data = await fetch(servicePath, {
                method: 'POST',
                body: JSON.stringify(patientToSend),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${sessionStorage.getItem('sessionToken')}`
                }
            });

            if (data.status === 200) {
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
                history.push('/patients');
            } else {
                let jsonObject = await data.json();
                alert(jsonObject.message);
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            }
        }

    }

    function todayDate() {
        var today = new Date();
        var year = today.getFullYear().toString();
        var month = (today.getMonth() + 1).toString();
        var day = today.getUTCDate().toString();
        var maxDate = year + "-" + (month.length === 1 ? '0' + month : month) + "-" + (day.length === 1 ? '0' + day : day);

        return maxDate;
    }

    function cancelButton() {
        if (screenMode.enteredOnEditMode) {
            changeScreenMode({
                ...screenMode,
                isEditing: true,
                enteredOnEditMode: true
            })
        } else {
            history.push('/patients');
        }
    }

    function addTreatment() {

        if (patientForm.prophylacticTreatments.length >= 1 && patientForm.prophylacticTreatments[0].treatmentDetails !== '') {
            changePatientState({
                ...patientForm,
                prophylacticTreatments: patientForm.prophylacticTreatments.concat({
                    id: null,
                    treatmentDetails: '',
                    startDate: ''
                })
            })
        }

    }

    function removeTreatment(key) {
        var treatmentsArray = patientForm.prophylacticTreatments;
        if (treatmentsArray.length === 1) {
            changePatientState({
                ...patientForm,
                prophylacticTreatments: [{
                    id: null,
                    treatmentDetails: '',
                    startDate: ''
                }]
            })
        } else if (treatmentsArray.length > 0) {
            var removedElement = treatmentsArray.splice(key, 1);

            changePatientState({
                ...patientForm,
                prophylacticTreatments: treatmentsArray
            })
        }

    }

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item xs={12} md={10} lg={10}>
                    <div className="backView">
                        <h2 className="addPatient">{screenMode.enteredOnEditMode ? 'Editar doente' : 'Adicionar doente'}</h2>
                        <form id="create-patient-form" className="formulario" onSubmit={handleSubmit} validate>
                            <ThemeProvider theme={theme}>
                                <Grid container className="personalData">
                                    <p className="addPatientSubtitle">Dados Pessoais</p>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <FormControl size="small" fullWidth variant="outlined">
                                                <InputLabel required color="secondary" htmlFor="nameInitials">Iniciais do paciente</InputLabel>
                                                <OutlinedInput
                                                    required
                                                    name='nameInitials'
                                                    type='text'
                                                    value={patientForm.nameInitials}
                                                    labelWidth={160}
                                                    onChange={handleChange}
                                                    onInput={(e) => {
                                                        e.target.value = e.target.value.toString().slice(0, 10)
                                                    }}
                                                    style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                    color="secondary"
                                                />
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <FormControl fullWidth variant="outlined">
                                                <div className="dateField">
                                                    <Autocomplete
                                                        id="healthCenter"
                                                        size="small"
                                                        onChange={handleChange}
                                                        options={radioValues.healthCenterOptions}
                                                        value={patientForm.healthCenter}
                                                        getOptionLabel={(option) => option.name ? option.name : ''}
                                                        color="secondary"
                                                        style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                        renderInput={(params) => <TextField required {...params} color="secondary" label="Centro de Saúde" variant="outlined" />}
                                                    />
                                                </div>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <div className="dateField">
                                                <TextField
                                                    size="small"
                                                    required
                                                    fullWidth
                                                    name='birthdate'
                                                    type='date'
                                                    label="Data de Nascimento"
                                                    value={patientForm.birthdate}
                                                    onChange={handleChange}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    inputProps={{
                                                        max: todayDate()
                                                    }}
                                                    style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                    color="secondary"
                                                    variant="outlined" />
                                            </div>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <FormControl size="small" fullWidth variant="outlined" disabled={screenMode.enteredOnEditMode}>
                                                <InputLabel required color="secondary" htmlFor="numeroUtente">Nº de Utente</InputLabel>
                                                <OutlinedInput
                                                    required
                                                    name='numeroUtente'
                                                    type='text'
                                                    value={patientForm.numeroUtente}
                                                    labelWidth={120}
                                                    onChange={handleChange}
                                                    style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                    color="secondary"
                                                />
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <FormControl fullWidth variant="outlined">
                                                <div className="dateField">
                                                    <Autocomplete
                                                        size="small"
                                                        id="gender"
                                                        onChange={handleChange}
                                                        options={['Feminino', 'Masculino']}
                                                        value={patientForm.gender}
                                                        getOptionLabel={(option) => option ? option : ''}
                                                        color="secondary"
                                                        style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                        renderInput={(params) => <TextField required {...params} color="secondary" label="Género" variant="outlined" />}
                                                    />
                                                </div>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <FormControl size="small" fullWidth variant="outlined" disabled={screenMode.enteredOnEditMode}>
                                                <InputLabel required color="secondary" htmlFor="numProcessoHospitalar">Nº de Processo</InputLabel>
                                                <OutlinedInput
                                                    required
                                                    name='numProcessoHospitalar'
                                                    type='text'
                                                    value={patientForm.numProcessoHospitalar}
                                                    labelWidth={130}
                                                    onChange={handleChange}
                                                    style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                    color="secondary"
                                                />
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <FormControl size="small" fullWidth variant="outlined" disabled={screenMode.enteredOnEditMode}>
                                                <InputLabel required color="secondary" htmlFor="email">Email</InputLabel>
                                                <OutlinedInput
                                                    required
                                                    name='email'
                                                    type='email'
                                                    value={patientForm.email}
                                                    labelWidth={60}
                                                    onChange={handleChange}
                                                    style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                    color="secondary"
                                                />
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid container className="tratamentoComorbilidades" spacing={4}>
                                    <Grid item xs={12} md={6} lg={6}>
                                        <p className="addPatientSubtitle">Tratamento</p>
                                        <Grid container>
                                            <Grid item xs={12} md={5} lg={5}>
                                                <p>Enxaqueca com Aura:</p>
                                            </Grid>
                                            <Grid item xs={12} md={7} lg={7}>
                                                <RadioGroup className="radioOptions" aria-label="quiz" name="migraineWithAura" value={radioValues.migraineWithAura} onChange={handleChange}>
                                                    <FormControlLabel value={true} control={<Radio />} label="Sim" />
                                                    <FormControlLabel value={false} control={<Radio />} label="Não" />
                                                </RadioGroup>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <p>Exclusivamente unilateral:</p>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <RadioGroup className="radioOptions" aria-label="quiz" name="exclusivelyUnilateral" value={radioValues.exclusivelyUnilateral} onChange={handleChange}>
                                                    <FormControlLabel value={true} control={<Radio />} label="Sim" />
                                                    <FormControlLabel value={false} control={<Radio />} label="Não" />
                                                </RadioGroup>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <p>Tratamento Sintomático:</p>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <RadioGroup className="radioOptions" aria-label="quiz" name="symptomaticTreatment" value={radioValues.symptomaticTreatment} onChange={handleChange}>
                                                    <FormControlLabel value={true} control={<Radio />} label="Sim" />
                                                    <FormControlLabel value={false} control={<Radio />} label="Não" />
                                                </RadioGroup>
                                            </Grid>
                                            <Grid container>
                                                <Grid item xs={12} md={6} lg={6}>
                                                    <FormControl size="small" fullWidth variant="outlined">
                                                        <InputLabel color="secondary" htmlFor="symptomaticTreatmentDetails">Tratamento</InputLabel>
                                                        <OutlinedInput
                                                            disabled={!radioValues.symptomaticTreatment}
                                                            required={radioValues.symptomaticTreatment}
                                                            name='symptomaticTreatmentDetails'
                                                            type='text'
                                                            value={patientForm.symptomaticTreatmentDetails}
                                                            labelWidth={85}
                                                            onChange={handleChange}
                                                            style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                            color="secondary"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <p>Tratamento Profilático:</p>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <RadioGroup className="radioOptions" aria-label="quiz" name="prophylacticTreatments" value={radioValues.prophylacticTreatments} onChange={handleChange}>
                                                    <FormControlLabel value={true} control={<Radio />} label="Sim" />
                                                    <FormControlLabel value={false} control={<Radio />} label="Não" />
                                                </RadioGroup>
                                            </Grid>
                                            <Grid container spacing={5}>
                                                <Grid item xs={12} md={12} lg={12}>
                                                    <ExpansionPanel expanded={prophylacticTreatmentList} disabled={!radioValues.prophylacticTreatments} onChange={() => setProphylacticTreatmentList(!prophylacticTreatmentList)}>
                                                        <ExpansionPanelSummary
                                                            expandIcon={<ExpandMoreIcon />}
                                                            aria-controls="panel1bh-content"
                                                            id="panel1bh-header"
                                                        >
                                                            <p>Lista de Tratamentos</p>
                                                        </ExpansionPanelSummary>
                                                        <ExpansionPanelDetails>
                                                            <Grid container>
                                                                <Grid item xs={12} md={12} lg={12}>
                                                                    {patientForm.prophylacticTreatments.map((prophylacticTreatment, key) => (
                                                                        <Grid container spacing={3}>
                                                                            <Grid item xs={12} md={5} lg={5}>
                                                                                <TextField
                                                                                    id={`prophylacticDesc-${key}`}
                                                                                    label="Descrição"
                                                                                    name='treatmentDetails'
                                                                                    onChange={handleProphylacticTreatmentChange}
                                                                                    value={prophylacticTreatment.treatmentDetails}
                                                                                    disabled={!radioValues.prophylacticTreatments}
                                                                                    required={radioValues.prophylacticTreatments}
                                                                                />
                                                                            </Grid>
                                                                            <Grid item xs={12} md={5} lg={5}>
                                                                                <div id="dataInicio">
                                                                                    <TextField
                                                                                        id={`prophylacticStartDate-${key}`}
                                                                                        label="Data de Inicio"
                                                                                        name='startDate'
                                                                                        onChange={handleProphylacticTreatmentChange}
                                                                                        value={prophylacticTreatment.startDate}
                                                                                        required={radioValues.prophylacticTreatments}
                                                                                        type="date"
                                                                                        inputProps={{
                                                                                            max: todayDate()
                                                                                        }}
                                                                                        className={classes.textFieldDate}
                                                                                        InputLabelProps={{
                                                                                            shrink: true,
                                                                                        }}
                                                                                    />
                                                                                </div>
                                                                            </Grid>
                                                                            <Grid item xs={12} md={2} lg={2} style={{ margin: 'auto 0' }}>
                                                                                <IconButton onClick={() => removeTreatment(key)} size="small">
                                                                                    <RemoveCircleIcon fontSize="small" />
                                                                                </IconButton>
                                                                            </Grid>
                                                                        </Grid>
                                                                    ))}
                                                                </Grid>
                                                                <Grid item xs={12} md={12} lg={12} style={{ margin: '5px 0' }}>
                                                                    <IconButton onClick={addTreatment} size="medium" >
                                                                        <AddCircleIcon /> <span>Adicionar Tratamento</span>
                                                                    </IconButton>
                                                                </Grid>
                                                            </Grid>


                                                        </ExpansionPanelDetails>
                                                    </ExpansionPanel>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} md={6} lg={6}>
                                        <p className="addPatientSubtitle">Comorbilidades</p>
                                        <Grid container>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <p>Depressão:</p>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <RadioGroup className="radioOptions" aria-label="quiz" name="depression" value={radioValues.depression} onChange={handleChange}>
                                                    <FormControlLabel value={true} control={<Radio />} label="Sim" />
                                                    <FormControlLabel value={false} control={<Radio />} label="Não" />
                                                </RadioGroup>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <p>Ansiedade:</p>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <RadioGroup className="radioOptions" aria-label="quiz" name="anxiety" value={radioValues.anxiety} onChange={handleChange}>
                                                    <FormControlLabel value={true} control={<Radio />} label="Sim" />
                                                    <FormControlLabel value={false} control={<Radio />} label="Não" />
                                                </RadioGroup>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <p>Insónias:</p>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={6}>
                                                <RadioGroup className="radioOptions" aria-label="quiz" name="insomnia" value={radioValues.insomnia} onChange={handleChange}>
                                                    <FormControlLabel value={true} control={<Radio />} label="Sim" />
                                                    <FormControlLabel value={false} control={<Radio />} label="Não" />
                                                </RadioGroup>
                                            </Grid>
                                            <Grid container>
                                                <Grid item xs={12} md={6} lg={6}>
                                                    <p>Outras doenças:</p>
                                                </Grid>
                                            </Grid>
                                            <Grid container>
                                                <Grid item xs={12} md={12} lg={12}>
                                                    <FormControl size="small" fullWidth variant="outlined">
                                                        <OutlinedInput
                                                            multiline
                                                            rows={5}
                                                            name='otherMedicalConditions'
                                                            type='text'
                                                            value={patientForm.otherMedicalConditions}
                                                            onChange={handleChange}
                                                            className={classes.textField}
                                                            style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                            color="secondary"
                                                        />
                                                    </FormControl>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </ThemeProvider>
                        </form>
                    </div>
                </Grid>
            </Grid>

            <div className="addButtonContainer" >
                <ThemeProvider theme={theme}>
                    <Button type="submit"
                        form="create-patient-form"
                        variant="contained"
                        size="medium"
                        color="secondary"
                        className="createPatientButton">
                        {screenMode.enteredOnEditMode ? 'Guardar' : 'Adicionar'}
                    </Button>
                    <Button
                        type="submit"
                        onClick={cancelButton}
                        id="cancelButton"
                        variant="contained"
                        size="medium"
                        color="primary"
                        style={{ color: '#fff' }}
                        className="createPatientButton">
                        Cancelar
                    </Button>
                </ThemeProvider>
            </div>
        </div>
    );
}

function ReadPatientForm({ patientForm, changePatientState, screenMode, changeScreenMode }) {
    const [patientDashboardContext, setPatientDashboardContext] = useContext(PatientDashboardContext);

    useEffect(() => {
        if (Object.keys(screenMode.patientProfile).length !== 0 && screenMode.patientProfile.constructor === Object) {
            setPatientDashboardContext({
                ...patientDashboardContext,
                patientProfile: screenMode.patientProfile
            })
        }
    }, [screenMode]);

    function editPatient() {
        changePatientState(patientDashboardContext.patientProfile)

        changeScreenMode({
            ...screenMode,
            isEditing: false,
            enteredOnEditMode: true
        })
    }

    return (
        <div>
            <Grid container>
                <Grid item xs={12} md={10} lg={10}>
                    <div className="backViewRead">
                        <h2 className="addPatient">Informações</h2>
                        <ThemeProvider theme={theme}>
                            <Grid container className="personalData">
                                <p className="addPatientSubtitle">Dados Pessoais</p>
                                <div className="readPatientInfo">
                                    <Grid container>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <p>Iniciais do paciente: <span>{patientDashboardContext.patientProfile.nameInitials}</span></p>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <p>Centro de Sáude: <span>{patientDashboardContext.patientProfile.healthCenter.name}</span></p>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <p>Data de Nascimento: <span>{patientDashboardContext.patientProfile.birthdate}</span></p>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <p>Nº de Utente: <span>{patientDashboardContext.patientProfile.numeroUtente}</span></p>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <p>Género: <span>{patientDashboardContext.patientProfile.gender}</span></p>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <p>Nº de Processo: <span>{patientDashboardContext.patientProfile.numProcessoHospitalar}</span></p>
                                        </Grid>
                                        <Grid item xs={12} md={6} lg={6}>
                                            <p>E-mail: <span>{patientDashboardContext.patientProfile.email}</span></p>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Grid>
                            <Grid container className="tratamentoComorbilidades">
                                <Grid item xs={12} md={6} lg={6}>
                                    <p className="addPatientSubtitle">Tratamento</p>
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <p>Enxaqueca com Aura: <span>{patientDashboardContext.patientProfile.migraineWithAura ? 'Sim' : 'Não'}</span></p>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <p>Esclusivamente unilateral: <span>{patientDashboardContext.patientProfile.exclusivelyUnilateral ? 'Sim' : 'Não'}</span></p>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <p>Tratamento Sintomático: <span>{patientDashboardContext.patientProfile.symptomaticTreatment ? 'Sim' : 'Não'}</span></p>
                                        </Grid>
                                        {
                                            patientDashboardContext.patientProfile.symptomaticTreatment === true ?
                                                <div className="treatmentsDetails">
                                                    <Grid item xs={12}>
                                                        <span>  Tratamento: {patientDashboardContext.patientProfile.symptomaticTreatmentDetails}</span>
                                                    </Grid>
                                                </div>
                                                :
                                                ''

                                        }
                                        <Grid item xs={12}>
                                            <p>Tratamento Profilático: <span>{patientDashboardContext.patientProfile.prophylacticTreatments.length > 0 ? 'Sim' : 'Não'}</span></p>
                                        </Grid>
                                        <Grid item xs={12}>
                                            {
                                                patientDashboardContext.patientProfile.prophylacticTreatments.length >= 1 ?
                                                    patientDashboardContext.patientProfile.prophylacticTreatments.map((prophylacticTreatment) => (
                                                        <Grid container className="treatmentsDetails">
                                                            <Grid item xs={12}>
                                                                <span>  Tratamento: {prophylacticTreatment.treatmentDetails}</span>
                                                            </Grid>
                                                            <Grid item xs={12}>
                                                                <span>  Data início: {prophylacticTreatment.startDate}</span>
                                                            </Grid>
                                                        </Grid>
                                                    ))
                                                    :
                                                    ''
                                            }
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12} md={6} lg={6}>
                                    <p className="addPatientSubtitle">Comorbilidades</p>
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <p>Depressão: <span>{patientDashboardContext.patientProfile.depression ? 'Sim' : 'Não'}</span></p>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <p>Ansiedade: <span>{patientDashboardContext.patientProfile.anxiety ? 'Sim' : 'Não'}</span></p>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <p>Insónia: <span>{patientDashboardContext.patientProfile.insomnia ? 'Sim' : 'Não'}</span></p>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <p>Outras doenças:</p>
                                        </Grid>
                                        {patientDashboardContext.patientProfile.otherMedicalConditions ?
                                            <Grid item xs={12}>
                                                <span>  {patientDashboardContext.patientProfile.otherMedicalConditions}</span>
                                            </Grid>
                                            :
                                            <span>  Não</span>
                                        }
                                    </Grid>
                                </Grid>
                            </Grid>
                        </ThemeProvider>

                    </div>
                </Grid>
            </Grid>

            <div className="addButtonContainer" >
                <ThemeProvider theme={theme}>
                    <Button
                        onClick={editPatient}
                        variant="contained"
                        size="medium"
                        color="secondary"
                        className="createPatientButton">
                        Editar
                            </Button>
                </ThemeProvider>
            </div>
        </div>
    )
}
