import React, { useState, createContext } from 'react';

export const PatientCRUDContext = createContext();

export function PatientCRUDProvider(props) {

    const [patientForm, setForm] = useState({
        nameInitials: '',
        healthCenter:{ 
            code:'',
            name:'',
            location:'',
            centroSaude:'',
        },
        birthdate:'',
        numeroUtente:'',
        gender:'',
        numProcessoHospitalar: '',
        email: '',
        migraineWithAura: false,
        exclusivelyUnilateral: false,
        symptomaticTreatment: false,
        symptomaticTreatmentDetails: '',
        prophylacticTreatments: [],
        depression: false,
        anxiety: false,
        insomnia: false,
        otherMedicalConditions: ''
    });

    return (
        <PatientCRUDContext.Provider value={[patientForm, setForm]}>
            {props.children}
        </PatientCRUDContext.Provider>
    );
}