import React, { useState, createContext, useEffect, useContext } from 'react';
import { UserInfoContext } from '../../UserInfo/UserInfoContext';
import * as API from '../../../Configs/API';

export const PatientsListContext = createContext();

export function PatientsListProvider(props) {

    const [patients, setPatients] = useState({
        data: [],
        displayedData: [],
        query: '',
        columnToSort: '',
        sortDirection: 'no-sort',
        page: 0,
        rowsPerPage: 10
    });

    const [userData, setUserData] = useContext(UserInfoContext);

    useEffect(() => {
        async function fetchMyPatients() {
            setUserData({
                ...userData,
                loadingOpen: true,
            })
            const servicePath = `${API.apiPath}api/professionals/mypatients/`;
            const data = await fetch(servicePath, {
                method: 'GET',
                headers: {
                    'Authorization' : `Bearer ${sessionStorage.getItem('sessionToken')}`
                }
            });

            
            const myPatientsItems = await data.json();
            if (data.status === 200) {
                for (var i = 0; i < myPatientsItems.length; i++) {
                    if (myPatientsItems[i].birthdate !== null) {
                        if (myPatientsItems[i].birthdate.length > 0) {
                            myPatientsItems[i].birthdate = myPatientsItems[i].birthdate[0] + "-" + (myPatientsItems[i].birthdate[1] < 10 ? "0" + myPatientsItems[i].birthdate[1] : myPatientsItems[i].birthdate[1]) + "-" + (myPatientsItems[i].birthdate[2] < 10 ? "0" + myPatientsItems[i].birthdate[2] : myPatientsItems[i].birthdate[2]);
                        }
                    }
                    if(myPatientsItems[i].gender !== null){
                        myPatientsItems[i].gender = myPatientsItems[i].gender.substring(0, 1);
                    }
                }
    
                setPatients({
                    data: myPatientsItems,
                    displayedData: patients.displayedData,
                    query: patients.query,
                    columnToSort: patients.query,
                    sortDirection: patients.sortDirection,
                    page: patients.page,
                    rowsPerPage: patients.rowsPerPage
                })
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            }else {
                alert(myPatientsItems.message);
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            }

            
        }

        fetchMyPatients();
    }, []);

    return (
        <PatientsListContext.Provider value={[patients, setPatients]}>
            {props.children}
        </PatientsListContext.Provider>
    );
}