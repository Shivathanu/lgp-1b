import React, { useContext, useState, useEffect } from 'react';
import './PatientsList.css';
import * as API from '../../../Configs/API';

import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Button from '@material-ui/core/Button';
import CheckIcon from '@material-ui/icons/Check';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import { ReactComponent as NoSort } from '../../../Resources/no_sort.svg'
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Paper from '@material-ui/core/Paper';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import SearchIcon from '@material-ui/icons/Search';
import SendIcon from '@material-ui/icons/Send';
import SvgIcon from '@material-ui/core/SvgIcon';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';

import { createMuiTheme } from '@material-ui/core/styles';
import { Link, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { PatientsListContext } from './PatientsListContext';
import { ThemeProvider } from '@material-ui/styles';
import { UserInfoContext } from '../../UserInfo/UserInfoContext';

/**
 * OVERRIDE STYLES FROM MATERIAL-UI
 */

const theme = createMuiTheme({
    palette: {
        secondary: {
            // This is green.A700 as hex.
            main: '#0f7387',
        },
    },
});

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        backgroundColor: '#B7B7B9',
        borderRadius: '30px',
    }
}));

/**
 * ALL ELEMENTS TO RENDER ON SCREEN
 */

export default function PatientsList(props) {
    const [patients, setPatients] = useContext(PatientsListContext);
    const [userData, setUserData] = useContext(UserInfoContext);
    const classes = useStyles();

    function searchBarChange(e) {
        setPatients({
            data: patients.data,
            displayedData: patients.displayedData,
            query: e.target.value.toUpperCase(),
            columnToSort: patients.columnToSort,
            sortDirection: patients.sortDirection,
            page: patients.page,
            rowsPerPage: patients.rowsPerPage
        });
    }

    function addPatientSubmit(e) {
        e.preventDefault();
        addPatient(e.target[0].value);
        e.target[0].value = '';
    }

    async function addPatient(code) {
        const servicePath = `${API.apiPath}api/professionals/mypatients/`;

        setUserData({
            ...userData,
            loadingOpen: true,
        })

        const data = await fetch(servicePath, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : `Bearer ${sessionStorage.getItem('sessionToken')}`,
                'patientCode': code
            }
        });

        const newPatientToList = await data.json();
        if (data.status === 200) {
            if (newPatientToList.code) {
                if (newPatientToList.gender !== null) {
                    newPatientToList.gender = newPatientToList.gender.substring(0, 1);
                }

                if (newPatientToList.birthdate.length > 0) {
                    newPatientToList.birthdate = newPatientToList.birthdate[0] + "-" + (newPatientToList.birthdate[1] < 10 ? "0" + newPatientToList.birthdate[1] : newPatientToList.birthdate[1]) + "-" + newPatientToList.birthdate[2];
                }

                setPatients({
                    data: patients.data.concat(newPatientToList),
                    displayedData: patients.displayedData,
                    query: patients.query,
                    columnToSort: patients.columnToSort,
                    sortDirection: patients.sortDirection,
                    page: patients.page,
                    rowsPerPage: patients.rowsPerPage
                });
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            }
        } else {
            alert(newPatientToList.message);
            setUserData({
                ...userData,
                loadingOpen: false,
            })
        }


    }

    function sortAndFilter(sortDirection, queryToFilter) {
        var returnValue = [];
        if (sortDirection) {
            if (sortDirection === 'asc') {
                returnValue = patients.data.sort((a, b) => (a[patients.columnToSort].toUpperCase() > b[patients.columnToSort].toUpperCase()) ? 1 : -1)
            } else {
                returnValue = patients.data.sort((a, b) => (a[patients.columnToSort].toUpperCase() < b[patients.columnToSort].toUpperCase()) ? 1 : -1)
            }

            returnValue = returnValue.filter((x) => {
                return Object.values(x).join("").includes(queryToFilter)
            });
        }


        return returnValue;
    }

    const handleChangePage = (event, newPage) => {
        var newState = { ...patients }
        newState.page = newPage;
        setPatients(newState);
    };

    const handleChangeRowsPerPage = (event) => {
        var newState = { ...patients }
        newState.rowsPerPage = parseInt(event.target.value, 10);
        newState.page = 0;
        setPatients(newState);
    };

    return (
        <div className="patientsListContainer">
            <div className="patientsListGrid">
                <Grid container spacing={4}>
                    <Grid item xs={12} md={9} lg={9}>
                        <div className="searchBarWrapper">
                            <form>
                                <FormControl fullWidth variant="outlined">
                                    <ThemeProvider theme={theme}>
                                        <InputLabel color="secondary" htmlFor="search-bar">Procurar Doentes</InputLabel>
                                        <OutlinedInput
                                            id='search-bar'
                                            name='search-bar'
                                            type='search'
                                            onChange={searchBarChange}
                                            labelWidth={130}
                                            style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                            color="secondary"
                                            endAdornment={<InputAdornment position="start"><SearchIcon /></InputAdornment>}
                                        />
                                    </ThemeProvider>
                                </FormControl>
                            </form>
                        </div>
                    </Grid>
                </Grid>
                <Grid container spacing={5}>
                    <Grid item xs={12} md={9} lg={9}>
                        <TableContainer component={Paper}>
                            <PatientsListTable aria-label="simple table"
                                header={[
                                    {
                                        name: "Iniciais do Nome",
                                        prop: 'nameInitials'
                                    },
                                    {
                                        name: "Código",
                                        prop: 'code'
                                    },
                                    {
                                        name: "Género",
                                        prop: 'gender'
                                    },
                                    {
                                        name: "Data de Nascimento",
                                        prop: 'birthdate'
                                    },
                                    {
                                        name: "",
                                        prop: 'remove_from_list'
                                    },
                                ]}
                                data={
                                    (patients.query !== '') ?
                                        (patients.columnToSort !== '' && patients.sortDirection !== 'no-sort') ?
                                            sortAndFilter(patients.sortDirection, patients.query)
                                            : patients.data.filter(x => Object.values(x).join("").includes(patients.query))
                                        : (patients.columnToSort !== '' && patients.sortDirection !== 'no-sort') ?
                                            (patients.sortDirection === 'asc') ?
                                                patients.data.sort((a, b) => (a[patients.columnToSort].toUpperCase() > b[patients.columnToSort].toUpperCase()) ? 1 : -1)
                                                : patients.data.sort((a, b) => (a[patients.columnToSort].toUpperCase() < b[patients.columnToSort].toUpperCase()) ? 1 : -1)
                                            : patients.data
                                }
                            />
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25]}
                            labelRowsPerPage={'Registos por página'}
                            component="div"
                            count={patients.displayedData.length}
                            rowsPerPage={patients.rowsPerPage}
                            page={patients.page}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        />
                    </Grid>
                    <Grid item xs={12} md={3} lg={3}>
                        <Paper className={classes.paper}>
                            <h2 className="addPatientCard">Associar Doente</h2>
                            <div className="addPatientFormWrapper">
                                <form onSubmit={addPatientSubmit}>
                                    <div className="addPatientFormElements">
                                        <FormControl variant="standard" size="small" className="patientCodeInput">
                                            <ThemeProvider theme={theme}>
                                                <OutlinedInput
                                                    id='add-patient-code'
                                                    name='add-patient-code'
                                                    type='text'
                                                    placeholder="Código"
                                                    style={{
                                                        borderRadius: '20px',
                                                        backgroundColor: '#fff'
                                                    }}
                                                    color="secondary"
                                                    startAdornment={<InputAdornment position="start">#</InputAdornment>}
                                                />
                                            </ThemeProvider>
                                        </FormControl>
                                        <ThemeProvider theme={theme}>
                                            <Button type="submit"
                                                variant="contained"
                                                size="small"
                                                color="secondary"
                                                className="patientCodeButton">
                                                <CheckIcon fontSize="small" />
                                            </Button>
                                        </ThemeProvider>
                                    </div>
                                </form>
                            </div>
                        </Paper>
                    </Grid>
                </Grid>
            </div>

            <div className="buttonContainer">
                <Link to={{
                    pathname: "/patient-form",
                    state: {
                        isEditing: false,
                        patientId: null
                    }
                }}>
                    <ThemeProvider theme={theme}>
                        <Button type="submit"
                            variant="contained"
                            size="medium"
                            color="secondary"
                            className="patientCodeButton">Adicionar Doente</Button>
                    </ThemeProvider>
                </Link>
            </div>
        </div>
    );
}

/**
 * PATIENTS LIST COMPONENT
 */

function PatientsListTable({ header, data }) {
    const [patients, setPatients] = useContext(PatientsListContext);
    const [userData, setUserData] = useContext(UserInfoContext);
    useEffect(() => {
        var newState = { ...patients }
        newState.displayedData = data;
        setPatients(newState);
    }, [patients.data, patients.query]);

    const history = useHistory();
    var lineActionsClick = false;

    const theme = createMuiTheme({
        palette: {
            secondary: {
                // This is green.A700 as hex.
                main: '#0f7387',
            },
        },
    });

    const [dialogManagement, setDialogManagement] = useState({
        isOpen: false,
        shareCode: false,
        patientCode: null
    });

    function shareCodeClick(patientCode) {
        lineActionsClick = true;
        setDialogManagement({
            isOpen: true,
            shareCode: true,
            patientCode: patientCode
        });
    }

    function removeItem(patientCode) {
        lineActionsClick = true;
        setDialogManagement({
            isOpen: true,
            shareCode: false,
            patientCode: patientCode
        });
    }

    function patientSelected(patientCode) {
        if (!lineActionsClick) {
            history.push(`/patients/${patientCode}`);
        }
    }

    async function handleClose(performAction) {
        lineActionsClick = false;
        if (performAction) {
            if (dialogManagement.shareCode) {
                //send mail TO-DO
                setDialogManagement({
                    isOpen: false,
                    shareCode: false,
                    patientCode: null
                })
            } else {
                const servicePath = `${API.apiPath}api/professionals/deletemypatient/`;

                setUserData({
                    ...userData,
                    loadingOpen: true,
                })

                const data = await fetch(servicePath, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization' : `Bearer ${sessionStorage.getItem('sessionToken')}`,
                        'professionalId': sessionStorage.getItem("professionalId"),
                        'patientCode': dialogManagement.patientCode
                    }
                });

                const retrievedData = await data;
                if (retrievedData.ok) {
                    setPatients({
                        data: patients.data.filter(obj =>
                            obj.code !== dialogManagement.patientCode
                        ),
                        displayedData: patients.displayedData,
                        query: patients.query,
                        columnToSort: patients.columnToSort,
                        sortDirection: patients.sortDirection,
                        page: patients.page,
                        rowsPerPage: patients.rowsPerPage
                    });
                    setUserData({
                        ...userData,
                        loadingOpen: false,
                    })
                }else{
                    setUserData({
                        ...userData,
                        loadingOpen: false,
                    })
                }

                setDialogManagement({
                    isOpen: false,
                    shareCode: false,
                    patientCode: null
                })
            }
        } else {
            setDialogManagement({
                isOpen: false,
                shareCode: false,
                patientCode: null
            })
        }

    };

    function sortColumn(columnToSort) {
        var newSortDirection = '';

        if (patients.sortDirection === 'no-sort') {
            newSortDirection = 'asc';
        } else if (patients.sortDirection === 'asc') {
            newSortDirection = 'desc';
        } else {
            newSortDirection = 'no-sort';
            columnToSort = '';
        }

        var newState = { ...patients };
        newState.sortDirection = newSortDirection;
        newState.columnToSort = columnToSort;
        setPatients(newState);
    }

    const emptyRows = patients.rowsPerPage - Math.min(patients.rowsPerPage, patients.displayedData.length - patients.page * patients.rowsPerPage);

    return (
        <div>
            <Table aria-label="simple table">
                <TableHead className="patientsTable">
                    <TableRow >
                        {header.map((x, i) =>
                            <TableCell align="center" key={i}>
                                <div
                                    style={{
                                        display: "inline-flex",
                                        textAlign: "center"
                                    }}
                                >
                                    <span>{x.name}</span>
                                    {
                                        x.name !== "" ?
                                            patients.columnToSort === '' ?
                                                <div>
                                                    <IconButton onClick={() => sortColumn(x.prop)} size="small">
                                                        <SvgIcon fontSize="small" component={NoSort} viewBox="0 0 600 476.6" />
                                                    </IconButton>
                                                </div>
                                                :
                                                (
                                                    patients.columnToSort === x.prop ?
                                                        patients.sortDirection === 'asc' ?
                                                            (
                                                                <IconButton onClick={() => sortColumn(x.prop)} size="small">
                                                                    <ArrowDropUpIcon fontSize="small" />
                                                                </IconButton>
                                                            )
                                                            :
                                                            (
                                                                <IconButton onClick={() => sortColumn(x.prop)} size="small">
                                                                    <ArrowDropDownIcon fontSize="small" />
                                                                </IconButton>
                                                            )
                                                        : null
                                                )
                                            :
                                            null
                                    }

                                </div>
                            </TableCell>
                        )}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {patients.displayedData
                        .slice(patients.page * patients.rowsPerPage, patients.page * patients.rowsPerPage + patients.rowsPerPage)
                        .map((patient, index) => (
                            <TableRow hover align="center" key={index} onClick={() => patientSelected(patient.code)}>
                                <TableCell align="center">{patient.nameInitials}</TableCell>
                                <TableCell align="center">#{patient.code}</TableCell>
                                <TableCell align="center">{patient.gender}</TableCell>
                                <TableCell align="center">{patient.birthdate}</TableCell>
                                <TableCell align="right" size="small"><IconButton onClick={() => removeItem(patient.code)} size="small"><RemoveCircleIcon /></IconButton></TableCell>
                            </TableRow>
                        ))}
                    {patients.displayedData.length === 0 ?
                        <TableRow style={{ height: 53 * emptyRows - 1 }}>
                            <TableCell colSpan={6} style={{ textAlign: 'center', fontWeight: 'bold' }}>Sem Doentes</TableCell>
                        </TableRow>
                        :
                        emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )}
                </TableBody>
            </Table>
            <Dialog open={dialogManagement.isOpen} onClose={() => handleClose(false)} aria-labelledby="form-dialog-title">
                {
                    dialogManagement.shareCode ?
                        <DialogTitle id="form-dialog-title">
                            Partilhar Código do Paciente
                    </DialogTitle> :
                        <DialogTitle id="form-dialog-title">
                            Remover Paciente da Lista
                    </DialogTitle>
                }
                <DialogContent>
                    {
                        dialogManagement.shareCode ?
                            <DialogContentText>
                                Insira o endereço email da pessoa com quem deseja partilhar o código {dialogManagement.patientCode}.
                        </DialogContentText> :
                            <DialogContentText>
                                Tem a certeza que pretende remover o paciente com o código {dialogManagement.patientCode} da sua lista?
                        </DialogContentText>
                    }
                    {
                        dialogManagement.shareCode ?
                            <ThemeProvider theme={theme}>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    id="name"
                                    label="Endereço de e-mail"
                                    type="email"
                                    color="secondary"
                                    fullWidth
                                />
                            </ThemeProvider> :
                            <p></p>
                    }
                </DialogContent>
                <DialogActions>
                    <ThemeProvider theme={theme}>
                        <Button onClick={() => handleClose(false)} color="secondary">
                            Cancelar
                        </Button>
                        {
                            dialogManagement.shareCode ?
                                <Button onClick={() => handleClose(true)} color="secondary">
                                    Enviar
                            </Button> :
                                <Button onClick={() => handleClose(true)} color="secondary">
                                    Confirmar
                            </Button>
                        }
                    </ThemeProvider>
                </DialogActions>
            </Dialog>
        </div>
    )
}

