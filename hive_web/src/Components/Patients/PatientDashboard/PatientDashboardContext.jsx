import React, { useState, createContext, useEffect, useContext } from 'react';
import { UserInfoContext } from '../../UserInfo/UserInfoContext';
import { useHistory } from 'react-router-dom';
import * as API from '../../../Configs/API';

export const PatientDashboardContext = createContext();

export function PatientDashboardProvider(props, { match }) {
    const [userData, setUserData] = useContext(UserInfoContext);
    const [patientInfo, setPatientInfo] = useState({
        patientProfile: {
            active: false,
            anxiety: false,
            birthdate: '',
            code: '',
            depression: false,
            email: '',
            exclusivelyUnilateral: false,
            gender: '',
            healthCenter: {},
            insomnia: false,
            migraineWithAura: false,
            nameInitials: '',
            numProcessoHospitalar: '',
            numeroUtente: '',
            otherMedicalConditions: '',
            prophylacticTreatments: [],
            symptomaticTreatment: null,
            symptomaticTreatmentDetails: ''
        },
        reportView: {
            exportToString: '',
            months: []
        },
        calendarView: [],
        appointmentsView: []
    });
    const history = useHistory();

    useEffect(() => {

        async function fetchPatientInfo() {
            var url = window.location.pathname.split('/');
            if (url.length === 4) {
                var patientCode = url[2];
            } else {
                var patientCode = url[url.length - 1];
            }


            setUserData({
                ...userData,
                loadingOpen: true,
            })

            const servicePath = `${API.apiPath}api/professionals/mypatients/${patientCode}`;
            const data = await fetch(servicePath, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${sessionStorage.getItem('sessionToken')}`
                }
            });

            const fetchedPatientInfo = await data.json();
            if (data.status === 200) {
                //format dates
                let birthdate = fetchedPatientInfo.patientProfile.birthdate;
                let prophylacticTreatments = fetchedPatientInfo.patientProfile.prophylacticTreatments;

                if (birthdate !== null) {
                    if (birthdate.length > 0) {
                        fetchedPatientInfo.patientProfile.birthdate = birthdate[0] + "-" + (birthdate[1] < 10 ? "0" + birthdate[1] : birthdate[1]) + "-" + (birthdate[2] < 10 ? "0" + birthdate[2] : birthdate[2]);
                    }
                }

                if (prophylacticTreatments !== null || prophylacticTreatments.length > 1) {
                    for (var i = 0; i < prophylacticTreatments.length; i++) {
                        fetchedPatientInfo.patientProfile.prophylacticTreatments[i] = {
                            ...prophylacticTreatments[i],
                            startDate: prophylacticTreatments[i].startDate[0] + "-" + (prophylacticTreatments[i].startDate[1] < 10 ? "0" + prophylacticTreatments[i].startDate[1] : prophylacticTreatments[i].startDate[1]) + "-" + (prophylacticTreatments[i].startDate[2] < 10 ? "0" + prophylacticTreatments[i].startDate[2] : prophylacticTreatments[i].startDate[2])
                        }
                    }
                } else {
                    prophylacticTreatments = [];
                    prophylacticTreatments[0] = {
                        id: null,
                        treatmentDetails: '',
                        startDate: ''
                    }
                }

                var id = 0;
                var calendarDiaryItems = [];
                for (const key in fetchedPatientInfo.calendarView) {
                    var month = fetchedPatientInfo.calendarView[key];
                    for (var i = 0; i < month.length; i++) {
                        calendarDiaryItems.push({
                            title: 'Diário do dia ' + (month[i].date[2] < 10 ? "0" + month[i].date[2] : month[i].date[2]) + "-" + (month[i].date[1] < 10 ? "0" + month[i].date[1] : month[i].date[1]) + "-" + month[i].date[0],
                            startDate: new Date(month[i].date[0], month[i].date[1] - 1, month[i].date[2], 9, 0),
                            endDate: new Date(month[i].date[0], month[i].date[1] - 1, month[i].date[2], 10, 0),
                            id: id,
                            data: {
                                emergencyServiceRecurrence: month[i].emergencyServiceRecurrence,
                                headacheIntensity: month[i].headacheIntensity,
                                menstruation: month[i].menstruation,
                                migraine: month[i].migraine,
                                observations: month[i].observations,
                                tookPainKillers: month[i].tookPainKillers,
                                tookTriptans: month[i].tookTriptans,
                                workIncapacity: month[i].workIncapacity,
                            },
                            gender: fetchedPatientInfo.patientProfile.gender,
                            diary: true
                        })
                        id++;
                    }
                }

                var calendarAppointmentItems = [];
                for (const key in fetchedPatientInfo.appointmentsView) {
                    var month = fetchedPatientInfo.appointmentsView[key];
                    for (var i = 0; i < month.length; i++) {
                        var startDate = new Date(month[i].dateTime[0], month[i].dateTime[1] - 1, month[i].dateTime[2], month[i].dateTime[3], month[i].dateTime[4]);
                        var endDate = new Date(startDate.getTime() + 30 * 60000);
                        calendarAppointmentItems.push({
                            title: 'Consulta de ' + month[i].type,
                            startDate: startDate,
                            endDate: endDate,
                            id: id,
                            data: {
                                doctor: month[i].doctor,
                                observations: month[i].observations,
                                place: month[i].place,
                                type: month[i].type
                            },
                            diary: false
                        })
                    }
                }

                setPatientInfo({
                    patientProfile: fetchedPatientInfo.patientProfile,
                    reportView: fetchedPatientInfo.reportView,
                    calendarView: calendarDiaryItems,
                    appointmentsView: calendarAppointmentItems
                })
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })


                var dashboardContext = {
                    patientProfile: fetchedPatientInfo.patientProfile,
                    reportView: fetchedPatientInfo.reportView,
                    calendarView: calendarDiaryItems,
                    appointmentsView: calendarAppointmentItems
                };

                if(url.length < 4){
                    history.push(`/patients/${patientCode}/report`);
                }

            } else {
                alert(fetchedPatientInfo.message);
                setUserData({
                    ...userData,
                    loadingOpen: false,
                })
            }
        }

        fetchPatientInfo();
    }, []);



    return (
        <PatientDashboardContext.Provider value={[patientInfo, setPatientInfo]}>
            {props.children}
        </PatientDashboardContext.Provider>
    );
}