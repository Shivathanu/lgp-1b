import React, { useContext, useState, useEffect } from 'react';
import './PatientCalendar.css';

import Paper from '@material-ui/core/Paper';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
    Scheduler,
    MonthView,
    DateNavigator,
    TodayButton,
    Toolbar,
    Appointments,
    AppointmentTooltip
} from '@devexpress/dx-react-scheduler-material-ui';
import { PatientDashboardContext } from '../PatientDashboardContext'
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

const style = ({ palette }) => ({
    icon: {
        color: palette.action.active,
    },
    textCenter: {
        textAlign: 'center',
    },
    header: {
        height: '260px',
        backgroundSize: 'cover',
    },
});

export default function PatientCalendar() {
    const [patientInfo/*, setPatientInfo*/] = useContext(PatientDashboardContext);
    const [calendarData, setCalendarData] = useState({
        data: []
    })

    useEffect(() => {
        setCalendarData({
            data: patientInfo.calendarView.concat(patientInfo.appointmentsView)
        })
    },[patientInfo])

    return (
        <div className="calendarCont">
            <div className="calendar">
                <Paper>
                    <Scheduler
                        data={calendarData.data}
                        locale={'pt-PT'}>
                        <ViewState />
                        <MonthView />
                        <Appointments
                            appointmentComponent={Appointment}
                        />
                        <AppointmentTooltip
                            contentComponent={Content}
                            showCloseButton
                        />
                        <Toolbar />
                        <DateNavigator />
                        <TodayButton />
                    </Scheduler>
                </Paper>
            </div>
            <div className="legendas">
                <div className="legandaCont">
                    <div id="cefaleiaTensao"></div>
                    <span>Cefaleia de Tensão</span>
                </div>
                <div className="legandaCont">
                    <div id="enxaqueca"></div>
                    <span>Enxaqueca</span>
                </div >
                <div className="legandaCont">
                    <div id="consulta"></div>
                    <span>Consulta</span>
                </div>
            </div>
        </div>
    )
}

const Appointment = ({
    children, style, ...restProps
}) => (
        <Appointments.Appointment
            {...restProps}
            style={{
                ...style,
                backgroundColor: restProps.data.diary ? (restProps.data.data.migraine ? '#0f7387' : '#74BBED') : '#E8A25D',
                borderRadius: '10px',
            }}
        >
            {children}
        </Appointments.Appointment>
    );

const Content = withStyles(style, { name: 'Content' })(({
    children, appointmentData, classes, ...restProps
}) => (
        <div id="cardWrapper" className={appointmentData.diary ? (appointmentData.data.migraine ? "enxaquecaWrapper" : "cefaleiaTensaoWrapper") : "consultaWrapper"}>
            <AppointmentTooltip.Content {...restProps} appointmentData={ appointmentData.diary ? {data : appointmentData.data, diary: appointmentData.diary, id: appointmentData.id, title: appointmentData.title} : {data : appointmentData.data, diary: appointmentData.diary, id: appointmentData.id, title: appointmentData.title, startDate: appointmentData.startDate, endDate: appointmentData.endDate}}>
                <Grid container>
                    {
                        appointmentData.diary === true ?
                            <div>
                                <h3 className={appointmentData.data.migraine ? "migraineDetailsTitle enxaquecaTitle" : "migraineDetailsTitle cefaleiaTensaoTitle"}>Detalhes da cefaleia</h3>
                                <Grid container className="migraineDetails">
                                    <Grid item xs={12}>
                                        <p>Qualidade da cefaleia: <span>{(appointmentData.data.migraine === true) ? 'Enxaqueca' : 'Cefaleia de Tensão'}</span></p>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>Intensidade da dor: <span>{appointmentData.data.headacheIntensity}</span></p>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>Tomou analgésico: <span>{(appointmentData.data.tookPainKillers) ? 'Sim' : 'Não'}</span></p>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>Tomou triptano: <span>{(appointmentData.data.tookTriptans) ? 'Sim' : 'Não'}</span></p>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>Faltou ao trabalho: <span>{(appointmentData.data.workIncapacity) ? 'Sim' : 'Não'}</span></p>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>Recorreu ao serviço de emergência: <span>{(appointmentData.data.emergencyServiceRecurrence) ? 'Sim' : 'Não'}</span></p>
                                    </Grid>
                                    {
                                        appointmentData.gender === "Feminino" ?
                                            <Grid item xs={12}>
                                                <p>Menstruação: <span>{(appointmentData.data.menstruation) ? 'Sim' : 'Não'}</span></p>
                                            </Grid>
                                            :
                                            null
                                    }
                                </Grid>
                            </div>
                            :
                            <div>
                                <h3 className="migraineDetailsTitle consultaTitle">Detalhes da consulta</h3>
                                <Grid container className="migraineDetails">
                                    <Grid item xs={12}>
                                        <p>Profissional: <span>{appointmentData.data.doctor}</span></p>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>Especialidade: <span>{appointmentData.data.type}</span></p>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>Localização: <span>{appointmentData.data.place}</span></p>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>Observações: <span>{appointmentData.data.observations}</span></p>
                                    </Grid>
                                </Grid>
                            </div>
                    }


                </Grid>
            </AppointmentTooltip.Content>
        </div>
    ));