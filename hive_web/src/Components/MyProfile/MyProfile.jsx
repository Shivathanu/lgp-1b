import React, { useState, useContext, useEffect } from 'react';
import './MyProfile.css';

import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CancelIcon from '@material-ui/icons/Cancel';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import IconButton from '@material-ui/core/IconButton';
import MuiAlert from '@material-ui/lab/Alert';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Paper from '@material-ui/core/Paper';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Snackbar from '@material-ui/core/Snackbar';
import TextField from '@material-ui/core/TextField';

import { createMuiTheme } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { UserInfoContext } from '../UserInfo/UserInfoContext';

import * as API from '../../Configs/API';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    large: {
        width: theme.spacing(17),
        height: theme.spacing(17),
    },
    input: {
        display: 'none',
    },
}));

const theme = createMuiTheme({
    palette: {
        secondary: {
            // This is green.A700 as hex.
            main: '#0f7387',
        },
        primary: {
            main: '#626263',
        }
    },
});

export default function MyProfile() {
    const [readMode, setReadMode] = useState(true);
    const [userData, setUserData] = useContext(UserInfoContext);

    function changeScreenMode(newMode) {
        setReadMode(newMode);
    }

    function changeUserData(newData) {
        setUserData(newData);
    }

    function logout() {
        sessionStorage.clear();
        window.location.replace('/');
    }

    return (
        <div>
            <h2>Perfil</h2>
            {readMode === true ?
                <div>
                    <MyProfileRead userData={userData} screenMode={readMode} changeScreenMode={changeScreenMode} />
                    <ThemeProvider theme={theme}>
                        <div className="exitButtonWrapper">
                            <Button
                                onClick={logout}
                                variant="contained"
                                size="medium"
                                color="secondary"
                                className="editProfileButton"
                                style={{ borderRadius: '20px', color: '#fff' }}>
                                SAIR</Button>
                        </div>
                    </ThemeProvider>
                </div>
                :
                <div>
                    <MyProfileUpdate userData={userData} changeUserData={changeUserData} screenMode={readMode} changeScreenMode={changeScreenMode} />
                </div>
            }
        </div>
    );
}

function MyProfileRead({ userData, screenMode, changeScreenMode }) {
    const classes = useStyles();

    function editInfo() {
        changeScreenMode(!screenMode);
    }

    function transform() {
        var profilePic = "/broken-image.jpg";
        if (userData.professional.image !== '' || userData.professional.image !== null || userData.professional.image !== undefined) {
            profilePic = 'data:image/jpeg;base64,' + userData.professional.image;
        }

        return profilePic;
    }

    return (
        <div>
            <Grid container>
                <Grid item xs={12} md={9} lg={9}>
                    <div className="backViewRead">
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6} lg={6}>
                                <span>
                                    <Avatar src={transform()} className={classes.large} />
                                </span>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6} id="editProfileGrid">
                                <ThemeProvider theme={theme}>
                                    <div className="editProfileButtonWrapper">
                                        <Button
                                            onClick={editInfo}
                                            variant="contained"
                                            size="medium"
                                            color="secondary"
                                            className="editProfileButton"
                                            style={{ borderRadius: '20px', color: '#fff' }}>
                                            Editar Perfil</Button>
                                    </div>

                                </ThemeProvider>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <p>Nome: <span>{userData.professional.clinicalName}</span></p>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <p>Especialidade: <span>{userData.professional.speciality}</span></p>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <p>E-mail: <span>{userData.professional.email}</span></p>
                            </Grid>
                            <Grid item xs={12} md={6} lg={6}>
                                <p>Local de Trabalho: <span>{userData.professional.healthCenter.name}</span></p>
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

function MyProfileUpdate({ userData, changeUserData, screenMode, changeScreenMode }) {
    const classes = useStyles();
    const [formValues, setFormValues] = useState({
        professionalValues: {
            ...userData.professional,
            emailIsValid: true,
            emailWarningText: ''
        },
        specialityOptions: [],
        healthCenterOptions: []
    })
    const [changePassword, setChangePassword] = useState(false);
    const [open, setOpen] = useState(false);
    var EMAIL_DOMAIN = '@ulsm.min-saude.pt';

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    function onChangeSnackBar() {
        setOpen(!open);
    }

    useEffect(() => {
        async function fetchSpeciality() {
            changeUserData({
                ...userData,
                loadingOpen: true
            })
            const servicePath = `${API.apiPath}api/professionals/specialities`;
            const data = await fetch(servicePath);
            const specialityItems = await data.json();
            if (data.status === 200) {
                var specialityItemsFinal = [];
                for (var key in specialityItems) {
                    specialityItemsFinal.push({
                        name: key,
                        healthCenterProvider: specialityItems[key]
                    })
                }

                var healthCenterOptionsFinalAPI = specialityItems[formValues.professionalValues.speciality];
                const healthCenterServicePath = API.apiPath + 'api' + healthCenterOptionsFinalAPI;
                const HCdata = await fetch(healthCenterServicePath);
                const healthCenterItems = await HCdata.json();
                if (HCdata.status === 200) {
                    setFormValues({
                        ...formValues,
                        professionalValues: {
                            ...formValues.professionalValues,
                            tempImage: formValues.professionalValues.image,
                        },
                        specialityOptions: specialityItemsFinal,
                        healthCenterOptions: healthCenterItems
                    })
                    changeUserData({
                        ...userData,
                        loadingOpen: false
                    })
                } else {
                    alert(healthCenterItems.message);
                    changeUserData({
                        ...userData,
                        loadingOpen: false
                    })
                }
            } else {
                alert(specialityItems.message);
                changeUserData({
                    ...userData,
                    loadingOpen: false
                })
            }

        }

        fetchSpeciality();
    }, [])

    async function handleChange(event) {

        if (event.target.id.includes('healthCenter')) {
            var splitedArray = event.target.id.split('-');
            setFormValues({
                ...formValues,
                professionalValues: {
                    ...formValues.professionalValues,
                    [splitedArray[0]]: formValues.healthCenterOptions[splitedArray[splitedArray.length - 1]]
                }
            })
        } else if (event.target.name !== '' || event.target.id.includes('speciality')) {
            setFormValues({
                ...formValues,
                professionalValues: {
                    ...formValues.professionalValues,
                    [event.target.name]: event.target.value,
                    emailWarningText: '',
                    emailIsValid: true
                }
            });
        }
    }

    function imageChange(e) {
        var files = e.target.files;

        if (files.length === 0) {
            return;
        }

        var mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            return;
        }

        var reader = new FileReader();
        reader.readAsDataURL(files[0]);

        reader.onload = (_event) => {
            var splittedArray = reader.result.toString().split(',');
            transform(splittedArray[1]);
            setFormValues({
                ...formValues,
                professionalValues: {
                    ...formValues.professionalValues,
                    tempImage: splittedArray[1]
                }
            })
        }
    }

    async function handleSubmit(e) {
        e.preventDefault();

        var professionalUpdated = Object.assign({}, formValues.professionalValues);
        var emailIsValid = validateEmail(professionalUpdated.email);

        if (emailIsValid) {
            delete professionalUpdated.tempImage;
            professionalUpdated.image = formValues.professionalValues.tempImage;

            delete professionalUpdated.speciality;
            professionalUpdated.speciality = formValues.professionalValues.speciality.name ? formValues.professionalValues.speciality.name : formValues.professionalValues.speciality;

            changeUserData({
                ...userData,
                loadingOpen: true
            })

            const servicePath = `${API.apiPath}api/professionals/update`;
            const data = await fetch(servicePath, {
                method: 'PUT',
                body: JSON.stringify(professionalUpdated),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${sessionStorage.getItem('sessionToken')}`
                }
            });

            if (data.status === 200) {
                changeUserData({
                    ...userData,
                    professional: professionalUpdated,
                    loadingOpen: false
                })
                changeScreenMode(!screenMode);
            } else {
                let jsonObject = await data.json();
                alert(jsonObject.message);
                changeUserData({
                    ...userData,
                    professional: {
                        ...userData.professional,
                        tempImage: userData.professional.image
                    },
                    loadingOpen: false
                })
                changeScreenMode(!screenMode);
            }
        } else {
            setFormValues({
                ...formValues,
                professionalValues: {
                    ...formValues.professionalValues,
                    emailWarningText: "O email inserido não é válido",
                    emailIsValid: false
                }
            })
        }

    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(email)) {
            if (email.indexOf(EMAIL_DOMAIN, email.length - EMAIL_DOMAIN.length) !== -1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function onChangePassword(newValue) {
        setChangePassword(!changePassword);
    }

    function cancelButton() {
        changeUserData({
            ...userData,
            professional: {
                ...userData.professional,
                tempImage: userData.professional.image
            }
        })
        changeScreenMode(!screenMode);
    }

    function transform() {
        var profilePic = "/broken-image.jpg";
        if (formValues.professionalValues.tempImage !== '' && formValues.professionalValues.tempImage !== null && formValues.professionalValues.tempImage !== undefined) {
            profilePic = 'data:image/jpeg;base64,' + formValues.professionalValues.tempImage;
        }

        return profilePic;
    }

    return (
        <div>
            <Grid container>
                <Grid item xs={12} md={9} lg={9}>
                    <div className="backView">
                        <form id="edit-profile-form" className="formulario" onSubmit={handleSubmit} validate>
                            <ThemeProvider theme={theme}>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} md={6} lg={6}>
                                        <div className="imageContainer">
                                            <span>
                                                <Avatar src={transform()} className={classes.large} />
                                            </span>
                                            <input accept="image/*" className={classes.input} id="icon-button-file" accept="image/*" type="file" name="file" onChange={imageChange} />
                                            <label htmlFor="icon-button-file">
                                                <IconButton color="secondary" aria-label="upload picture" component="span">
                                                    <PhotoCamera />
                                                </IconButton>
                                            </label>
                                        </div>
                                    </Grid>
                                    <Grid item xs={12} md={6} lg={6} id="editProfileGrid">
                                        <span></span>
                                    </Grid>
                                    <Grid item xs={12} md={6} lg={6}>
                                        <FormControl size="small" fullWidth variant="outlined" disabled={changePassword}>
                                            <InputLabel required color="secondary" htmlFor="clinicalName">Nome</InputLabel>
                                            <OutlinedInput
                                                required
                                                name='clinicalName'
                                                type='text'
                                                value={formValues.professionalValues.clinicalName}
                                                labelWidth={80}
                                                onChange={handleChange}
                                                style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                color="secondary"
                                            />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} md={6} lg={6}>
                                        <FormControl fullWidth variant="outlined">
                                            <div className="dateField">
                                                <Autocomplete
                                                    disabled
                                                    id="speciality"
                                                    size="small"
                                                    onChange={handleChange}
                                                    options={formValues.specialityOptions}
                                                    value={formValues.professionalValues.speciality}
                                                    getOptionLabel={(option) => option.name ? option.name : option}
                                                    color="secondary"
                                                    style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                    renderInput={(params) => <TextField required {...params} color="secondary" label="Especialidade" variant="outlined" />}
                                                />
                                            </div>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} md={6} lg={6}>
                                        <FormControl error={!formValues.professionalValues.emailIsValid} size="small" fullWidth variant="outlined" disabled={changePassword}>
                                            <InputLabel required color="secondary" htmlFor="email">E-mail</InputLabel>
                                            <OutlinedInput
                                                required
                                                name='email'
                                                type='email'
                                                value={formValues.professionalValues.email}
                                                labelWidth={85}
                                                onChange={handleChange}
                                                style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                color="secondary"
                                                aria-describedby="email-error"
                                            />
                                            <FormHelperText
                                                id="email-error"
                                                style={!formValues.professionalValues.emailIsValid ? { visibility: 'visible' } : { visibility: 'hidden' }}
                                            >{formValues.professionalValues.emailWarningText}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} md={6} lg={6}>
                                        <FormControl fullWidth variant="outlined">
                                            <div className="dateField">
                                                <Autocomplete
                                                    disabled={changePassword}
                                                    id="healthCenter"
                                                    size="small"
                                                    onChange={handleChange}
                                                    options={formValues.healthCenterOptions}
                                                    value={formValues.professionalValues.healthCenter}
                                                    getOptionLabel={(option) => option.name ? option.name : option}
                                                    color="secondary"
                                                    style={{ borderRadius: '20px', backgroundColor: '#fff' }}
                                                    renderInput={(params) => <TextField required {...params} color="secondary" label="Centro de Saúde" variant="outlined" />}
                                                />
                                            </div>
                                        </FormControl>
                                    </Grid>
                                    {
                                        changePassword === false ?
                                            <Grid item xs={12} md={6} lg={6}>
                                                <p>Palavra-passe: <span id="changePassSpan"><a onClick={onChangePassword}>Alterar</a></span></p>
                                            </Grid>
                                            :
                                            <Grid item xs={12} md={6} lg={6}>
                                                <ChangePasswordComponent onChangePassword={onChangePassword} changeUserData={changeUserData} userData={userData} onChangeSnackBar={onChangeSnackBar} />
                                            </Grid>
                                    }

                                </Grid>
                            </ThemeProvider>
                        </form>
                    </div>
                </Grid>
            </Grid>
            <div className="addButtonContainer" >
                <ThemeProvider theme={theme}>
                    <Button
                        disabled={changePassword}
                        type="submit"
                        form="edit-profile-form"
                        variant="contained"
                        size="medium"
                        color="secondary"
                        className="createPatientButton">
                        Guardar
                    </Button>
                    <Button
                        type="submit"
                        onClick={cancelButton}
                        id="cancelButton"
                        variant="contained"
                        size="medium"
                        color="primary"
                        style={{ color: '#fff' }}
                        className="createPatientButton">
                        Cancelar
                    </Button>
                </ThemeProvider>
            </div>
            <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    Password alterada com sucesso!
                </Alert>
            </Snackbar>
        </div>
    )
}

function ChangePasswordComponent({ onChangePassword, changeUserData, userData, onChangeSnackBar }) {
    const [changePasswordData, setChangePasswordData] = useState({
        oldPassword: '',
        newPassword: '',
        repeatedNewPassword: '',
        changeSuccess: true,
        changeWarning: ''
    })

    function handleChange(e) {
        setChangePasswordData({
            ...changePasswordData,
            [e.target.name]: e.target.value
        })
    }

    async function handleSubmit(e) {
        e.preventDefault();
        var newPassObj = Object.assign({}, changePasswordData);
        delete newPassObj.changeSuccess;
        delete newPassObj.changeWarning;

        changeUserData({
            ...userData,
            loadingOpen: true
        })

        const servicePath = `${API.apiPath}api/professionals/changepassword`;
        const data = await fetch(servicePath, {
            method: 'PUT',
            body: JSON.stringify(newPassObj),
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : `Bearer ${sessionStorage.getItem('sessionToken')}`
            }
        });

        if (data.status === 200) {
            onChangePassword();
            onChangeSnackBar();
            changeUserData({
                ...userData,
                loadingOpen: false
            })
        } else {
            let jsonObject = await data.json();
            setChangePasswordData({
                ...changePasswordData,
                changeSuccess: false,
                changeWarning: jsonObject.message
            })
            changeUserData({
                ...userData,
                loadingOpen: false
            })
        }
    }

    function cancelPassChange() {
        onChangePassword();
    }

    return (
        <div>
            <Paper elevation={10} className="changePassPaper">
                <h3>Mudança de palavra-passe</h3>
                <ThemeProvider theme={theme}>
                    <Grid container spacing={1}>
                        <Grid item xs={12}>
                            <FormControl error={!changePasswordData.changeSuccess} size="small" fullWidth variant="outlined">
                                <InputLabel color="secondary" htmlFor="oldPassword" required>Palavra-passe atual</InputLabel>
                                <OutlinedInput
                                    required
                                    value={changePasswordData.oldPassword}
                                    name='oldPassword'
                                    type='password'
                                    onChange={handleChange}
                                    labelWidth={155}
                                    style={{ borderRadius: '20px', backgroundColor: "#fff" }}
                                    color="secondary" />
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl error={!changePasswordData.changeSuccess} size="small" fullWidth variant="outlined">
                                <InputLabel color="secondary" htmlFor="newPassword" required>Nova palavra-passe</InputLabel>
                                <OutlinedInput
                                    required
                                    value={changePasswordData.newPassword}
                                    name='newPassword'
                                    type='password'
                                    onChange={handleChange}
                                    labelWidth={155}
                                    style={{ borderRadius: '20px', backgroundColor: "#fff" }}
                                    color="secondary" />
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl error={!changePasswordData.changeSuccess} size="small" fullWidth variant="outlined">
                                <InputLabel color="secondary" htmlFor="repeatedNewPassword" required>Repetir nova palavra-passe</InputLabel>
                                <OutlinedInput
                                    required
                                    value={changePasswordData.repeatedNewPassword}
                                    name='repeatedNewPassword'
                                    type='password'
                                    onChange={handleChange}
                                    labelWidth={208}
                                    style={{ borderRadius: '20px', backgroundColor: "#fff" }}
                                    color="secondary" />
                                <FormHelperText
                                    id="password-error"
                                    style={!changePasswordData.changeSuccess ? { visibility: 'visible' } : { visibility: 'hidden' }}
                                >{changePasswordData.changeWarning}</FormHelperText>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <div id="changePassButtonWrapper">
                        <IconButton onClick={cancelPassChange} size="medium">
                            <CancelIcon />
                        </IconButton>
                        <Button onClick={handleSubmit}
                            variant="contained"
                            size="small"
                            color="secondary"
                            className="changePass">Alterar palavra-passe</Button>
                    </div>
                </ThemeProvider>
            </Paper>
        </div>
    )
}

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}